<?php /* @var $this Controller */  

    $session = new CHttpSession;
    $session->open();

    if($session['uid'])
	 //User in already logged in
		$this->redirect(Yii::app()->request->baseUrl.'/index.php/site/index');

	else
	 //Redirect to login page
		$this->redirect(Yii::app()->request->baseUrl.'/index.php/');
?>
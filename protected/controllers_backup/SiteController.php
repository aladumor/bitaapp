<?php
class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
                $session=new CHttpSession;
                $session->open();
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index',array('username' => $session["fullname"]));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{   
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/index");
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
                $this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
	}

	/**
	 * Displays the login page
	 */
	public function actionForgotPassword()
	{
		$model=new LoginForm;
		// collect user input data
		if(isset($_POST) && $_POST != '' && count($_POST)>0)
		{
			/*Reset Password link to be sent to User*/
			$body1 = "Your Reset Password Link is <a href='<?php echo Yii::app()->request->baseUrl; ?>/index.php/site/ForgotPassword'>Reset Password</a>";

			$email=empty($_POST['email'])?0:$_POST['email'];
			$message = new YiiMailMessage;
			$message->setBody($body1, 'text/html');	
			$message->setsubject('Reset Password');
			$message->addTo($email);
			$message->from = Yii::app()->params['adminEmail'];
			Yii::app()->mail->send($message);

		 	$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
		}
		$this->render('ForgotPassword',array('model'=>$model));
	}

	/**
	 * Reset Password
	 */
	public function actionResetPassword()
	{
		$model=new LoginForm;
		$u_id = $_REQUEST['id'];
		// collect user input data
		if(isset($_POST) && $_POST != '' && count($_POST)>0)
		{
			$password=empty($_POST['password'])?0:$_POST['password'];
			$user_pass = base64_encode($password);
			//$confirm_password=empty($_POST['confirm_password'])?0:$_POST['confirm_password'];

			/* @desc :update password data in user master.*/
			$connection = Yii::app()->db;
			$sql = "UPDATE `user_master` SET `user_pass`='".$user_pass."' WHERE u_id = '".$u_id."'";
			$dataReader = $connection->createCommand($sql)->query();

		 	$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
		}
		$this->render('ResetPassword',array('model'=>$model));
	}

	/*
	* ComingSoonPage
	*/
	public function actionComingSoonPage()
	{
		$this->render('ComingSoonPage');
	}
}
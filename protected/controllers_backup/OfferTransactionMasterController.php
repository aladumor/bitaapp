<?php

class OfferTransactionMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
       

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
                $session=new CHttpSession;
                $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Payapi','checkPaymentMethod','androidPush','Pushios'),
				'users'=>array($session["uname"]),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Payapi'),
				'users'=>array($session["uname"]),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array($session["uname"]),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actioncheckPaymentMethod()
	{
		//print_r($_REQUEST);die();
		$o_id=$_REQUEST['o_id'];
		$error_flag=0;
		$error_msg='';
		if($_REQUEST['type']=='on_pre')
			$certificate='BevRageMerchant_Developer.pem';
		else
			$certificate='BevRageck.pem';
	   	if(!empty($o_id))
	   	{

	   		/*
	   		* fecth order details and user details
	   		*/
	   		$connection = Yii::app()->db;
	   		$payer_id=$_REQUEST['payer_id'];
	   		$cmd="SELECT `dt_id`,`token`,badge,user_master.* FROM `user_device_master`,user_master WHERE user_master.u_id=".$payer_id." 
					and user_master.`u_id`=user_device_master.`u_id`  and user_master.is_active=0";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			if(!empty($rows))
			{

				$uid=$rows[0]['u_id'];
				$type="redemeed";			
				$deviceToken=$rows[0]['token'];
				//chk is deposit amount is suffecient for payment
				
				$cmd2="SELECT * FROM user_master WHERE user_master.u_id IN (SELECT u_id from offer_master where o_id=".$o_id.")";
				$dataReader=$connection->createCommand($cmd2)->query();
				$rows2=$dataReader->readAll();
				
				if(!empty($rows2))
				{
					if($rows2[0]['deposit_amount']!='' && ($rows2[0]['deposit_amount']>=$_REQUEST['amount']))
					{
						if($rows[0]['active_payment_method_id']==1 )
						{

							//PAYPAL Transaction
							$receiver_email='pooja.shah@credencys.com';
							$amount=$_REQUEST['amount'];
							$response= $this->actionPayapi($receiver_email,$amount);	
							
							if(!empty($response))
							{
								if($response["responseEnvelope.ack"] == "Success")
								{
							   		$error_flag=0;
							   		$pay_key_id=$response["payKey"];
								}
								else
								{
									$error_flag=1;
									$error_msg=urldecode($kArray["error(0).message"]);
								}
							}
						}
						else
						{
							//VENMO TRANSACTION
							$error_flag=0;
						}
					}
					else
					{
						//amount not sufficient to pay customers
						$error_flag=1;
						$error_msg="Brand Partner Deposit Amount Limit Recahed! ";
					}

					//SEND PUSH NOTIFICATIONs
					if($error_flag==0 && $deviceToken!='')
					{
						$status=1;
						$msg="Your Reimbursement is Successfull,Cheers!";
						if($rows[0]['dt_id']==1)
						{	
							$badge=$rows[0]['badge'];
							$this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
							$cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
							$dataReader=$connection->createCommand($cmd);
							$rows=$dataReader->execute();
							
						}
						else
						{
							$this->androidPush($uid,$type,$msg,$deviceToken);
						}
					}
					else
					{	
						$status=3;		
						$msg="Sorry, Error in Reimbursement of offer !";
						if($rows[0]['dt_id']==1)
						{			
							$badge=$rows[0]['badge'];		
							$this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
							$cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
							$dataReader=$connection->createCommand($cmd);
							$rows=$dataReader->execute();
						}
						else
						{
							$this->androidPush($uid,$type,$msg,$deviceToken);
						}
					}
				}
				else
				{
					$this->actionAdmin();
					exit;
				}
				
					
			}
			else
			{
				$status=2;	
				//user id not found
				$error_flag=1;
				$error_msg="Merchant Not Active Any More! ";
				$this->actionAdmin();exit;
			}


			if($error_flag==1)
			{
				$status=2;
				//send to view page with error message.
				$this->actionAdmin();exit;

			}
			else
			{
				if(!empty($pay_key_id))
				{
					if($_REQUEST['type']=='off_pre')
					$cmd="UPDATE `offer_transaction_master` SET customer_payment_status=".$status." ,transaction_id='".$pay_key_id."' where ot_id=".$_REQUEST['ot'];
					else
						$cmd="UPDATE `offer_transaction_master` SET payment_status=".$status.",transaction_id='".$pay_key_id."' where ot_id=".$_REQUEST['ot'];
					
					$dataReader=$connection->createCommand($cmd);
					$rows=$dataReader->execute();
					$error_msg="Successfull Payment !";
					
				}				
			}

				/*$this->render('view',array(
				'model'=>$this->loadModel($_REQUEST['ot']),'error_msg'=>$error_msg
			));*/
				$this->actionAdmin();exit;
		}
		else
		{
			
			/*$this->render('view',array(
				'model'=>$this->loadModel($_REQUEST['ot']),
			));*/
			$this->actionAdmin();exit;
		}
	}

	public function actionPayapi($receiver_email,$amount)
	{
		   //turn php errors on
		   ini_set("track_errors", true);

		   //set PayPal Endpoint to sandbox
		   $url = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");

		   //set PayPal Endpoint to live
		   //$url = trim("https://svcs.paypal.com/AdaptivePayments/Pay");

		   
		   

		   //PayPal SANDBOX API Credentials
		   $app_id = 'APP-80W284485P519543T';//  sandbox
		   $api_username = "mitchb-facilitator_api1.bevrage.com";
		   $api_password = "AYKGUWNDNNHMFXVP";
		   $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AazG6HsiIpUp3NxmRNO-4NaxSnkT";
		   


		   //PayPal LIVE API Credentials
		  /* $app_id = 'APP-2RM4658855985480F';//live;
		   $api_username = "mitchb_api1.bevrage.com";
		   $api_password = "55PSXR9XQTZTY93E";
		   $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AbJpKwnQGTIHON6MQofMMvBIaggN";*/
		   

		   $amount =1 ;//$amount; //TODO
		   $API_RequestFormat = "NV";
		   $API_ResponseFormat = "NV";

		   $receiver_email = $receiver_email; //TODO
		   //Create request payload with minimum required parameters
		   $bodyparams = array ("requestEnvelope.errorLanguage" => "en_US",
		                     "actionType" => "PAY",
		                     "cancelUrl" => "http://localhost/beverage/index.php/site/index",
		                     "returnUrl" => "http://localhost/commonadmin/index.php/CategoryMaster/admin",
		                     "currencyCode" => "USD",
		                     "receiverList.receiver(0).email" => 'mitchb-buyer@bevrage.com',//$receiver_email,
		                     "receiverList.receiver(0).amount" => $amount,
		                     "senderEmail"=> "mitchb-facilitator@bevrage.com",
		       );

		   // convert payload array into url encoded query string
		   $body_data = http_build_query($bodyparams, "", chr(38));


		   	try
		   	{

			   	//create request and add headers
			   	$params = array("http" => array(
			       "method" => "POST",                                                 
			       "content" => $body_data,                                             
			       "header" =>  "X-PAYPAL-SECURITY-USERID: " . $api_username . "\r\n" .
			                    "X-PAYPAL-SECURITY-SIGNATURE: " . $api_signature . "\r\n" .
			                    "X-PAYPAL-SECURITY-PASSWORD: " . $api_password . "\r\n" .
			                    "X-PAYPAL-APPLICATION-ID: " . $app_id . "\r\n" .
			                    "X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
			                    "X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n"
			   ));


			   	//create stream context
			    $ctx = stream_context_create($params);


			   	//open the stream and send request
			    $fp = @fopen($url, "r", false, $ctx);

			   	//get response
			    $response = stream_get_contents($fp);

			   	//check to see if stream is open
			    if ($response === false) 
			    {
			       throw new Exception("php error message = " . "$php_errormsg");
			    }

			   	//close the stream
			   	fclose($fp);

			   	//parse the ap key from the response

			   	$keyArray = explode("&", $response);

			   	foreach ($keyArray as $rVal)
			   	{
			       	list($qKey, $qVal) = explode ("=", $rVal);
			       	$kArray[$qKey] = $qVal;
			   	}

			   	//print the response to screen for testing purposes
			   	If( $kArray["responseEnvelope.ack"] == "Success") 
			   	{

			       /* foreach ($kArray as $key =>$value)
			        {
			       		echo $key . ": " .$value . "<br/>";
			   		}*/
			   		
			   		return $kArray;
			    }
			   	else 
			   	{
			   		return $kArray;
			       	//echo 'ERROR Code: ' .  $kArray["error(0).errorId"] . " <br/>";
			     	//echo 'ERROR Message: ' .  urldecode($kArray["error(0).message"]) . " <br/>";
			     	//die();
			   	}

		   	}
		   	catch(Exception $e) 
		   	{
		   		echo "Message: ||" .$e->getMessage()."||";
		   	}

		
	}


	public function actionView($id)
	{

		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new OfferTransactionMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferTransactionMaster']))
		{
			$model->attributes=$_POST['OfferTransactionMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ot_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferTransactionMaster']))
		{
			$model->attributes=$_POST['OfferTransactionMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ot_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OfferTransactionMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new OfferTransactionMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OfferTransactionMaster']))
			$model->attributes=$_GET['OfferTransactionMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OfferTransactionMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OfferTransactionMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OfferTransactionMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offer-transaction-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	/*
	*send push notification of return cashback 
	*/
	public function Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate)
	{
				
		if ($deviceToken != '0') 
		{
            $ctx = stream_context_create();            
            $passphrase = '1234';
            $certificate= $certificate;//'BevRageMerchant_Developer.pem';//'BevRageck.pem';            
            stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            $body['aps'] = array(
                'badge' => $badge,
                'alert' => $msg,
                'sound' => 'default',
                'flag'=>1,
                
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            $flag = 0;
            if (!$result) {
                $flag = 1;
            }
            fclose($fp);
            if ($flag == 0) {

                return true;
            } else {
                return FALSE;
            }
        } 
        else 
        {
            return FALSE;
        }
	}

	//android push on successfull cashback  
    public function androidPush($uid,$type,$msg,$deviceToken)
    {
      
        // Replace with real BROWSER API key from Google APIs
        $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4";       
        // Replace with real client registration IDs 
        $registrationIDs =array($deviceToken);       
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
        'registration_ids'  => $registrationIDs,
        'data'              => array( "message" => $msg,'flag'=>1),
        );
        
        $headers = array( 
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json',
        );
      
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) 
        {
                die('Problem occurred: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo $result;die();
    }

}

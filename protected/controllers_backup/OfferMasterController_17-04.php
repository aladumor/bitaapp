<?php

class OfferMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $session=new CHttpSession;
        $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','ReportDashboard','GenerateLocationReports','generate','changeStatus','GenerateOfferReports','GenerateCampaignReports'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new OfferMaster;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferMaster']))
		{
			/*echo "<pre>";
			print_r($_POST['OfferMaster']);	
			die();*/
			
			$codefilename='qr'.date('YmdHis').'.png';						
			/* save offer image */
				$offerImage=CUploadedFile::getInstance($model,'offer_image');
		        if($offerImage)
		        {  		        	
		            $rnd = rand(0,9999);
		            $f1=str_replace(' ','_', $offerImage);
		            $fileName = "{$rnd}-".date('YmdHis').".png";  
		            $model->offer_image=$fileName;	           
	                $imagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$fileName;
	                $offerImage->saveAs($imagepath);                 
                        }else{
                            $fileName = "";
                        }		        
	        /* end of image save code */

			$model->attributes=$_POST['OfferMaster'];
			$model->code_redeem_file=$codefilename;
			$model->start_date=date('Y-m-d',strtotime($_POST['OfferMaster']['start_date']));
			$model->end_date=date('Y-m-d',strtotime($_POST['OfferMaster']['end_date']));
			$model->created_on=gmdate('Y-m-d H:i:s');
			$model->modified_on=gmdate('Y-m-d H:i:s');
			$model->product_price=empty($_POST['OfferMaster']['product_price'])? null :$_POST['OfferMaster']['product_price'];
			$model->discounted_price=empty($_POST['OfferMaster']['discounted_price'])? null :$_POST['OfferMaster']['discounted_price'];
			$session=new CHttpSession;
                         $session->open();
			$model->u_id=$session['uid'];
			$model->city_id="26318";
			$model->offer_image=$fileName;
			if($model->save())
			{
				
				/* GENERATE QR CODE */
					$urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice/offer_detail?o_id=".$model->o_id;
					$code=new QRCode($urlpath);		 
					
					// to flush the code directly
					//$code->create();

					// to save the code to file				
					$codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_codes" . DIRECTORY_SEPARATOR.$codefilename;
					$code->create($codepath);
				/* END OF QR CODE */

				/* save the offer cities */
                            
                                if($_POST['OfferMaster']['u_id'] != ""){
                                    foreach ($_POST['OfferMaster']['u_id'] as $value) 
					{
						$connection = Yii::app()->db;
						$cmd="INSERT INTO `offer_location_assoc`(`ol_id`, `o_id`, `city_id`) 
														VALUES (NUll,'".$model->o_id."','".$value."')";
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
					}
                                } 
					
				/* end of code of save city*/

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	*Generate QR code for Offers
	**/
  	 public function actiongenerate()//$id,$ctime
  	{
	   /*$this->widget('application.extensions.qrcode_google.QRCode', array(
	   'content' => 'http://localhost/bevrage/index.php/site/publicbinarytree?obj_id='.$id, // qrcode data content
	   'filePath' => YiiBase::getPathOfAlias('webroot.images/google'),//$path_to_img
	   'filename' => $ctime.'.png', // image name (make sure it should be .png)
	   'width' => '150', // qrcode image height
	   'height' => '150', // qrcode image width
	   'encoding' => 'UTF-8', // qrcode encoding method
	   'correction' => 'H', // error correction level (L,M,Q,H)
	   'watermark' => 'No' // set Yes if you want watermark image in Qrcode else 'No'. for 'Yes', you need to set watermark image $stamp in QRCode.php
	   ));*/
	   //$this->redirect(array('index','id'=>$id));
		//Yii::import('ext.qrcode.QRCode');
		$code=new QRCode("http://dev.credencys.com/bevrage/index.php/webservice/offer_detail?o_id=3");
		 
		// to flush the code directly
		//$code->create();
		 
		// to save the code to file
		$code->create('D:/xampp/htdocs/bevrage/qr20150209234117.png');
  	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferMaster']))
		{
			
			$session=new CHttpSession;
            $session->open();
				
			/*echo "<pre>";
			print_r($_POST);	
			die();*/
			$model->attributes=$_POST['OfferMaster'];
			$model->u_id=$session['uid'];
			$model->start_date=date('Y-m-d',strtotime($_POST['OfferMaster']['start_date']));
			$model->end_date=date('Y-m-d',strtotime($_POST['OfferMaster']['end_date']));
			$model->modified_on=gmdate('Y-m-d H:i:s');
			$model->product_price=empty($_POST['OfferMaster']['product_price'])? null :$_POST['OfferMaster']['product_price'];
			$model->discounted_price=empty($_POST['OfferMaster']['discounted_price'])? null :$_POST['OfferMaster']['discounted_price'];
			
			/*save ne Image */						
				$offerImage=CUploadedFile::getInstance($model,'offer_image');
		        if($offerImage)
		        {  	
		        	/* delete old image */
		        	$oldimagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$model->offer_image;
		        	@unlink($oldimagepath);

		        	/*Save new image*/        	
		            $rnd = rand(0,9999);
		            $f1=str_replace(' ','_', $offerImage);
		            $fileName = "{$rnd}-".date('YmdHis').".png"; 
		            $model->offer_image=$fileName;	           
	                $imagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$fileName;
	                $offerImage->saveAs($imagepath);                 
		        }					    	
			/* end of save image*/			
			if($model->save())
			{
				/* update the offer cities */
					foreach ($_POST['OfferMaster']['u_id'] as $value) 
					{
						$connection = Yii::app()->db;
						
						/* DELETE OLD ENTRIES*/
						$cmd="DELETE FROM `offer_location_assoc` where `o_id`= ".$id;
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
						
						/*INSERT NEW ENTRIES*/
						$cmd="INSERT INTO `offer_location_assoc`(`ol_id`, `o_id`, `city_id`) 
														VALUES (NUll,'".$model->o_id."','".$value."')";
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
					}
				/* end of code of update city*/

				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/*
	* change the status tof offer
	*/
	public function actionchangeStatus()
	{
		$status=$_GET['status'];
		if($status==0)
			$status=1;
		else
			$status=0;
		$o_id=$_GET['id'];
		$connection = Yii::app()->db;
		$cmd="UPDATE  `offer_master` SET is_active=".$status." WHERE o_id=".$o_id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();
		$this->redirect(array('admin'));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE  `offer_master` SET is_active='1',is_delete='1' WHERE o_id=".$id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OfferMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new OfferMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OfferMaster']))
			$model->attributes=$_GET['OfferMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OfferMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OfferMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OfferMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offer-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/* 
	* -------------------REPORTS ---------------------------------------
	*/
	


	/*
	*Individual Offer Reports
	*/
	public function actionGenerateOfferReports()
	{
		$this->render('GenerateOfferReports');
		/* REPORT 1: 
		* Individual Offer Report 
		*/
		if(isset($_REQUEST['o_id']) && isset($_REQUEST['uid']))
		{
			/* GET TOTAL FACEBOOK SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'fb_count' FROM `share_record` WHERE `media_type`='fb' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query;
			$fbrows=$dataReader->readAll();
			$fbshare=empty($fbrows[0]['fb_count'])?0:$fbrows[0]['fb_count'];



			/* GET TOTAL TWITTER SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'tw_count' FROM `share_record` WHERE `media_type`='twitter' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query;
			$twrows=$dataReader->readAll();
			$twshare=empty($twrows[0]['tw_count'])?0:$twrows[0]['tw_count'];

			/* GET TOTAL gplus SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'gp_count' FROM `share_record` WHERE `media_type`='gplus' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query;
			$gprows=$dataReader->readAll();
			$gpshare=empty($gprows[0]['gp_count'])?0:$gprows[0]['gp_count'];

			/* GET TOTAL sms SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'sms_count' FROM `share_record` WHERE `media_type`='sms' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query;
			$textrows=$dataReader->readAll();
			$textshare=empty($textrows[0]['sms_count'])?0:$textrows[0]['sms_count'];


			
		}
	}


	/*
	*Campaign Reports
	*/
	public function actionGenerateCampaignReports()
	{
		
		/* REPORT 1: 
		* Campaign Report 
		*/
		if(isset($_REQUEST['uid']))
		{
			$resultArray='';
			if($_REQUEST['rid']==1) //Super Admin Report
			{
				$connection = Yii::app()->db;
				/* Fetch all campaigns of Brand Partner */
				$cmd="SELECT * FROM `campaign_master` WHERE `u_id`=".$_REQUEST['uid'];
				$dataReader=$connection->createCommand($cmd)->query();
				$Camprows=$dataReader->readAll();

				/*Fetch the no of offers of each campaigns */
				if(!empty($Camprows))
				{
					foreach ($Camprows as $key => $value) 
					{
						$cmd="SELECT count(*) as offer_count FROM `offer_master` WHERE `cm_id`=".$value['cm_id'];
						$dataReader=$connection->createCommand($cmd)->query();
						$offerrows=$dataReader->readAll();
						if(!empty($offerrows))
						{
							$resultArray[$value['campaign_name']]=$offerrows[0]['offer_count'];
							$campaignsArray[$value['cm_id']]=$value['campaign_name'];
						}
						else
						{
							$resultArray[$value['campaign_name']]=0;
							$campaignsArray[$value['cm_id']]=$value['campaign_name'];
						}
					}
				}
				else
				{
					//No campaigns 
				}
				
				$cmd="SELECT count(*) FROM `campaign_master` WHERE `u_id`=".$_REQUEST['uid'];
				$dataReader=$connection->createCommand($cmd)->query();
				$Camprows=$dataReader->readAll();
				
				//$this->render('GenerateCampaignReports',array('rows'));
				$this->render('GenerateCampaignReports');
			}
			else //Brand Partner Report
			{
				$this->render('GenerateCampaignReports');
			}
			die();
		}
		$this->render('GenerateCampaignReports');

	}

	/* 
	* Report Dashboard
	*/
	public function actionReportDashboard()
	{
		$this->render('ReportDashboard');
	}	

	/*
	* LOCATION BASED REPORTS
	*/
	public function actionGenerateLocationReports()
	{
		$this->render('GenerateLocationReports');
	}
}


<?php
/* @var $this FeedbackMasterController */
/* @var $model FeedbackMaster */

$this->breadcrumbs=array(
	'Feedback Masters'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List FeedbackMaster', 'url'=>array('index')),
	//array('label'=>'Create FeedbackMaster', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#feedback-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>


<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'feedback-master-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive footable',
	'filter'=>$model,
	'columns'=>array(
		//'f_id',
		//'u_id',
                array(
                    'name'=>'UserName',
                    'value'=>'$data->UserMaster["user_name"]'
                ),
		'message',
		'date',
//		array(
//			'class'=>'CButtonColumn',
//		),
           /* array(
			class'=>'CButtonColumn',
                        template'=>'{view}'
		),*/
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-search"></i>',
                            'imageUrl'=>false,
                    ),
                    
            )
        )
		
	),
)); ?>


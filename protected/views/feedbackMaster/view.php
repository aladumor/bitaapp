<?php
/* @var $this FeedbackMasterController */
/* @var $model FeedbackMaster */

$this->breadcrumbs=array(
	'Feedback Masters'=>array('index'),
	$model->f_id,
);

$this->menu=array(
//	array('label'=>'List FeedbackMaster', 'url'=>array('index')),
//	array('label'=>'Create FeedbackMaster', 'url'=>array('create')),
//	array('label'=>'Update FeedbackMaster', 'url'=>array('update', 'id'=>$model->f_id)),
//	array('label'=>'Delete FeedbackMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->f_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage FeedbackMaster', 'url'=>array('admin')),
);
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
<div class="summary panel-heading">&nbsp;</div>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
	'attributes'=>array(
		//'f_id',
		//'u_id',
                'UserMaster.user_name',
		'message',
		'date',
	),
)); ?>

</div>

<?php
/* @var $this FeedbackMasterController */
/* @var $data FeedbackMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('f_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->f_id), array('view', 'id'=>$data->f_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::encode($data->u_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('message')); ?>:</b>
	<?php echo CHtml::encode($data->message); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>
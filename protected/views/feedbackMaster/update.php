<?php
/* @var $this FeedbackMasterController */
/* @var $model FeedbackMaster */

$this->breadcrumbs=array(
	'Feedback Masters'=>array('index'),
	$model->f_id=>array('view','id'=>$model->f_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List FeedbackMaster', 'url'=>array('index')),
	array('label'=>'Create FeedbackMaster', 'url'=>array('create')),
	array('label'=>'View FeedbackMaster', 'url'=>array('view', 'id'=>$model->f_id)),
	array('label'=>'Manage FeedbackMaster', 'url'=>array('admin')),
);
?>

<h1>Update FeedbackMaster <?php echo $model->f_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
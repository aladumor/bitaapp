<?php
/* @var $this FeedbackMasterController */
/* @var $model FeedbackMaster */

$this->breadcrumbs=array(
	'Feedback Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FeedbackMaster', 'url'=>array('index')),
	array('label'=>'Manage FeedbackMaster', 'url'=>array('admin')),
);
?>

<h1>Create FeedbackMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
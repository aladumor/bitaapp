<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'scan-receipts-master-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'o_id'); ?>
		<?php echo $form->textField($model,'o_id'); ?>
		<?php echo $form->error($model,'o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
		<?php echo $form->error($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receipt_image'); ?>
		<?php echo $form->textField($model,'receipt_image'); ?>
		<?php echo $form->error($model,'receipt_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'product_image'); ?>
		<?php echo $form->textField($model,'product_image'); ?>
		<?php echo $form->error($model,'product_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'ot_id'); ?>
		<?php echo $form->textField($model,'ot_id'); ?>
		<?php echo $form->error($model,'ot_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
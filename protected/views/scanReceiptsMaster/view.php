<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */

$this->breadcrumbs = array(
    'Offer Transactions' => array('../offertransactionMaster/admin'),
    'Transaction Details'
    //$model->srm_id,
);

$this->menu = array(
//    array('label' => 'List ScanReceiptsMaster', 'url' => array('index')),
//    array('label' => 'Create ScanReceiptsMaster', 'url' => array('create')),
//    array('label' => 'Update ScanReceiptsMaster', 'url' => array('update', 'id' => $model->srm_id)),
//    array('label' => 'Delete ScanReceiptsMaster', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->srm_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Transactions', 'url' => array('offertransactionMaster/admin')),
);


?>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>

<script type="text/javascript">
    function confirmbox () 
    {
        <?php if($model->OfferTransactionMaster->redeem_by=='Merchant')
            $amount=$model->OfferTransactionMaster->merchant_amount;
        else
            $amount=$model->OfferTransactionMaster->customer_amount
        ?>
        var x=confirm("Are you sure you are ready to make the payment? ");
        if ( x== true) 
        {
            x="1"; 
        } else {
            x = "You pressed Cancel!";
        }  
        if(x=="1")  
        {
           location.href = "<?php echo Yii::app()->request->baseUrl?>/index.php/OfferTransactionMaster/checkPaymentMethod?o_id=<?php echo $model->o_id?>&type=<?php echo $_REQUEST['type']?>&ot=<?php echo $_REQUEST['ot']?>&redeem_by=Customer&payer_id=<?php echo $model->OfferTransactionMaster->payer_id ?>&amount=<?php echo $amount ?>";         
        }
    }
</script>
<?php if($model->OfferTransactionMaster->customer_payment_status==4){?>
<div class="ml20" style="color: red;"><h4><i class="fa fa-exclamation-triangle"></i>
<span >Bad Paypal Account Request.</h4></span></div>
<?php }?>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl manage_trans_div">
<a href="../offertransactionMaster/admin" class="manage_trans">Manage Transaction</a>

<div class="summary panel-heading"></div>

<?php
    $this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
    'attributes' => array(
      
         array(
                    'name'=>'Merchant Name',
                    'header'=>'Merchant Name',
                    'value' =>OfferMerchantAssoc::model()->getDetails($model->o_id,"merchant"),
             ), 
          array(
                    'name'=>'Establishment',
                    'header'=>'Establishment',
                    'value' =>OfferMerchantAssoc::model()->getDetails($model->o_id,"establishment"),
             ), 
       'OfferMaster.offer_name',
        
        'OfferTransactionMaster.redeem_by',
        array(
            'name'=>'Redeem Date',
            'header'=>'Redeem Date',
            //'value'=>'date("d M Y",strtotime($data["work_date"]))'
            'value'=>Yii::app()->dateFormatter->format("d MMM y",strtotime($model->OfferTransactionMaster->redeem_date))
        ),

        'OfferTransactionMaster.customer_amount',        
        'OfferTransactionMaster.transaction_id',
        'OfferTransactionMaster.transaction_time',
        array(
            'name'=>'receipt_image',
            'type'=>'raw',
            'value'=>$model->showphoto_from_database('rec'),            
        ),
        
        array(
            'name'=>'product_image',
            'type'=>'raw',
            'value'=>$model->showphoto_from_database('pro'),            
        ),
        array(
            'header'=>'Type',
            'name'=>'Offer Type',
            'type'=>'raw',
            'value' => "Liquor store",
        ),
        array(
                    'name'=>'User Email',
                    'header'=>'User Email',
                    'value' =>offerTransactionMaster::model()->getEmail($model->OfferTransactionMaster->unique_code)
             ), 
        array(
            'name'=>'Payment Status',
            'type'=>'raw',
            'value'=>$model->getPaymentStatus($model->OfferTransactionMaster->customer_payment_status),            
        ),
        array(            
            'header'=>'Customer Address',
            'name'=>'Customer Address',
            'value' =>UserDetailMaster::model()->getaddress($model->OfferTransactionMaster->unique_code),
        ),            
        
    ),
));

 if($model->OfferTransactionMaster->redeem_by=='Merchant')
    $amount=$model->OfferTransactionMaster->merchant_amount;
else
    $amount=$model->OfferTransactionMaster->customer_amount
?>

<?php if($model->OfferTransactionMaster->customer_payment_status==2 || $model->OfferTransactionMaster->customer_payment_status==3){?>
<a class="btn btn-primary" onclick= "confirmbox()" href="javascript:void(0);">Pay Now </a>
<a class="btn btn-danger" href="<?php echo Yii::app()->request->baseUrl.'/index.php/OfferTransactionMaster/CustomDelete?id='.$model->ot_id?>">Invalid Transaction</a>
<?php }?>


<a class="btn btn-primary" href="<?php echo Yii::app()->request->baseUrl.'/index.php/OfferTransactionMaster/admin'?>">Back</a>
<?php  if(isset($_REQUEST['error'])){?>
<label><?php echo $_REQUEST['error'] ; ?></label>
<?php }?></div>

<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */

$this->breadcrumbs=array(
	'Scan Receipts Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ScanReceiptsMaster', 'url'=>array('index')),
	array('label'=>'Manage ScanReceiptsMaster', 'url'=>array('admin')),
);
?>

<h1>Create ScanReceiptsMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
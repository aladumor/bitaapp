<?php
/* @var $this ScanReceiptsMasterController */
/* @var $data ScanReceiptsMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('srm_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->srm_id), array('view', 'id'=>$data->srm_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_id')); ?>:</b>
	<?php echo CHtml::encode($data->o_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::encode($data->u_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt_image')); ?>:</b>
	<?php echo CHtml::encode($data->receipt_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_image')); ?>:</b>
	<?php echo CHtml::encode($data->product_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_id')); ?>:</b>
	<?php echo CHtml::encode($data->ot_id); ?>
	<br />


</div>
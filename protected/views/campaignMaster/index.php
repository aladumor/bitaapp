<?php
/* @var $this CampaignMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Campaign Masters',
);

$this->menu=array(
	array('label'=>'Create CampaignMaster', 'url'=>array('create')),
	array('label'=>'Manage CampaignMaster', 'url'=>array('admin')),
);
?>

<h1>Campaign Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

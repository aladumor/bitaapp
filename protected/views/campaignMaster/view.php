
<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */

$this->breadcrumbs=array(
	'Campaigns'=>array('admin'),
	//$model->cm_id,
	"View",
);

$this->menu=array(
	//array('label'=>'List CampaignMaster', 'url'=>array('index')),
	//array('label'=>'Create CampaignMaster', 'url'=>array('create')),
	//array('label'=>'Update CampaignMaster', 'url'=>array('update', 'id'=>$model->cm_id)),
	//array('label'=>'Delete CampaignMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cm_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Campaigns', 'url'=>array('admin')),
);
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
<div class="summary panel-heading">&nbsp;</div>
<?php $this->widget('zii.widgets.CDetailView', array(
 'data'=>$model,
 'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
 
 'attributes'=>array(
  //'cm_id',
  'campaign_name',
  //'u_id',
  'user.user_name',
  
  //'b_id',
  //'is_active',
  //'is_delete',
 ),
)); ?>
</div>


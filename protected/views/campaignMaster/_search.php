<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'cm_id'); ?>
		<?php echo $form->textField($model,'cm_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'campaign_name'); ?>
		<?php echo $form->textField($model,'campaign_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'b_id'); ?>
		<?php echo $form->textField($model,'b_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_active'); ?>
		<?php echo $form->textField($model,'is_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_delete'); ?>
		<?php echo $form->textField($model,'is_delete'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
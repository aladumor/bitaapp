<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */

$this->breadcrumbs=array(
	'Campaigns'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List CampaignMaster', 'url'=>array('index')),
	array('label'=>'Manage Campaigns', 'url'=>array('admin')),
);
?>

<!--<h1>Create Campaign</h1> -->

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
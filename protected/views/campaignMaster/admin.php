
<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */

$this->breadcrumbs=array(
	'Campaigns'=>array('admin'),
	'Manage Campaigns',
);

$this->menu=array(
	//array('label'=>'List CampaignMaster', 'url'=>array('index')),
	array('label'=>'Create Campaigns', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#campaign-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!--<h1>Manage Campaigns </h1> -->

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> -->

<?php ///echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'campaign-master-grid',
	
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive footable',
	//'filter'=>$model,
	'columns'=>array(
		//'cm_id',
		'campaign_name',
		'user.user_name',
		//'b_id',
		//'is_active',
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
		//'is_delete',
		/*array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
		),*/
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-search"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
	),
)); ?>
<div class="button-bottom">
	<a class="btn btn-primary" href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/offerMaster/GenerateCampaignReports">View Analytics</a>
</div>
<div>
	
</div>


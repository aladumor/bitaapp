<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */
/* @var $form CActiveForm */
?>

<div class="form dataTables_wrapper">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'campaign-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col-md-6">
		<?php echo $form->labelEx($model,'campaign_name'); ?>
		<?php echo $form->textField($model,'campaign_name',array('size'=>60,'maxlength'=>100, 'class'=>'form-control mb15')); ?>
        <div class="clearfix buttons">
			<?php echo CHtml::submitButton( $model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-info')); ?>
		</div><br />

	</div>


	
		<?php echo $form->hiddenField($model,'b_id',array('value'=>'1')); ?>


	

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this CampaignMasterController */
/* @var $model CampaignMaster */

$this->breadcrumbs=array(
	'Campaigns '=>array('admin'),
	//$model->cm_id=>array('view','id'=>$model->cm_id),
	'Update Campaign',
);

$this->menu=array(
	//array('label'=>'List CampaignMaster', 'url'=>array('index')),
	//array('label'=>'Create CampaignMaster', 'url'=>array('create')),
	//array('label'=>'View CampaignMaster', 'url'=>array('view', 'id'=>$model->cm_id)),
	array('label'=>'Manage Campaigns', 'url'=>array('admin')),
	
);
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
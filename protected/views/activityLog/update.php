<?php
/* @var $this ActivityLogController */
/* @var $model ActivityLog */

$this->breadcrumbs=array(
	'Activity Logs'=>array('index'),
	$model->logid=>array('view','id'=>$model->logid),
	'Update',
);

$this->menu=array(
	array('label'=>'List ActivityLog', 'url'=>array('index')),
	array('label'=>'Create ActivityLog', 'url'=>array('create')),
	array('label'=>'View ActivityLog', 'url'=>array('view', 'id'=>$model->logid)),
	array('label'=>'Manage ActivityLog', 'url'=>array('admin')),
);
?>

<h1>Update ActivityLog <?php echo $model->logid; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this ActivityLogController */
/* @var $model ActivityLog */

$this->breadcrumbs=array(
	'Activity Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ActivityLog', 'url'=>array('index')),
	array('label'=>'Manage ActivityLog', 'url'=>array('admin')),
);
?>

<h1>Create ActivityLog</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
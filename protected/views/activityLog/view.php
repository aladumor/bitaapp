<?php
/* @var $this ActivityLogController */
/* @var $model ActivityLog */

$this->breadcrumbs=array(
	'Activity Logs'=>array('index'),
	$model->logid,
);

$this->menu=array(
	array('label'=>'List ActivityLog', 'url'=>array('index')),
	array('label'=>'Create ActivityLog', 'url'=>array('create')),
	array('label'=>'Update ActivityLog', 'url'=>array('update', 'id'=>$model->logid)),
	array('label'=>'Delete ActivityLog', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->logid),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ActivityLog', 'url'=>array('admin')),
);
?>

<h1>View ActivityLog #<?php echo $model->logid; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'logid',
		'ur_id',
		'ut_id',
		'u_id',
		'action',
		'description',
		'log_time',
	),
)); ?>

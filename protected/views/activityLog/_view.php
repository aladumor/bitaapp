<?php
/* @var $this ActivityLogController */
/* @var $data ActivityLog */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('logid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->logid), array('view', 'id'=>$data->logid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ur_id')); ?>:</b>
	<?php echo CHtml::encode($data->ur_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ut_id')); ?>:</b>
	<?php echo CHtml::encode($data->ut_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::encode($data->u_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('action')); ?>:</b>
	<?php echo CHtml::encode($data->action); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('log_time')); ?>:</b>
	<?php echo CHtml::encode($data->log_time); ?>
	<br />


</div>
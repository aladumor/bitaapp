<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'lt_id'); ?>
		<?php echo $form->textField($model,'lt_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'liquor_type'); ?>
		<?php echo $form->textField($model,'liquor_type',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_active'); ?>
		<?php echo $form->textField($model,'is_active'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */
/* @var $form CActiveForm */
?>

<div class="form dataTables_wrapper">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'liquor-type-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row col-md-6">
		<?php echo $form->labelEx($model,'liquor_type'); ?>
		<?php echo $form->textField($model,'liquor_type',array('size'=>10,'maxlength'=>100, 'class'=>'form-control mb15')); ?>
		<?php echo $form->error($model,'liquor_type'); ?>
		<div class="clearfix buttons">
			<?php echo CHtml::submitButton( $model->isNewRecord ? 'Create' : 'Save',array('class' => 'btn btn-info')); ?>
		</div>
	</div>

	
	<!-- <div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div> -->

<?php $this->endWidget(); ?>

</div><!-- form -->
<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
$this->breadcrumbs=array(
	'Liquor Filters'=>array('admin'),
	'Manage Liquor Filters',
);

$this->menu=array(
	//array('label'=>'List LiquorType', 'url'=>array('index')),
	array('label'=>'Create New Filter', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#liquor-type-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
</style>
<?php

	$pageSizeDropDown = CHtml::dropDownList(
		'pageSize',
		$pageSize,
		array( 10 => 10, 25 => 25, 50 => 50, 100 => 100 ),
		array(
			'class'    => 'change-pagesize',
			'onchange'=>"$.fn.yiiGridView.update('user-detail-master-grid',{ data:{pageSize: $(this).val() }})",
		)
	);
?>
<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> -->
<div class="dataTables_wrapper">	
	<div class="col-md-12">
		<a class="btn btn-green pull-right offer-btn" style="margin-bottom:10px; !important" href="create"><i class="fa fa-plus"></i>&nbsp;Create New Filter</a>
	</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'liquor-type-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'template'=>'{pager}{items}{pager}' ,
	'filter'=>$model,
	'columns'=>array(
		//'lt_id',
		'liquor_type',
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
	),
)); ?>
</div>
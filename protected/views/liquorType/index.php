<?php
/* @var $this LiquorTypeController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Liquor Types',
);

$this->menu=array(
	array('label'=>'Create LiquorType', 'url'=>array('create')),
	array('label'=>'Manage LiquorType', 'url'=>array('admin')),
);
?>

<h1>Liquor Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

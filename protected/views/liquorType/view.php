<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */

$this->breadcrumbs=array(
	'Liquor Filters'=>array('admin'),
	//$model->cm_id,
	"View",
);

$this->menu=array(	
	array('label'=>'Create New Filter', 'url'=>array('create')),
	//array('label'=>'Update LiquorType', 'url'=>array('update', 'id'=>$model->lt_id)),
	//array('label'=>'Delete LiquorType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->lt_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Liquor Filters', 'url'=>array('admin')),
);
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
<div class="summary panel-heading">&nbsp;</div>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
	'attributes'=>array(
		//'lt_id',
		'liquor_type',
		'is_active',
	),
)); ?>
</div>
</div>

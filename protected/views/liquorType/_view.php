<?php
/* @var $this LiquorTypeController */
/* @var $data LiquorType */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('lt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->lt_id), array('view', 'id'=>$data->lt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('liquor_type')); ?>:</b>
	<?php echo CHtml::encode($data->liquor_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />


</div>
<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */

$this->breadcrumbs=array(
	'Liquor Filters'=>array('admin'),
	'Update',
);

$this->menu=array(
	//array('label'=>'List LiquorType', 'url'=>array('index')),
	array('label'=>'Create New Filter', 'url'=>array('create')),
	//array('label'=>'View LiquorType', 'url'=>array('view', 'id'=>$model->lt_id)),
	array('label'=>'Manage Liquor Filters', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
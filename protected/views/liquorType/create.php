<?php
/* @var $this LiquorTypeController */
/* @var $model LiquorType */

$this->breadcrumbs=array(
	'Liquor Filters'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List LiquorType', 'url'=>array('index')),
	array('label'=>'Manage Liquor Filters', 'url'=>array('admin')),
);
?>



<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
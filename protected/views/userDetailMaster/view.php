<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */

$this->breadcrumbs=array(
	'User'=>array('admin'),
	//$model->name,
	'User Details',
);

$this->menu=array(
	//array('label'=>'List UserDetailMaster', 'url'=>array('index')),
	//array('label'=>'Create UserDetailMaster', 'url'=>array('create')),
	 array('label'=>'Update User Detail', 'url'=>array('update', 'id'=>$model->u_id)),
	//array('label'=>'Delete UserDetailMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->u_id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Details', 'url'=>array('admin')),
);
?>
<style>
.ac_heading{
	display:none !important;
	
}

.content{
display:none !important;
}
</style>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl manage_trans_div">

	<a class="manage_trans" href="update/<?php echo $model->u_id?>"><i class="fa fa-plus"></i>&nbsp;Edit Profile</a>

<!-- <div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl manage_trans_div">
 -->
<div class="summary panel-heading"></div>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	 'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
	'attributes'=>array(
		'user_name',
		'name',
		'email',
		'phone_no',
		
	),
)); ?>
</div>
<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */

$this->breadcrumbs=array(
	'Brand Partners'=>array('admin'),
	'Manage Brand Partner',
);

$agent_role_id = '2';
$superuser_role_id = '1';
$session = new CHttpSession;
$session->open();
if($session['rid'] == $superuser_role_id)
{
	$this->menu=array(
		//array('label'=>'List UserDetailMaster', 'url'=>array('index')),
		array('label'=>'Create Brand Partner', 'url'=>array('create')),
	);
}
else
{
?>
<html>
<head>
	<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
	</style>
	
	</head>
</html>
<?php
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-detail-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!-- <h1>Manage Brand Partners</h1> -->


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-detail-master-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'filter'=>$model,
	'columns'=>array(
		//'u_id',
		//'unique_code',
		//'login_type',
		//'ur_id',
		//'ut_id',
		//'email',
		
		'user_name',
		'name',
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
		//'user_pass',
		//'addressline_1',
		//'addressline_2',
		//'city_id',
		//'state_id',
		//'country_id',
		//'zipcode',
		// 'phone_no',
		// 'name',
		// 'profile_picture',
		// 'dob',
		// 'gps_flag',
		// 'email_flag',
		// 'notification_flag',
		// 'reward_points',
		// 'active_payment_method_id',
		// 'venmo_email',
		// 'paypal_email',
		// 'is_active',
		// 'is_delete',
		// 'created_on',
		// 'created_by',
		// 'modified_on',
		// 'modified_by',
		/*array(
			'class'=>'CButtonColumn',
		),*/
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
	),
)); ?>

<div class="manage_icons"><?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */

$this->breadcrumbs=array(
	'Customers'=>array('userDashboard'),
	'Manage Customers',
);
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
$agent_role_id = '2';
$superuser_role_id = '1';
$session = new CHttpSession;
$session->open();
if($session['rid'] == $superuser_role_id)
{
	$this->menu=array(
		//array('label'=>'List UserDetailMaster', 'url'=>array('index')),
		array('label'=>'Create Brand Partner', 'url'=>array('create')),
	);
}
else
{
?>
<html>
<head>
	<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
	</style>
	
	</head>
</html>
<?php
}


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#user-detail-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>
<!-- <h1>Manage Brand Partners</h1> -->

<?php

	$pageSizeDropDown = CHtml::dropDownList(
		'pageSize',
		$pageSize,
		array( 10 => 10, 25 => 25, 50 => 50, 100 => 100 ),
		array(
			'class'    => 'change-pagesize',
			'onchange'=>"$.fn.yiiGridView.update('user-detail-master-grid',{ data:{pageSize: $(this).val() }})",
		)
	);
	?>
 <div class="dataTables_wrapper">	
<div class="col-md-12">
	<a class="btn btn-green pull-right offer-btn" href="exporttoexcel"><i class="fa fa-download"></i>&nbsp;Download Email List</a>
</div>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-detail-master-grid',
	'dataProvider'=>$model->customersearch(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'filter'=>$model,
	'template'=>'{pager}{items}{pager}' ,
	'columns'=>array(
		//'u_id',
		//'unique_code',
		//'login_type',
		//'ur_id',
		//'ut_id',
		//'email',
		
		'user_name',
		'email',
		'name',		
		/*array(
            'header' => 'Role',
            'name' => 'ur_id',
            'value' => '($data->ur_id == "4") ? "Customer" : "Merchant"'
        ),  */      
		 array(
            'type' => 'raw',
            'header'=>'Age',
            'value' => function($data){
		            	if( !empty($data->dob)  && $data->dob!= '0000-00-00'){
		            		 $from = new DateTime($data->dob);
							 $to   = new DateTime(date('Y-m-d'));
							return $from->diff($to)->y;
		            	}
		            	else
		            	{
		            		return "Not available";
		            	}
            	},
        ),
		array(
            'header' => 'Current Payment Method',
          	 'type' => 'raw',
            'value' => function($data){
		            	if($data->active_payment_method_id== '1')
		            	{
		            		return "Paypal";
		            	}
		            	else if($data->active_payment_method_id== '2')
		            	{
		            		return "Bank";
		            	}
		            	else if($data->active_payment_method_id== '3')
		            	{
		            		return "Check";
		            	}
		            	else
		            	{
		            		return "Not Set";
		            	}
            	},
        ),
		'addressline_1',
		'zipcode',
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
		array(
        'class'=>'CLinkColumn',
        'label'=> '<i class="fa fa-history"></i>',
        'urlExpression' => 'Yii::app()->createUrl("/UserDetailMaster/accountHistory?id=" . $data->u_id."&code=".$data->unique_code."&points=".$data->reward_points)',
        'header'=>'Account History'
      	),
      	
		/*array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )*/
	),
)); ?>
</div></div>
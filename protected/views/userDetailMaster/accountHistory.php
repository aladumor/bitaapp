<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */

$this->breadcrumbs=array(
	'Manage Customers'=>array('customer'),
	'Account History',
);

/*$this->menu=array(
	//array('label'=>'List UserDetailMaster', 'url'=>array('index')),
	//array('label'=>'Create UserDetailMaster', 'url'=>array('create')),
	// array('label'=>'Update User Detail', 'url'=>array('update', 'id'=>$model->u_id)),
	//array('label'=>'Delete UserDetailMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->u_id),'confirm'=>'Are you sure you want to delete this item?')),
	//array('label'=>'Manage Details', 'url'=>array('admin')),
);*/
?>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>
<script type="text/javascript">
	$(document).ready(function(){
		getshareHistory();
	});
	function getRedeemHistory() 
	{
		var urlstr="<?php echo Yii::app()->createUrl('UserDetailMaster/ajaxgetRedeemHistory?')?>";
		var code="<?php echo $_REQUEST['code']?>";
		$.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {code:code },
          success: function(msg) { 
                if(msg!='')  
                {
                	$('#redeemDiv').show();
                	$('#redeemDiv').html(msg);
               		
                } 
               

          },
          error: function(msg) {
              alert('Something went wrong.Try Again!')
          }
        });
	}
	function getshareHistory() 
	{
		var urlstr="<?php echo Yii::app()->createUrl('UserDetailMaster/ajaxgetShareHistory?')?>";
		var code="<?php echo $_REQUEST['code']?>";
		var uid="<?php echo $_REQUEST['id']?>";
		$.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {code:code ,id:uid},
          success: function(msg) { 
                if(msg!='')  
                {
                	
                	$('#shareDiv').html(msg);
               		
                } 
               

          },
          error: function(msg) {
              alert('Something went wrong.Try Again!')
          }
        });
	}
</script>
<div class="ac_history">
	<div class="row">
    	<div class="col-md-12">
        	<div class="trans_details">
            <h3><i class="fa fa-info-circle"></i> Transaction Details</h3>		
            <div>
                <label>Total Redemptions:</label> <span><a href="javascript:void(0);" id="<?php echo $total_redemption?>" onclick="getRedeemHistory()"><?php echo $total_redemption?></a></span>
            </div>
            <div>
                <label>Total Amount Received:</label> <span><a href="#"><?php echo $total_amount?></a></span>
            </div>
            <div>
                <label>Reward Points:</label><span><a href="#"><?php echo empty($_REQUEST['points'])?0:$_REQUEST['points']; ?></a></span>
            </div>
        </div>
        </div>	
        <div class="">
        	<div class="col-md-6">
                <div id="shareDiv">
                    
                </div>
            </div>
            <div class="col-md-6">
                <div id="redeemDiv" style="display:none;">
                    
                </div>
            </div>
        </div>
    </div>
	</div>
</div>
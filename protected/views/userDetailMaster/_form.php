<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
//adding text options using append to 
 	function addRow1(tableID) 
 	{
		var rowCounter1 = $('#rowCounter1').val();
		rowCounter1=parseInt(rowCounter1)+1;
		//$("#dataTable1").append("<div class='wd_thirty1' id='y"+rowCounter1+"'><input type='text' name='textbox2["+rowCounter1+"]' value='' class='required'/><a href='javascript:void(0)' class='remove_btn' onClick=$('#y"+rowCounter1+"').remove();></a></div>");
		$("#productHead").append("<div class='col-md-4 col-sm-4 brand-input mb15 pt15' id='y"+rowCounter1+"'><span><label>Product Name</label></span><input type='text' name='new_product["+rowCounter1+"]' value='' class='required form-control mb15 width100p'/><span><label>UPC Code</label></span><input type='text' name='new_upc["+rowCounter1+"]' value='' class='required form-control mb15 width100p'/><a href='javascript:void(0)' class='remove-data' onClick=$('#y"+rowCounter1+"').remove();><i class='fa fa-remove'></i> Delete</a></div>");
		document.getElementById('rowCounter1').value=rowCounter1;
	}
</script>

<!-- Start of Helmet Details-->
<?php $ac=Yii::app()->controller->action->id;
if ($ac=='create')
{
?>
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validation.js"></script>

	<script type="text/javascript">
	// validate form
	$(document).ready(function() {
	$("#user-detail-master-form").validate(
	{
		rules: 
		{
			'UserDetailMaster[user_name]':
			{
				required:true,
				//maxlength: 50,
			} ,
			'UserDetailMaster[name]':
			{
				required:true,
				//maxlength: 50,
			} ,
			'brand_name':
			{
				required:true,
				//maxlength: 10,
			} ,
		},
		messages:
		{
			'UserDetailMaster[user_name]':
			{
				required:"Username cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
			'UserDetailMaster[name]':
			{
				required:"Name cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
			'brand_name':
			{
				required:"Relation cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
		},
	});
});
</script>


<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-detail-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<div class="col-md-4">
		<?php echo $form->labelEx($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'user_name'); ?>
        </div>
		<div class="col-md-4">
		<label>Account Name</label>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'name'); ?>
		</div>
        <div class="col-md-4">
		<label>Brand Name</label>
		<?php echo CHtml::textField("brand_name",'',array('id'=>'brand_name','class'=>'form-control mb15 width100p'));?>
        </div>
	</div>


	<div class="row" id="productHead">
    	 <h3 class="col-md-12">Add Brand Products</h3><br />

		<div class="col-md-4 col-sm-4 brand-input mb15 pt15">
        
		<!--<div class="clearfix"></div> -->
		<span><label>Product Name</label></span>
		<input type='text' name='product_name' id = "product_name" class='required form-control mb15 width100p'/>
		<span><label>UPC Code</label></span>
		<input type='text' name='upc_code' id = "upc_code" class='required form-control mb15 width100p'/>
        </div>
		<!--<div  id="dataTable1" class="genrated_div col-md-4"></div> -->
		
		<input type="hidden" id="rowCounter1" value="0"/>
        
	</div>
	

	<div class="row col-md-12 buttons add-btn">
      	<span><INPUT type="button" value="Add More" onClick="addRow1('productHead')" class="btn btn-primary mini_btn btn btn-default" /></span>
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info')); ?>
	</div>



<?php $this->endWidget(); ?>

</div><!-- form -->
</div>

<?php
}
else
{
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
	
	<div class="form dataTables_wrapper clearfix">
<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validation.js"></script>
<script type="text/javascript">
	// validate form
	$(document).ready(function() {
	$("#user-detail-master-form").validate(
	{
		rules: 
		{
			'UserDetailMaster[user_name]':
			{
				required:true,
				//maxlength: 50,
			} ,
			'UserDetailMaster[name]':
			{
				required:true,
				//maxlength: 50,
			} ,
			'brand_name':
			{
				required:true,
				//maxlength: 10,
			} ,
			'user_pass' :
			{
				required:true,
				minlength:8,
				validpassword:true,
			}
		},
		messages:
		{
			'UserDetailMaster[user_name]':
			{
				required:"Username cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
			'UserDetailMaster[name]':
			{
				required:"Name cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
			'brand_name':
			{
				required:"Relation cannot be blank.",
				//maxlength:"Maximum 50 characters.",
			},
			'user_pass' :
			{
				required:"Please provide password.",				
				minlength:"Password should be minimum 8 characters.",
				validpassword:"Password must contain a minimum of one lower case character,one upper case character and one digit.",
			}
		},
		
	});
	$.validator.addMethod("validpassword", function(value) 
	{		  
	    return value.match(/^.*(?=.{8,})(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).*$/); 
	    //for special charcter (?=.*[\W]).*$/
	});
});
</script>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-detail-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
    	<div class="col-md-4 col-sm-4">
			<?php echo $form->labelEx($model,'user_name'); ?>
			<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'user_name'); ?>
		</div>
		<div class="col-md-4 col-sm-4">
			<?php echo $form->labelEx($model,'email'); ?>
			<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'email'); ?>
		</div>
        <div class="col-md-4 col-sm-4">
			<?php echo $form->labelEx($model,'phone_no'); ?>
			<?php echo $form->textField($model,'phone_no',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'phone_no'); ?>
		</div>
    	<div class="col-md-4 col-sm-4">
			<?php $password = base64_decode($model->user_pass);?>
			<?php echo $form->labelEx($model,'user_pass'); ?>
			<?php echo CHtml::passwordField("user_pass",$password,array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'user_pass'); ?>
        </div>
       
        <?php
		//$session = new CHttpSession;
		//$session->open();
		//if($session['rid'] == '1')
		//{?>
			<!-- <div class="col-md-4 col-sm-4">
            <label>Deposit Amount</label>
            <?php echo $form->textField($model,'deposit_amount',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
            <?php echo $form->error($model,'deposit_amount'); ?>
        	</div> -->
		<?php //} ?>
       
	</div>
	<?php
	$session = new CHttpSession;
	$session->open();
	if($session['rid'] == '2' || ($model->u_id!=$session['uid']) )
	{
		?>
			<div class="row">
				 <div class="col-md-4 col-sm-4">
		            <?php echo $form->labelEx($model,'name'); ?>
		            <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		            <?php echo $form->error($model,'name'); ?>
		        </div>

		        <div class="col-md-4 col-sm-4">
		            <label>Brand Name</label>
		            <?php echo CHtml::textField("brand_name",$BrandName,array('id'=>'brand_name','class'=>'form-control mb15 width100p'));?>
		        </div>
	        	
			</div>
		<?php
	}
	?>

	<?php
	
	if($session['rid'] == '1' && (isset($model->u_id ) && $model->u_id!=$session['uid']))
	{
		?>
			
			<?php
			if($product_namerows != '0')
			{
			?>
				<div class="row">
	            <h3>Add Brand Products</h3>
				<?php foreach($product_namerows as $value)
				{?>
	            <div class="col-md-4 col-sm-4 brand-input" id="<?php echo "d".$value['pm_id']; ?>">
					
					<div class="clearfix" ></div>
					<span><label>Product Name</label></span>
					<input type="hidden" id="<?php echo $value['pm_id']; ?>">
					<!--<input type='text' name='old_product_name[".$value['product_name']."]' id = "old_product_name" value="<?php //echo $value['product_name']; ?>" class='required'/>-->
					<?php
					echo CHtml::textField("old_product_name[".$value['pm_id']."]",$value["product_name"],array('id'=>$value['pm_id'],'class'=>'required form-control mb15 width100p'));	
					?>
					<span><label>UPC Code</label></span>
					<?php
					echo CHtml::textField("old_upc_code[".$value['pm_id']."]",$value["upc_code"],array('id'=>$value['pm_id'],'class'=>'required form-control mb15 width100p'));	
					?>	
					<a onclick="deleteProducts('<?php echo $value['pm_id']; ?>')" class="remove-data" href="javascript:void(0)"><i class="fa fa-remove"></i> Delete</a>
	            </div>	
				<?php
				}
				?>
					<div  id="productHead" class="genrated_div"></div>
						
	                    
	                    

				</div>
			<?php
			}
			?>

		<?php
	}
	?>

	
	<br />

	<?php if($session['rid'] == '1' && (isset($model->u_id ) && $model->u_id!=$session['uid'])) {?>
    	<div class="row col-md-12 buttons add-btn">
			<span><INPUT type="button" value="Add More" onClick="addRow1('productHead')" class="btn btn-primary mini_btn btn btn-default" /></span>
			<input type="hidden" id="rowCounter1" value="0"/>
	<?php }?>
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info')); ?>
		</div>
<input type="hidden" id='deletedPid' name="deletedPids" />
<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<script type="text/javascript">
	
function  deleteProducts(id) 
{
	
	$('#d'+id).remove();

	var t=$('#deletedPid').val();
	var newvalue=t+",'"+id+"'";
	$('#deletedPid').val(newvalue);
}

</script>
<?php
}
?>
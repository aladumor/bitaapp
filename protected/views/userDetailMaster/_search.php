<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unique_code'); ?>
		<?php echo $form->textField($model,'unique_code',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'login_type'); ?>
		<?php echo $form->textField($model,'login_type',array('size'=>11,'maxlength'=>11)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ur_id'); ?>
		<?php echo $form->textField($model,'ur_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ut_id'); ?>
		<?php echo $form->textField($model,'ut_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email'); ?>
		<?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_name'); ?>
		<?php echo $form->textField($model,'user_name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'user_pass'); ?>
		<?php echo $form->textField($model,'user_pass',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addressline_1'); ?>
		<?php echo $form->textField($model,'addressline_1',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'addressline_2'); ?>
		<?php echo $form->textField($model,'addressline_2',array('size'=>60,'maxlength'=>200)); ?>
	</div>



	
	<div class="row">
		<?php echo $form->label($model,'zipcode'); ?>
		<?php echo $form->textField($model,'zipcode',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'phone_no'); ?>
		<?php echo $form->textField($model,'phone_no'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'profile_picture'); ?>
		<?php echo $form->textField($model,'profile_picture',array('size'=>50,'maxlength'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'dob'); ?>
		<?php echo $form->textField($model,'dob'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'gps_flag'); ?>
		<?php echo $form->textField($model,'gps_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'email_flag'); ?>
		<?php echo $form->textField($model,'email_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'notification_flag'); ?>
		<?php echo $form->textField($model,'notification_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'reward_points'); ?>
		<?php echo $form->textField($model,'reward_points'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'active_payment_method_id'); ?>
		<?php echo $form->textField($model,'active_payment_method_id'); ?>
	</div>

	

	<div class="row">
		<?php echo $form->label($model,'paypal_email'); ?>
		<?php echo $form->textField($model,'paypal_email',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_active'); ?>
		<?php echo $form->textField($model,'is_active'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'is_delete'); ?>
		<?php echo $form->textField($model,'is_delete'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_on'); ?>
		<?php echo $form->textField($model,'created_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_on'); ?>
		<?php echo $form->textField($model,'modified_on'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'modified_by'); ?>
		<?php echo $form->textField($model,'modified_by'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
<?php
/* @var $this SiteController */

/*$this->pageTitle=Yii::app()->name;*/
//$data = json_decode(Yii::app()->bitly->shorten('http://www.betaworks.com')->getResponseData(),true);

//$shorturl = ($data["status_code"]== '200') ? $data["data"]["url"] : '';
//print_r($shorturl);
?>
<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/select2.min.js'; ?>"></script>
<script src="http://code.highcharts.com/highcharts.js"></script>
<!-- <script src="http://code.highcharts.com/modules/exporting.js"></script> -->
<!-- <script src="http://highslide-software.github.io/export-csv/export-csv.js"></script>
 -->
<script type="text/javascript">
$(document).ready(function() {
    $('#content').removeClass('panel');
     /*Highcharts.setOptions({
           colors: ['#2EC9E7', '#33a3b7', '#65d8ed', '#98d7e2', '#afd4db','#AFD4DB', '#93a9ad', '#a7b9bc','#a7b9bc']
          });*/
          // Build AGE  chart
          <?php if(!empty($data['ageData'])){?>
          $('#ageGraph').highcharts({
                credits: {
                      enabled: false
                  },
              chart: {
                  plotBackgroundColor: null,
                  backgroundColor:'transparent',                  
                  plotBorderWidth: null,
                  plotShadow: false
              },
              title: {
                  text: ''
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.y}</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: false
                      },
                      showInLegend: false
                  }
              },
              series: [{
                  type: 'pie',
                  name: 'value',
                  data: [
                      /*['20-29',   45.0],
                      ['30-39',       26.8],                      
                      ['40-49',    8.5],
                      ['50-59',     6.2],
                      ['60+',   0.7]*/
                       <?php 
                        if(!empty($data['ageData']))
                        {
                          foreach ($data['ageData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['cnt']; ?>],  
                        <?php }            
                        }?>   

                  ]
              }]
          });
          <?php }else{?>
          $('#ageGraph').html('No Data Available.')
          <?php }?>

          //BUILD GENDER CHART
          <?php if(!empty($data['genderData'])){?>
          $('#genderGraph').highcharts({
                credits: {
                      enabled: false
                  },
                  chart: {
                      plotBackgroundColor: null,
                      backgroundColor:'transparent',                  
                      plotBorderWidth: null,
                      plotShadow: false
                  },
                  title: {
                      text: ''
                  },
                  tooltip: {
                      pointFormat: '{series.name}: <b>{point.y}</b>'
                  },
                  plotOptions: {
                      pie: {
                          allowPointSelect: true,
                          cursor: 'pointer',
                          dataLabels: {
                              enabled: false
                          },
                          showInLegend: false
                      }
                  },
                  series: [{
                      type: 'pie',
                      name: 'value',
                      data: [
                          /*['20-29',   45.0],
                          ['30-39',       26.8],                      
                          ['40-49',    8.5],
                          ['50-59',     6.2],
                          ['60+',   0.7]*/
                           <?php 
                            if(!empty($data['genderData']))
                            {
                              foreach ($data['genderData'] as $key => $value) { ?>
                                [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['cnt']; ?>],  
                            <?php }            
                            }?>   

                      ]
                  }]
              });
          <?php }else{?>
          $('#genderGraph').html('No Data Available.')
          <?php }?>

           //BUILD Device CHART
          <?php if(!empty($data['deviceData'])){?>
          $('#deviceGraph').highcharts({
              chart: {
                  plotBackgroundColor: null,
                  backgroundColor:'transparent',                  
                  plotBorderWidth: null,
                  plotShadow: false
              },
              credits: {
                      enabled: false
                },
              title: {
                  text: ''
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.y}</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: false
                      },
                      showInLegend: false
                  }
              },
              series: [{
                  type: 'pie',
                  name: 'value',
                  data: [
                      /*['20-29',   45.0],
                      ['30-39',       26.8],                      
                      ['40-49',    8.5],
                      ['50-59',     6.2],
                      ['60+',   0.7]*/
                       <?php 
                        if(!empty($data['deviceData']))
                        {
                          foreach ($data['deviceData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['cnt']; ?>],  
                        <?php }            
                        }?>   

                  ]
              }]
          });
          <?php }else{?>
          $('#deviceGraph').html('No Data Available.')
          <?php }?>
});
</script>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>
<!--<h1>Welcome <?php echo $username ;?>  To BevRage</h1> -->

 <section>

    <div class="pageheader">
        <div class="media">
            <div class="pageicon pull-left">
                <i class="fa fa-home"></i>
            </div>
            <div class="media-body">
                <ul class="breadcrumb">
                    <li><a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/site/index"><i class="glyphicon glyphicon-home"></i></a></li>
                    <li>Customer Dashboard</li>
                </ul>
                <h4> Customer Dashboard</h4>
            </div>
        </div><!-- media -->
    </div><!-- pageheader -->
                    
    <div class="contentpanel dashboard">
        <div class="panel panel-default clearfix">
        <div class="col-md-12">
       
        <div class="col-md-6">
        	<div class="icons">
            	<a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/UserDetailMaster/customer"><h3>Total App Users</h3><span class="glyphicon glyphicon-user"></span>
            	<h3><?php echo $users ; ?></h3></a>
            </div>
        </div>
        <div class="col-md-6">
          <div class="icons">
              <a href="<?php echo Yii::app()->request->baseUrl ?>/index.php/UserDetailMaster/exporttoexcel"><h3>Click to Download</h3><span class="glyphicon glyphicon-cloud-download"></span>
              <h3>App User Data</h3></a>
            </div>
        </div>
        </div>
        <div class="col-md-12">
        <div class="col-md-4">
        	<div class="icons">
            	<h3>Gender Break down</h3>
                <div id="genderGraph"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="icons">
                <h3>Age Break down</h3>
                <div id="ageGraph"></div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="icons">
                <h3>Device Break Down</h3>
                <div id="deviceGraph"></div>
            </div>
        </div>                                               
        </div>
    </div><!-- contentpanel -->
                    
    </div><!-- mainpanel -->
</section>

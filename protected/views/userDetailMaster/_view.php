<?php
/* @var $this UserDetailMasterController */
/* @var $data UserDetailMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->u_id), array('view', 'id'=>$data->u_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unique_code')); ?>:</b>
	<?php echo CHtml::encode($data->unique_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('login_type')); ?>:</b>
	<?php echo CHtml::encode($data->login_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ur_id')); ?>:</b>
	<?php echo CHtml::encode($data->ur_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('ut_id')); ?>:</b>
	<?php echo CHtml::encode($data->ut_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email')); ?>:</b>
	<?php echo CHtml::encode($data->email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('user_name')); ?>:</b>
	<?php echo CHtml::encode($data->user_name); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('user_pass')); ?>:</b>
	<?php echo CHtml::encode($data->user_pass); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressline_1')); ?>:</b>
	<?php echo CHtml::encode($data->addressline_1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressline_2')); ?>:</b>
	<?php echo CHtml::encode($data->addressline_2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city_id')); ?>:</b>
	<?php echo CHtml::encode($data->city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_id')); ?>:</b>
	<?php echo CHtml::encode($data->state_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('country_id')); ?>:</b>
	<?php echo CHtml::encode($data->country_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->zipcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone_no')); ?>:</b>
	<?php echo CHtml::encode($data->phone_no); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('profile_picture')); ?>:</b>
	<?php echo CHtml::encode($data->profile_picture); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('dob')); ?>:</b>
	<?php echo CHtml::encode($data->dob); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gps_flag')); ?>:</b>
	<?php echo CHtml::encode($data->gps_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('email_flag')); ?>:</b>
	<?php echo CHtml::encode($data->email_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notification_flag')); ?>:</b>
	<?php echo CHtml::encode($data->notification_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reward_points')); ?>:</b>
	<?php echo CHtml::encode($data->reward_points); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('active_payment_method_id')); ?>:</b>
	<?php echo CHtml::encode($data->active_payment_method_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('venmo_email')); ?>:</b>
	<?php echo CHtml::encode($data->venmo_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('paypal_email')); ?>:</b>
	<?php echo CHtml::encode($data->paypal_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_delete')); ?>:</b>
	<?php echo CHtml::encode($data->is_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_on')); ?>:</b>
	<?php echo CHtml::encode($data->modified_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_by')); ?>:</b>
	<?php echo CHtml::encode($data->modified_by); ?>
	<br />

	*/ ?>

</div>
<?php
/* @var $this UserDetailMasterController */
/* @var $model UserDetailMaster */

$this->breadcrumbs=array(
	'Manage Brand Partners'=>array('admin'),
	'Create Brand Partner',
);

$this->menu=array(
	//array('label'=>'List UserDetailMaster', 'url'=>array('index')),
	array('label'=>'Manage Brand Partners', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
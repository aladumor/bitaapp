<?php
/* @var $this UserDetailMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'User Detail Masters',
);

$this->menu=array(
	array('label'=>'Create UserDetailMaster', 'url'=>array('create')),
	array('label'=>'Manage UserDetailMaster', 'url'=>array('admin')),
);
?>

<h1>User Detail Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

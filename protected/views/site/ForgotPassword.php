<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
?>
<style>
.before_login #mainmenu, .before_login header{display:none;}
.before_login .mainwrapper:before{border-right:none;}
.before_login .mainwrapper .mainpanel{margin:0;}
.before_login .mainwrapper .panel{background:none;margin:0 auto;}
.before_login div.form .row{background:none;}
.before_login div.form input, div.form textarea, div.form select{margin:0;}
.input-group{width:100%;}
.input-group .form-control{color:#fff;}
.input-group .form-control input{border:1px solid #fff;color:#fff; }
div.form label{display:inline;font-weight:normal; }
</style>
<div class="logo text-center"><img alt="Bevrage" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-primary.png"></div>
<br>
<h4 class="text-center mb5">Forgot Password?</h4>
<p class="text-center">Sign in to your account</p>
<div class="mb30"></div>
<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'forgot-password-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
        
	),
)); ?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validation.js"></script>
	<script type="text/javascript">
	// validate form
$(document).ready(function() {
	$("#forgot-password-form").validate(
	{
		rules: 
		{
			'email':
			{
				required:true,
				email: true,
			},
		},
		messages:
		{
			'email':
			{
				required:"Email Address cannot be blank.",
				email:"Email Address should be in proper format.",
			},
		},
	});
	$(".main_container").addClass('panel panel-signin');
});
</script>

	<p class="note text-center">Fields with <span class="required">*</span> are required.</p>
     <div class="input-group mb15 required">
    	<div class="input-group mb15">
        
		<?php echo CHtml::textField("email","",array('id'=>"email", 'class'=>'form-control',"placeholder"=>"Email Address")); ?>
        <?php 
        	if($msg){?>
        	<label class="error"><?php echo $msg; ?></label>
        <?php }?>
        </div>
     </div>   
	
	<div class="clearfix text-center">
     
		<?php echo CHtml::submitButton('Send', array('class' => 'btn btn-success pl30 pr30')); ?>
        <br>
<br>

            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/login" class="mt20 clearfix" >Back to login</a>
	</div>
        

<?php $this->endWidget(); ?>
</div><!-- form -->

<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
?>
<style>
.before_login #mainmenu, .before_login header{display:none;}
.before_login .mainwrapper:before{border-right:none;}
.before_login .mainwrapper .mainpanel{margin:0;}
.before_login .mainwrapper .panel{background:none;margin:0 auto;}
.before_login div.form .row{background:none;}
.before_login div.form input, div.form textarea, div.form select{margin:0;}
.input-group{width:100%;}
.input-group .form-control input{border:1px solid #fff; }
div.form label{display:inline;font-weight:normal; }
.before_login .mainwrapper{background:none;}
</style>
<script type="text/javascript">
$(document).ready(function(e) {
    $('.main_container').addClass('panel panel-signin');
});
</script>
<div class="logo text-center"><img alt="Bevrage" src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-primary.png"></div>
<br>
<h4 class="text-center mb5">Already a Member?</h4>
<p class="text-center">Sign in to your account</p>
<div class="mb30"></div>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
        
	),
)); ?>

	<!--<p class="note">Fields with <span class="required">*</span> are required.</p> -->
        
	<div class="input-group mb15 required">
    	<div class="input-group mb15">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		<?php //echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', array("class" => " form-control","placeholder"=>"Username")); ?>
        </div>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="input-group mb15 required">
    	<div class="input-group mb15">
    	<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		<?php //echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array("class" => "form-control","placeholder"=>"Password")); ?>
        </div>
		<?php echo $form->error($model,'password'); ?>

	</div>

	<div class="clearfix text-center">
        

            <?php echo CHtml::submitButton('Sign In', array('class' => 'btn btn-success pl30 pr30 mb10')); ?>
<br>
<br>

            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/ForgotPassword" class="mt20 clearfix" >Forgot Password?</a>

    </div>
        

<?php $this->endWidget(); ?>
</div><!-- form -->
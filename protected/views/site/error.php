<?php
/* @var $this SiteController */
/* @var $error array */

$this->pageTitle=Yii::app()->name . ' - Error';
$this->breadcrumbs=array(
	'Error',
);
?>

<div class="span-19 contentpanel">
<div id="product-master-grid" class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer">
<div class="text-center mt80"> 
<h2>Error <?php echo $code; ?></h2>

<div class="error">
<?php echo CHtml::encode($message); ?>
</div>
</div>
</div>
</div>
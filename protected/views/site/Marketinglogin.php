<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
$model=new LoginForm;
?>

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'login-form',
	'enableClientValidation'=>true,
	'clientOptions'=>array(
		'validateOnSubmit'=>true,
        
	),
)); ?>
<div class="input-group mb15 required">
    	<div class="input-group mb15">
        <span class="input-group-addon"><i class="glyphicon glyphicon-user"></i></span>
		<?php //echo $form->labelEx($model,'username'); ?>
		<?php echo $form->textField($model,'username', array("class" => " form-control","placeholder"=>"Username")); ?>
        </div>
		<?php echo $form->error($model,'username'); ?>
	</div>

	<div class="input-group mb15 required">
    	<div class="input-group mb15">
    	<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
		<?php //echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password', array("class" => "form-control","placeholder"=>"Password")); ?>
        </div>
		<?php echo $form->error($model,'password'); ?>

	</div>

	<div class="clearfix text-center">
        

            <?php echo CHtml::submitButton('Sign In', array('class' => 'btn btn-success pl30 pr30 mb10')); ?>
<br>
<br>

            <a href="<?php echo Yii::app()->request->baseUrl;?>/index.php/site/ForgotPassword" class="mt20 clearfix" >Forgot Password?</a>

    </div>
     <?php $this->endWidget(); ?>
</div><!-- form -->   

<?php 
//echo "<pre>";
//print_r($offerList);
?>
<script type="text/javascript">
	function getEmail () 
	{
		$('#userDiv').show();
	}
	function getQrCode () 
	{
		var email=$('#emailId').val();
        var urlstr="<?php echo Yii::app()->createUrl('site/ajaxgetQr?')?>";
         $.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {email:email,oid:<?php echo $offerList[0]['o_id'] ;?> },
          success: function(msg) { 
                if(msg!='')  
                {
                    $('#QrDiv').show();
                    $('#QrDiv').html('');
                    $('#QrDiv').html(msg);
                }
                else
                {                    
                    $('#QrDiv').hide();
                }
                
          },
          error: function(msg) {
              alert('Something went wrong.Try Again!'); 
          }
        });
	}
</script>

<div id="home">
	
  <div class="background-overlay">
  	<div class="container">
    	<div class="row">
    		<div class="col-lg-4 text-center">
      <button class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      
    </div>
    		<div class="col-lg-4 text-center header-title">
      <h2>Welcome to</h2>
      <img alt="Big logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/big-logo.png" class="big-logo"></div>
      	</div>
	</div>
    
  </div>
</div>

<div class="container inner-page">
	<div class="row">
    	<h1 class="text-center" style="visibility: visible;">Offer Details</h1>
        <div data-wow-offset="200" class="container-colored-line wow bounceIn animated" style="visibility: visible;"> 
      		<hr class="colored-line">
      		<div class="add-colored-line"></div>
    	</div>
<div class="offer-details">
<div class="col-md-6">
	<h2><?php echo $offerList[0]['offer_name'] ;?></h2>
	<h6>Details </h6>
	<p><?php echo $offerList[0]['offer_desc'] ;?></p>
    
    <div class="offers-date">
		<p><i class='fa fa-calendar'></i> Start Date :<label><?php echo date('m-d-Y',strtotime($offerList[0]['start_date'])) ;?></label></p>
		<p><i class='fa fa-calendar'></i> Expiry Date :<label><?php echo date('m-d-Y',strtotime($offerList[0]['end_date'])) ;?></label></p>
	</div>
	
	<h6>Terms Of Use</h6>
    	<ul> 
        	<li>1. Offer available only to adults 21 years of age or older
			<li>2. You can redeem only one Bar/Restaurant offer every 24h
			<li>3. Gratuity is included</li>
       </ul>
	<!-- <img src="<?php// echo $offerList[0]['qrImage'] ;?>"> -->
	<h6>Available Locations:</h6>
	<?php  foreach ($offerList[0]['merchantList'] as $key => $value) 
	{?>
    	<div class="offer-location-area">
		
		<h6><?php echo $value['establishment_name'];?><h6>
		<h4>Phone:<h4>
		<i class="fa fa-phone"></i>  <?php echo $value['phone'];?>
		<h4>Address:</h4>
         <label><i class='fa fa-map-marker'></i> <?php echo $value['m_street']." ".$value['city_name']." " .$value['m_state']." ".$value['m_zipcode'] ;?></label>
		</div>
	<?php }?>
	<!-- <a href="javascript:void(0)" onclick="getEmail()">Redeem</a> -->
</div>
<div class="col-md-5 offer-img">
	<img src="<?php echo "http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/".$offerList[0]['offer_image'] ;?>" alt= "Not Available" />
    
    <div class="app-store-link">
    	<a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/app-store.jpg" alt=""  /></a>
        <a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/play-store.jpg" alt=""  /></a>
    </div>
    
	</div>
	</div>
	</div>
</div>



<footer class="footer">
 <div class="social-buttons">	
  	<div class="container">
    	<div class="row">
        	<a class="ffb_icon" target="_blank" title="Facebook" href="https://www.facebook.com/BevrageApp"></a>
            <a class="ftw_icon" target="_blank" title="Twitter" href="https://twitter.com/BevrageApp"></a> 
            <a class="fg_icon" target="_blank" title="Instagram" href="http://instagram.com/BevrageApp"></a> 
    	</div>
    </div>
 </div>
 
 <div class="container footer-nav">
 	<div class="row">
    	<div class="col-md-3">
        	<h3>About</h3>
            <ul>
            	<li class="nav-onpremise"><a href="#">How it works</a></li>
                <li class="nav-cont"><a href="javascript:void(0)">Where it works</a></li>
                <li><a target="_blank" href="faq.html">FAQ</a></li>
                <li><a href="javascript:void(0)">Press</a></li>
                <li class="nav-cont"><a href="mailto:partners@bevrage.com">Contact Support</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Partner With Us</h3>
            <ul>
            	<li><a href="brands.html">Brands</a></li>
                <li><a href="merchant.html">Merchant</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Legal</h3>
            <ul>
            	<li><a href="javascript:void(0)">Terms and Conditions</a></li>
                <li><a href="javascript:void(0)">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Get the APP</h3>
            <a class="download-button" href="https://itunes.apple.com/us/app/bevrage/id975122649?ls=1&amp;mt=8" target="_blank">
            	 <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page//bn-app.png" alt="App store button">
            </a>
            <a class="download-button" href=""> 
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/bn-google.png" alt="App store button">
            </a>
        </div>
    </div>
    <div class="row">
    	 <p class="copy-right">
       	&copy; Copyright 2015. All Rights Reserved. BEVRAGE<sup><font size="1">TM</font></sup> co. </p>
    </div>
 </div>
 
 
</footer>

<!-- <div id="userDiv" style="display:none;">
	<input placeholder="Email" name='Email' id="emailId"></input>
	<a href="javascript:void(0)" onclick="getQrCode()">Submit</a>	
</div>
<div id="Qrdiv">
	
</div> -->
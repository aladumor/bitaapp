<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

$this->pageTitle=Yii::app()->name . ' - Login';
/*$this->breadcrumbs=array(
	'Login',
);*/
?>

<h1>Reset Password</h1>




<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reset-password-form',
	//'enableClientValidation'=>true,
	//'clientOptions'=>array(
		//'validateOnSubmit'=>true,
        
	//),
)); ?>

<script src="<?php echo Yii::app()->request->baseUrl; ?>/js/validation.js"></script>
 	<script type="text/javascript">
	// validate form
$(document).ready(function() {
	$("#reset-password-form").validate(
	{
		rules: 
		{
			'password':
			{
				required:true,
				email: true,
			},
		},
		messages:
		{
			'password':
			{
				required:"Email Address cannot be blank.",
				email:"Email Address should be in proper format.",
			},
		},
	});
});
</script>

	<p class="note">Fields with <span class="required">*</span> are required.</p>
        
	<div class="row">
		<label>Password:</label>
		<?php echo CHtml::passwordField("password","",array('id'=>"password"));	 ?>
	</div>

	<div class="row">
		<label>Confirm Password:</label>
		<?php echo CHtml::passwordField("confirm_password","",array('id'=>"confirm_password"));	 ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Save'); ?>
	</div>
        

<?php $this->endWidget(); ?>
</div><!-- form -->

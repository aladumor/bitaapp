

<script type="text/javascript">
	$(document).ready(function(){
		filter("all","3");
	});
	function filter(arg1,arg2)
    {     
    	if(arg1=='4') 
    		var arg1=$("#BevrageCities_bc_id" ).val();
    	else
    		$("#BevrageCities_bc_id" ).val('');
    	if(arg1=='') 
    		var arg1="all";                
        var urlstr="<?php echo Yii::app()->createUrl('site/ajaxmarketingOffers?')?>";
         $.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {val:arg1,filterby: arg2},
          success: function(msg) { 
                if(msg!='')  
                {
                    $('#offersDiv').show();
                    $('#offersDiv').html('');
                    $('#offersDiv').html(msg);
                    
                    if(arg2==2 || arg2==3)
                    {
                        if(arg1==1)
                        {                      
                          $('#onPremise').addClass('activeTab');
                          $('#offPremise').removeClass('activeTab');
                          $('#all').removeClass('activeTab');
                        }
                        else if(arg1==2)
                        {
                          $('#onPremise').removeClass('activeTab');
                          $('#offPremise').addClass('activeTab');
                          $('#all').removeClass('activeTab');
                        }
                        else if(arg1=="all")
                        {
                          $('#onPremise').removeClass('activeTab');
                          $('#offPremise').removeClass('activeTab');
                          $('#all').addClass('activeTab');
                        }
                    }
                    else
                    {
                      $('#onPremise').removeClass('activeTab');
                      $('#offPremise').removeClass('activeTab');
                      $('#all').removeClass('activeTab');
                    }


                }
                else
                {                    
                    $('#offersDiv').hide();
                    alert('No Offers Found!');
                    filter("all","3");
                    $("#BevrageCities_bc_id" ).val('');
                }
                
          },
          error: function(msg) {
              alert('Something went wrong.Try Again!');
              $("#BevrageCities_bc_id" ).val('');
              filter("all","3");
          }
        });
    }
</script>

<div id="home">
	
  <div class="background-overlay">
  	<div class="container">
    	<div class="row">
    		<div class="col-lg-4 text-center">
      <button class="navbar-toggle" type="button"> <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
      
    </div>
    		<div class="col-lg-4 text-center header-title">
      <h2>Welcome to</h2>
      <img alt="Big logo" src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/big-logo.png" class="big-logo"></div>
      	</div>
	</div>
    
  </div>
</div>


<div class="container inner-page">
	<div class="row">
    	<h1 class="text-center" style="visibility: visible;">Offers</h1>
        <div data-wow-offset="200" class="container-colored-line wow bounceIn animated" style="visibility: visible;"> 
      		<hr class="colored-line">
      		<div class="add-colored-line"></div>
    	</div>
        
		<div class="offer-list">
        	
            <div class="offer-tab">
            	<div class="offer-click">
                	<ul class="clearfix">
	          			<li><a href="javascript:void(0);" id="all" class="activeTab" onclick="filter('','3')">All offers</a></li>
						<li><a href="javascript:void(0);" id="onPremise" onclick="filter('1','2')">Bars And Restaurant</a></li>
						<li><a href="javascript:void(0);" id="offPremise" onclick="filter('2','2')">Liqour Stores</a></li>
                    </ul>
                </div>
                <?php
        			$model=new BevrageCities;         
				    $criteria = new CDbCriteria;
				    $static = array(
	        		'' => '-- Select Cities --',
				    );
				    echo CHtml::activeDropDownList($model, 'bc_id', $static + CHtml::listData(BevrageCities::model()->findAll($criteria), 'bc_id', 'city_name'), array('onChange'=>'filter("4","1")')
	    );
				?>
            </div>
	
	<div id="offersDiv">
  </div>
</div>
	</div>
</div>

<footer class="footer">
 <div class="social-buttons">	
  	<div class="container">
    	<div class="row">
        	<a class="ffb_icon" target="_blank" title="Facebook" href="https://www.facebook.com/BevrageApp"></a>
            <a class="ftw_icon" target="_blank" title="Twitter" href="https://twitter.com/BevrageApp"></a> 
            <a class="fg_icon" target="_blank" title="Instagram" href="http://instagram.com/BevrageApp"></a> 
    	</div>
    </div>
 </div>
 
 <div class="container footer-nav">
 	<div class="row">
    	<div class="col-md-3">
        	<h3>About</h3>
            <ul>
            	<li class="nav-onpremise"><a href="#">How it works</a></li>
                <li class="nav-cont"><a href="javascript:void(0)">Where it works</a></li>
                <li><a target="_blank" href="faq.html">FAQ</a></li>
                <li><a href="javascript:void(0)">Press</a></li>
                <li class="nav-cont"><a href="mailto:partners@bevrage.com">Contact Support</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Partner With Us</h3>
            <ul>
            	<li><a href="brands.html">Brands</a></li>
                <li><a href="merchant.html">Merchant</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Legal</h3>
            <ul>
            	<li><a href="javascript:void(0)">Terms and Conditions</a></li>
                <li><a href="javascript:void(0)">Privacy Policy</a></li>
            </ul>
        </div>
        <div class="col-md-3">
        	<h3>Get the APP</h3>
            <a class="download-button" href="https://itunes.apple.com/us/app/bevrage/id975122649?ls=1&amp;mt=8" target="_blank">
            	 <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page//bn-app.png" alt="App store button">
            </a>
            <a class="download-button" href=""> 
            	<img src="<?php echo Yii::app()->request->baseUrl; ?>/images/offer-page/bn-google.png" alt="App store button">
            </a>
        </div>
    </div>
    <div class="row">
    	 <p class="copy-right">
       	&copy; Copyright 2015. All Rights Reserved. BEVRAGE<sup><font size="1">TM</font></sup> co. </p>
    </div>
 </div>
 
 
</footer>

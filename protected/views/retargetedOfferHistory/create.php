<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */

$this->breadcrumbs=array(
	'Retargeted Offer'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List RetargetedOfferHistory', 'url'=>array('index')),
	array('label'=>'Manage Retargeted Offers', 'url'=>array('admin')),
);
?>

<!-- <h1>Create RetargetedOfferHistory</h1>
 -->
<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
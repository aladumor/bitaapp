<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */

$this->breadcrumbs=array(
	'Retargeted Offer'=>array('admin'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List RetargetedOfferHistory', 'url'=>array('index')),
	array('label'=>'Setup New Targeted offer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#retargeted-offer-history-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<!-- <h1>Manage Retargeted Offer Histories</h1> -->

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
 -->
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'retargeted-offer-history-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	//'filter'=>$model,
	'columns'=>array(
		//'rt_id',
		//'targeted_o_id',
		array(
			 'header'=>"Traget offer",
            'name' => 'targeted_o_id',
            'value' => 'OfferMaster::model()->getname($data->targeted_o_id)',//'$data->OfferMaster->offer_name',
            'type'=>'raw'
        ),
		//'o_id',
		array(
            'name' => 'o_id',
            'value' => 'OfferMaster::model()->getname($data->o_id)',//'$data->OfferMaster->offer_name',
             'type'=>'raw'
        ),       
		'date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */

$this->breadcrumbs=array(
	'Retargeted Offer Histories'=>array('index'),
	$model->rt_id,
);

$this->menu=array(
	array('label'=>'List RetargetedOfferHistory', 'url'=>array('index')),
	array('label'=>'Create RetargetedOfferHistory', 'url'=>array('create')),
	array('label'=>'Update RetargetedOfferHistory', 'url'=>array('update', 'id'=>$model->rt_id)),
	array('label'=>'Delete RetargetedOfferHistory', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->rt_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RetargetedOfferHistory', 'url'=>array('admin')),
);
?>

<h1>View RetargetedOfferHistory #<?php echo $model->rt_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rt_id',
		'targeted_o_id',
		'o_id',
		'date',
	),
)); ?>

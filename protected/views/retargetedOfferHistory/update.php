<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */

$this->breadcrumbs=array(
	'Retargeted Offer Histories'=>array('index'),
	$model->rt_id=>array('view','id'=>$model->rt_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List RetargetedOfferHistory', 'url'=>array('index')),
	array('label'=>'Create RetargetedOfferHistory', 'url'=>array('create')),
	array('label'=>'View RetargetedOfferHistory', 'url'=>array('view', 'id'=>$model->rt_id)),
	array('label'=>'Manage RetargetedOfferHistory', 'url'=>array('admin')),
);
?>

<h1>Update RetargetedOfferHistory <?php echo $model->rt_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
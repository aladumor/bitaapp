<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
	function loadoffers () 
	{
		var val=$("#RetargetedOfferHistory_o_id" ).val();


	}
</script>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'retargeted-offer-history-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row col-md-12">
		<?php //echo $form->labelEx($model,'targeted_o_id'); ?>
		<label>Select Target Offer: <span class="required">*</span></label>		
          <?php
           /* if (!$model->isNewRecord)
                $selected = $model->cm_id;
            else
                $selected = 0;
            */
            $session = new CHttpSession;
            $session->open();
            $criteria = new CDbCriteria;
            if($session["rid"]==2)
                $criteria = "o_id in (select o_id from offer_master where u_id=" . $session["uid"].")";                            
               
            $static = array(
                '' => '-- Select Offer --',              
            );
            echo CHtml::activeDropDownList($model, 'targeted_o_id', $static + CHtml::listData(OfferMaster::model()->findAll($criteria), 'o_id', 'offer_name'),array('onchange' =>'loadoffers()' ));
            ?>    
		<?php //echo $form->error($model,'targeted_o_id'); ?>
	</div>

	<div class="form-group row col-md-12">
		<?php //echo $form->labelEx($model,'o_id'); ?>
		<label>Select Offer: <span class="required">*</span></label>
		<?php
           /* if (!$model->isNewRecord)
                $selected = $model->cm_id;
            else
                $selected = 0;
            */
            $session = new CHttpSession;
            $session->open();
            $criteria = new CDbCriteria;
            if($session["rid"]==2)
                $criteria = "o_id in (select o_id from offer_master where is_active=0 and  u_id=" . $session["uid"]." order by o_id desc)";                            
            else
                $criteria = "o_id in (select o_id from offer_master where is_active=0 order by o_id desc)";
            $static = array(
                '' => '-- Select Offer --',              
            );
            echo CHtml::activeDropDownList($model, 'o_id', $static + CHtml::listData(OfferMaster::model()->findAll($criteria), 'o_id', 'offer_name'),array('onchange' =>'loadoffers()' ));
            ?>    
		<?php //echo $form->error($model,'o_id'); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
		<?php echo $form->error($model,'date'); ?>
	</div> -->

	<div class="col-md-12 buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Send Notifications' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
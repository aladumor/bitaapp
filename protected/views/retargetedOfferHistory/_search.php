<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $model RetargetedOfferHistory */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'rt_id'); ?>
		<?php echo $form->textField($model,'rt_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'targeted_o_id'); ?>
		<?php echo $form->textField($model,'targeted_o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'o_id'); ?>
		<?php echo $form->textField($model,'o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
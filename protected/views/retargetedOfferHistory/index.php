<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Retargeted Offer Histories',
);

$this->menu=array(
	array('label'=>'Create RetargetedOfferHistory', 'url'=>array('create')),
	array('label'=>'Manage RetargetedOfferHistory', 'url'=>array('admin')),
);
?>

<h1>Retargeted Offer Histories</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

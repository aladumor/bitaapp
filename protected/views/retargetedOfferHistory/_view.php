<?php
/* @var $this RetargetedOfferHistoryController */
/* @var $data RetargetedOfferHistory */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('rt_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->rt_id), array('view', 'id'=>$data->rt_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('targeted_o_id')); ?>:</b>
	<?php echo CHtml::encode($data->targeted_o_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_id')); ?>:</b>
	<?php echo CHtml::encode($data->o_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date')); ?>:</b>
	<?php echo CHtml::encode($data->date); ?>
	<br />


</div>
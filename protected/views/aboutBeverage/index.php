<?php
/* @var $this AboutBeverageController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'About Beverages',
);

$this->menu=array(
	array('label'=>'Create AboutBeverage', 'url'=>array('create')),
	array('label'=>'Manage AboutBeverage', 'url'=>array('admin')),
);
?>

<h1>About Beverages</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

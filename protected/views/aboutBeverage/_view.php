<?php
/* @var $this AboutBeverageController */
/* @var $data AboutBeverage */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('im_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->im_id), array('view', 'id'=>$data->im_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('about_us_title')); ?>:</b>
	<?php echo CHtml::encode($data->about_us_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('about_us')); ?>:</b>
	<?php echo CHtml::encode($data->about_us); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('privacy_policy_title')); ?>:</b>
	<?php echo CHtml::encode($data->privacy_policy_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('privacy_policy')); ?>:</b>
	<?php echo CHtml::encode($data->privacy_policy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_and_c_title')); ?>:</b>
	<?php echo CHtml::encode($data->t_and_c_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('t_and_c')); ?>:</b>
	<?php echo CHtml::encode($data->t_and_c); ?>
	<br />


</div>
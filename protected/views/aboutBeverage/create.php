<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */

$this->breadcrumbs=array(
	'About Beverages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List AboutBeverage', 'url'=>array('index')),
	array('label'=>'Manage AboutBeverage', 'url'=>array('admin')),
);
?>

<h1>Create AboutBeverage</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
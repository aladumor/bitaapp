<html>
	<head>
	<style>
	.ac_heading{
		display:none !important;
	}
	.content{
	display:none !important;
	}
  .bioDiv img{max-width: 70%;}
	</style>
	<!--<script>
    $(window).load(function() {
	$('.modal-slider').flexslider({
	animation: "fade"
	}); 
	});
    </script> -->
	</head>
</html>
<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */

$this->breadcrumbs=array(
	'About Beverage'=>array('view/1'),
	//$model->im_id,
);

$this->menu=array(
	array('label'=>'List AboutBeverage', 'url'=>array('index')),
	array('label'=>'Create AboutBeverage', 'url'=>array('create')),
	array('label'=>'Update AboutBeverage', 'url'=>array('update', 'id'=>$model->im_id)),
	array('label'=>'Delete AboutBeverage', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->im_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage AboutBeverage', 'url'=>array('admin')),
);
?>

<div id="action" class="clearfix dataTables_wrapper"> 
    <!-- Div #action description --> 
    <h1 class="wow fadeInDown animated animated text-center">Meet Our Team</h1>
    <div class="row text-center our-team">
      <div data-wow-offset="100" class="bounceIn mar">
        <div class="span-2 bioDiv"> <img alt="" data-modal="modal-1" class="md-trigger" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/team1.png">
          <h3> <a data-modal="modal-1" class="md-trigger"> Dounya </a> </h3>
          
          <!-- /container -->
          
          <div id="modal-1" class="md-modal md-effect-1">
            <div class="md-content">
              <h3>Dounya Discala </h3>
              <p class="fltleft md-close"> <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/m-close.png"></p>
              <div>
                <p class="wel-font2"><strong>Dounya Discala</strong> brings more than seven years of international marketing and business development to BEVRAGE, including five years as the Marketing and Business Development Director for Ralph Lauren Home in Paris, France. Dounya also has broad experience with media and communications strategies as well as partnership development.</p>
                <p class="wel-font2"> Her business insight, attention to detail and dedication to customer service coupled with her essential love of building relationships enables her to service the needs of her clients. With Ralph Lauren, she introduced new product lines and uses her existing skills in marketing and negotiation to expand the market presence drastically. Working at Ralph Lauren also allowed her to further explore her passion for interior design and architecture. </p>
                <p class="wel-font2">Dounya earned a Masters Degree in marketing from the Ecole Superieure de Commerce de Paris. Dounya is a travel enthusiast, and has found that her international background and language skills have prepared her well to work with international clientele. She is fluent in French and German and proficient in Arabic and Spanish. </p>
              </div>
            </div>
          </div>
          <div class="md-overlay"></div>
          <!-- the overlay element -->
          
          <div class="midea"><a href="mailto:dounyad@bevrage.com">DounyaD @ BEVRAGE</a></div>
        </div>
        <div class="span-2 bioDiv"> <img alt="" data-modal="modal-2" class="md-trigger" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/team2.png">
          <h3> <a data-modal="modal-2" class="md-trigger"> Mike </a></h3>
          <div id="modal-2" class="md-modal md-effect-14">
            <div class="md-content">
              <h3>Mike Magolnick</h3>
              <p class="fltleft md-close"> <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/m-close.png"></p>
              <div id="ex3" class="pophor">
                <p class="wel-font2"><strong>Mike Magolnick</strong> is a twenty-year executive, a 3-time Amazon Bestselling Author and a popular keynote speaker. </p>
                <p class="wel-font2"> Mike has served as a C-level executive for two public companies (President/COO of IMC Group and CEO of Complete Identity) and over the past 20 years, has become one of the foremost business-strategy and venture-planning professionals in North America having been involved in the execution of hundreds of business plans representing a collective of more than $3 Billion in global revenue. Mike is also a digital media pioneer and among the most connected people in the world with a personal reach of more than thirty five million including business and political leaders, celebrities, authors and more. </p>
                <p class="wel-font2"> In the early 1990’s Mike was the founder and managing partner for Venture Consulting, a business and strategic planning firm in South Florida. He went on to build a number of companies, several of which have gone on to become multi-million dollar businesses. In 1999 Mike started SolutionHome and invented the Internet’s first domain name appraisal formula. He was personally responsible for more than 50,000 domain name appraisals, served as an expert witness for major Internet court cases and participated in the development of several important industry-shaping events such as the drafting of the Internet Cybersquatting Law and the development and growth of ICANN. In 2004 Mike founded Complete Identity, an international brand strategy company that worked with $100 million+ companies. During the mid-2000s Mike became of the foremost experts on all things Internet, digital and social media. </p>
                <p class="wel-font2"> Mike has been a recognized business and Internet expert having been interviewed and/or appearing in print in the Wall Street Journal, the New York Times, American Venture Magazine, Entrepreneur Magazine, The American Business Journal and other national publications.  Mike has a degree in Corporate Governance from Tulane Law School. Mike also serves as the President of North Texas Mensa. </p>
                <p class="wel-font2"> </p>
              </div>
            </div>
            
          </div>
          
         
          <!-- the overlay element -->
          
          <div class="midea"> <a href="mailto:mikem@bevrage.com">MikeM @ BEVRAGE</a> </div>
           <div class="md-overlay"></div>
        </div>
        
        <div class="span-2 bioDiv"> <img alt="" data-modal="modal-3" class="md-trigger" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/team3.png">
          <h3><a data-modal="modal-3" class="md-trigger"> Mitch </a></h3>
          <div id="modal-3" class="md-modal md-effect-10">
            <div class="md-content">
              <h3>Mitch Berkoff</h3>
              <p class="fltleft md-close"> <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/m-close.png"></p>
              <div>
                <p class="wel-font2"><strong>Mitch Berkoff</strong> brings more than 10 years of retail experience in the liquor industry to BEVRAGE.  Mitch is the fourth generation of his family that has entered into the liquor industry with a vast network of relationships.  As a recent graduate of University of Connecticut School of Business, Mitch brings a diverse background in finance and marketing.  His first summer in college was spent interning at Du Pasquier and Co focusing on investment banking and capital placement.  As part of the investment banking team he assisted in executing PIPE transactions for micro-cap companies in the healthcare sector.  Having also interned at Conair Corporation for six years, Mitch played a vital role as assistant to director of public events and trade shows, where his range of responsibilities extended from pre-event planning to on-site execution as well as exhibit program management. </p>
              </div>
            </div>
          </div>
          <div class="md-overlay"></div>
          <!-- the overlay element -->
          
          <div class="midea"> <a href="mailto:mitchb@bevrage.com">MitchB @ BEVRAGE</a> </div>
        </div>
        <div class="span-2 bioDiv"> <img alt="" data-modal="modal-4" class="md-trigger" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/team4.png">
          <h3><a data-modal="modal-4" class="md-trigger"> Chelsea </a></h3>
          <div id="modal-4" class="md-modal md-effect-8">
            <div class="md-content">
              <h3>Chelsea Sciarretta </h3>
              <p class="fltleft md-close"> <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/m-close.png"></p>
              <div>
                <p class="wel-font2"><strong>Chelsea Sciarretta</strong> joins BEVRAGE after an impressive early career working on brand relations, marketing, communications and community outreach for several major brands including The Coca-Cola Company and IMG. Chelsea has also been an important part of the sports marketing team for IMG College. Her work there has not only helped to coordinate efforts for promotion ofUniversity of Connecticut corporate sponsors, she has also facilitated promotional events to support major brand partners such as Dunkin' Donuts, Webster Bank and AAA. In addition to her work in the US, Chelsea has had the opportunity to work in a marketing and brand partner support role in Florence, Italy giving her important international marketing skills that helped a strong transition into her role as a brand ambassador for Coca-Cola. Chelsea will help lead BEVRAGE's brand ambassador programs as well as grassroots marketing efforts.</p>
              </div>
            </div>
          </div>
          <div class="md-overlay"></div>
          <!-- the overlay element -->
          
          <div class="midea"> <a href="mailto:chelseas@bevrage.com">ChelseaS @ BEVRAGE</a> </div>
        </div>
        <div class="span-2 bioDiv"> <img alt="" data-modal="modal-5" class="md-trigger" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/team6.png">
          <h3><a data-modal="modal-5" class="md-trigger"> Sophia </a></h3>
          <div id="modal-5" class="md-modal md-effect-9">
            <div class="md-content">
              <h3> Sophia Sanchez </h3>
              <p class="fltleft md-close"> <img alt="" src="<?php echo Yii::app()->request->baseUrl; ?>/images/photos/m-close.png"></p>
              <div>
                <p class="wel-font2"><strong>Sophia Sanchez</strong> brings more than eight years of high-level executive administration experience to BEVRAGE. Her overall administrative expertise in HR, AR/AP, corporate bookkeeping, and integrated organizational planning is a key component to the success of the corporation and its team members.  Sophia served as part of the management team for a building project company for over five years, during which time she became fluent in various software programs and processes. She is also proficient in organizing schedules and deliverables as well as detailed corporate/statistical reporting.</p>
              </div>
            </div>
          </div>
          <div class="md-overlay"></div>
          <!-- the overlay element -->
          
          <div class="midea"> <a href="mailto:sophias@bevrage.com">SophiaS @ BEVRAGE</a> </div>
        </div>
        <div class="clearfix"></div>
        <div class="col-md-12 wow bounceIn animated aboutus animated" style="visibility: visible;">
          <p class="font1"><?php echo $model->about_us; ?></p>
        </div>
        <div class="clearfix"></div>
      </div>
    </div>
  </div>

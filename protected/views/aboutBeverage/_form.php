<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'about-beverage-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'about_us_title'); ?>
		<?php echo $form->textArea($model,'about_us_title',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'about_us_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'about_us'); ?>
		<?php echo $form->textArea($model,'about_us',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'about_us'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_policy_title'); ?>
		<?php echo $form->textArea($model,'privacy_policy_title',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'privacy_policy_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'privacy_policy'); ?>
		<?php echo $form->textArea($model,'privacy_policy',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'privacy_policy'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_and_c_title'); ?>
		<?php echo $form->textArea($model,'t_and_c_title',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'t_and_c_title'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'t_and_c'); ?>
		<?php echo $form->textArea($model,'t_and_c',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'t_and_c'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
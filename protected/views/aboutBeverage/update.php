<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */

$this->breadcrumbs=array(
	'About Beverages'=>array('index'),
	$model->im_id=>array('view','id'=>$model->im_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List AboutBeverage', 'url'=>array('index')),
	array('label'=>'Create AboutBeverage', 'url'=>array('create')),
	array('label'=>'View AboutBeverage', 'url'=>array('view', 'id'=>$model->im_id)),
	array('label'=>'Manage AboutBeverage', 'url'=>array('admin')),
);
?>

<h1>Update AboutBeverage <?php echo $model->im_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
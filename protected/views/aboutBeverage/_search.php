<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'im_id'); ?>
		<?php echo $form->textField($model,'im_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'about_us_title'); ?>
		<?php echo $form->textArea($model,'about_us_title',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'about_us'); ?>
		<?php echo $form->textArea($model,'about_us',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'privacy_policy_title'); ?>
		<?php echo $form->textArea($model,'privacy_policy_title',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'privacy_policy'); ?>
		<?php echo $form->textArea($model,'privacy_policy',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_and_c_title'); ?>
		<?php echo $form->textArea($model,'t_and_c_title',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'t_and_c'); ?>
		<?php echo $form->textArea($model,'t_and_c',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
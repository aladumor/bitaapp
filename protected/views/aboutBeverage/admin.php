<?php
/* @var $this AboutBeverageController */
/* @var $model AboutBeverage */

$this->breadcrumbs=array(
	'About Beverages'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List AboutBeverage', 'url'=>array('index')),
	array('label'=>'Create AboutBeverage', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#about-beverage-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage About Beverages</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'about-beverage-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'columns'=>array(
		'im_id',
		'about_us_title',
		'about_us',
		'privacy_policy_title',
		'privacy_policy',
		't_and_c_title',
		/*
		't_and_c',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

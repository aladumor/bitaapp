<?php
/* @var $this ProductMasterController */
/* @var $model ProductMaster */

$this->breadcrumbs=array(
	'Codes'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List ProductMaster', 'url'=>array('index')),
	array('label'=>'Manage Codes', 'url'=>array('admin')),
);
?>

<!-- <h1>Create ProductMaster</h1> -->

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
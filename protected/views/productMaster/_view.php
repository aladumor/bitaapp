<?php
/* @var $this ProductMasterController */
/* @var $data ProductMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('pm_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->pm_id), array('view', 'id'=>$data->pm_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('product_name')); ?>:</b>
	<?php echo CHtml::encode($data->product_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('upc_code')); ?>:</b>
	<?php echo CHtml::encode($data->upc_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_delete')); ?>:</b>
	<?php echo CHtml::encode($data->is_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />


</div>
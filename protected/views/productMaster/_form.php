
<?php
/* @var $this ProductMasterController */
/* @var $model ProductMaster */
/* @var $form CActiveForm */
?>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
    	<div class="col-md-4">
		<?php echo $form->labelEx($model,'product_name'); ?>
		<?php echo $form->textField($model,'product_name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p disable','disabled'=>true)); ?>
		<?php echo $form->error($model,'product_name'); ?>
		</div>
        <div class="col-md-4">
		<?php echo $form->labelEx($model,'upc_code'); ?>
		<?php echo $form->textField($model,'upc_code',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'upc_code'); ?>
		</div>
        
        </div>
	</div>

	<div class="row col-md-12 buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>

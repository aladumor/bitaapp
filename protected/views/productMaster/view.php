<?php
/* @var $this ProductMasterController */
/* @var $model ProductMaster */

$this->breadcrumbs=array(
	'Codes'=>array('admin'),
	'Code Details'
	//$model->pm_id,
);

$this->menu=array(
	//array('label'=>'List ProductMaster', 'url'=>array('index')),
	//array('label'=>'Create ProductMaster', 'url'=>array('create')),
	//array('label'=>'Update Product', 'url'=>array('update', 'id'=>$model->pm_id)),
	//array('label'=>'Delete ProductMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->pm_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Codes', 'url'=>array('admin')),
);
?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'pm_id',
		'product_name',
		'upc_code',
		array(
		    'name'=>'Is Active',
		    'header'=>'Is Active',
		    //'value'=>'date("d M Y",strtotime($data["work_date"]))'
		    'value' => ($model->is_active == "0") ? "Yes" : "No"
		),
		//'is_delete',
		//'created_by',
	),
)); ?>

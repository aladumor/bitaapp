<?php
/* @var $this ProductMasterController */
/* @var $model ProductMaster */

$this->breadcrumbs=array(
	'Codes'=>array('admin'),
	//$model->pm_id=>array('view','id'=>$model->pm_id),
	'Update Code',
);

$this->menu=array(
	//array('label'=>'List ProductMaster', 'url'=>array('index')),
	//array('label'=>'Create ProductMaster', 'url'=>array('create')),
	//array('label'=>'View ProductMaster', 'url'=>array('view', 'id'=>$model->pm_id)),
	array('label'=>'Manage Codes', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
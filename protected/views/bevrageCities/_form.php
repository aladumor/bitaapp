<?php
/* @var $this BevrageCitiesController */
/* @var $model BevrageCities */
/* @var $form CActiveForm */
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix bev_city">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'bevrage-cities-form',
	'enableAjaxValidation'=>false,
)); ?>
<?php echo $form->errorSummary($model); ?>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
	

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'city_name'); ?>
		<?php echo $form->textField($model,'city_name',array('size'=>50,'maxlength'=>50,'class'=>input_box)); ?>
	</div>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'state_code'); ?>
		<?php echo $form->textField($model,'state_code',array('size'=>10,'maxlength'=>10,'class'=>input_box)); ?>
	</div>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->dropDownList($model,'type',array(''=>'-- Select Type--',"1"=>"On Premise","2"=>"Off Premise","3"=>"Both")); ?>
		<?php //die;?>
	</div>

	<div class="col-md-12 buttons">		
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
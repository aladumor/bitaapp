<?php
/* @var $this BevrageCitiesController */
/* @var $data BevrageCities */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('bc_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->bc_id), array('view', 'id'=>$data->bc_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city_name')); ?>:</b>
	<?php echo CHtml::encode($data->city_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_code')); ?>:</b>
	<?php echo CHtml::encode($data->state_code); ?>
	<br />


</div>
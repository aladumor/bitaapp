<?php
/* @var $this BevrageCitiesController */
/* @var $model BevrageCities */

$this->breadcrumbs=array(
	'Bevrage Cities'=>array('index'),
	$model->bc_id,
);

$this->menu=array(
	array('label'=>'List BevrageCities', 'url'=>array('index')),
	array('label'=>'Create BevrageCities', 'url'=>array('create')),
	array('label'=>'Update BevrageCities', 'url'=>array('update', 'id'=>$model->bc_id)),
	array('label'=>'Delete BevrageCities', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->bc_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BevrageCities', 'url'=>array('admin')),
);
?>

<h1>View BevrageCities #<?php echo $model->bc_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'bc_id',
		'city_name',
		'state_code',
	),
)); ?>

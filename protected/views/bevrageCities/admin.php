<div class="manage_icons"><?php
/* @var $this BevrageCitiesController */
/* @var $model BevrageCities */

$this->breadcrumbs=array(
	'Bevrage Cities'=>array('index'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List BevrageCities', 'url'=>array('index')),
	array('label'=>'Add City', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#bevrage-cities-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
</style>
<!-- <h1>Manage Bevrage Cities</h1> -->

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
 -->
 <div class="dataTables_wrapper">
<div class="col-md-12">
	<a class="btn btn-green pull-right offer-btn" href="create"><i class="fa fa-plus"></i>&nbsp;Add City</a>
</div>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'bevrage-cities-grid',
	'dataProvider'=>$model->search(),
	'template'=>'{pager}{items}{pager}' ,
	'itemsCssClass' => 'table table-striped table-bordered responsive footable',
	'filter'=>$model,
	'columns'=>array(
		//'bc_id',
		'city_name',
		'state_code',
		array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(
                   
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        ),
	),
)); ?>	
</div></die>



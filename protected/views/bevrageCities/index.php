<?php
/* @var $this BevrageCitiesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Bevrage Cities',
);

$this->menu=array(
	array('label'=>'Create BevrageCities', 'url'=>array('create')),
	array('label'=>'Manage BevrageCities', 'url'=>array('admin')),
);
?>

<h1>Bevrage Cities</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

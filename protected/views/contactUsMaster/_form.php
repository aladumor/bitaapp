<style>
  #map-canvas {
    width: 500px;
    height: 400px;
  }
</style>
<?php
/* @var $this ContactUsMasterController */
/* @var $model ContactusMaster */
/* @var $form CActiveForm */

?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">

<div class="form">
<script src="http://maps.googleapis.com/maps/api/js">
</script>
<script>
var myCenter=new google.maps.LatLng(41.043449,-73.535039);
var marker;

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:15,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  animation:google.maps.Animation.BOUNCE
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'contactus-master-form',
	'enableAjaxValidation'=>false,
)); ?>


	<!-- <p class="note">Fields with <span class="required">*</span> are required.</p> -->

	<?php //echo $form->errorSummary($model); ?>

	<!-- <div class="row">
    	
        <div class="col-md-6">
        	<ul class="conntact-form">
                  <li>
                      <?php echo $form->labelEx($model,'name'); ?>
                      <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
                      <?php echo $form->error($model,'name'); ?>
                  </li>	
                  <li>
                      <?php echo $form->labelEx($model,'subject'); ?>
                      <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>500,'class'=>'form-control mb15 width100p')); ?>
                      <?php echo $form->error($model,'subject'); ?>
                  </li>
                  <li>
                      <?php echo $form->labelEx($model,'message'); ?>
                      <?php echo $form->textArea($model,'message',array('rows'=>6, 'cols'=>57,'class'=>'form-control mb15 width100p')); ?>
                      <?php echo $form->error($model,'message'); ?>
                  </li>
                  <li>
                  	<div class="buttons">
                      <?php echo CHtml::submitButton($model->isNewRecord ? 'Submit' : 'Save'); ?>
                     </div>
                  </li>   
            </ul>
        </div>
        
        <div class="col-md-6">
    		<h3>Contact Details</h3>
				<label><i class="fa fa-phone"></i> &nbsp; Contact No : (203) 803-1995</label>
				<label><i class="fa fa-envelope"></i>&nbsp;  Email : <a href="mailto:info@BEVRAGE.com">help@bevrage.com</a></label>
				<label><i class="fa fa-map-marker"></i> &nbsp; Office Address : BEVRAGETM LLC <br/>
                                                       17 Cedar Street <br/>
                                                       Stamford, CT 06902<br/></label>    	
        </div>
        
	</div>
<br />
	<div class="row col-md-12">
	    <div id="googleMap" style="width:500px;height:300px;"></div>
	</div>	 -->

<div class="conact" data-wow-offset="100">
    <div class="">
      <div class="col-md-3 col-sm-4 text-center">
        <div class="con-box text-center">
         <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/loc.png" width="23" height="43" alt="">
        </div>
        <h3> BEVRAGE<sup><font size=1>TM</font></sup> CO </h3>
        
      </div>
      <div class="col-md-3 col-sm-4 text-center">
        <div class="con-box text-center"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/phn.png" width="22" height="37" alt=""> </div>
        <h3> 203-803-1995 </h3>
      </div>
      <div class="col-md-3 col-sm-4 text-center">
        <div class="con-box text-center"> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/ms.png" class="ms" width="33" height="22" alt=""> </div>
        <h3> <a href="mailto:help@bevrage.com">help@bevrage.com</a> </h3>
      </div>
    </div>
  </div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
</div>

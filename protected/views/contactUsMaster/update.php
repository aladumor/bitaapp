<?php
/* @var $this ContactUsMasterController */
/* @var $model ContactusMaster */

$this->breadcrumbs=array(
	'Contactus Masters'=>array('index'),
	$model->name=>array('view','id'=>$model->contact_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ContactusMaster', 'url'=>array('index')),
	array('label'=>'Create ContactusMaster', 'url'=>array('create')),
	array('label'=>'View ContactusMaster', 'url'=>array('view', 'id'=>$model->contact_id)),
	array('label'=>'Manage ContactusMaster', 'url'=>array('admin')),
);
?>

<h1>Update ContactusMaster <?php echo $model->contact_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
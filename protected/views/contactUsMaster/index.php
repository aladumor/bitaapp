<?php
/* @var $this ContactUsMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Contactus Masters',
);

$this->menu=array(
	array('label'=>'Create ContactusMaster', 'url'=>array('create')),
	array('label'=>'Manage ContactusMaster', 'url'=>array('admin')),
);
?>

<h1>Contactus Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

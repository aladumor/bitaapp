

<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
$this->breadcrumbs = array(
    'Offer Transactions ' => array('admin'),
    'Manage Transactions',
);

$this->menu = array(
    array('label' => 'Manage Transactions', 'url' => array('admin')),
        //array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#offer-transaction-master-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<!--<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>-->

<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>
    <script>
        $('#offer-master-grid').removeClass('.dataTables_wrapper');
		
		$('.dataTables_wrapper .btn').on('click', function(){
    	$('.dataTables_wrapper .btn.selected').removeClass('selected');
   		$('.dataTables_wrapper .btn').addClass('selected');
});
    </script>
<div class="search-form" style="display:none">
    <?php
    $this->renderPartial('_search', array(
        'model' => $model,
    ));
    ?>
    
</div><!-- search-form -->

<?php
//    $type = $model->type;
//    print_r($type);die;
?>
<div class="dataTables_wrapper ">
    <label class="ml20"><h3 class="media-heading"><i class="fa fa-filter"> Filter By :</i></h3></label>
    <a class="btn btn-primary" href="admin?type=2">Pending </a>
    <a class="btn btn-primary" href="admin?type=1">Completed</a>
    <a class="btn btn-primary" href="admin?type=3">Cancel</a>
    <a class="btn btn-primary" href="admin?type=desc">Latest</a>
    <a class="btn btn-primary" href="admin?type=0">All</a>
    <?php

    $pageSizeDropDown = CHtml::dropDownList(
        'pageSize',
        $pageSize,
        array( 10 => 10, 25 => 25, 50 => 50, 100 => 100 ),
        array(
            'class'    => 'change-pagesize',
            'onchange'=>"$.fn.yiiGridView.update('offer-transaction-master-grid',{ data:{pageSize: $(this).val() }})",
        )
    );
    ?>  
<?php
$session = new CHttpSession;
$session->open();
if($session['rid'] == 1)
{
$this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'offer-transaction-master-grid',
    'dataProvider' => $model->search(),
    'template'=>'{pager}{items}{pager}' ,
    'itemsCssClass' => 'table table-striped table-bordered responsive',
    'filter' => $model,
    'columns' => array(
         array(
            'type' => 'raw',
            'header'=>'Account Name',
            'value' => function($data){return $data->getAccountName($data->o_id);},
        ),
        array(
            'name' => 'OfferName',
            'value' => '$data->OfferMaster["offer_name"]',
        ),
        //'redeem_by',
        //'redeem_date',
        array(
            'name'=>'redeem_date',
            'header'=>'Redeem Date Time',
            //'value'=>'date("d M Y",strtotime($data["work_date"]))'
            //'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->redeem_date))'
            'value' => function ($model) {
                    $redeemDate= Yii::app()->dateFormatter->format("d MMM y",strtotime($model->redeem_date)). ' ' . $model->transaction_time;
                    /*$usersTimezone = 'America/New_York';
                    $date = new DateTime( $redeemDate, new DateTimeZone($usersTimezone) );
                    return $date->format('d M y H:i:s');*/
                    return $redeemDate;
                },
        ),
        //'total_amount',
        array(
            'name' => 'UserName',
            'value' => '$data->UserMaster["email"]',
        ),
        array(
            'header' => 'Payment Status',
            'name' =>'payment_status',
            'value' => function($data){
                        if($data->type == "off_pre"){
                           return $data->getPaymentStatus($data->customer_payment_status);
                        }else{
                            return $data->getPaymentStatus($data->payment_status);
                        }  
                    },
        ),
        array(
            'header'=>'Amount Payable',
            'value' => function($data){
                        if($data->type == "off_pre"){
                            echo $data->customer_amount;
                        }else{
                            echo $data->merchant_amount;                        
                        }  
                    },
        ),
        array(
            'name'=>'PaymentMethod',
            'header'=>' Current Payment Method',
            'value' => function($data){
                          return $data->getPaymentMethod($data->unique_code);
                       },  
        ),
       'transaction_id',
       /* array(
            'header'=>'Transaction Id',
            'value' => function($data){
                        if($data->type == "off_pre"){
                           return $data->getPaymentStatus($data->customer_payment_status);
                        }else{
                            return $data->getPaymentStatus($data->payment_status);
                        }  
                    },
        ),*/
        array(
            'header'=>'Type',
            'value' => function($data){
                        if($data->type == "off_pre"){
                            echo "Liquore Store";
                        }else{
                            echo "Bars & Restaurant";                        
                        }  
                    },
        ),
        array
            (
            'class' => 'CButtonColumn',
            'template' => '{Verify}',
            'header' =>"Verify Payment",
                'buttons' => array
                (
                'Verify' => array(
                   'label' => '<i class="fa fa-eye"></i>',
                   'name'=>'View',
                    'url' => function($data){                        
                        if($data->type == "off_pre" ){
                                return Yii::app()->createUrl("scanReceiptsMaster/otView", array("type"=>$data->type,"ot"=>$data->ot_id));
                            
                        }else{
                                return Yii::app()->createUrl("OfferTransactionMaster/".$data->ot_id);                            

                        }
                       
                    },
                 )
 
            ),     
          
        ),
    ),
    )
);
}
else
{
    $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'offer-transaction-master-grid',
    'dataProvider' => $model->search(),
    'itemsCssClass' => 'table table-striped table-bordered responsive',
    'filter' => $model,
    'columns' => array(
      
        array(
            'name' => 'OfferName',
            'value' => '$data->OfferMaster["offer_name"]',
        ),
        //'redeem_by',
        //'redeem_date',
        array(
            'name'=>'redeem_date',
            'header'=>'Redeem Date Time',
            //'value'=>'date("d M Y",strtotime($data["work_date"]))'
            //'value'=>'Yii::app()->dateFormatter->format("d MMM y",strtotime($data->redeem_date))'
            'value' => function ($model) {
                    $redeemDate= Yii::app()->dateFormatter->format("d MMM y",strtotime($model->redeem_date)). ' ' . $model->transaction_time;
                    /*$usersTimezone = 'America/New_York';
                    $date = new DateTime( $redeemDate, new DateTimeZone($usersTimezone) );
                    return $date->format('d M y H:i:s');*/
                    return $redeemDate;
                },
        ),
        //'total_amount',
        array(
            'name' => 'UserName',
            'value' => '$data->UserMaster["user_name"]',
        ),
        array(
            'header'=>'Amount Payable',
            'value' => function($data){
                        if($data->type == "off_pre"){
                            echo $data->customer_amount;
                        }else{
                            echo $data->merchant_amount;                        
                        }  
                    },
        ),
        array(
            'header'=>'Payment Status',
            'value' => function($data){
                        if($data->type == "off_pre"){
                           return $data->getPaymentStatus($data->customer_payment_status);
                        }else{
                            return $data->getPaymentStatus($data->payment_status);
                        }  
                    },
        ),
        //'type',
        array(
            'header'=>'Type',
            'value' => function($data){
                        if($data->type == "off_pre"){
                            echo "Liquore Store";
                        }else{
                            echo "Bars & Restaurant";                        
                        }  
                    },
        ),
		
        
    ),
    )
);

}
?>
</div>

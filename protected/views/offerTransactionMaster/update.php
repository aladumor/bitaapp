<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */

$this->breadcrumbs=array(
	'Offer Transaction Masters'=>array('index'),
	$model->ot_id=>array('view','id'=>$model->ot_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List OfferTransactionMaster', 'url'=>array('index')),
	array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
	array('label'=>'View OfferTransactionMaster', 'url'=>array('view', 'id'=>$model->ot_id)),
	array('label'=>'Manage OfferTransactionMaster', 'url'=>array('admin')),
);
?>

<h1>Update OfferTransactionMaster <?php echo $model->ot_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
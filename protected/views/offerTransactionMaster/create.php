<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */

$this->breadcrumbs=array(
	'Offer Transaction Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OfferTransactionMaster', 'url'=>array('index')),
	array('label'=>'Manage OfferTransactionMaster', 'url'=>array('admin')),
);
?>

<h1>Create OfferTransactionMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
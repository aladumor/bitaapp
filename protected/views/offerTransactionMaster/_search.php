<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'ot_id'); ?>
		<?php echo $form->textField($model,'ot_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'o_id'); ?>
		<?php echo $form->textField($model,'o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'redeem_by'); ?>
		<?php echo $form->textField($model,'redeem_by',array('size'=>8,'maxlength'=>8)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'redeem_date'); ?>
		<?php echo $form->textField($model,'redeem_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'total_amount'); ?>
		<?php echo $form->textField($model,'total_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'unique_code'); ?>
		<?php echo $form->textField($model,'unique_code',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payer_id'); ?>
		<?php echo $form->textField($model,'payer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'type'); ?>
		<?php echo $form->textField($model,'type',array('size'=>7,'maxlength'=>7)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'merchant_amount'); ?>
		<?php echo $form->textField($model,'merchant_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'brand_partner_amount'); ?>
		<?php echo $form->textField($model,'brand_partner_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_amount'); ?>
		<?php echo $form->textField($model,'customer_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'payment_status'); ?>
		<?php echo $form->textField($model,'payment_status',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transaction_id'); ?>
		<?php echo $form->textField($model,'transaction_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'transaction_time'); ?>
		<?php echo $form->textField($model,'transaction_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
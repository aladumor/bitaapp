<!-- <html>
<head>
<script src="https://www.paypalobjects.com/js/external/dg.js" type="text/javascript"></script>
</head>
<form action="https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay" target="PPDGFrame" class="standard"> -->
<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */

$this->breadcrumbs=array(
	'Offer Transactions'=>array('admin'),
	'Transaction Details',
);

$this->menu=array(
//	array('label'=>'List OfferTransactionMaster', 'url'=>array('index')),
//	array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
//	array('label'=>'Update OfferTransactionMaster', 'url'=>array('update', 'id'=>$model->ot_id)),
//	array('label'=>'Delete OfferTransactionMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ot_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Transactions', 'url'=>array('admin')),
);
?>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>

<script type="text/javascript">
    function confirmbox () 
    {
        <?php if($model->redeem_by=='Merchant')
			$amount=$model->merchant_amount;
		else
			$amount=$model->customer_amount;
        ?>
        var x=confirm("Are you sure you are ready to make the payment? ");
        if ( x== true) 
        {
            x="1"; 
        } else {
            x = "You pressed Cancel!";
        }  
        if(x=="1")  
        {
           location.href = "<?php echo Yii::app()->request->baseUrl?>/index.php/OfferTransactionMaster/checkPaymentMethod?o_id=<?php echo $model->o_id?>&type=<?php echo $model->type?>&ot=<?php echo $model->ot_id?>&redeem_by=<?php echo $model->redeem_by?>&payer_id=<?php echo $model->payer_id ?>&amount=<?php echo $amount ?>";         
        }
    }
</script>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer">
     <a href="admin" class="manage_trans">Manage Transaction</a>
  	<div class="white-bg clearfix">
    <div class="summary panel-heading">&nbsp;</div>
   
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('Class'=>'table table-small responsive'),
	'attributes'=>array(
		 array(
                    'name'=>'Merchant Name',
                    'header'=>'Merchant Name',
                    'value' =>OfferMerchantAssoc::model()->getDetails($model->o_id,"merchant"),
             ), 
          array(
                    'name'=>'Establishment',
                    'header'=>'Establishment',
                    'value' =>OfferMerchantAssoc::model()->getDetails($model->o_id,"establishment"),
             ), 
        'OfferMaster.offer_name',
		'redeem_by',
		array(
            'name'=>'Redeem Date',
            'header'=>'Redeem Date',
            //'value'=>'date("d M Y",strtotime($data["work_date"]))'
            'value'=>Yii::app()->dateFormatter->format("d MMM y",strtotime($model->redeem_date))
        ),
		//'total_amount',
		//'u_id',
        //'UserMaster.user_name',
		//'unique_code',
		//'payer_id',
		//'type',
		'merchant_amount',
        'transaction_id',       
        
        'transaction_time',
        array(
            'header'=>'Type',
            'name'=>'Offer Type',
            'type'=>'raw',
            'value' => "Bars & Restaurant",
        ),		
		/*array(
            'name'=>'User Email',
            'header'=>'User Email',
            'value' =>
             ), */
        array(
            'name'=>'User Email',
            'header'=>'User Email',
            'filter'=>false,
            'type'=>'html',
            'value'=>'CHtml::link(offerTransactionMaster::model()->getEmail($model->unique_code), Yii::app()->baseUrl."/index.php/UserDetailMaster/accountHistoryid=1&code=cust_3445&points=10)',
        ),
        //http://localhost/bevrage/index.php/UserDetailMaster/accountHistory?id=1&code=cust_3445&points=10
		array(
            'name'=>'Payment Status',
            'type'=>'raw',
            'value'=>$model->getPaymentStatus($model->payment_status),            
        ),
        array(            
            'header'=>'Customer Address',
            'name'=>'Customer Address',
            'value' =>UserDetailMaster::model()->getaddress($model->unique_code),
        ), 
		
        
	),
));

if($model->redeem_by=='Merchant')
	$amount=$model->merchant_amount;
else
	$amount=$model->customer_amount
?>



<?php if(isset($error_msg)) {?>
<label><?php echo $error_msg;?></label>
<?php }?>

</div><br />
<?php if($model->payment_status==2 || $model->payment_status==3){?>
<a class="btn btn-primary" onclick="confirm()">Pay Now </a>
<a class="btn btn-danger" href="<?php echo Yii::app()->request->baseUrl.'/index.php/OfferTransactionMaster/delete?id='.$model->ot_id?>">Invalid Transaction</a>

<?php }?>
<a class="btn btn-info" href="<?php echo Yii::app()->request->baseUrl.'/index.php/OfferTransactionMaster/admin'?>">Back</a>
<?php  if(isset($_REQUEST['error'])){?>
<label><?php echo $_REQUEST['error'] ; ?></label>
<?php }?>
</div>

<!--<script>
function myFunction() {
	var x = document.getElementById("paykey").value;
	alert(x);
	window.location="https://www.sandbox.paypal.com/webapps/adaptivepayment/flow/pay?payKey="+x;
}
</script>-->
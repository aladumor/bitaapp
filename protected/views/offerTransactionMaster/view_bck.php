<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */

$this->breadcrumbs=array(
	'Offer Transactions'=>array('admin'),
	'Transaction Details',
);

$this->menu=array(
//	array('label'=>'List OfferTransactionMaster', 'url'=>array('index')),
//	array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
//	array('label'=>'Update OfferTransactionMaster', 'url'=>array('update', 'id'=>$model->ot_id)),
//	array('label'=>'Delete OfferTransactionMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ot_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Transactions', 'url'=>array('admin')),
);
?>



<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('itemCssClass'=>'table table-striped table-bordered responsive'),
	'attributes'=>array(
		//'ot_id',
		//'o_id',
                'OfferMaster.offer_name',
		'redeem_by',
		'redeem_date',
		'total_amount',
		//'u_id',
                'UserMaster.user_name',
		//'unique_code',
		'payer_id',
		//'type',
		'merchant_amount',
		'brand_partner_amount',
		'customer_amount',
		'payment_status',
		'transaction_id',
		'transaction_time',
	),
));
    echo CHtml::submitButton('Verify',array('id'=>'Verify', 'name'=> 'Verify'));
?>

 <?php
/* @var $this OfferTransactionMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Offer Transaction Masters',
);

$this->menu=array(
	array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
	array('label'=>'Manage OfferTransactionMaster', 'url'=>array('admin')),
);
?>

<h1>Offer Transaction Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'offer-transaction-master-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'o_id'); ?>
		<?php echo $form->textField($model,'o_id'); ?>
		<?php echo $form->error($model,'o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'redeem_by'); ?>
		<?php echo $form->textField($model,'redeem_by',array('size'=>8,'maxlength'=>8)); ?>
		<?php echo $form->error($model,'redeem_by'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'redeem_date'); ?>
		<?php echo $form->textField($model,'redeem_date'); ?>
		<?php echo $form->error($model,'redeem_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'total_amount'); ?>
		<?php echo $form->textField($model,'total_amount'); ?>
		<?php echo $form->error($model,'total_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
		<?php echo $form->error($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'unique_code'); ?>
		<?php echo $form->textField($model,'unique_code',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'unique_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payer_id'); ?>
		<?php echo $form->textField($model,'payer_id'); ?>
		<?php echo $form->error($model,'payer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'type'); ?>
		<?php echo $form->textField($model,'type',array('size'=>7,'maxlength'=>7)); ?>
		<?php echo $form->error($model,'type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'merchant_amount'); ?>
		<?php echo $form->textField($model,'merchant_amount'); ?>
		<?php echo $form->error($model,'merchant_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'brand_partner_amount'); ?>
		<?php echo $form->textField($model,'brand_partner_amount'); ?>
		<?php echo $form->error($model,'brand_partner_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_amount'); ?>
		<?php echo $form->textField($model,'customer_amount'); ?>
		<?php echo $form->error($model,'customer_amount'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment_status'); ?>
		<?php echo $form->textField($model,'payment_status',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'payment_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'transaction_id'); ?>
		<?php echo $form->textField($model,'transaction_id'); ?>
		<?php echo $form->error($model,'transaction_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'transaction_time'); ?>
		<?php echo $form->textField($model,'transaction_time'); ?>
		<?php echo $form->error($model,'transaction_time'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
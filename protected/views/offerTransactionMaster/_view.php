<?php
/* @var $this OfferTransactionMasterController */
/* @var $data OfferTransactionMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('ot_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->ot_id), array('view', 'id'=>$data->ot_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_id')); ?>:</b>
	<?php echo CHtml::encode($data->o_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('redeem_by')); ?>:</b>
	<?php echo CHtml::encode($data->redeem_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('redeem_date')); ?>:</b>
	<?php echo CHtml::encode($data->redeem_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('total_amount')); ?>:</b>
	<?php echo CHtml::encode($data->total_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::encode($data->u_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unique_code')); ?>:</b>
	<?php echo CHtml::encode($data->unique_code); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('payer_id')); ?>:</b>
	<?php echo CHtml::encode($data->payer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('type')); ?>:</b>
	<?php echo CHtml::encode($data->type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('merchant_amount')); ?>:</b>
	<?php echo CHtml::encode($data->merchant_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('brand_partner_amount')); ?>:</b>
	<?php echo CHtml::encode($data->brand_partner_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_amount')); ?>:</b>
	<?php echo CHtml::encode($data->customer_amount); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_status')); ?>:</b>
	<?php echo CHtml::encode($data->payment_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_id')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transaction_time')); ?>:</b>
	<?php echo CHtml::encode($data->transaction_time); ?>
	<br />

	*/ ?>

</div>
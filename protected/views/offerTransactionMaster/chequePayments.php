<div class="check_details">    <?php
/* @var $this OfferTransactionMasterController */
/* @var $model OfferTransactionMaster */

$this->breadcrumbs = array(
    'Offer Transactions ' => array('admin'),
    'Payments',
);

$this->menu = array(
    array('label' => 'Manage Transactions', 'url' => array('admin')),
        //array('label'=>'Create OfferTransactionMaster', 'url'=>array('create')),
);
?>
<script type="text/javascript">
    $(document).ready(function(){
      var type=$("input[name=paytype]:checked", '#chequedetails').val();
      ajaxOrderhistory('2');
      if(type==2)  
      {
        $('#chequedetails').validate({
        rules: {
                    'chq_no': {
                        required: true,                        
                    },
                    'chq_issue_date' :
                    {
                        required: true,
                    },
                    'paytype' :
                    {
                        required: true,
                    },
                    'startdate' :
                    {
                        required: true,
                    },
                    'enddate' :
                    {
                        required: true,
                    },

                },
        messages: {
                    'chq_no': {
                        required: "Please enter check number.",                        
                    },
                    'chq_issue_date' :
                    {
                        required: "Please enter check issue date.",
                    },
                    'paytype' :
                    {
                        required: "Please select payment interval tye.",
                    },
                    'startdate' :
                    {
                        required: "Please enter start date.",
                    },
                    'enddate' :
                    {
                        required: "Please enter end date.",
                    },                  
                },
            errorPlacement: function(error, element) {
                $("#errorplace").show();
                error.appendTo($("#errorplace"));
            },
            success: function(label) { 
                if($("#errorplace").text() == ''){
                    $("#errorplace").hide();
                   
                }
                $($("#errorplace").children()).each(function(){
                    if($(this).text()==''){
                        $(this).remove();
                    }
                })
            }
        
        });
      }
      else
      {
          $('#chequedetails').validate({
          rules: {
                      'chq_no': {
                          required: true,                        
                      },
                      'chq_issue_date' :
                      {
                          required: true,
                      },
                      'paytype' :
                      {
                          required: true,
                      },                    
                  },
          messages: {
                      'chq_no': {
                          required: "Please enter check number.",                        
                      },
                      'chq_issue_date' :
                      {
                          required: "Please enter check issue date.",
                      },
                      'paytype' :
                      {
                          required: "Please select payment interval tye.",
                      },
                      

                  },
            errorPlacement: function(error, element) {
                $("#errorplace").show();
                error.appendTo($("#errorplace"));
            },
            success: function(label) { 
                if($("#errorplace").text() == ''){
                    $("#errorplace").hide();
                   
                }
                $($("#errorplace").children()).each(function(){
                    if($(this).text()==''){
                        $(this).remove();
                    }
                })
            }
        
        });

      } 
   
});

</script>
<script>
  $(function() {
    $( "#startdate" ).datepicker();
    $( "#enddate" ).datepicker();
    $( "#chq_issue_date" ).datepicker();
  });
  
  function showdate(arg) 
  {
  	if(arg=='show')
  		$('#datediv').show();
  	else
  	{
  		//alert('hi');
  		$('#datediv').hide();
  		
  	}	
  		
  }
  function ajaxOrderhistory (status) 
  {
        var val=$("#code" ).val();       
       
        var urlstr="<?php echo Yii::app()->createUrl('offerTransactionMaster/ajaxPendingRecords?')?>";
         $.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {code:val,status:status},
          success: function(msg) { 
                if(msg!='')  
                {
                    $('#records').show();
                    $('#records').html('');
                    $('#records').html(msg);
                }
                else
                {                    
                    $('#records').hide();
                    alert('No Pending Payouts');
                }
                
          },
          error: function(msg) {
              alert('Something went wrong.Try Again!')
          }
        });
  }

  function payCheque(arg)
  {
      if($('#chequedetails').valid())
      {
         
          var st_date='';
          var end_date='';
          if(arg=='date')
          {
            st_date=$('#startdate').val();
            end_date=$('#enddate').val();
            chq_no= $('#chq_no').val(); 
            chq_issue_date= $('#chq_issue_date').val();   
          }
          var type=$("input[name=paytype]:checked", '#chequedetails').val()
        if(type==2)
        {
          //alert(type);
          location.href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/offerTransactionMaster/payCheque?payfilter=date&st_date="+st_date+"&end_date="+end_date+"&u_id=<?php echo $uid ?>&code=<?php echo $code ?>&chq_no="+chq_no+"&chq_issue_date="+chq_issue_date;
        }
        else
        {    
          //alert(type);  
          location.href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/offerTransactionMaster/payCheque?payfilter=all&u_id=<?php echo $uid ?>&code=<?php echo $code ?>&chq_no="+chq_no+"&chq_issue_date="+chq_issue_date;
        }
      }
  	  
  	
  }
  function paytransaction(transcid)
  {
      var x=confirm("Are you sure you are ready to make the payment? ");
      if ( x== true) 
      {
          x="1"; 
      } else {
          x = "You pressed Cancel!";
      }  
      if(x=="1")  
      {
        if($('#chequedetails').valid())
        {
          chq_no= $('#chq_no').val(); 
          chq_issue_date= $('#chq_issue_date').val();
          location.href="<?php echo Yii::app()->request->baseUrl; ?>/index.php/offerTransactionMaster/payCheque?ot_id="+transcid+"&payfilter=single&u_id=<?php echo $uid ?>&code=<?php echo $code ?>&chq_no="+chq_no+"&chq_issue_date="+chq_issue_date;
        }
      }
  }
  </script>
<form name="chequedetails" id="chequedetails">
<div id="errorplace" class="errorSummary" style="display:none;"></div>
<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
	<div class="summary panel-heading">&nbsp; </div>
    <table id="yw0" class="table table-striped table-bordered responsive footable">
    	<tr class="odd">
        	<th>Customer Name:</th>
            <td><?php echo $customerName?></td>
        </tr>
        <tr>
        	<th>Customer Address:(For sending check )</th>
            <td><?php echo $address?></td>
        </tr>
        <tr>
        	<th>Last Payout Date:</th>
            <td><?php echo empty($lastDate)?"Not Available" : date("m/d/Y",strtotime($lastDate)) ?></td>
        </tr>
        <tr>
        	<th>Last Payout Amount:</th>
            <td><?php echo empty($lastPaid)?"Not Available" :$lastPaid ?></td>
        </tr>
        <tr>
        	<th>Total Outstanding Amount:</th>
            <td><?php echo empty($outstandingAmt)?"No Outstanding": $outstandingAmt ;?></td>
        </tr>
    </table>
</div>  
 
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl"> 
<?php if(!empty($outstandingAmt )){?>
<!-- <label>View Transactions :</label>
<input type="radio" name="transaction" value="1" onclick="ajaxOrderhistory('2')" >View Pending Transactions<br>
<input type="radio" name="transaction" value="2"  onclick="ajaxOrderhistory('3')">View Cancel Transactions -->
<!-- <a class="btn btn-primary" onclick="ajaxOrderhistory('2')">View Pending Transactions</a>
<a class="btn btn-primary" onclick="ajaxOrderhistory('3')">View Cancel Transactions</a> -->
<div id="records"></div>
<br>
<div id="transactionGrid"></div>
<br>
<!-- Pay all o/s -->
<!-- <a class="btn btn-primary" onclick="showdate('hide')">Pay All O/s</a> -->
<input type="hidden" id="uid" value="<?php echo $uid ?>"/>
<input type="hidden" id="code" value="<?php echo $code ?>"/>

<!-- END -->
<!-- Payment -->

<!-- <label>Select Payment option :</label>
<input type="radio" name="paytype" value="1" onclick="showdate('hide')" >Pay All O/s<br>
<input type="radio" name="paytype" value="2" onclick="showdate('show')" checked="checked">Pay By Date -->

<br><br />
<div id="datediv" class="clearfix" >
<!-- 
	<div class="col-md-3">
		<label>Start Date:</label>
		<input id="startdate" name="startdate" class="form-control"></input>
	</div>
  <div class="col-md-3">
		<label>End Date:</label>
		<input id="enddate" name="enddate" class="form-control"></input>
	</div> -->
<!-- END -->
 
</div>

<!-- CHEQUE DETAILS -->

<div>
  <label>Check Number:</label>
  <input type="text" id="chq_no" class="form-control" value="" name="chq_no"/>
</div>
<div>
  <label>Check Issue Date:</label>
  <input id="chq_issue_date" name="chq_issue_date" class="form-control"></input>
</div>
<!-- END -->
</form>
<!-- <div class="col-md-3">
    <input class="btn btn-success"  id="pay" name="Pay" value="Pay" type="button"  onclick="payCheque('date')">
</div> -->

<?php }?>
<div class="col-md-3">
  <a class="btn btn-success" href="admin" >Back</a>
</div>
</div>
</div>
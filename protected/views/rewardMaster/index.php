<?php
/* @var $this RewardMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Reward Masters',
);

$this->menu=array(
	array('label'=>'Create RewardMaster', 'url'=>array('create')),
	array('label'=>'Manage RewardMaster', 'url'=>array('admin')),
);
?>

<h1>Reward Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

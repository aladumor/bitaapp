<?php
/* @var $this RewardMasterController */
/* @var $model RewardMaster */

$this->breadcrumbs=array(
	'Rewards'=>array('admin'),
	//$model->rm_id=>array('view','id'=>$model->rm_id),
	'Update',
);

$this->menu=array(
	/*array('label'=>'List RewardMaster', 'url'=>array('index')),
	array('label'=>'Create RewardMaster', 'url'=>array('create')),
	array('label'=>'View RewardMaster', 'url'=>array('view', 'id'=>$model->rm_id)),*/
	array('label'=>'Manage Rewards', 'url'=>array('admin')),
);
?>

<!-- <h1>Update RewardMaster <?php //echo $model->rm_id; ?></h1> -->

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
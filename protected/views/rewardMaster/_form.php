<?php
/* @var $this RewardMasterController */
/* @var $model RewardMaster */
/* @var $form CActiveForm */
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'reward-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'action_name'); ?>
		<?php echo $form->textField($model,'action_name',array('size'=>60,'maxlength'=>500,'readonly'=>true,'class'=>input_box)); ?>
		<?php echo $form->error($model,'action_name'); ?>
	</div>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'points'); ?>
		<?php echo $form->textField($model,'points',array('class'=>input_box)); ?>
	</div>

	<!-- <div class="row">
		<?php echo $form->labelEx($model,'is_active'); ?>
		<?php echo $form->textField($model,'is_active'); ?>
		<?php echo $form->error($model,'is_active'); ?>
	</div> -->

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
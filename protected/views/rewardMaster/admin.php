<?php
/* @var $this RewardMasterController */
/* @var $model RewardMaster */

$this->breadcrumbs=array(
	'Reward'=>array('admin'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List RewardMaster', 'url'=>array('index')),
	//array('label'=>'Create RewardMaster', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#reward-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
	</style>
<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p> -->


<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'reward-master-grid',
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
		//'rm_id',
		'action_name',
		'points',
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
		array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-search"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        ),
	),
)); ?>

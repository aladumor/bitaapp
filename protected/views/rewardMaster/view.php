<?php
/* @var $this RewardMasterController */
/* @var $model RewardMaster */

$this->breadcrumbs=array(
	'Reward Masters'=>array('index'),
	$model->rm_id,
);

$this->menu=array(
	array('label'=>'List RewardMaster', 'url'=>array('index')),
	array('label'=>'Create RewardMaster', 'url'=>array('create')),
	array('label'=>'Update RewardMaster', 'url'=>array('update', 'id'=>$model->rm_id)),
	array('label'=>'Delete RewardMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->rm_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage RewardMaster', 'url'=>array('admin')),
);
?>

<h1>View RewardMaster #<?php echo $model->rm_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'rm_id',
		'action_name',
		'points',
		'is_active',
	),
)); ?>

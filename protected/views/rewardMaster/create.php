<?php
/* @var $this RewardMasterController */
/* @var $model RewardMaster */

$this->breadcrumbs=array(
	'Reward'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List RewardMaster', 'url'=>array('index')),
	array('label'=>'Manage Rewards', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this StatesController */
/* @var $model States */

$this->breadcrumbs=array(
	'States'=>array('admin'),
	//$model->state_id,
        'Terms And Conditions Details',
);

$this->menu=array(
//	array('label'=>'List States', 'url'=>array('index')),
//	array('label'=>'Create States', 'url'=>array('create')),
	array('label'=>'Update States Details', 'url'=>array('update', 'id'=>$model->state_id)),
//	array('label'=>'Delete States', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->state_id),'confirm'=>'Are you sure you want to delete this item?')),
//	array('label'=>'Manage States', 'url'=>array('admin')),
);
?>
<style>
.ac_heading{
	display:none !important;
	
}

.content{
display:none !important;
}
</style>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl manage_trans_div">

	<a class="manage_trans" href="update/<?php echo $model->state_id?>"><i class="fa fa-plus"></i>&nbsp;Edit State Details</a>

<!-- <div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl manage_trans_div">
 -->
<div class="summary panel-heading"></div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
                'state_id',
		'state',
		'state_code',	
               array(
                    'label'=>'termsandconditions',
                    'value'=>strip_tags($model->termsandconditions),
                    'name'=>'m_fullname'
                ),

	),
)); ?>
</div>

<?php
/* @var $this StatesController */
/* @var $model States */

$this->breadcrumbs=array(
	'States'=>array('admin'),
	'Manage Terms And Conditions',
);

$this->menu=array(
	//array('label'=>'List States', 'url'=>array('index')),
	array('label'=>'Create Terms And Conditions', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#states-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'states-grid',
	'dataProvider'=>$model->search(),
        'itemsCssClass' => 'table table-striped table-bordered responsive adjust1',
	'filter'=>$model,
	'columns'=>array(
		'state',
		'state_code',
		'termsandconditions',
		'state_id',
//		array(
//			'class'=>'CButtonColumn',
//		),
                array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
	),
)); ?>

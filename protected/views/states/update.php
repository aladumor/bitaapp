<?php
/* @var $this StatesController */
/* @var $model States */

$this->breadcrumbs=array(
	'States'=>array('admin'),
//	$model->state_id=>array('view','id'=>$model->state_id),
	'Update State',
);

$this->menu=array(
//	array('label'=>'List States', 'url'=>array('index')),
//	array('label'=>'Create States', 'url'=>array('create')),
//	array('label'=>'View States', 'url'=>array('view', 'id'=>$model->state_id)),
	array('label'=>'Manage States Details', 'url'=>array('admin')),
);
?>

<h1>Update States <?php echo $model->state_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
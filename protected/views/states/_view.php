<?php
/* @var $this StatesController */
/* @var $data States */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->state_id), array('view', 'id'=>$data->state_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state')); ?>:</b>
	<?php echo CHtml::encode($data->state); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('state_code')); ?>:</b>
	<?php echo CHtml::encode($data->state_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('termsandconditions')); ?>:</b>
	<?php echo CHtml::encode($data->termsandconditions); ?>
	<br />


</div>
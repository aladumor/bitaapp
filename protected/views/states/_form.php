<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true).'/tinymce/js/tinymce/tinymce.min.js';?>"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image|sizeselect | fontselect |  fontsizeselect"
});
</script>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'states-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<div class="row">
            <div class="col-md-4">
		<?php echo $form->labelEx($model,'state'); ?>
		<?php echo $form->textField($model,'state',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'state'); ?>
            </div>
        
            <div class="col-md-4">
		<?php echo $form->labelEx($model,'state_code'); ?>
		<?php echo $form->textField($model,'state_code',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'state_code'); ?>
            </div>
            </div>
           <div class="row">
		<?php echo $form->labelEx($model,'termsandconditions'); ?>
		<?php echo $form->textArea($model,'termsandconditions',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'termsandconditions'); ?>
            </div>
        
	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
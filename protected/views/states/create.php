<?php
/* @var $this StatesController */
/* @var $model States */

$this->breadcrumbs=array(
	'Manage Terms And Conditions'=>array('admin'),
	'Create Terms And Conditions',
);

$this->menu=array(
	//array('label'=>'List States', 'url'=>array('index')),
	array('label'=>'Manage Terms And Conditions', 'url'=>array('admin')),
);
?>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php
/* @var $this MerchandiseMasterController */
/* @var $data MerchandiseMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->m_id), array('view', 'id'=>$data->m_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('point_required')); ?>:</b>
	<?php echo CHtml::encode($data->point_required); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('bp_id')); ?>:</b>
	<?php echo CHtml::encode($data->bp_id); ?>
	<br />


</div>
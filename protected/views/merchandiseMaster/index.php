<?php
/* @var $this MerchandiseMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Merchandise Masters',
);

$this->menu=array(
	array('label'=>'Create MerchandiseMaster', 'url'=>array('create')),
	array('label'=>'Manage MerchandiseMaster', 'url'=>array('admin')),
);
?>

<h1>Merchandise Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

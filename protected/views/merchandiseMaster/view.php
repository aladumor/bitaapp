<?php
/* @var $this MerchandiseMasterController */
/* @var $model MerchandiseMaster */

$this->breadcrumbs=array(
	'Merchandise Masters'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List MerchandiseMaster', 'url'=>array('index')),
	array('label'=>'Create MerchandiseMaster', 'url'=>array('create')),
	array('label'=>'Update MerchandiseMaster', 'url'=>array('update', 'id'=>$model->m_id)),
	array('label'=>'Delete MerchandiseMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->m_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MerchandiseMaster', 'url'=>array('admin')),
);
?>

<h1>View MerchandiseMaster #<?php echo $model->m_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'm_id',
		'name',
		'point_required',
		'is_active',
		'bp_id',
	),
)); ?>

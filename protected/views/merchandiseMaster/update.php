<?php
/* @var $this MerchandiseMasterController */
/* @var $model MerchandiseMaster */

$this->breadcrumbs=array(
	'Merchandise'=>array('admin'),
	//$model->name=>array('view','id'=>$model->m_id),
	'Update',
);

$this->menu=array(
	//array('label'=>'List MerchandiseMaster', 'url'=>array('index')),
	array('label'=>'Add Merchandise', 'url'=>array('create')),
	//array('label'=>'View MerchandiseMaster', 'url'=>array('view', 'id'=>$model->m_id)),
	array('label'=>'Manage Merchandise', 'url'=>array('admin')),
);
?>

<!-- <h1>Update MerchandiseMaster <?php echo $model->m_id; ?></h1> -->

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
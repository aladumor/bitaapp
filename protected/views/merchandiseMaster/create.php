<?php
/* @var $this MerchandiseMasterController */
/* @var $model MerchandiseMaster */

$this->breadcrumbs=array(
	'Merchandise'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List MerchandiseMaster', 'url'=>array('index')),
	array('label'=>'Manage Merchandise', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
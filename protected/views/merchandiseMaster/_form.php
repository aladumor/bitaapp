<?php
/* @var $this MerchandiseMasterController */
/* @var $model MerchandiseMaster */
/* @var $form CActiveForm */
?>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'merchandise-master-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100,'class'=>input_box)); ?>
		
	</div>

	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'point_required'); ?>
		<?php echo $form->textField($model,'point_required',array('class'=>input_box)); ?>
		
	</div>
	<div class="form-group row col-md-12">
		<?php echo $form->labelEx($model,'image'); ?>
		<?php echo $form->fileField($model,'image'); ?>
		
	</div>

	

	<div class="col-md-12 buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
<?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */
/* @var $form CActiveForm */
?>
<script type="text/javascript"
     src="http://maps.google.com/maps/api/js?sensor=set_to_true_or_false">
</script>
<script type="text/javascript">
	function getlatLong () 
	{
		var geocoder = new google.maps.Geocoder();
		var  street = document.getElementById("MerchantMaster_m_address").value;
		var city = document.getElementById("MerchantMaster_m_city").value;
		var address = document.getElementById("MerchantMaster_m_address").value+" "+city;
		geocoder.geocode( { 'address': address}, function(results, status) {
		  if (status == google.maps.GeocoderStatus.OK)
		  {
		  		$('#MerchantMaster_m_lat').val(results[0].geometry.location.lat());
		  		$('#MerchantMaster_m_long').val(results[0].geometry.location.lng());
		      
		  }
		});
	}
</script>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'merchant-master-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'m_name'); ?>
		<?php echo $form->textField($model,'m_name',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'m_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'establishment_name'); ?>
		<?php echo $form->textField($model,'establishment_name',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'establishment_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'m_address'); ?>
		<?php echo $form->textArea($model,'m_address',array('rows'=>6, 'cols'=>50,'onchange'=>'getlatLong()')); ?>
		<?php echo $form->error($model,'m_address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'m_city'); ?>
		 <div class="form-group row col-md-12">
          <?php
            $selectedoption = '';
            if (!$model->isNewRecord) {
                $city = OfferLocationAssoc::model()->findAllByAttributes(array('o_id' => $model->o_id));
                foreach ($city as $value) {
                    $selectedoption[$value->city_id] = array('selected' => 'selected');
                }
            }
            $criteria = new CDbCriteria;
            $static = array(
                '' => '-- Select Cities --',
            );
            echo CHtml::activeDropDownList($model, 'm_city', $static + CHtml::listData(BeverageCities::model()->findAll($criteria), 'bc_id', 'city_name'), array('class' => 'required form-control mb15 width100p', 'options' => $selectedoption)
            );
            ?>	
		<?php echo $form->error($model,'m_city'); ?>
	</div>
	
		<?php echo $form->hiddenField($model,'m_lat',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->hiddenField($model,'m_long',array('size'=>20,'maxlength'=>20)); ?>	
	</div> 

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
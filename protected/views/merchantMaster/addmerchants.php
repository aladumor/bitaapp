
<?php $this->breadcrumbs=array(
	'Merchants'=>array('admin'),
	'Add Merchants',
);

$this->menu=array(
	array('label'=>'Manage Merchants', 'url'=>array('admin')),
	array('label'=>'Create Merchants', 'url'=>array('create')),
);
?>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">
<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'merchant-master-form',
	'enableAjaxValidation'=>false,
	'htmlOptions' => array('enctype' => 'multipart/form-data'),
)); ?>


	<div id="errorplace" class="errorSummary" style="display:none;"></div>
	<p class="note">Fields with <span class="required">*</span> are required.</p>
<div class="clearfix form-input">
		
	<div class="form-group row col-md-12">
		<label> Uplaod CSV File</label>
		<?php echo $form->fileField($model,'m_name',array('accept'=>'.csv')); ?>
		<?php echo $form->error($model,'m_name'); ?>
	</div>
	<div class="buttons">
	
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
   	
	</div>
</div>

<?php $this->endWidget(); ?>
<?php if(isset($count)){?>
<div>
	<h4>Summary</h4>
    <div class="row clearfix">
		<div class="col-md-3">
        	<div class="report-box">
				<label>Total Scanned Records :</label>
				<h2><?php echo $total; ?></h2>
            </div>
		</div>

        <div class="col-md-3">
        	<div class="report-box">
        		<label>Total Failed Insert :</label>
        		<h2><?php echo $total-$count; ?></h2>
        	</div>
        </div>
    
        <div class="col-md-3">
        	<div class="report-box">
        		<label>Total Successfull Insert :</label>
        		<h2><?php  echo $count; ?></h2>
        	</div>
        </div>
        <div class="col-md-3"></div>
	</div><br />
	<div>
	<?php if(isset($fail)){

	?>
	<div class="summary panel-heading">Error Report</div>
	
	<table class="table table-striped table-bordered responsive rpt-tbl">
	<?php foreach ($fail as $key => $value) 
	{?>
			
            	<tr>
					<td width="20%"><label><?php echo $key?></label></td>
					<td width="30%"><label>Error : <?php echo $value?></label></td>	
					<?php if( $value!='Merchant Already Exists.'){?>				
						<td width="10%"><a href="create" class="btn btn-primary">Add Manually</a></td>
					<?php }?>
                </tr>
			
            	
			
	<?php } ?>
	</table>
	<?php }?>
	</div>
</div>
<?php }?>
</div>
</div>


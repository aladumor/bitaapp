<?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */

$this->breadcrumbs=array(
	'Merchant'=>array('admin'),
	'Create',
);

$this->menu=array(
	//array('label'=>'List MerchantMaster', 'url'=>array('index')),
	array('label'=>'Manage Merchant', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<div class="manage_icons"><?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); 
$this->breadcrumbs=array(
	'Merchant'=>array('admin'),
	'Manage',
);

$this->menu=array(
	//array('label'=>'List MerchantMaster', 'url'=>array('index')),
	array('label'=>'Create Merchant', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#merchant-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
</style>
	<script>
$('#merchant-master-grid').removeClass('.dataTables_wrapper');
    </script>
<?php

	$pageSizeDropDown = CHtml::dropDownList(
		'pageSize',
		$pageSize,
		array( 10 => 10, 25 => 25, 50 => 50, 100 => 100 ),
		array(
			'class'    => 'change-pagesize',
			'onchange'=>"$.fn.yiiGridView.update('merchant-master-grid',{ data:{pageSize: $(this).val() }})",
		)
	);
	?>	
<div class="search-form" style="display:none">

<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="dataTables_wrapper">
<div class="page-size-wrap col-md-3">
	<span>Display by:</span><?= $pageSizeDropDown; ?>
</div>
<div class="col-md-9">
	<a class="btn btn-green pull-right offer-btn" href="create"><i class="fa fa-plus"></i>&nbsp;Create Merchant</a>
	<a class="btn btn-green pull-right offer-btn" href="addmerchants"><i class="fa fa-plus"></i>&nbsp;Upload Merchant CSV</a>
</div>
    
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'merchant-master-grid',
	'template'=>'{pager}{items}{pager}' ,
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'filter'=>$model,
	'columns'=>array(
		//'m_id',
		'm_name',
		'establishment_name',
		//'phone',
		'm_street',
		array(
            'name' => 'city_search',
            'header'=>'City',
            'value' => '$data->BevrageCities["city_name"]',
        ),
        'm_state',
		/*array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),*/

		/*'m_street',
		'm_zipcode',
		'm_city',
		'm_state',
		
		
		'm_lat',
		'm_long',
		'is_active',
		*/
		array(
            'class'=>'CButtonColumn',
            'template'=>'{update}{delete}',
            /*'header'=>CHtml::dropDownList('pageSize',$pageSize,array(20=>20,50=>50,100=>100),array(
		        // change 'user-grid' to the actual id of your grid!!
		        'onchange'=>"$.fn.yiiGridView.update('merchant-master-grid',{ data:{pageSize: $(this).val() }})",
		    )),*/
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
	),
)); ?>
</div></div>
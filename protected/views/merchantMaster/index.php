<?php
/* @var $this MerchantMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Merchant Masters',
);

$this->menu=array(
	array('label'=>'Create MerchantMaster', 'url'=>array('create')),
	array('label'=>'Manage MerchantMaster', 'url'=>array('admin')),
);
?>

<h1>Merchant Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

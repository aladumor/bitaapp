<?php
/* @var $this MerchantMasterController */
/* @var $data MerchantMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->m_id), array('view', 'id'=>$data->m_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_name')); ?>:</b>
	<?php echo CHtml::encode($data->m_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('establishment_name')); ?>:</b>
	<?php echo CHtml::encode($data->establishment_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_street')); ?>:</b>
	<?php echo CHtml::encode($data->m_street); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_zipcode')); ?>:</b>
	<?php echo CHtml::encode($data->m_zipcode); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_city')); ?>:</b>
	<?php echo CHtml::encode($data->m_city); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('phone')); ?>:</b>
	<?php echo CHtml::encode($data->phone); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('m_lat')); ?>:</b>
	<?php echo CHtml::encode($data->m_lat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('m_long')); ?>:</b>
	<?php echo CHtml::encode($data->m_long); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	*/ ?>

</div>
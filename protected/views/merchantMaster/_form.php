<?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */
/* @var $form CActiveForm */
?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#merchant-master-form').validate({
        rules: {
                    'MerchantMaster[m_name]': {
                        required: true,                       
                    },
                    'MerchantMaster[establishment_name]' :
                    {
                        required: true,
                    },
                    'MerchantMaster[merchant_type]' :
                    {
                        required: true,
                    },
                    /*'MerchantMaster[phone]':
                    {
                        required: true,
                        //digits: true,
                    },*/
                    'MerchantMaster[m_street]':
                    {
                        required: true,
                    },
                    'MerchantMaster[m_city]':
                    {
                        required: true,
                    },
                    'MerchantMaster[m_state]':
                    {
                        required: true,
                        
                    },
                    'MerchantMaster[m_zipcode]': 
                    {
                        required:true,
                    }
                   
                },
        messages: {
                    'MerchantMaster[m_name]': {
                        required: "Please Enter Merchant Name.",                       
                    },
                    'MerchantMaster[establishment_name]' :
                    {
                        required:"Please Enter Establishment Name",
                    },
                    'MerchantMaster[merchant_type]' :
                    {
                        required: "Please Select Establishment Category.",
                    },
                   /* 'MerchantMaster[phone]':
                    {
                        required: "Please Enter Phone Number.",
                        //igits: "Only Digits allowed for Phone Number.",
                    },*/
                    'MerchantMaster[m_street]':
                    {
                        required: "Please Enter Block no / Street Name ",
                    },
                    'MerchantMaster[m_city]':
                    {
                        required: "Please Select City.",
                    },
                    'MerchantMaster[m_state]':
                    {
                        required: "Please Select State.",
                    },
                    'MerchantMaster[m_zipcode]': 
                    {
                        required:"Please Enter Zipcode.",                        
                    }                  
                },
            errorPlacement: function(error, element) {
                $("#errorplace").show();
                error.appendTo($("#errorplace"));
            },
            success: function(label) { 
                if($("#errorplace").text() == ''){
                    $("#errorplace").hide();
                }
                $($("#errorplace").children()).each(function(){
                    if($(this).text()==''){
                        $(this).remove();
                    }
                })
            }
        
        });

});
</script>
<script type="text/javascript">
	function save(arg) 
	{
		if(arg=='city')
			$('#city').val($("#MerchantMaster_m_city option:selected").text());
		else
			$('#state').val($("#MerchantMaster_m_state option:selected").text());
	}
</script>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'merchant-master-form',
	'enableAjaxValidation'=>false,
)); ?>
	<div id="errorplace" class="errorSummary" style="display:none;"></div>
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	
    <div class="clearfix form-input">
		<div class="col-md-6">
    	
    	<div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'m_name'); ?>
			<?php echo $form->textField($model,'m_name',array('size'=>50,'maxlength'=>50,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'m_name'); ?>
		</div>
	
    	<div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'establishment_name'); ?>
			<?php echo $form->textField($model,'establishment_name',array('size'=>50,'maxlength'=>50,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'establishment_name'); ?>
		</div>
        
        <div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'merchant_type'); ?>
			<?php echo $form->dropDownList($model,'merchant_type',array(''=>'-- Select Type--',"1"=>"Bars & Restaurant","2"=>"Liquor Store")); ?> 
			<?php echo $form->error($model,'merchant_type'); ?>
    	</div>

    	   <div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'phone'); ?>
			<?php
                        $this->widget('CMaskedTextField', array(
                        'model' => $model,
                        'attribute' => 'phone',
                        'mask' => '(999) 999-9999',
                        'htmlOptions' => array('size' => 10,'class'=>'form-control mb15 width100p')
                        ));
                    ?>
			
			<?php echo $form->error($model,'phone'); ?>
        </div>

       
        
	</div>
    	<div class="col-md-6">
    	 
		<div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'m_street'); ?>
			<?php echo $form->textField($model,'m_street',array('size'=>50,'maxlength'=>50,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'m_street'); ?>
		</div>
		
       

		<div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'m_city'); ?>

          <?php
            $selectedoption = '';
            if (!$model->isNewRecord) {
                $city = MerchantMaster::model()->findAllByAttributes(array('m_city' => $model->m_city));
                foreach ($city as $value) {
                    $selectedoption[$value->m_city] = array('selected' => 'selected');
                }
            }
            $criteria = new CDbCriteria;
            $criteria->order='city_name ASC';
            $static = array(
                '' => '-- Select Cities --',
            );
            echo CHtml::activeDropDownList($model, 'm_city', $static + CHtml::listData(BevrageCities::model()->findAll($criteria), 'bc_id', 'city_name'), array('class' => 'required form-control mb15 width100p', 'onclick'=>'save("city")','options' => $selectedoption)
            );
            ?>	
		<?php echo $form->error($model,'m_city'); ?>
		</div>
        
		<div class="form-group row col-md-12">
	
		<label>State</label>
		<?php
            $selectedoption = '';
            if (!$model->isNewRecord) {
                $state = MerchantMaster::model()->findAllByAttributes(array('m_state' => $model->m_state));
                foreach ($state as $value) {
                    $selectedoption[$value->m_state] = array('selected' => 'selected');
                }
            }
            $criteria = new CDbCriteria; 
            $criteria->select= 'distinct state_code,bc_id'  ; 
            $criteria->order='state_code ASC';
            $static = array(
                '' => '-- Select State --',
            );
           // print_r(CHtml::listData(BevrageCities::model()->findAll(array('distinct' => true)), 'state_code', 'state_code'));die();

            echo CHtml::activeDropDownList($model, 'm_state', $static + CHtml::listData(BevrageCities::model()->findAll(array('distinct' => true)), 'state_code', 'state_code'), array('class' => 'required form-control mb15 width100p', 'onclick'=>'save("state")','options' => $selectedoption)
            );
            ?>	
		<?php echo $form->error($model,'m_state'); ?>		
		</div>
        
         <div class="form-group row col-md-12">
			<?php echo $form->labelEx($model,'m_zipcode'); ?>
			<?php echo $form->textField($model,'m_zipcode',array('size'=>50,'maxlength'=>50,'class'=>'form-control mb15 width100p')); ?>
			<?php echo $form->error($model,'m_zipcode'); ?>
        </div>
	</div>
    </div>
	<input type="hidden" id="state" name="statename" ></input>	
	<input type="hidden" id="city" name="cityname"></input>

	<div class="row buttons ml10">
    	
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
       	
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
</div>
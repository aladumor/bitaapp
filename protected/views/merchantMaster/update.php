<?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */

$this->breadcrumbs=array(
	'Merchant'=>array('admin'),
	'Update',
);

$this->menu=array(
	//array('label'=>'List MerchantMaster', 'url'=>array('index')),
	array('label'=>'Create Merchant', 'url'=>array('create')),
	//array('label'=>'View MerchantMaster', 'url'=>array('view', 'id'=>$model->m_id)),
	array('label'=>'Manage Merchant', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
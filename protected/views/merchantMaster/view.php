<?php
/* @var $this MerchantMasterController */
/* @var $model MerchantMaster */

$this->breadcrumbs=array(
	'Manage Merchants'=>array('admin'),
	//$model->m_id,
);

/*$this->menu=array(
	array('label'=>'List MerchantMaster', 'url'=>array('index')),
	array('label'=>'Create MerchantMaster', 'url'=>array('create')),
	array('label'=>'Update MerchantMaster', 'url'=>array('update', 'id'=>$model->m_id)),
	array('label'=>'Delete MerchantMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->m_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MerchantMaster', 'url'=>array('admin')),
);*/
?>

<h1>View MerchantMaster #<?php echo $model->m_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		//'m_id',
		'm_name',
		'establishment_name',
		'm_street',
		'm_zipcode',
		'm_city',
		'phone',
		'm_lat',
		'm_long',
		'is_active',
	),
)); ?>

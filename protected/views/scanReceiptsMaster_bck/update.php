<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */

$this->breadcrumbs=array(
	'Scan Receipts Masters'=>array('index'),
	$model->srm_id=>array('view','id'=>$model->srm_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ScanReceiptsMaster', 'url'=>array('index')),
	array('label'=>'Create ScanReceiptsMaster', 'url'=>array('create')),
	array('label'=>'View ScanReceiptsMaster', 'url'=>array('view', 'id'=>$model->srm_id)),
	array('label'=>'Manage ScanReceiptsMaster', 'url'=>array('admin')),
);
?>

<h1>Update ScanReceiptsMaster <?php echo $model->srm_id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
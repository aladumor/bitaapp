<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'srm_id'); ?>
		<?php echo $form->textField($model,'srm_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'o_id'); ?>
		<?php echo $form->textField($model,'o_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'u_id'); ?>
		<?php echo $form->textField($model,'u_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date'); ?>
		<?php echo $form->textField($model,'date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'receipt_image'); ?>
		<?php echo $form->textField($model,'receipt_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'product_image'); ?>
		<?php echo $form->textField($model,'product_image'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ot_id'); ?>
		<?php echo $form->textField($model,'ot_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->
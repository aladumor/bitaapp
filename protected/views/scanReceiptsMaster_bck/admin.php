<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */

$this->breadcrumbs=array(
	'Scan Receipts Masters'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ScanReceiptsMaster', 'url'=>array('index')),
	array('label'=>'Create ScanReceiptsMaster', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#scan-receipts-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Scan Receipts Masters</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'scan-receipts-master-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'srm_id',
		'o_id',
		'u_id',
		'date',
		'receipt_image',
		'product_image',
		/*
		'ot_id',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>

<?php
/* @var $this ScanReceiptsMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Scan Receipts Masters',
);

$this->menu=array(
	array('label'=>'Create ScanReceiptsMaster', 'url'=>array('create')),
	array('label'=>'Manage ScanReceiptsMaster', 'url'=>array('admin')),
);
?>

<h1>Scan Receipts Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

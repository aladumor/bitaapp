<?php
/* @var $this ScanReceiptsMasterController */
/* @var $model ScanReceiptsMaster */

$this->breadcrumbs = array(
    'Offer Transactions' => array('admin'),
    'Transaction Details'
    //$model->srm_id,
);

$this->menu = array(
//    array('label' => 'List ScanReceiptsMaster', 'url' => array('index')),
//    array('label' => 'Create ScanReceiptsMaster', 'url' => array('create')),
//    array('label' => 'Update ScanReceiptsMaster', 'url' => array('update', 'id' => $model->srm_id)),
//    array('label' => 'Delete ScanReceiptsMaster', 'url' => '#', 'linkOptions' => array('submit' => array('delete', 'id' => $model->srm_id), 'confirm' => 'Are you sure you want to delete this item?')),
    array('label' => 'Manage Transactions', 'url' => array('offertransactionMaster/admin')),
);
?>


<?php
$this->widget('zii.widgets.CDetailView', array(
    'data' => $model,
    'htmlOptions'=>array('itemCssClass'=>'test'),
    'attributes' => array(
      //  'srm_id',
        //'o_id',
        
       'OfferMaster.offer_name',
        //'u_id',
        'UserMaster.user_name',
        'OfferTransactionMaster.redeem_by',
        'OfferTransactionMaster.redeem_date'.
        'OfferTransactionMaster.total_amount',
        'OfferTransactionMaster.merchant_amount',
        'OfferTransactionMaster.brand_partner_amount',
        'OfferTransactionMaster.customer_amount',
        'OfferTransactionMaster.payment_status',
        'OfferTransactionMaster.transaction_id',
        'OfferTransactionMaster.transaction_time',
        'date',
       // 'receipt_image',
        array(
            'name'=>'receipt_image',
            'type'=>'raw',
            'value'=>$model->showphoto_from_database(),            
        ),
        
        array(
            'name'=>'product_image',
            'type'=>'raw',
            'value'=>$model->showphoto_from_database(),            
        ),
      //  'product_image',
     //   'ot_id',
//        array(
////            'name' => 'Category',
//            'type' => 'raw',
//            'value' => CHtml::link('Verify', '#'),
//        ),
    ),
));
 echo CHtml::submitButton('Verify',array('id'=>'Verify', 'name'=> 'Verify'));
?>

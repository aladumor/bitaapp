<?php
/* @var $this InformationMasterController */
/* @var $model InformationMaster */

$this->breadcrumbs=array(
	'Information Masters'=>array('index'),
	$model->im_id,
);

$this->menu=array(
	array('label'=>'List InformationMaster', 'url'=>array('index')),
	array('label'=>'Create InformationMaster', 'url'=>array('create')),
	array('label'=>'Update InformationMaster', 'url'=>array('update', 'id'=>$model->im_id)),
	array('label'=>'Delete InformationMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->im_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage InformationMaster', 'url'=>array('admin')),
);
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
<div class="summary panel-heading">&nbsp;</div>


<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
	'attributes'=>array(
		'im_id',
		'about_us_title',
//		'about_us',
                array(
                    'label'=>'About US',
                    'value'=>strip_tags($model->about_us),
                    'name'=>'about_us'
                ),
		'privacy_policy_title',
//		'privacy_policy',
                array(
                    'label'=>'Privacy Policy',
                    'value'=>strip_tags($model->privacy_policy),
                    'name'=>'privacy_policy'
                ),
		't_and_c_title',
		//'t_and_c',
                array(
                    'label'=>'termsandconditions',
                    'value'=>strip_tags($model->t_and_c),
                    'name'=>'t_and_c'
                ),
	),
)); ?>

</div>
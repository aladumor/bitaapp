<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true).'/tinymce/js/tinymce/tinymce.min.js';?>"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image|sizeselect | fontselect |  fontsizeselect"
});
</script>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">
<div class="form dataTables_wrapper clearfix update-info">

	<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'information-master-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
            <div class="col-md-6">
		<?php echo $form->labelEx($model,'about_us_title'); ?>
		<?php echo $form->textField($model,'about_us_title',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'about_us_title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
		<?php echo $form->labelEx($model,'about_us'); ?>
		<?php echo $form->textArea($model,'about_us',array('rows'=>6, 'cols'=>50,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'about_us'); ?>
		</div>
         </div>
	
	<div class="row">
    	 <div class="col-md-6">
		<?php echo $form->labelEx($model,'privacy_policy_title'); ?>
		<?php echo $form->textField($model,'privacy_policy_title',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'privacy_policy_title'); ?>
        </div>
        </div>
        <div class="row">	
            <div class="col-md-12">
		<?php echo $form->labelEx($model,'privacy_policy'); ?>
		<?php echo $form->textArea($model,'privacy_policy',array('rows'=>6, 'cols'=>50,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'privacy_policy'); ?>
            </div>
	</div>
    <br />
<br />
	<div class="row">
            <div class="col-md-6">
		<?php echo $form->labelEx($model,'t_and_c_title'); ?>
		<?php echo $form->textField($model,'t_and_c_title',array('size'=>60,'maxlength'=>100,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'t_and_c_title'); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
		<?php echo $form->labelEx($model,'t_and_c'); ?>
		<?php echo $form->textArea($model,'t_and_c',array('rows'=>6, 'cols'=>50,'class'=>'form-control mb15 width100p')); ?>
		<?php echo $form->error($model,'t_and_c'); ?>
            </div>
    </div>
    

	<div class="buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->

</div>
</div>
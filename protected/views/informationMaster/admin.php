<?php
/* @var $this InformationMasterController */
/* @var $model InformationMaster */

$this->breadcrumbs=array(
	'Information '=>array('admin'),
	'Manage Information',
);

$this->menu=array(
	//array('label'=>'List InformationMaster', 'url'=>array('index')),
	array('label'=>'Update Information', 'url'=>array('update', 'id'=>'1')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#information-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>





<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<div class="info-master">
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'information-master-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive adjust',
	//'filter'=>$model,
	'columns'=>array(
		//'im_id',
		'about_us_title',
                array(
                    'header'=>Yii::t('about_us', 'about us'),
//                    'value'=>'CHtml::link(strip_tags($data[\'about_us\']),"#")',
                    'type'=>'raw',
                    'value'=>'((strlen($data->about_us)>50) ? (substr(strip_tags($data[\'about_us\']),0,700)."...") : (strip_tags($data[\'about_us\'])))',
                ),
            
		'privacy_policy_title',
                array(
                    'header'=>Yii::t('privacy_policy', 'Privacy policy'),
                    'value'=>'((strlen($data->privacy_policy)>50) ? (substr(strip_tags($data[\'privacy_policy\']),0,50)."...") : (strip_tags($data[\'privacy_policy\'])))',
                    'type'=>'raw',
                ),
            
		't_and_c_title',            
                array(
                    'header'=>Yii::t('t_and_c', 'Terms and condition'),
                    'value'=>'strip_tags($data[\'t_and_c\'])',
                    'value'=>'((strlen($data->t_and_c)>50) ? (substr(strip_tags($data[\'t_and_c\']),0,50)."...") : (strip_tags($data[\'t_and_c\'])))',
                    'type'=>'raw',
                ),
		/*
		't_and_c',
		*/
		/*array(
			'class'=>'CButtonColumn',
		),*/
		
		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-search"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )
		
	),
)); ?>
</div>
<?php
/* @var $this InformationMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Information Masters',
);

$this->menu=array(
	array('label'=>'Create InformationMaster', 'url'=>array('create')),
	array('label'=>'Manage InformationMaster', 'url'=>array('admin')),
);
?>

<h1>Information Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

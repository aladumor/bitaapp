<?php
/* @var $this InformationMasterController */
/* @var $model InformationMaster */

$this->breadcrumbs=array(
	'Information Masters'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List InformationMaster', 'url'=>array('index')),
	array('label'=>'Manage InformationMaster', 'url'=>array('admin')),
);
?>

<h1>Create InformationMaster</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
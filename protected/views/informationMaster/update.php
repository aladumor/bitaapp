<?php
/* @var $this InformationMasterController */
/* @var $model InformationMaster */

$this->breadcrumbs=array(
	'Information'=>array('index'),
	
	'Update Information',
);

$this->menu=array(
	//array('label'=>'List InformationMaster', 'url'=>array('index')),
	//array('label'=>'Create InformationMaster', 'url'=>array('create')),
	//array('label'=>'View InformationMaster', 'url'=>array('view', 'id'=>$model->im_id)),
	array('label'=>'Manage Information', 'url'=>array('admin')),
);
?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>
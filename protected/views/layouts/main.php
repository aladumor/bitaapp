
<?php
 /* @var $this Controller */  
    $session = new CHttpSession;
    $session->open();
    $pid = ($session["pid"] != '') ? $session["pid"]: @$_GET["pid"] ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
      <meta name="language" content="en" />

  <!-- blueprint CSS framework -->
  <?php /*?><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" /><?php */?>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
  <!--[if lt IE 8]>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
  <![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.css" />
    <?php /*?><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main_v2.css" /><?php */?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.datatables.css" />
    <link href="http://cdn.datatables.net/responsive/1.0.1/css/dataTables.responsive.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/component.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-override.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
    <!--<link href="http://fooplugins.com/footable/css/footable.standalone.css" rel="stylesheet" type="text/css" /> -->
        <?php /*?><script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js'; ?>"></script><?php */?>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/jquery-1.11.1.min.js'; ?>"></script>

    <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js'; ?>"></script>

        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery.validate.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/modernizr.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/custom.js'; ?>"></script>
        <!--<script type="text/javascript" src="<?php //echo Yii::app()->baseUrl.'/js/select2.min.js'; ?>"></script>-->
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/jquery.tagsinput.min.js'; ?>"></script>
         <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/bootstrap-timepicker.min.js'; ?>"></script>
         <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/jquery.maskedinput.min.js'; ?>"></script>
  <title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
    if (Yii::app()->user->id >= '1') {
        $page_class = "after_login";
        $status = 1 ;
    } else {
        $page_class = "before_login";
        $status = 0;
    }
    ?>
     <?php 
	$act = Yii::app()->controller->action->id ;
	if($act=='OffersList' || $act=='offerDetails' ) {?>
		<body class="offer-page"> 
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/offeer-page.css" />
	<?php }else{?>
    	<body class="<?php echo $page_class; ?>">
    <?php }?>
    <!--[if IE 8]>
  <style>
.grid-view table.items tr th, .grid-view table.items tr td {
  
   white-space: pre-wrap;;
   word-break:break-all;

}
  </style>
<![endif]-->
    <header>
        <div id="header" class="headerwrapper">
                <div id="logo"><?php // echo CHtml::encode(Yii::app()->name);  ?>
                <div class="for_logo header-left">
                    <a href="#"><!-- <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo1.png" width="25" /> --> <img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo-inner.png"></a>
                    <div class="pull-right">
                        <a class="menu-collapse" href="#">
                            <i class="fa fa-bars"></i>
                        </a>
                    </div>
                    </div>
                    
                    <div class="right_content header-right">
                    <?php if ($status == 1) { ?>
                    <div class="up_right_links pull-right">
                      <div class="btn-group btn-group-option">
                          <button class="btn btn-default dropdown-toggle test" data-toggle="dropdown" type="button"><i class="fa fa-caret-down"></i></button>
                            <ul role="menu" class="dropdown-menu pull-right">
                              <li><a href="<?php echo  Yii::app()->request->baseUrl; ?>/index.php/UserDetailMaster/view?id=<?php echo $session["uid"]; ?>" ><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                              <!--<li><a href="#"><i class="glyphicon glyphicon-star"></i> Activity Log</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li> -->
                              <li class="divider"></li>
                              <li><a href="<?php echo  Yii::app()->request->baseUrl; ?>/index.php/site/logout?rid=<?php echo Yii::app()->user->id?>"><i class="glyphicon glyphicon-log-out"></i> Signout</a></li>
                              <!--<li><a href="#"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li> -->
                            </ul>
                        </div>
                        <?php /*?><?php
                            $user = Yii::app()->user; // just a convenience to shorten expressions

                            $this->widget('zii.widgets.CMenu', array(
                                'items' => array(
                                    array('label' => 'Login', 'url' => array('/site/login'), 'visible' => !Yii::app()->user->isGuest),
                                     array('label'=>'', 'url'=>array('/site/logout?rid='.Yii::app()->user->id), 
                            'linkOptions'=>array('class'=>'fa fa-sign-out logout', 'title'=>'Logout') ,
                                    'visible'=>(Yii::app()->user->id == '1' || Yii::app()->user->id == '2' || Yii::app()->user->id == '3' || Yii::app()->user->id == '4'))
                                ),
                            ));
                            ?><?php */?>
                        <!--<div id="upper_links" class="up_links" >
                            <a href="#"><i class="fa fa-home"></i></a>
                            <span class="mybtn"><i class="fa fa-bars"></i></span>
                        </div> -->
                        <div id="login">
                            
                        </div>
                    </div>
                    <?php } ?>
                    <span class="usernname pull-right mt5">Welcome , <?php echo $session["fullname"]; ?></span>
                    </div>
                </div>
            </div>
        </header>
        <section>
        <div id="wrap" class="mainwrapper">
            <!--<div class="side_bar_border"> &nbsp;</div> -->
            <!-- header -->
            <div id="mainmenu"  class="mainnav leftpanel">
            <div class="media profile-left">
                       
                         
                       
                        <div class="media-body">
                        	<div class="pull-left user-icn">
                        		<i class="fa fa-user"></i>
                        	</div>	
                            <div class="pull-left">
                            <h4 class="media-heading"><?php echo $session["fullname"]; ?></h4>
                            <?php if( $session["rid"]==1) 
                                    $str="Super Admin";
                                  else
                                    $str="Brand Partner";

                            ?>
                            <small class="text-muted"><?php echo $str; ?></small>
                            </div>
                        </div>
                    </div>
                <?php
                    $cContro = Yii::app()->controller->id ;
                    $act = Yii::app()->controller->action->id ;
                ?>
    <?php $this->widget('zii.widgets.CMenu',array(
      'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),

      'items'=>array(
                     array('label'=>'Dashboard',
                      'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/site/index"><i class="fa fa-dashboard"></i><span>Dashboard</span></a>',

                                       //'url'=>array('/offerMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'site') ? 'active ':'')
                           ),

                     array('label'=>'Manage Bevrage Locations',
                      'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/BevrageCities/admin"><i class="fa fa-globe"></i><span>Bevrage Locations</span></a>',

                                       //'url'=>array('/offerMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'BevrageCities') ? 'active ':'')
                           ),
                    array('label'=>'Manage Brand Partners', 
                    'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/UserDetailMaster/admin"><i class="fa fa-users"></i><span>Manage Brand Partners</span></a>',
                                      'url'=>array('UserDetailMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'UserDetailMaster') ? 'active':'')
                               ),
                    array('label'=>'Manage Customers', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/UserDetailMaster/userDashboard"><i class="fa fa-user"></i><span>Manage Customers</span></a>', 
                                       'url'=>array('UserDetailMaster/userDashboard'), 
                                       'visible'=>$session["rid"] == '1' ,
                                       'itemOptions'=>array('class'=> ($act == 'userDashboard') ? 'active':'')
                               ),
                    array('label'=>'Manage Merchants', 
                  'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/MerchantMaster/admin"><i class="fa fa-glass"></i><span>Manage Merchants</span></a>', 
                                       'url'=>array('MerchantMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'MerchantMaster') ? 'active':'')
                               ),
                    array('label'=>'Manage Codes', 
                    'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/productMaster/admin"><i class="fa fa-barcode"></i><span>Manage Codes</span></a>',
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>$session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
                    array('label'=>'Manage Filters', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/LiquorType/admin"><i class="fa fa-beer"></i><span>Manage Liquor Filters</span></a>', 
                                       'url'=>array('LiquorType/admin'), 
                                       'visible'=>$session["rid"] == '1' ,
                                       'itemOptions'=>array('class'=> ($cContro == 'LiquorType') ? 'active':'')
                               ),
                               array('label'=>'Reports',
                    'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/offerMaster/ReportDashboard"><i class="fa fa-pie-chart"></i><span>Reports</span></a>', 
                                       //'url'=>array('/campaignMaster/admin'), 
                                       'visible'=>$session["rid"] >= '1',
                                       'itemOptions'=>array('class'=> ($act == 'ReportDashboard') ? 'active':'')
                               ),
                              array('label'=>'Campaigns', 
                'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/CampaignMaster/admin"><i class="fa fa-edit"></i><span>Campaigns</span></a>', 
                                       'url'=>array('CampaignMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'CampaignMaster') ? 'active':'')
                               ),
                 array('label'=>'Offers', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/OfferMaster/admin"><i class="fa fa-bars"></i><span>Offers</span></a>', 
                                       'url'=>array('OfferMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'OfferMaster') ? 'active':'')
                               ),
                
                 array('label'=>'Rewards', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/RewardMaster/admin"><i class="fa fa-star"></i><span>Rewards</span></a>', 
                                       'url'=>array('RewardMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'RewardMaster') ? 'active':'')
                               ),
                 array('label'=>'Merchandise', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/MerchandiseMaster/admin"><i class="fa fa-trophy"></i><span>Merchandise</span></a>', 
                                       'url'=>array('MerchandiseMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'MerchandiseMaster') ? 'active':'')
                               ),
                 array('label'=>'Transaction History', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/OfferTransactionMaster/admin"><i class="fa fa-history"></i><span>Transaction History</span></a>', 
                                       'url'=>array('OfferTransactionMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'OfferTransactionMaster') ? 'active':'')
                               ),
                 /*array('label'=>'Payments', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/site/index"><i class="fa fa-credit-card"></i><span>Payments</span></a>', 
                                       //'url'=>array('PaymentMaster/admin'), 
                                       'visible'=>$session["rid"] == '1' || $session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'PaymentMaster') ? 'active':'')
                               ),*/
                 array('label'=>'Feedbacks', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/FeedbackMaster/admin"><i class="fa fa-comments"></i><span>Feedbacks</span></a>', 
                                       'url'=>array('FeedbackMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'FeedbackMaster') ? 'active':'')
                               ),
                /*array('label'=>'About Beverage',
                      'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/AboutBeverage/view/1"><i class="fa fa-info-circle"></i><span>About Beverage</span></a>',
                                       'url'=>array('/AboutBeverage/view/1'), 
                                       'visible'=>$session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'AboutBeverage') ? 'active ':'')
                           ),*/

                array('label'=>'Contact Beverage',
                      'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/ContactUsMaster/create"><i class="fa fa-envelope"></i><span>Contact Bevrage</span></a>',
                                       'url'=>array('/ContactUsMaster/create'), 
                                       'visible'=>$session["rid"] == '2',
                                       'itemOptions'=>array('class'=> ($cContro == 'ContactusMaster') ? 'active ':'')
                           ),
              array('label'=>'Manage Information',
                      'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/informationMaster/admin"><i class="fa fa-envelope"></i><span>Manage Information</span></a>',
                                       'url'=>array('/informationMaster/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'informationMaster') ? 'active ':'')
                           ),
                array('label'=>'State TnC', 
                 'template' => '<a href="'.Yii::app()->request->baseUrl.'/index.php/states/admin"><i class="fa fa-check-square-o "></i><span>State TnC</span></a>', 
                                       'url'=>array('states/admin'), 
                                       'visible'=>$session["rid"] == '1',
                                       'itemOptions'=>array('class'=> ($cContro == 'State TnC') ? 'active':'')
                               ),
                 

      ), 
                    )); ?>
            </div><!-- mainmenu -->
            
            <div class="main_container mainpanel">
            <?php 
      /*
        echo "<pre>";
        print_R($this->breadcrumbs);
        die;
        <div class="pageicon pull-left">
                            <i class="fa fa-th-list"></i>
                          </div>
        */
        if(isset($this->breadcrumbs))
        {
          $customHeaderTitle='';
          $customBreadcrumStr =  '<div class="pageheader"><div class="media">
                          
                          <div class="media-body">
                            <ul class="breadcrumb">
                              <li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>';
          foreach($this->breadcrumbs as $key => $val)
          {
            if(!is_numeric($key))
            {
              $href = Yii::app()->baseUrl."/index.php/".$cContro."/".$val[0];
              $customBreadcrumStr .= '<li><a href="'.$href.'">'.$key.'</a></li>';
              $href = "";
            }
            else
            {
              $customBreadcrumStr .= '<li>'.$val.'</li>';
              $customHeaderTitle = '<h4>'.$val.'</h4>';
            }
          }
          $customBreadcrumStr .= '</ul>';
          $customBreadcrumStr .= $customHeaderTitle;
          $customBreadcrumStr .= '</div></div></div>';
          if(!empty($customHeaderTitle))
             echo $customBreadcrumStr;
        }
        /*
        if(isset($this->breadcrumbs)):?>
                    <?php $this->widget('zii.widgets.CBreadcrumbs', 
              array(
                'htmlOptions'=>array('class'=>'pageheader'),
                  'links'=>$this->breadcrumbs,
                  'homeLink'=> (Yii::app()->user->id == '1') ?
                        //'<a href="'.Yii::app()->baseUrl.'/index.php/userMaster/admin">Home</a>' : 
                        //<a href="'.Yii::app()->baseUrl.'/index.php/site/index">Home</a>
                        '<li><a href="'.Yii::app()->baseUrl.'/index.php/site/index"><i class="glyphicon glyphicon-home"></i></a></li>' :
                        //'<a href="'.Yii::app()->baseUrl.'/index.php/userMaster/update/'.$session["uid"].'">Home</a>'
                        '<a href="'.Yii::app()->baseUrl.'/index.php/site/index/'.$session["uid"].'">Home</a>'
              )
            ); 
            */
          ?><!-- breadcrumbs -->
                    
                    
            <?php 
        //endif
      ?>
            
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>
        </div><!-- page -->
        <?php /*?><div id="footer">
    Copyright &copy; <?php echo date('Y'); ?> 
All Rights Reserved to <a href="" target="_blank"> Comapany Name </a>
   <span class="foot"><br /></span>
&nbsp; &nbsp; Powered By <a href="http://www.credencys.com/" target="_blank">Credencys Solutions Inc.</a>
    
  </div><?php */?>
      </section>
        <!-- footer -->
<script type="text/javascript">
$(window).load(function(){
  var winWidth = $(window).width();
  var winHeight = $(document).height()-60;
  $('.leftpanel').height(winHeight + 'px');
  
});
$(document).ready(function(){
  var winWidth = $(window).width();
  var winHeight = $(document).height()-60;
  //alert(winHeight)
  $(window).resize(function () {
	  //alert("Image is loaded");
        $('.leftpanel').height(winHeight + 'px');
    });
  
  if (winWidth <768) { $("#mainmenu:not(:eq(1))").hide();}
  $(".mybtn").click(function(){
   // $("#mainmenu").toggleClass("main_nav1");
  $("#mainmenu").slideToggle();
  });
  $('.panel-primary-head').find("h1").appendTo('pageheader');
 //$('pageheader').find('h1').wrap('div');
  $('.summary').addClass('panel-heading');
  $('#content').addClass('panel panel-primary-head clearfix');
  $('.grid-view').addClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
  $('.yiiPager').addClass('pagination');
 //$('#content').removeClass('panel');
  /*$(function () {
    $('.footable').footable();
  });*/


// Tags Input
jQuery('#tags').tagsInput({width:'auto'});
 
// Textarea Autogrow
//jQuery('#autoResizeTA').autogrow();

// Spinner
var spinner = jQuery('#spinner').spinner();
spinner.spinner('value', 0);

// Form Toggles
//jQuery('.toggle').toggles({on: true});

// Time Picker
jQuery('#timepicker').timepicker({defaultTIme: false});
jQuery('#timepicker2').timepicker({showMeridian: false});
jQuery('#timepicker3').timepicker({minuteStep: 15});

// Date Picker
jQuery('#datepicker').datepicker();
jQuery('#datepicker-inline').datepicker();
jQuery('#datepicker-multiple').datepicker({
	numberOfMonths: 3,
	showButtonPanel: true
});

jQuery('#OfferMaster_start_date').datepicker();
jQuery('#OfferMaster_end_date').datepicker();


// Input Masks
//jQuery("#date").mask("99/99/9999");
//jQuery("#phone").mask("(999) 999-9999");
//jQuery("#ssn").mask("999-99-9999");

// Select2
//jQuery("#select-basic, #select-multi").select2();
/*jQuery('.select-search-hide').select2({
	minimumResultsForSearch: -1
});
*/
function format(item) {
	return '<i class="fa ' + ((item.element[0].getAttribute('rel') === undefined)?"":item.element[0].getAttribute('rel') ) + ' mr10"></i>' + item.text;
}

// This will empty first option in select to enable placeholder
//jQuery('select option:first-child').text('');

/*jQuery("#select-templating").select2({
	formatResult: format,
	formatSelection: format,
	escapeMarkup: function(m) { return m; }
});*/
});
</script>
    </body>
</html>

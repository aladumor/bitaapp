<?php /* @var $this Controller */  
    $session = new CHttpSession;
    $session->open();
    $pid = ($session["pid"] != '') ? $session["pid"]: @$_GET["pid"] ; 
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<?php /*?><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" /><?php */?>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome/css/font-awesome.css" />
    <?php /*?><link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main_v2.css" /><?php */?>
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/style.datatables.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/animate.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap-override.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/jquery-ui-1.10.3.custom/css/ui-lightness/jquery-ui-1.10.3.custom.css" />
        <?php /*?><script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery-1.9.1.js'; ?>"></script><?php */?>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/jquery-1.11.1.min.js'; ?>"></script>
		<script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery-ui-1.10.3.custom.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/css/jquery-ui-1.10.3.custom/js/jquery.validate.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/bootstrap.min.js'; ?>"></script>
        <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/modernizr.min.js'; ?>"></script>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>
<?php
    if (Yii::app()->user->id >= '1') {
        $page_class = "after_login";
        $status = 1 ;
    } else {
        $page_class = "before_login";
        $status = 0;
    }
    ?>
    <body class="<?php echo $page_class; ?>">
    <!--[if IE 8]>
  <style>
.grid-view table.items tr th, .grid-view table.items tr td {
  
   white-space: pre-wrap;;
   word-break:break-all;

}
  </style>
<![endif]-->
		<header>
        <div id="header" class="headerwrapper">
                <div id="logo"><?php // echo CHtml::encode(Yii::app()->name);  ?>
                <div class="for_logo header-left">
                    <a href="#"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/logo1.png" width="25" /></a>
                    </div>
                    
                    <div class="right_content header-right">
                    <?php if ($status == 1) { ?>
                    <div class="up_right_links pull-right">
                    	<div class="btn-group btn-group-option">
                        	<button class="btn btn-default dropdown-toggle test" data-toggle="dropdown" type="button"><i class="fa fa-caret-down"></i></button>
                            <ul role="menu" class="dropdown-menu pull-right">
                              <li><a href="#"><i class="glyphicon glyphicon-user"></i> My Profile</a></li>
                              <!--<li><a href="#"><i class="glyphicon glyphicon-star"></i> Activity Log</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-cog"></i> Account Settings</a></li>
                              <li><a href="#"><i class="glyphicon glyphicon-question-sign"></i> Help</a></li> -->
                              <li class="divider"></li>
                              <li><?php
                            $user = Yii::app()->user; // just a convenience to shorten expressions

                            $this->widget('zii.widgets.CMenu', array(
                                'items' => array(
                                    array('label' => 'Login', 'url' => array('/site/login'), 'visible' => !Yii::app()->user->isGuest),
                                     array('label'=>'', 'url'=>array('/site/logout?rid='.Yii::app()->user->id), 
									 'linkOptions'=>array('class'=>'fa fa-sign-out logout', 'title'=>'Logout') ,
                                    'visible'=>(Yii::app()->user->id == '1' || Yii::app()->user->id == '2' || Yii::app()->user->id == '3' || Yii::app()->user->id == '4'))
                                ),
                            ));
                            ?></li>
                              <!--<li><a href="#"><i class="glyphicon glyphicon-log-out"></i>Sign Out</a></li> -->
                            </ul>
                        </div>
                        <!--<div id="upper_links" class="up_links" >
                            <a href="#"><i class="fa fa-home"></i></a>
                            <span class="mybtn"><i class="fa fa-bars"></i></span>
                        </div> -->
                        <div id="login">
                            
                        </div>
                    </div>
                    <?php } ?>
                    <span class="usernname">Welcome , <?php echo $session["fullname"]; ?></span>
                    </div>
                </div>
            </div>
        </header>
        <section>
        <div id="wrap" class="mainwrapper">
            <!--<div class="side_bar_border"> &nbsp;</div> -->
            <!-- header -->
            <div id="mainmenu"  class="mainnav leftpanel">
            <div class="media profile-left">
                        <a href="profile.html" class="pull-left profile-thumb">
                            <img alt="" src="../../images/photos/profile.png" class="img-circle">
                        </a>
                        <div class="media-body">
                            <h4 class="media-heading">Dounya D</h4>
                            <small class="text-muted">Brand Partner</small>
                        </div>
                    </div>
                <?php
                    $cContro = Yii::app()->controller->id ;
                    $act = Yii::app()->controller->action->id ;
                ?>
		<?php $this->widget('zii.widgets.CMenu',array(
      'htmlOptions'=>array('class'=>'nav nav-pills nav-stacked'),
			'items'=>array(
						
                           array('label'=>'Dashboard',
						   			  'template' => '<a href="../site/index"><i class="fa fa-home"></i><span>Dashboard</span></a>',
                                       //'url'=>array('/offerMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > 0,
                                       'itemOptions'=>array('class'=> ($cContro == 'offerMaster') ? 'active ':'')
                           ),
						   array('label'=>'Manage Brand Partners', 
						   			'template' => '<a href="../UserDetailMaster/admin"><i class="fa fa-users"></i><span>Manage Brand Partners</span></a>',
                                      //'url'=>array('UserDetailMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'UserDetailMaster') ? 'active':'')
                               ),
                               array('label'=>'Reports',
							   		'template' => '<a href="../campaignMaster/admin"><i class="fa fa-pie-chart"></i><span>Reports</span></a>', 
                                       //'url'=>array('/campaignMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'campaignMaster') ? 'active':'')
                               ),
                              array('label'=>'Campaigns', 
							  'template' => '<a href="../campaignMaster/admin"><i class="fa fa-edit"></i><span>Campaigns</span></a>', 
                                      // 'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Offers', 
							   'template' => '<a href="../offerMaster/admin"><i class="fa fa-bars"></i><span>Offers</span></a>', 
                                      // 'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Manage Users', 
							   'template' => '<a href="../UserDetailMaster/admin"><i class="fa fa-users"></i><span>Manage Users</span></a>', 
                                       //'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Loyalty Programs',
							   'template' => '<a ><i class="fa fa-file-text"></i><span>Loyalty Programs</span></a>',  
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Rewards', 
							   'template' => '<a href="site/index"><i class="fa fa-star"></i><span>Rewards</span></a>', 
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Transaction History', 
							   'template' => '<a href="site/index"><i class="fa fa-history"></i><span>Transaction History</span></a>', 
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Payments', 
							   'template' => '<a href="site/index"><i class="fa fa-credit-card"></i><span>Payments</span></a>', 
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   array('label'=>'Feedbacks', 
							   'template' => '<a href="site/index"><i class="fa fa-comments"></i><span>Feedbacks</span></a>', 
                                       'url'=>array('productMaster/admin'), 
                                       'visible'=>Yii::app()->user->id > '0',
                                       'itemOptions'=>array('class'=> ($cContro == 'productMaster') ? 'active':'')
                               ),
							   

										/*array('label'=>'Manage Catalogs', 
                                        'url'=>array('/HotelInformationMaster/admin'), 
                                        'visible'=>Yii::app()->user->id == '1',
                                        'itemOptions'=>array('class'=> ($cContro == 'userMaster') ? 'active':'')
                            ),*/
							
							
								/*array('label'=>'Manage Status', 
                                        'url'=>array('/OnOffStatus/admin'), 
                                        'visible'=>Yii::app()->user->id == '1',
                                        'itemOptions'=>array('class'=> ($cContro == 'userMaster') ? 'active':'')
                            ),*/
							 	//		array('label'=>'', 
//                                        'url'=>array('/HotelInformationMaster/admin'), 
//                                        'visible'=>Yii::app()->user->id == '1',
//                                        'itemOptions'=>array('class'=> ($cContro == 'othermaster') ? 'active':'')
//                                ),
//                                array('label'=>'', 
//                                        'url'=>array('/HotelInformationMaster/admin'), 
//                                        'visible'=>Yii::app()->user->id == '1',
//                                        'itemOptions'=>array('class'=> ($cContro == 'othermaster') ? 'active':'')
//                                ),
			), 
                    )); ?>
            </div><!-- mainmenu -->
            
            <div class="main_container mainpanel">
            <?php 
			/*
				echo "<pre>";
				print_R($this->breadcrumbs);
				die;
				<div class="pageicon pull-left">
														<i class="fa fa-th-list"></i>
													</div>
				*/
				/*if(isset($this->breadcrumbs))
				{
					$customBreadcrumStr =  '<div class="pageheader"><div class="media">
													
													<div class="media-body">
														<ul class="breadcrumb">
															<li><a href="#"><i class="glyphicon glyphicon-home"></i></a></li>';
					foreach($this->breadcrumbs as $key => $val)
					{
						if(!is_numeric($key))
						{
							$href = Yii::app()->baseUrl."/index.php/".$cContro."/".$val[0];
							$customBreadcrumStr .= '<li><a href="'.$href.'">'.$key.'</a></li>';
							$href = "";
						}
						else
						{
							$customBreadcrumStr .= '<li>'.$val.'</li>';
							$customHeaderTitle = '<h4>'.$val.'</h4>';
						}
					}
					$customBreadcrumStr .= '</ul>';
					$customBreadcrumStr .= $customHeaderTitle;
					$customBreadcrumStr .= '</div></div></div>';
					echo $customBreadcrumStr;
				}*/
				/*
				if(isset($this->breadcrumbs)):?>
                    <?php $this->widget('zii.widgets.CBreadcrumbs', 
							array(
								'htmlOptions'=>array('class'=>'pageheader'),
									'links'=>$this->breadcrumbs,
									'homeLink'=> (Yii::app()->user->id == '1') ?
												//'<a href="'.Yii::app()->baseUrl.'/index.php/userMaster/admin">Home</a>' : 
												//<a href="'.Yii::app()->baseUrl.'/index.php/site/index">Home</a>
												'<li><a href="'.Yii::app()->baseUrl.'/index.php/site/index"><i class="glyphicon glyphicon-home"></i></a></li>' :
												//'<a href="'.Yii::app()->baseUrl.'/index.php/userMaster/update/'.$session["uid"].'">Home</a>'
												'<a href="'.Yii::app()->baseUrl.'/index.php/site/index/'.$session["uid"].'">Home</a>'
							)
						); 
						*/
					?><!-- breadcrumbs -->
                    
                    
            <?php 
				//endif
			?>
            
                <?php echo $content; ?>
            </div>
            <div class="clear"></div>
        </div><!-- page -->
        <?php /*?><div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> 
All Rights Reserved to <a href="" target="_blank"> Comapany Name </a>
   <span class="foot"><br /></span>
&nbsp; &nbsp; Powered By <a href="http://www.credencys.com/" target="_blank">Credencys Solutions Inc.</a>
		
	</div><?php */?>
    	</section>
        <!-- footer -->
    <script type="text/javascript">
$(document).ready(function(){
	var winWidth = $(window).width();
  if (winWidth <768) { $("#mainmenu:not(:eq(1))").hide();}
  $(".mybtn").click(function(){
   // $("#mainmenu").toggleClass("main_nav1");
	$("#mainmenu").slideToggle();
  });
  $('.panel-primary-head').find("h1").appendTo('pageheader');
 //$('pageheader').find('h1').wrap('div');
  $('.summary').addClass('panel-heading');
  $('#content').addClass('panel panel-primary-head');
  $('.grid-view').addClass('dataTables_wrapper form-inline dt-bootstrap no-footer');
  $('.yiiPager').addClass('pagination');
});
</script>
    </body>
</html>

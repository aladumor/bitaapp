<?php /* @var $this Controller */ ?>

<?php $this->beginContent('//layouts/main'); ?>
<div class="span-19 contentpanel">
	<div id="content" style="position:relative;">
    <div id="sidebar" class="sideOperations">
    <div class="ac_heading">Operations  <span class="pull_right"><i class="fa fa-caret-down"></i></span></div>
      <div class="content">
	<?php
		$this->beginWidget('zii.widgets.CPortlet', array(
			//'title'=>'Operations',
		));
		$this->widget('zii.widgets.CMenu', array(
			'items'=>$this->menu,
			'htmlOptions'=>array('class'=>'operations'),
		));
		$this->endWidget();
	?>
     </div>
	</div>
		<?php echo $content; ?>
	</div><!-- content -->
</div>
<div class="span-5 last">
	<!-- sidebar -->
</div>
<?php $this->endContent(); ?>
<script type="text/javascript">
$(".content:not(:eq(2))").hide();
$(document).click( function(e){
	if($(e.target).attr('class')=='ac_heading')
	{$('.content').slideToggle();}
	else
	{ $('.content').slideUp()
	}
});

</script>

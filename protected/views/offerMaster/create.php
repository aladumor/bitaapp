<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */

$this->breadcrumbs=array(
	'Offers'=>array('admin'),
	'Create Offer',
);

$this->menu=array(
	//array('label'=>'List OfferMaster', 'url'=>array('index')),
	array('label'=>'Manage Offers', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<script type="text/javascript">
    $(document).ready(function(){
        $('#SendPush').validate({
        rules: {
                    'pushmsgbox': {
                        required: true,                       
                        maxlength:256
                    },
                    
                },
        messages: {
                    'pushmsgbox': {
                        required: "Please enter message text.",  
                        maxlength: "Only 256 characters allowed."                     
                    },
                                   
                },
            errorPlacement: function(error, element) {
                $('#flashsuccess').remove();
                $("#errorplace").show();
                error.appendTo($("#errorplace"));

            },
            success: function(label) { 
                if($("#errorplace").text() == ''){
                    $("#errorplace").hide();
                }
                $($("#errorplace").children()).each(function(){
                    if($(this).text()==''){
                        $(this).remove();
                    }
                })
            }
        
        });

});
</script>
<style>
    .ac_heading{
        display:none !important;
        
    }
    
    .content{
    display:none !important;
    }
</style>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'SendPush',
        'enableAjaxValidation' => false,        
    ));
    ?>
    <?php if($status==1){?>
    <div  id="flashsuccess" class="flash-success"> Message Sent successfully. </div>
    <?php } ?>
    <div id="errorplace" class="errorSummary" style="display:none;"></div>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
	<label>Message<span class="error">*</span></label>
	<input type="textarea" id="pushbox" name="pushmsgbox" rows="8" cols="50" maxlength="256" placeholder="Max length 256 characters" class="input_box"></input>
	<input type="submit" name="send Notifications" value="Send" class="send_btn" ></input>

    </div>
    </div>
<?php $this->endWidget(); ?>
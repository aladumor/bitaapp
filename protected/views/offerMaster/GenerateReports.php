<html>
  <head>
    <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>  

    <script type="text/javascript">

      // Load the Visualization API and the piechart package.
      google.load('visualization', '1.0', {'packages':['corechart']});

      // Set a callback to run when the Google Visualization API is loaded.
      google.setOnLoadCallback(drawChart);

      // Callback that creates and populates a data table,
      // instantiates the pie chart, passes in the data and
      // draws it.
      function drawChart() {

        // Create the data table.
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Age Group');
        data.addColumn('number', 'Redemptions');
        data.addRows([
          ['21-30', 3],
          ['31-40', 1],
          ['41-50', 1],
          ['51-60', 1],
          ['61+', 2]
        ]);

        // Set chart options
        var options = {'title':'Age Wise Offer Redemption',
                       'width':500,
                        legendTextStyle: { color: '#FFF' },
    					titleTextStyle: { color: '#FFF' },
                        backgroundColor: 'transparent', 
                        colors: ['#2EC9E7', '#33a3b7', '#65d8ed', '#98d7e2', '#afd4db'] ,                              
                       'height':300};

        // Instantiate and draw our chart, passing in some options.
        var chart = new google.visualization.PieChart(document.getElementById('chart_div'));
        chart.draw(data, options);

         /*
        * LOCATION BASED REPORTS
        */
        var data2 = new google.visualization.DataTable();
        data2.addColumn('string', 'Location');
        data2.addColumn('number', 'Redemptions');
        data2.addRows([
          ['New York', 58],
          ['California', 20],
          ['Texas', 20],
          ['Chicago',20],
          ['Houston', 35]
        ]);

        // Set chart options
        var options2 = {'title':'Location Based Offer Redemption',
                       'width':700,
                       'is3D':true,
                        vAxis: {
						    textStyle:{color: '#FFF'}
						},
						hAxis: {
						    textStyle:{color: '#FFF'}
						},
						chartArea: {  width: "50%", height: "70%" }, 						
						bar: {groupWidth: "50%"},
						colors: ['#2EC9E7'],
						legendTextStyle: { color: '#FFF' },
    					titleTextStyle: { color: '#FFF' },
                        backgroundColor: 'transparent',
                        //isStacked: true,
                       'height':300
                   };

        var chart2 = new google.visualization.ColumnChart(document.getElementById('chart2_div'));
        chart2.draw(data2, options2);

        /*
        * TIME BASED REPORTS
        */
        var data3 = new google.visualization.DataTable();
        data3.addColumn('string', 'Time Based');
        data3.addColumn('number', 'Redemptions');
        data3.addRows([
          ['10am  -12pm',9],
          ['12pm - 3pm', 10],
          ['3pm - 7pm', 11],
          ['7pm - 11pm', 6],
          ['11pm +', 35]
        ]);

        // Set chart options
        var options3 = {'title':'Time Wise Offer Redemption',
                       'width':500,
                        legendTextStyle: { color: '#FFF' },
    					titleTextStyle: { color: '#FFF' },
                        backgroundColor: 'transparent', 
                        colors: ['#AFD4DB', '#93a9ad', '#a7b9bc', '#96a1a3', '#c5d8db'] ,                              
                       'height':300
                   };

        var chart3 = new google.visualization.PieChart(document.getElementById('chart3_div'));
        chart3.draw(data3, options3);


        /*
        * DATE BASED REPORTS
        */
        var data4 = new google.visualization.DataTable();
         data4.addColumn('date', 'Date');
    	data4.addColumn('number', 'Redemption');    	
        data4.addRow([new Date(2015, 0, 3), 10])
        data4.addRow([new Date(2015, 0, 5), 18])
        data4.addRow([new Date(2015, 1, 15), 26])
    	data4.addRow([new Date(2015, 1, 23), 15])
    	data4.addRow([new Date(2015, 2, 4), 40])
    	data4.addRow([new Date(2015, 6, 2), 50])
    	data4.addRow([new Date(2015, 6, 15), 50])
    	data4.addRow([new Date(2015, 7, 5), 30])
    	;

        // Set chart options
        var options4 = {'title':'Date Wise Offer Redemption',
                       'width':700,
                        vAxis: {
						    textStyle:{color: '#FFF'}

						},
						hAxis: {
						    textStyle:{color: '#FFF'},
						    gridlines: {
						        color: 'transparent'
						    }
						},
						//legend: {position: 'bottom', textStyle: {fontSize: 14}},												
						colors: ['#2EC9E7'],
						legendTextStyle: { color: '#FFF' },
    					titleTextStyle: { color: '#FFF' },
                        backgroundColor: 'transparent', 
                        pointSize: 5, 
                        chartArea: {  width: "50%", height: "70%" },                         
                       'height':300,
                       	explorer: {
					        maxZoomOut:2,
					        keepInBounds: true
					    }
                   };

        var chart4 = new google.visualization.LineChart(document.getElementById('chart4_div'));
        chart4.draw(data4, options4);


      }
    </script>
  </head>

  <body>
  	<div>
  		<span>Total Facebook Share:</span><span>  1500</span><br/>
  		<span>Total Twitter Share:</span><span>  550</span><br/>
  		<span>Total Google Plus Share: </span><span> 80</span><br/>
  		<span>Total Scans: </span><span> 2500</span>
  	</div>

  	<div>
  		<span>Offer Name:  </span><span>15% off On Malibu cocunut Rum</span><br/>
  		<span>Offer Type:  </span><span>Bars & Restaurant</span><br/>
  		<span>Product Name: </span><span> Malibu Rum</span><br/>
  		<span>Time Period:</span><span> 01/01/2015 - 07/30/2015  </span><br/>  		
    <!--Div that will hold the pie chart-->
    <div id="chart_div"></div>
    <div id="chart2_div"></div>
    <div id="chart3_div"></div>
    <div id="chart4_div"></div>
  </body>
</html>
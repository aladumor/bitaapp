<?php

class UserDetailMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */


	public function accessRules()
	{
                $session=new CHttpSession;
                $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UserDetailMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserDetailMaster']))
		{
			$model->login_type = 1;
			$model->ur_id = 2;
			$model->ut_id = 1;
			$password = base64_encode(rand(00000,99999));
			$model->user_pass = base64_encode($password);
			$model->attributes=$_POST['UserDetailMaster'];
			if($model->save())
			{
				/*Last inserted id of */
				$Id = Yii::app()->db->getLastInsertId();

				$brand_name=empty($_POST['brand_name'])? null :$_POST['brand_name'];
				if(!empty($brand_name))
				{
					$connection = Yii::app()->db;
					$cmd="INSERT INTO `brand_master`(`u_id`,`brand_name`) VALUES ('".$Id."','".$brand_name."')";
					$dataReader = $connection->createCommand($cmd)->query();
				}

				/*For Add Brand Details*/
				$product_name=empty($_POST['product_name'])? null :$_POST['product_name'];
				$upc_code=empty($_POST['upc_code'])? null :$_POST['upc_code'];
				if(!empty($product_name) && !empty($upc_code))
				{
					$connection = Yii::app()->db;
					$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$product_name.'","'.$upc_code.'","'.$Id.'")';
					$dataReader = $connection->createCommand($cmd)->query();
				}
				$new_product=empty($_POST['new_product'])? null :$_POST['new_product'];
				$new_upc=empty($_POST['new_upc'])? null :$_POST['new_upc'];
				if($new_product != null)
				{
					foreach($new_product as $key => $value2)
					{
						$p_name = $value2;
						$upc_code = $new_upc[$key];
						$connection = Yii::app()->db;
						$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$p_name.'","'.$upc_code.'","'.$Id.'")';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/*For Add Brand Details*/

				/*
				BRAND PARTNERS -> PAYPAL PAYMENTS				
				*/
				//$resultPay=$this->actionRequestPayment($model->id);
				/*if($resultPay=="1")
				{
					//success
				}
				else
				{
					//fail
				}*/
				/* send email */
				/*$message = new YiiMailMessage;  
		        $msg= "Thanks For joining BEVRAGE Team. 
		       	\n Following is your Login Credentials for Brand Partner Portal :
		       	\n Username: ".$_POST['UserDetailMaster']['email']."
		       	\n Pasword: ".$password."\n
		        \n Best regrads,
		        \n Dounya Irrgang
		        \n CEO 
		        \n BEVRAGE CO.," ;   
		        $message->setBody($msg, 'text/html');       
		        $message->setsubject('Welcome to BEVRAGE');      
		        $message->addTo($email);         
		        $message->from = "service@bevRage.com";        
		        Yii::app()->mail->send($message); */
				/* end */
				
			}
				
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionRequestPayment()
      {
          $e=new ExpressCheckout;
 
          $products=array(
 
                '0'=>array(
                      'NAME'=>'p1',
                      'AMOUNT'=>'250.00',
                      'QTY'=>'2'
                      ),
                '1'=>array(
                      'NAME'=>'p2',
                      'AMOUNT'=>'300.00',
                      'QTY'=>'2'
                      ),
                '2'=>array(
                      'NAME'=>'p3',
                      'AMOUNT'=>'350.00',
                      'QTY'=>'2'
                      ),
 
                );
                         /*Optional */
                   $shipping_address=array(
 
            'FIRST_NAME'=>'Sirin',
            'LAST_NAME'=>'K',
            'EMAIL'=>'sirinibin2006@gmail.com',
            'MOB'=>'0918606770278',
            'ADDRESS'=>'mannarkkad', 
            'SHIPTOSTREET'=>'mannarkkad',
            'SHIPTOCITY'=>'palakkad',
            'SHIPTOSTATE'=>'kerala',
            'SHIPTOCOUNTRYCODE'=>'IN',
            'SHIPTOZIP'=>'678761'
                                          ); 
 
            $e->setShippingInfo($shipping_address); // set Shipping info Optional
 
            $e->setCurrencyCode("EUR");//set Currency (USD,HKD,GBP,EUR,JPY,CAD,AUD)
 
            $e->setProducts($products); /* Set array of products*/
 
            $e->setShippingCost(5.5);/* Set Shipping cost(Optional) */
 
 
            $e->returnURL=Yii::app()->createAbsoluteUrl("site/PaypalReturn");
 
                    $e->cancelURL=Yii::app()->createAbsoluteUrl("site/PaypalCancel");
 
            $result=$e->requestPayment(); 
 
            /*
              The response format from paypal for a payment request
            Array
        (
            [TOKEN] => EC-9G810112EL503081W
            [TIMESTAMP] => 2013-12-12T10:29:35Z
            [CORRELATIONID] => 67da94aea08c3
            [ACK] => Success
            [VERSION] => 65.1
            [BUILD] => 8725992
        )
                */
 
 
        if(strtoupper($result["ACK"])=="SUCCESS")
          {
            /*redirect to the paypal gateway with the given token */
            header("location:".$e->PAYPAL_URL.$result["TOKEN"]);
          } 
 
 
 
         }          
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		/*Fetch brand_name from brand_master*/
		$connection = Yii::app()->db;
		$brand_name = "select brand_name from brand_master where u_id = ".$id;
		$brand_namerows = $connection->createCommand($brand_name)->queryAll();
		$BrandName = empty($brand_namerows[0]['brand_name']) ? null : $brand_namerows[0]['brand_name'];

		/*Fetch product_details from product_master*/
		$product_name = "select product_name,upc_code,pm_id from product_master where created_by = ".$id;
		$product_namerows = $connection->createCommand($product_name)->queryAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserDetailMaster']))
		{
			/*echo "<pre>";
			print_r($_POST);
			
			die();*/
			$model->attributes=$_POST['UserDetailMaster'];
			if(empty($_POST['user_pass']))
				$model->user_pass=$model->user_pass;
			if($model->save())
				/*Any new product Added */
				$new_product=empty($_POST['new_product'])? null :$_POST['new_product'];
				$new_upc=empty($_POST['new_upc'])? null :$_POST['new_upc'];
				if($new_product != null)
				{
					foreach($new_product as $key => $value2)
					{
						$p_name = $value2;
						$upc_code = $new_upc[$key];
						$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$p_name.'","'.$upc_code.'","'.$id.'")';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/* end */
				/* update old product */
				$old_product_name=empty($_POST['old_product_name'])? null :$_POST['old_product_name'];
				$old_upc_code=empty($_POST['old_upc_code'])? null :$_POST['old_upc_code'];
				$pm_id=empty($_POST['pm_id'])? null :$_POST['pm_id'];
				
				if($old_product_name != null)
				{
					if(!empty($_POST['deletedPids']))
					{
						$deletedPids=ltrim ($_POST['deletedPids'], ',');
						$cmd="DELETE FROM `product_master` where pm_id IN (".$deletedPids.")";
						$dataReader = $connection->createCommand($cmd)->query();
					}
					
					foreach($old_product_name as $key => $value3)
					{
						$product_name = $value3;
						$upc_code = $old_upc_code[$key];
						$cmd='UPDATE `product_master` SET product_name = "'.$product_name.'",upc_code = "'.$upc_code.'" where pm_id ="'.$key.'"';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/* end*/
				$brand_name=empty($_POST['brand_name'])? null :$_POST['brand_name'];
				if($brand_name != null)
				{
					$cmd="UPDATE `brand_master` SET brand_name = '".$brand_name."' where u_id ='".$id."'";
					$dataReader = $connection->createCommand($cmd)->query();
				}

				$model->email=empty($_POST['email'])? null :$_POST['email'];
				$model->phone_no=empty($_POST['phone_no'])? null :$_POST['phone_no'];
				

				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'BrandName'=>$BrandName,'product_namerows'=>$product_namerows,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE user_master SET is_active=1 ,is_delete=1 where u_id=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();

		$cmd="UPDATE product_master SET is_active=1 ,is_delete=1 where created_by=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();

		$cmd="UPDATE offer_master SET is_active=1 ,is_delete=1 where u_id=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UserDetailMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UserDetailMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserDetailMaster']))
			$model->attributes=$_GET['UserDetailMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UserDetailMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UserDetailMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UserDetailMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-detail-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}

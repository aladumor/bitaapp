<?php
/* @var $this OfferMasterController */
/* @var $data OfferMaster */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('o_id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->o_id), array('view', 'id'=>$data->o_id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cm_id')); ?>:</b>
	<?php echo CHtml::encode($data->cm_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('u_id')); ?>:</b>
	<?php echo CHtml::encode($data->u_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('offer_name')); ?>:</b>
	<?php echo CHtml::encode($data->offer_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('city_id')); ?>:</b>
	<?php echo CHtml::encode($data->city_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('offer_desc')); ?>:</b>
	<?php echo CHtml::encode($data->offer_desc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('start_date')); ?>:</b>
	<?php echo CHtml::encode($data->start_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('end_date')); ?>:</b>
	<?php echo CHtml::encode($data->end_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maximum_redemption')); ?>:</b>
	<?php echo CHtml::encode($data->maximum_redemption); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('offer_image')); ?>:</b>
	<?php echo CHtml::encode($data->offer_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('code_redeem_file')); ?>:</b>
	<?php echo CHtml::encode($data->code_redeem_file); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_active')); ?>:</b>
	<?php echo CHtml::encode($data->is_active); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('is_delete')); ?>:</b>
	<?php echo CHtml::encode($data->is_delete); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_by')); ?>:</b>
	<?php echo CHtml::encode($data->modified_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_on')); ?>:</b>
	<?php echo CHtml::encode($data->created_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('modified_on')); ?>:</b>
	<?php echo CHtml::encode($data->modified_on); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loyalty_offer_id')); ?>:</b>
	<?php echo CHtml::encode($data->loyalty_offer_id); ?>
	<br />

	*/ ?>

</div>
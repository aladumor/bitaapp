<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */

$this->breadcrumbs=array(
	'Offers'=>array('admin'),
	//$model->o_id,
);

$this->menu=array(
	//array('label'=>'List OfferMaster', 'url'=>array('index')),
	//array('label'=>'Create OfferMaster', 'url'=>array('create')),
	//array('label'=>'Update OfferMaster', 'url'=>array('update', 'id'=>$model->o_id)),
	//array('label'=>'Delete OfferMaster', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->o_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Offers', 'url'=>array('admin')),
);
?>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer view-tbl">
<div class="summary panel-heading">&nbsp;</div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	 'htmlOptions'=>array('class'=>'table table-striped table-bordered responsive footable'),
	'attributes'=>array(
		//'o_id',
		//'cm_id',
		'campaign.campaign_name',
		'product.product_name',
		'user.user_name',
		//'u_id',
		'offer_name',
		//'city_id',
		'offer_desc',
		array(
		    'name'=>'Start Date',
		    'header'=>'Start Date',
		    //'value'=>'date("d M Y",strtotime($data["work_date"]))'
		    'value'=>Yii::app()->dateFormatter->format("d MMM y",strtotime($model->start_date)),
		),
		array(
		    'name'=>'End Date',
		    'header'=>'End Date',
		    //'value'=>'date("d M Y",strtotime($data["work_date"]))'
		    'value'=>Yii::app()->dateFormatter->format("d MMM y",strtotime($model->end_date)),
		),
		'maximum_redemption',
		array(
            'label'=>'Offer image',
            'type'=>'raw',
            'value'=>CHtml::image(Yii::app()->baseUrl."/images/offer_images/".$model->offer_image,'not found',array('width'=>150,'height'=>100)),

        ),
		array(
            'label'=>'Code',
            'type'=>'raw',
            'value'=>CHtml::image(Yii::app()->baseUrl."/images/offer_codes/".$model->code_redeem_file,'not found',array('width'=>100,'height'=>100)),

        ),
		//'code_redeem_file',
		array(
		    'name'=>'Is Active',
		    'header'=>'Is Active',
		    //'value'=>'date("d M Y",strtotime($data["work_date"]))'
		    'value' => ($model->is_active == "0") ? "Yes" : "No"
		),
		array(
		    'name'=>'Loyalty Offer',
		    'header'=>'Loyalty Offer',
		    //'value'=>'date("d M Y",strtotime($data["work_date"]))'
		    'value' => ($model->loyalty_offer_id == "1") ? "No" : "Yes"
		),
		
	),
)); ?>
</div>
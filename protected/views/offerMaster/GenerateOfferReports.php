<?php 
 //print_r($data);die();
/*if(!empty($data['dateData']))
{
  foreach ($data['dateData'] as $key => $value) { 
        echo "[Date.UTC(".$value['dateIndex'].",".$value['totalcount']."],"; 
    }
  }die();
*/
$this->breadcrumbs=array(
  'Reports'=>array('admin'),
  'Offer Report  ',
);
?>
<!---------------------- HIGH CHARTS ------------>
<!DOCTYPE html>
<html>
<head>
    <style>
        .ac_heading{
            display:none !important;
            
        }
        
        .content{
        display:none !important;
        }
    </style>
  <title></title>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" />
  <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/select2.min.js'; ?>"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <script src="http://highslide-software.github.io/export-csv/export-csv.js"></script>
  <script type="text/javascript">
    $(function () {

      $(document).ready(function () {

          Highcharts.setOptions({
           colors: ['#2EC9E7', '#33a3b7', '#65d8ed', '#98d7e2', '#afd4db','#AFD4DB', '#93a9ad', '#a7b9bc']
          });
          // Build the chart
          <?php if(!empty($data['ageData'])){?>
          $('#Agecontainer').highcharts({
              chart: {
                  plotBackgroundColor: null,
                  backgroundColor:'transparent',                  
                  plotBorderWidth: null,
                  plotShadow: false
              },
              title: {
                  text: 'Redemption Per Age'
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: false
                      },
                      showInLegend: true
                  }
              },
              series: [{
                  type: 'pie',
                  name: 'Redeem Percentage',
                  data: [
                      /*['20-29',   45.0],
                      ['30-39',       26.8],                      
                      ['40-49',    8.5],
                      ['50-59',     6.2],
                      ['60+',   0.7]*/
                       <?php 
                        if(!empty($data['ageData']))
                        {
                          foreach ($data['ageData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['totalcount']; ?>],  
                        <?php }            
                        }?>   

                  ]
              }]
          });
          <?php }else{?>
          $('#Agecontainer').html('No Data Available.')
          <?php }?>
          

            //build Location chart

          <?php if(!empty($data['locationData'])){?>
          $('#LocationContainer').highcharts({
            chart: {
                type: 'column',
                backgroundColor:'transparent',
            },
            title: {
                text: 'Location Based Redemption',
        
            },
            
            xAxis: {
                categories: [
                    /*'New York',
                    'New Jersey',
                    'Texas',
                    'Chicago',
                    'Miami',
                    'Washington D.C',
                    'Orlando',
                    'Houston',*/
                    <?php if(!empty($data['locationData']))
                        {
                          foreach ($data['locationData'] as $key => $value) { 
                            echo "'".$value["option_value"]."',"; 
                        }  
                      }
                    ?> 
                    
                ],
                crosshair: true,
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Redemptions'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Redeem Count: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Location',
                maxPointWidth: 30,
                color: '#2EC9E7',
                data: [
                <?php 
                        if(!empty($data['locationData']))
                        {
                          foreach ($data['locationData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['cnt']; ?>],  
                        <?php }            
                        }?>  
                ]
            }]
          });
          <?php }else{?>
          $('#LocationContainer').html('No Data Available.')
          <?php }?>
          

         //build gender chart

          <?php if(!empty($data['genderData'])){?>
          $('#GenderContainer').highcharts({
            chart: {
                type: 'column',
                backgroundColor:'transparent',
            },
            title: {
                text: 'Gender Based Redemption',
        
            },
            
            xAxis: {
                categories: [
                    /*'New York',
                    'New Jersey',
                    'Texas',
                    'Chicago',
                    'Miami',
                    'Washington D.C',
                    'Orlando',
                    'Houston',*/
                    <?php if(!empty($data['genderData']))
                        {
                          foreach ($data['genderData'] as $key => $value) { 
                            echo "'".$value["option_value"]."',"; 
                        }  
                      }
                    ?> 
                    
                ],
                crosshair: true,
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Redemptions'
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">Redeem Count: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: [{
                name: 'Gender Based',
                color: '#2EC9E7',
                data: [
                <?php 
                        if(!empty($data['genderData']))
                        {
                          foreach ($data['genderData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['cnt']; ?>],  
                        <?php }            
                        }?>  
                ]
            }]
          });
          <?php }else{?>
          $('#GenderContainer').html('No Data Available.')
          <?php }?>

          // Build Time chart
          <?php if(!empty($data['timeData'])){?>
          $('#Timecontainer').highcharts({
              chart: {
                  plotBackgroundColor: null,
                  backgroundColor: 'transparent',
                  plotBorderWidth: null,
                  plotShadow: false
              },
              title: {
                  text: 'Time of Redemption'
              },
              tooltip: {
                  pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
              },
              plotOptions: {
                  pie: {
                      allowPointSelect: true,
                      cursor: 'pointer',
                      dataLabels: {
                          enabled: false
                      },
                      showInLegend: true
                  }
              },
              series: [{
                  type: 'pie',
                  
                  name: 'Redeem Percentage',
                  data: [                                          
                     /* ['10am  -12pm',9],
                      ['12pm - 3pm', 10],
                      ['3pm - 7pm', 11],
                      ['7pm - 11pm', 6],
                      ['11pm +', 35]*/
                      <?php 
                        if(!empty($data['timeData']))
                        {
                          foreach ($data['timeData'] as $key => $value) { ?>
                            [<?php echo "'".$value["option_value"]."'"; ?>, <?php echo $value['totalcount']; ?>],  
                        <?php }            
                        }?>    
                  ]
              }]
          });
          <?php }else{?>
          $('#Timecontainer').html('No Data Available.')
          <?php }?>



          //Date wise Chart
          <?php 
          if(!empty($data['dateData']))
          {?>
            $('#Datecontainer').highcharts({
              chart: {
                  type: 'spline',
                  zoomType: 'x',
                  backgroundColor:'transparent',

              },
              title: {
                  text: 'Redemption Per Date'
              },
              xAxis: {
              type: 'datetime',
              tickInterval: 30 * 24 * 3600 * 1000,
              dateTimeLabelFormats: { // don't display the dummy year
                  month: '%b',
                  //year: '%b'
              },
              title: {
                  text: 'Date'
              }
              },
              yAxis: {             
                  title: {
                      text: 'Redemption'
                  },
                  min: 0
              },
             
              tooltip: {
                  headerFormat: '<b>Redeem Count</b><br>',
                  pointFormat: '{point.x:%b %e ,%Y}: {point.y}'
              },

              plotOptions: {
                  spline: {
                      marker: {
                          enabled: true
                      }
                  }
              },

              series: [{
                  name: 'Date Wise',
                  color: '#2EC9E7',
                  // Define the data points. All series have a dummy year
                  // of 1970/71 in order to be compared on the same x axis. Note
                  // that in JavaScript, months start at 0 for January, 1 for February etc.
                  /*data: [
                      [Date.UTC(2014,  10, 10), 18],
                      [Date.UTC(2014,  10, 18), 18],
                      [Date.UTC(2014,  10, 24), 19],
                      [Date.UTC(2014,  11,  4), 49],
                      [Date.UTC(2014,  11, 11), 79],
                      [Date.UTC(2015,  1, 10), 18],                    
                  ]*/
                  data: [ 
                        
                        <?php 
                        if(!empty($data['dateData']))
                        {

                          foreach ($data['dateData'] as $key => $value) { ?>
                            [Date.UTC(<?php echo $value['dateIndex']; ?>), <?php echo $value['totalcount']; ?>],  
                        <?php }            
                        }?>            
                    ]  
              }]
            });
          <?php }else{?>
            //No redemption Data available
            $('#dateData').html('No Data Available.')
          <?php  }?>
      });

    });
  </script>
</head>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer white-bg">
<h3><?php echo $_GET['offer_name']?></h3>
<div class="clearfix">
<div class="col-md-12 mt30">
    <div class="socialData offer-tbl col-md-6">
      <ul class="clearfix">
          <li>Total Facebook Share</li>
            <li><?php echo $data['fb'];?></li>
        </ul>
      <ul class="clearfix">
          <li>Total Twitter Share</li>
            <li><?php echo $data['tw'];?></li>
        </ul>
        <ul class="clearfix">
          <li>Total Google Plus Share</li>
            <li><?php echo $data['gplus'];?></li>
        </ul>
      <ul class="clearfix">
          <li>Total Redemptions</li>
            <li><?php echo $data['scan'];?></li>
        </ul>
      <ul class="clearfix">
        <li>Total SMS Share</li>
          <li><?php echo $data['sms'];?></li>
      </ul>
    </div>

    <div class="socialData offer-tbl col-md-6">
      <ul class="clearfix">
          <li>Offer Name</li>
            <li><?php echo $data['offer_name'];?></li>
        </ul>
      <ul class="clearfix">
            <li>Offer Type</li>
            <li><?php echo $data['offer_type'];?></li>
        </ul>
        <ul class="clearfix">
          <li>Product Name</li>
            <li><?php echo $data['product_name'];?></li>
        </ul>
        <ul class="clearfix">
            <li>Time Period</li>
            <li><?php echo $data['duration'];?></li>
        </ul>
    </div>
</div>

<div class="col-md-12 mt20">
    <div id="Agecontainer" class="col-md-5"></div>
    <div class="col-md-1"></div>
    <div id="GenderContainer" class="col-md-5"></div> 
           
</div>
<div class="col-md-12 mt20">
    <div id="Timecontainer" class="col-md-5"></div>
    <div class="col-md-1"></div>
    <div id="Datecontainer" class="col-md-5"></div>
</div>
<div class="col-md-12 mt20">
  <div id="LocationContainer" class="col-md-10"></div>  
</div>
</div>
<div>
  <a href="ReportDashboard" class="btn btn-primary"> Back </a>
</div>
</div>
</html>









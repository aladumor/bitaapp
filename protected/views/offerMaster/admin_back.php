<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */

$this->breadcrumbs=array(
	'Offers'=>array('admin'),
	'Manage Offers',
);

$this->menu=array(
	//array('label'=>'List OfferMaster', 'url'=>array('index')),
	array('label'=>'Create Offer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#offer-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>
<?php 
$session=new CHttpSession;
$session->open();
if($session['rid']==1)
{?>
<html>
<head>
	<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
	</style>
	
	</head>
</html>
<?php }
?>
<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
 -->
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'offer-master-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	//'filter'=>$model,
	'columns'=>array(

		array(
            'header' => 'Campaign',
            'name' => 'cm_id',
            'value' => '($data->cm_id == "0") ? "None" : "{$data->campaign->campaign_name}"'
        ),		
		'user.user_name',
		'offer_name',

		'maximum_redemption',
		
		array(
                    'type'=>'raw',
                    'header'=>'Offer Image',
                    'value' =>'offerMaster::model()->getimage($data->offer_image)'
             ),		
		/*array(
            'header' => 'Loyalty Offer',
            'name' => 'loyalty_offer_id',
            'value' => '($data->loyalty_offer_id == "1") ? "No" : "Yes"'
        ),*/
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
        array(
        'class'=>'CLinkColumn',
        'label'=> 'Change',
        'urlExpression'=>'"changeStatus?id=".$data->o_id."&status=".$data->is_active',
        'header'=>'Change Status'
      	),
        array(
        'class'=>'CLinkColumn',
        'label'=>'View Report',
        'urlExpression'=>'"GenerateOfferReports?id=".$data->o_id',
        'header'=>'Analytics'
      ),

		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}{delete}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-search"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),
            )
        )

	),
)); ?>

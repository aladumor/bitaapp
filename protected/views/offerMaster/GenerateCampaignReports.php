<?php 
$this->breadcrumbs=array(
    'Reports'=>array('ReportDashboard'),
    'Campaign Report',
);

$session=new CHttpSession;
$session->open();
$uid=$session['uid'];
?>
<!---------------------- HIGH CHARTS ------------>
<!DOCTYPE html>
<html>
<head>
    <style>
        .ac_heading{
            display:none !important;
            
        }
        
        .content{
        display:none !important;
        }
    </style>
  <title></title>
  <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/select2.css" />
  <script type="text/javascript" src="<?php echo Yii::app()->baseUrl.'/js/select2.min.js'; ?>"></script>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <script src="http://highslide-software.github.io/export-csv/export-csv.js"></script>
  <script type="text/javascript">

    $(function () {

        $(document).ready(function () {

              Highcharts.setOptions({
               colors: ['#2EC9E7', '#33a3b7', '#65d8ed', '#98d7e2', '#afd4db','#AFD4DB', '#93a9ad', '#a7b9bc']
              });
              // Build the chart
             
              
              // Build  Bar  chart
              <?php if(!empty($campaigns)){?>
              $('#CampaignContainer').highcharts({
                    chart: {
                    type: 'column',
                    backgroundColor:'transparent',
                    },
                    title: {
                        text: 'Top Grossing Campaign Report'
                    },
                    subtitle: {
                        text: 'Click on any Campaign Column to view it\'s offer Report'
                    },
                    xAxis: {
                        categories: [
                            /*'Labour Day',
                            'Valentine\'s Day',
                            'Christmas Celebration',
                            'New Year\s Eve',
                            'Independence Day',
                            'Spring Break',
                            'Super Bowl',
                            'FIFA',
                            */
                            <?php 
                        if(!empty($campaigns))
                        {
                          foreach ($campaigns as $key => $value) { ?>
                            <?php echo '"'.$key.'"'; ?>,  
                        <?php }            
                        }?>
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Offer Count'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">Offer Count: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        },
                        series: {
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {                                        
                                        var campaign = this.category;                                       
                                        var urlstr="<?php echo Yii::app()->createUrl('OfferMaster/getajaxOffers')?>";
                                        $.ajax({
                                              type: "POST",
                                              url: urlstr,
                                              dataType: 'html',
                                              data: {campaign: campaign,uid:<?php echo $_GET['uid']?>,rid:<?php echo $_GET['rid']?>},
                                              success: function(msg) { 
                                              var r="['10% off on Absolute Green Apple',   45.0],['4 shots @ $20',26.8],['Corona Beer at 20% off',8.5]";
                                                $('#Agecontainer').show();
                                                $('#Agecontainer').highcharts({
                                                      chart: {
                                                          plotBackgroundColor: null,
                                                          backgroundColor:'transparent',                  
                                                          plotBorderWidth: null,
                                                          plotShadow: false
                                                      },
                                                      legend: {         
                                                      align: 'left',
                                                      verticalAlign: 'middle',
                                                      //backgroundColor: '#D6D6D6',
                                                      borderRadius: 0,
                                                      enabled: true,           
                                                      itemMarginTop:4,
                                                      itemMarginBottom: 5,
                                                      width:540,
                                                      itemWidth:270,
                                                      itemStyle: {
                                                        width:250,         
                                                        font: '12pt "Lato",sans-serif',
                                                        }
                                                      },
                                                      title: {
                                                          text: campaign +': Offer Report'
                                                      },
                                                      tooltip: {
                                                          pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
                                                      },
                                                      plotOptions: {
                                                          pie: {
                                                              allowPointSelect: true,
                                                              cursor: 'pointer',
                                                              dataLabels: {
                                                                  enabled: false
                                                              },
                                                              showInLegend: true
                                                          }
                                                      },
                                                      series: [{
                                                          type: 'pie',
                                                          name: 'Redeem Count',
                                                          data: 
                                                              /*['10% off on Absolute Green Apple',   45.0],
                                                              ['4 shots @ $20',       26.8],                      
                                                              ['Corona Beer at 20% off',    8.5],
                                                              ['Chivas Regal at 15% off',     6.2],
                                                              ['Tuborg crate at 10% off',   0.7]*/
                                                              JSON.parse("[" + msg + "]")
                                                          
                                                      }]
                                                });
                                              },
                                              error: function(msg) {
                                                alert('Something went wrong.Try Again!');
                                                $('#EmptyDiv').show();
                                              }
                                        });                         
                                        //location.href="generatechart?charttype=<?php //echo $_GET['charttype']?>&point="+date+"&category=<?php //echo $category?>&criteria="+crt;     
                                    }
                                }
                            },

                        }
                    },

                    series: [{
                        name: 'Campaign',
                        color: '#2EC9E7',
                        data: [
                        <?php 
                        if(!empty($campaigns))
                        {
                          foreach ($campaigns as $key => $value) { ?>
                            <?php echo $value; ?>,  
                        <?php }            
                        }?>]
                    }]
              });
              <?php }else{?>
               $('#CampaignContainer').html('No Data Available !');
              <?php }?>              

        });//Document.ready close
    });//function close
  </script>
</head>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer">
<div class="white-bg clearfix">
    <div class="col-md-12 mt20">
        <div id="CampaignContainer"></div>        
    </div>
    <div class="col-md-12 mt20">
        <div id="Agecontainer" style="display:none;"></div>
        <div id="EmptyDiv" style="display:none;"><label> No Data To Display !!</label></div>
    </div>

</div>
<div>
  <a href="ReportDashboard" class="btn btn-primary"> Back </a>
</div>
</div>
</html>








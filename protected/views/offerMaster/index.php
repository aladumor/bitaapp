<?php
/* @var $this OfferMasterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Offer Masters',
);

$this->menu=array(
	array('label'=>'Create OfferMaster', 'url'=>array('create')),
	array('label'=>'Manage OfferMaster', 'url'=>array('admin')),
);
?>

<h1>Offer Masters</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>

<script type="text/javascript" src="<?php echo Yii::app()->getBaseUrl(true).'/tinymce/js/tinymce/tinymce.min.js';?>"></script>
<script type="text/javascript">
tinymce.init({
    selector: "textarea",
    fontsize_formats: "8pt 10pt 12pt 14pt 18pt 24pt 36pt",
    plugins: [
        "advlist autolink lists link image charmap print preview anchor",
        "searchreplace visualblocks code fullscreen",
        "insertdatetime media table contextmenu paste"
    ],
    toolbar: "insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image|sizeselect | fontselect |  fontsizeselect"
});
</script>
<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */
/* @var $form CActiveForm */
?>
<style type="text/css">
</style>
<script src="http://jqueryvalidation.org/files/dist/additional-methods.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#offer-master-form').validate({
        rules: {
                    'OfferMaster[cm_id]': {
                        required: true,
                        
                    },
                    'OfferMaster[offer_name]' :
                    {
                        required: true,
                    },
                     'OfferMaster[lt_id]':
                    {
                        required: true,
                    },
                    'OfferMaster[u_id][]' :
                    {
                        required: true,
                    },
                    'OfferMaster[offer_desc]':
                    {
                        required: true,
                    },
                    'OfferMaster[start_date]':
                    {
                        required: true,
                    },
                    'OfferMaster[end_date]':
                    {
                        required: true,
                    },
                    'OfferMaster[maximum_redemption]':
                    {
                        required: true,
                        digits: true,
                    },
                    'OfferMaster[offer_image]': 
                    {
                        required:true,
                        extension: "jpeg|jpg|gif|png",
                    },
                    'OfferMaster[reimbursement_type]' :
                    {
                        required:true,
                    },
                    'OfferMaster[product_price]' :
                    {
                        required: true,
                        digits: true,
                    },
                    'OfferMaster[discounted_price]' :
                    {
                        required: true,
                        digits: true,
                    },
                    'OfferMaster[pm_id]':
                    {
                        required: true,
                    },
                    'merchants[]':
                    {
                        required: true,
                    },
                    'OfferMaster[offer_type]':
                    {
                        required: true,
                    }

                },
        messages: {
                    'OfferMaster[cm_id]': 
                    {
                        required: 'Please select Campaign.',
                        
                    },
                    'OfferMaster[offer_name]' :
                    {
                        required: 'Please enter Offer Name.',
                    },
                     'OfferMaster[lt_id]':
                    {
                        required: "Please select Liquor Filter.",
                    },
                    'OfferMaster[u_id][]' :
                    {
                        required: 'Please select Atleast One City.',
                    },
                    'OfferMaster[offer_desc]':
                    {
                        required: 'Please enter Offer Description.',
                    },
                    'OfferMaster[start_date]':
                    {
                        required: 'Please select Start Date.',
                    },
                    'OfferMaster[end_date]':
                    {
                        required: 'Please select End Date.',
                    },
                    'OfferMaster[maximum_redemption]':
                    {
                        required: 'Please enter Maximum Redemption Value.',
                        digits: 'Please enter Numeric Redemption Value.',
                    },
                    'OfferMaster[offer_image]': 
                    {
                        required:'Please Upload Offer Image.',
                        extension: 'Please enter valid Image file.Allowed Formats are JPEG or PNG.'
                    },
                    'OfferMaster[reimbursement_type]' :
                    {
                        required:'Please select Reimbursement Type.',
                    },
                    'OfferMaster[product_price]' :
                    {
                        required: 'Please enter Product Price.',
                        digits: 'Please enter Numeric value for Product Price.',
                    },
                    'OfferMaster[discounted_price]' :
                    {
                        required: 'Please enter Reimbursement Price.',
                        digits: 'Please Enster Numeric value for Reimbursement Price.',
                    },
                    'OfferMaster[pm_id]':
                    {
                        required: 'Please select Product.',
                    },
                    'merchants[]':
                    {
                        required: 'Please select Atleast One Establishment.',
                    },
                    'OfferMaster[offer_type]':
                    {
                        required: 'Please select Offer Type.',
                    }

                },
            errorPlacement: function(error, element) {
                $("#errorplace").show();
                error.appendTo($("#errorplace"));
            },
            success: function(label) { 
                if($("#errorplace").text() == ''){
                    $("#errorplace").hide();
                }
                $($("#errorplace").children()).each(function(){
                    if($(this).text()==''){
                        $(this).remove();
                    }
                })
            }
        
        });

     $('#select_all').click( function()
     {
           
            $('#city_dd option').prop('selected', true);  
            merchantDropdwn();          
     });
      $('#select_all_merchants').click( function()
     {           
            $('#merchant_dd option').prop('selected', true);  
               
     });
       

});
</script>

<script type="text/javascript">
    
    function checkSize(max_img_size, obj)
    {         
        var input = obj;
        // check for browser support (may need to be modified)
        if (input.files && input.files.length == '1')
        {
            if (input.files[0].size > max_img_size)
            {
                alert("The file must be less than 2MB");//(max_img_size / 1024 / 1024)
                return false;
            }
            else
            {
                PreviewImage(obj);
                return true;
            }
        } 
    }
    function PreviewImage(obj) 
    {
        var oFReader = new FileReader();
        //alert();
        oFReader.readAsDataURL(obj.files[0]);
        oFReader.onload = function(oFREvent) 
        {
            $('.prvwUpload').show();
            $(obj).parent().find('.prvwUpload img').attr('src', oFREvent.target.result);           
        };
    }
    
    $(document).ready(function() 
    {
        <?php if (!$model->isNewRecord) {
            ?>
                    cityDropdwn();                  
                    $(document).on("change",$("#city_dd"),function(){
                       
                    });                    
                    $('#merchantDrpDwndiv').show(); 
                    $('#cityDrpDwndiv').show()
                    $('.imagefileclass').hide();
                    $('.prvwUpload').show();

        <?php } else { ?>
                    $('#merchantDrpDwndiv').hide();
                    $('#cityDrpDwndiv').hide();
                    $('.imagefileclass').show();
                    $('.prvwUpload').hide();
                    
        <?php } ?>
        
    });

    function merchantDropdwn()
    {
       // setTimeout(5000);
        var val=$("#city_dd" ).val();       
       // alert(val);
        <?php if (!$model->isNewRecord) {?>
            var id=<?php echo $model->o_id;        
        }else {?>
            var id='';
        <?php }?>

        var urlstr="<?php echo Yii::app()->createUrl('offerMaster/getMerchants?')?>";
         $.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {cid: val,oid:id},
          success: function(msg) { 
                if(msg!='')  
                {
                    $('#merchantDrpDwndiv').show();
                    $('#merchantdiv').html('');
                    $('#merchantdiv').html(msg);
                }
                else
                {                    
                    $('#merchantDrpDwndiv').hide();
                    alert('No BevRage Merchants For this Location !');
                }
                
          },
          error: function(msg) {
              alert('Something went wrong.Try Again!')
          }
        });
    }
    function getredeemtype () {
         var val=$( "#OfferMaster_offer_type" ).val();
         <?php if (!$model->isNewRecord) {?>
             $('#redeemType').show();
            var id=<?php echo $model->o_id; 

        }else {?>
            var id='';
            if(val=='1')
                $('#redeemType').show();
            else
                $('#redeemType').hide();
        <?php }?>
    }
    function cityDropdwn()
    {
        var val=$( "#OfferMaster_offer_type" ).val();
        
        
        <?php if (!$model->isNewRecord) {?>
            $('#redeemType').show();
            var id=<?php echo $model->o_id; 

        }else {?>
            var id='';
            /*if(val=='1')
                $('#redeemType').show();
            else
                $('#redeemType').hide();*/
        <?php }?>
       var state=$('#stateDrpDwn').val();
        var urlstr="<?php echo Yii::app()->createUrl('offerMaster/getcities?')?>";
         $.ajax({
          type: "POST",
          url: urlstr,
          dataType: 'html',
          data: {type: val,oid:id,state:state},
          success: function(msg) { 
                if(msg!='')  
                {
                    $('#cityDrpDwndiv').show();
                    $('#citydiv').html('');
                    $('#citydiv').html(msg);
                    <?php if (!$model->isNewRecord){ ?>
                        merchantDropdwn();
                    <?php }?>
                }
                else
                {                    
                    $('#cityDrpDwndiv').hide();
                    alert('No BevRage Cities for selected offer Type!');
                }
                
          },
          error: function(msg) {
              alert('Something went wrong.Try Again!')
          }
        });
    }

    function removeImage()
    {
        $('#imagepreview').remove();
        $('#imgremovebtn').remove();
        $('.imagefileclass').show();
    }

</script>

<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer form-tbl">

<div class="form dataTables_wrapper clearfix">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'offer-master-form',
        'enableAjaxValidation' => false,
        'htmlOptions' => array('enctype' => 'multipart/form-data'),
    ));
    ?>
     <div id="errorplace" class="errorSummary" style="display:none;"></div>
    <p class="note">Fields with <span class="required">*</span> are required.</p>
            <?php //echo $form->errorSummary($model); ?>
    <div class="col-md-6">
        <div class="form-group row col-md-12">
            <?php echo $form->labelEx($model, 'cm_id'); ?>
            <?php
            if (!$model->isNewRecord)
                $selected = $model->cm_id;
            else
                $selected = 0;
            
            $session = new CHttpSession;
            $session->open();
            $criteria = new CDbCriteria;
            if($session["rid"]==2)
                $criteria = "is_active=0 and is_delete=0 and u_id=" . $session["uid"];                            
            else
                $criteria = "is_active=0 and is_delete=0 ";   
            $static = array(
                '' => '-- Select Campaign --',
                '0' => 'None'
            );
            echo CHtml::activeDropDownList($model, 'cm_id', $static + CHtml::listData(CampaignMaster::model()->findAll($criteria), 'cm_id', 'campaign_name'), array(/*'class'=>'select-search-hide width100p',*/'options'=>array($selected=>array('selected'=>'selected')),)
            );?>        
            <?php // echo $form->error($model, 'cm_id'); ?>
        </div>  

        <div class="form-group row col-md-12">
            <?php echo $form->labelEx($model, 'offer_name'); ?>
            <?php echo $form->textField($model,'offer_name',array('size'=>60,'maxlength'=>200, 'class'=>'form-control mb15 width100p')); ?>
            <?php //echo $form->error($model, 'offer_name'); ?>
        </div>

        <div class="form-group row col-md-12">
            <?php echo $form->labelEx($model, 'pm_id'); ?>
            <?php
            if (!$model->isNewRecord)
                $selected = $model->pm_id;
            else
                $selected = 0;
            $session = new CHttpSession;
            $session->open();
            $criteria = new CDbCriteria;
            if($session["rid"]==2)
                $criteria = "is_active=0 and is_delete=0 and created_by=" . $session["uid"];                            
            else
                $criteria = "is_active=0 and is_delete=0 ";     
            $static = array(
                '' => '-- Select Product --',
            );
            echo CHtml::activeDropDownList($model, 'pm_id', $static + CHtml::listData(ProductMaster::model()->findAll($criteria), 'pm_id', 'product_name'), array(/*'class'=>'select-search-hide width100p',*/'options'=>array($selected=>array('selected'=>'selected')),)
            );
            ?>
            <?php //echo $form->error($model, 'pm_id'); ?>
        </div>

        <div class="form-group row col-md-12">
        
            <?php echo $form->labelEx($model, 'offer_type'); ?>
            <?php echo $form->dropDownList($model,'offer_type',array(''=>'-- Select Type--',"1"=>"On Premise","2"=>"Off Premise"),array('onChange'=>'getredeemtype()')); ?>           
            <?php //echo $form->error($model, 'offer_type'); ?>
        </div>

        <div id="redeemType" class="form-group row col-md-12" style="display:none;">        
            <?php echo $form->labelEx($model, 'redeem_type'); ?>
            <?php echo $form->dropDownList($model,'redeem_type',array(''=>'-- Select Type--',"1"=>"QR Code","2"=>"Receipt")); ?>           
            <?php //echo $form->error($model, 'offer_type'); ?>
        </div>
        <!-- load offer based city -->
        <div id="stateDrpDwndiv" >
           
            <div id="statediv" class="form-group row col-md-12">
            <label>State<span class="error">*</span></label>
            <?php $criteria = new CDbCriteria;  
            if (!$model->isNewRecord)
                $selected = $model->state_id;
            else
                $selected = 0;          
            $static = array(
                '' => '-- Select state --',
            );
            echo CHtml::activeDropDownList($model, 'state_id', 
                $static + CHtml::listData(BevrageCities::model()->findAll($criteria), 'state_code', 'state_code'),
                array('name'=>'stateDrpDwn','onchange'=>'cityDropdwn()','options'=>array($selected=>array('selected'=>'selected'))))
            ?>
            </div>            
        </div>
        
        <div id="cityDrpDwndiv" >
            <div class="form-group row col-md-12">
                <label>Offer Location</label> <input type="button" id="select_all" name="select_all"  class="btn-sm btn-info" value="Select All">
            </div>
            <div id="citydiv" class="form-group row col-md-12">

            </div>            
        </div>
        

        <!-- load city based merchants-->
        <div id="merchantDrpDwndiv">
            <div class="form-group row col-md-12">
            <label>Select Participating Bars/LiquoreStore :</label><input type="button" id="select_all_merchants"  class="btn-sm btn-info" name="select_all_merchants" value="Select All">
            </div>
            <div id="merchantdiv" class="form-group row col-md-12">

            </div>            
        </div>
    <!-- END  -->
        
    </div>
    <div class="col-md-6">
   
    <div class="form-group row col-md-12">
        <div class="row col-md-5 date-field">
            <?php echo $form->labelEx($model,'start_date'); ?>
        
        <?php 
        if(!$model->isNewRecord)
        {
            $model->start_date=date('m/d/y',strtotime($model->start_date));
        }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                           'model'=>$model,
                           'name'=>'OfferMaster[start_date]', 
                           'value'=>$model->start_date,
                           'options'=>array(       
                                        'dateFormat' => 'mm-dd-yy', 
                                        //'showButtonPanel'=>true,
                                        'showAnim'=>'slide',
                                        'minDate'=>-3,
                                        'size'=>10,
                           ),
                           'htmlOptions'=>array(
                                        'style'=>'',
                                        'size'=>8, 
                                        'class'=>'width100p form-control',
                                        ),
                           ));
        ?>
        <?php // echo $form->error($model,'start_date'); ?>
        </div>
        <div class="col-md-5 row date-field">
        <?php echo $form->labelEx($model,'end_date'); ?>
        <?php 
        if(!$model->isNewRecord)
        {
            $model->end_date=date('m/d/y',strtotime($model->end_date));
        }
        $this->widget('zii.widgets.jui.CJuiDatePicker',array(
                           'model'=>$model,
                           'name'=>'OfferMaster[end_date]', 
                           'value'=>$model->end_date,
                           'options'=>array(       
                                        'dateFormat' => 'mm-dd-yy', 
                                        //'showButtonPanel'=>true,
                                        'showAnim'=>'slide',
                                        'minDate'=>-3,
                                        'size'=>10,
                           ),
                           'htmlOptions'=>array(
                                        'style'=>'',
                                        'size'=>8,
                                        'class'=>'width100p form-control',
                                        ),
                           ));
        ?>
        <?php //echo $form->error($model,'end_date'); ?>
    </div>
    </div>

    <div class="form-group row col-md-12">
        <?php echo $form->labelEx($model,'maximum_redemption'); ?>
        <?php echo $form->textField($model,'maximum_redemption',array('class'=>'form-control mb15 width100p')); ?>
        <?php //echo $form->error($model,'maximum_redemption'); ?>
    </div>
    <div class="form-group row col-md-12">        
            <?php echo $form->labelEx($model, 'lt_id'); ?>
            <?php if (!$model->isNewRecord)
                $selected = $model->lt_id;
            else
                $selected = 0;
            
            $session = new CHttpSession;
            $session->open();
            $criteria = new CDbCriteria;
            $criteria = "is_active=0";   
            $static = array(
                '' => '-- Select Filter --',
                
            );
            echo CHtml::activeDropDownList($model, 'lt_id', $static + CHtml::listData(LiquorType::model()->findAll($criteria), 'lt_id', 'liquor_type'), array(/*'class'=>'select-search-hide width100p',*/'options'=>array($selected=>array('selected'=>'selected')),)
            );?>     
            <?php //echo $form->error($model, 'offer_type'); ?>
    </div>
    <div class="form-group row col-md-12">
        <?php echo $form->labelEx($model,'offer_image'); ?><span>Recommended Size: 600x200</span>
        <?php 
            if(!$model->isNewRecord)
            {
                ?>  
                <?php echo CHtml::image(Yii::app()->request->baseUrl.'/images/offer_images/'.$model->offer_image,"offer_image",array("width"=>50 ,'height'=>50 )); ?>           
                <a id="imgremovebtn" onclick="removeImage()">Change</a>
            <?php
            }?>
                                <input id="" type="hidden" name="offer_image" value="">
                <div class='imagefileclass'>
                <?php echo $form->fileField($model, 'offer_image', array('size' => 60, 'maxlength' => 200, "onChange" => "return checkSize(2000000, this)")); ?>
                <div class="prvwUpload"><img src="<?php echo Yii::app()->request->baseUrl; ?>/images/preview-image.jpg" alt="" title="Preview image" height="120" width="120" /></div>                                                                
                </div>
            <?php ///echo $form->error($model,'offer_image'); ?>
    </div>
        
   <!--  <div class="form-group row col-md-12">        
        <?php// echo $form->labelEx($model, 'reimbursement_type'); ?>
        <?php// echo $form->textField($model,'reimbursement_type',array('0'=> 'Select Type',"1"=>"Cash","2"=>"Merchandise")); ?>           
        <?php //echo $form->error($model, 'reimbursement_type'); ?>
    </div>  -->
    <?php echo $form->hiddenField($model,'reimbursement_type',array('value'=>1)); ?> 
    <div class="form-group row col-md-12">
        <?php echo $form->labelEx($model,'discounted_price'); ?>
        <?php echo $form->textField($model,'discounted_price',array('class'=>'form-control mb15 width100p','placeholder'=>'e.g : 4')); ?>
        <?php //echo $form->error($model,'discounted_price'); ?>
    </div>
   

</div>
    <div class="form-group row col-md-12">
       <?php echo $form->labelEx($model, 'offer_desc'); ?>
       <?php echo $form->textArea($model,'offer_desc',array('rows'=>6, 'cols'=>50)); ?>
       <?php //echo $form->error($model, 'offer_desc'); ?>
   </div> 
    <div class="col-md-12 buttons">
        <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-primary btn-lg mr5')); ?>
    </div>
    </div>

    <!-- form -->    
    
<?php $this->endWidget(); ?>
<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */

$this->breadcrumbs=array(
	'Offers'=>array('admin'),
	//$model->o_id=>array('view','id'=>$model->o_id),
	'Update Offer',
);

$this->menu=array(
	//array('label'=>'List OfferMaster', 'url'=>array('index')),
	//array('label'=>'Create OfferMaster', 'url'=>array('create')),
	//array('label'=>'View OfferMaster', 'url'=>array('view', 'id'=>$model->o_id)),
	array('label'=>'Manage Offers', 'url'=>array('admin')),
);
?>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
<?php 
$this->breadcrumbs=array(
    'Reports'=>array('ReportDashboard'),
    'Campaign Report',
);

$session=new CHttpSession;
$session->open();
$uid=$session['uid'];
?>
<!---------------------- HIGH CHARTS ------------>
<!DOCTYPE html>
<html>
<head>
    <style>
        .ac_heading{
            display:none !important;
            
        }
        
        .content{
        display:none !important;
        }
    </style>
  <title></title>
  <script src="http://code.highcharts.com/highcharts.js"></script>
  <script src="http://code.highcharts.com/modules/exporting.js"></script>
  <script src="http://highslide-software.github.io/export-csv/export-csv.js"></script>
  <script type="text/javascript">

    $(function () {

        $(document).ready(function () {

              Highcharts.setOptions({
               colors: ['#2EC9E7', '#33a3b7', '#65d8ed', '#98d7e2', '#afd4db','#AFD4DB', '#93a9ad', '#a7b9bc']
              });
              // Build the chart
             
              
              // Build Location Bar  chart
              $('#CampaignContainer').highcharts({
                    chart: {
                    type: 'column',
                    backgroundColor:'transparent',
                    },
                    title: {
                        text: 'Location Based Redemptions'
                    },
                    /*subtitle: {
                        text: 'Click on any Campaign Column to view it\'s offer Report'
                    },*/
                    xAxis: {
                        categories: [
                            'New York',
                            'Dallas',
                            'Houston',
                            'Miami',
                            'Chicago',
                            'Los Angeles',
                            'Las Vegas',
                            'Orlando',
                            
                        ],
                        crosshair: true
                    },
                    yAxis: {
                        min: 0,
                        title: {
                            text: 'Offer Count'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">Offer Count: </td>' +
                            '<td style="padding:0"><b>{point.y:.1f}</b></td></tr>',
                        footerFormat: '</table>',
                        shared: true,
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        },
                        series: {
                            cursor: 'pointer',
                            point: {
                                events: {
                                    click: function () {                                        
                                        var campaign = this.category;                                                                              
                                    }
                                }
                            },

                        }
                    },

                    series: [{
                        name: 'Redemptions',
                        color: '#2EC9E7',
                        data: [49.0, 71.0, 106.0, 129.0, 144.0, 176.0, 135.0, 148.0]
                    }]
              });
                            

        });//Document.ready close
    });//function close
  </script>
</head>
<div class="grid-view dataTables_wrapper form-inline dt-bootstrap no-footer">
<div class="white-bg clearfix">
    <div class="col-md-12 mt20">
        <div id="CampaignContainer"></div>        
    </div>
    <div class="col-md-12 mt20">
        <div id="Agecontainer" style="display:none;"></div>
        <div id="EmptyDiv" style="display:none;"><label> No Data To Display !!</label></div>
    </div>
</div>
</div>
</html>








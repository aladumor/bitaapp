<?php
/* @var $this OfferMasterController */
/* @var $model OfferMaster */

$this->breadcrumbs=array(
	'Offers'=>array('admin'),
	'Manage Offers',
);

$this->menu=array(
	//array('label'=>'List OfferMaster', 'url'=>array('index')),
	array('label'=>'Create Offer', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#offer-master-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<html>
<head>
	<style>
	.ac_heading{
		display:none !important;
		
	}
	
	.content{
	display:none !important;
	}
	</style>
	<script>
$('#offer-master-grid').removeClass('.dataTables_wrapper');
    </script>
	</head>
</html>

<!-- <p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
 -->
<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->
<div class="dataTables_wrapper">
	
    <label class="ml20"><h3 class="media-heading">Filter By :</h3></label>
    <a class="btn btn-primary" href="admin?order='0'">Active offers</a>
    <a class="btn btn-primary" href="admin?order=1">Expired Offers</a>
    <a class="btn btn-primary"  href="admin?order=">All Offers</a>
    <?php 
        $session=new CHttpSession;
        $session->open();
        if($session['rid']!=1)
        {
    ?>
    <a class="btn btn-green pull-right offer-btn" href="create"><i class="fa fa-plus"></i>&nbsp;Create Offer</a>
    <?php }?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'offer-master-grid',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table table-striped table-bordered responsive',
	'filter'=>$model,
	'columns'=>array(
        array(
                    'type'=>'raw',
                    'header'=>'Brand',
                    'value' =>'offerMaster::model()->getBrand($data->u_id)'
             ),
		/*array(
            'header' => 'Campaign',
            'name' => 'cm_id',
            'value' => '($data->cm_id == "0") ? "None" : "{$data->campaign->campaign_name}"'
        ),	*/	
		'user.user_name',       
        'product.product_name',
		'offer_name',
		'maximum_redemption',
		array(
            'header' => 'Redemptions to Date ',
            'type'=>'raw',
         'value' =>'offerMaster::model()->getRedemptioncount($data->o_id)'
        ),
        array(     
            'header' => 'Time Remaining',
            'type'=>'raw',
            'value' =>'offerMaster::model()->getTime($data->o_id)'
        ),  
		/*array(
                    'type'=>'raw',
                    'header'=>'Offer Image',
                    'value' =>'offerMaster::model()->getimage($data->offer_image)'
             ),		*/
		/*array(
            'header' => 'Loyalty Offer',
            'name' => 'loyalty_offer_id',
            'value' => '($data->loyalty_offer_id == "1") ? "No" : "Yes"'
        ),*/
		array(
            'header' => 'Is Active',
            'name' => 'is_active',
            'value' => '($data->is_active == "0") ? "Active" : "Inactive"'
        ),
        array(
        'class'=>'CLinkColumn',
        'label'=> 'Change',
        'urlExpression'=>'"changeStatus?id=".$data->o_id."&status=".$data->is_active',
        'header'=>'Change Status'
      	),
        array(
        'class'=>'CLinkColumn',
        'label'=>'View Report',
        'urlExpression'=>'"GenerateOfferReports?o_id=".$data->o_id."&offer_type=".$data->offer_type."&offer_name=".$data->offer_name."&start_date=".$data->start_date."&end_date=".$data->end_date',
        'header'=>'Analytics'
      ),

		array(
            'class'=>'CButtonColumn',
            'template'=>'{view}{update}',
            'buttons'=>array(
                   'view'=>array(
                            'label'=>'<i class="fa fa-eye"></i>',
                            'imageUrl'=>false,
                    ),
                     'update'=>array(
                            'label'=>'<i class="fa fa-edit"></i>',
                            'imageUrl'=>false,
                    ),
                    /*'delete'=>array(
                            'label'=>'<i class="fa fa-remove"></i>',
                            'imageUrl'=>false,
                    ),*/
            )
        )

	),
)); ?>
</div>

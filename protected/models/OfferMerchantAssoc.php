<?php

/**
 * This is the model class for table "offer_merchant_assoc".
 *
 * The followings are the available columns in table 'offer_merchant_assoc':
 * @property integer $om_id
 * @property integer $o_id
 * @property integer $m_id
 */
class OfferMerchantAssoc extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OfferMerchantAssoc the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offer_merchant_assoc';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('o_id, m_id', 'required'),
			array('o_id, m_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('om_id, o_id, m_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'om_id' => 'Om',
			'o_id' => 'O',
			'm_id' => 'M',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('om_id',$this->om_id);
		$criteria->compare('o_id',$this->o_id);
		$criteria->compare('m_id',$this->m_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	 public function getDetails($oid,$type)
    {
    	$connection = Yii::app()->db;
    	if($type=="merchant")	   		
    	{
    		$col="m_name";
    	}
    	else
    	{
    		$col="establishment_name";
    	}
   		$cmd="SELECT ".$col." from merchant_master where m_id in (SELECT m_id from offer_merchant_assoc where o_id =".$oid.")";
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{
			return $rows[0][$col];
		}
		else
		{
			return "Not available";
		}
    }
}
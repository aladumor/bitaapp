<?php

/**
 * This is the model class for table "feedback_master".
 *
 * The followings are the available columns in table 'feedback_master':
 * @property integer $f_id
 * @property integer $u_id
 * @property string $message
 * @property string $date
 */
class FeedbackMaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        public $UserName;
	public function tableName()
	{
		return 'feedback_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('u_id, message, date', 'required'),
			array('u_id', 'numerical', 'integerOnly'=>true),
			array('message', 'length', 'max'=>500),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UserName,f_id, u_id, message, date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'UserMaster' =>array(self::BELONGS_TO,'UserMaster','u_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'f_id' => 'F',
			'u_id' => 'U',
			'message' => 'Message',
			'date' => 'Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
                $criteria->with = array('UserMaster');

		$criteria->compare('f_id',$this->f_id);
		$criteria->compare('u_id',$this->u_id);
		$criteria->compare('message',$this->message,true);
		$criteria->compare('date',$this->date,true);
                $criteria->compare('UserMaster.user_name',$this->UserName,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FeedbackMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

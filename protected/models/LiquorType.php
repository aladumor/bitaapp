<?php

/**
 * This is the model class for table "liquor_type".
 *
 * The followings are the available columns in table 'liquor_type':
 * @property integer $lt_id
 * @property string $liquor_type
 * @property integer $is_active
 *
 * The followings are the available model relations:
 * @property OfferMaster[] $offerMasters
 */
class LiquorType extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LiquorType the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'liquor_type';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('is_active', 'numerical', 'integerOnly'=>true),
			array('liquor_type', 'required'),
			array('liquor_type', 'length', 'max'=>50),
			array('liquor_type', 'unique','className' => 'LiquorType',
								        'attributeName' => 'liquor_type',
								        'message'=>'Filter Name Already Exists.'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('lt_id, liquor_type, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'offerMasters' => array(self::HAS_MANY, 'OfferMaster', 'lt_id'),
		);
	}
	public function behaviors() {
        return array(
                'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                ),
        );
	}
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'lt_id' => 'Lt',
			'liquor_type' => 'Liquor Type Name',
			'is_active' => 'Is Active',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('lt_id',$this->lt_id);
		$criteria->compare('liquor_type',$this->liquor_type,true);
		$criteria->compare('is_active',$this->is_active);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
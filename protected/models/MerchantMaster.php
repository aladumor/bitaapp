<?php

/**
 * This is the model class for table "merchant_master".
 *
 * The followings are the available columns in table 'merchant_master':
 * @property integer $m_id
 * @property string $m_name
 * @property string $establishment_name
 * @property string $m_street
 * @property integer $m_zipcode
 * @property integer $m_city
 * @property integer $phone
 * @property string $m_lat
 * @property string $m_long
 * @property integer $is_active
 */
class MerchantMaster extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MerchantMaster the static model class
	 */
	public $city_search;
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'merchant_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('m_name, establishment_name, m_street, m_zipcode, m_city,m_state, merchant_type', 'required','on'=>'uplaodform'),
			array('m_zipcode, m_city,is_active', 'numerical', 'integerOnly'=>true,'on'=>'uplaodform'),
			array('m_name, m_street', 'length', 'max'=>50,'on'=>'uplaodform'),	

			array('m_name', 'file','types'=>'csv', 'on'=>'uplaodcsv','message'=>'Invalid Format'),	

			array('establishment_name', 'length', 'max'=>100,'on'=>'uplaodform'),
			array('m_lat, m_long', 'length', 'max'=>20,'on'=>'uplaodform'),
			/*array('m_name', 'unique','className' => 'MerchantMaster',
								        'attributeName' => 'm_name',
								        'message'=>'Merchant Already Exists.','on'=>'uplaodform'),*/
			array('establishment_name', 'UniqueAttributesValidator', 'with'=>'m_name,m_city,merchant_type','on'=>'uplaodform'),

			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('m_id, m_name,city_search, establishment_name, m_street, m_zipcode, m_city, phone, m_lat, m_long, is_active', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'BevrageCities' => array(self::BELONGS_TO, 'BevrageCities', 'm_city'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'm_id' => 'M',
			'm_name' => 'Merchant Name',
			'establishment_name' => 'Establishment Name',
			'm_street' => 'Block no / Street Name',
			'm_zipcode' => 'Zipcode',
			'm_city' => 'City',
			'phone' => 'Conatct No',
			'm_lat' => 'M Lat',
			'm_long' => 'M Long',
			'is_active' => 'Is Active',
			'm_state'=>'State',
			'created_by'=>'created_by',
			'merchant_type'=>'Establishment Category'
		);
	}
	public function behaviors() {
        return array(
                'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                ),
        );
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		$session=new CHttpSession;
        $session->open();
		$criteria=new CDbCriteria;

		$criteria->with = array( 'BevrageCities' );
		$criteria->compare('m_id',$this->m_id);
		$criteria->compare('m_name',$this->m_name,true);
		$criteria->compare('establishment_name',$this->establishment_name,true);
		$criteria->compare('m_street',$this->m_street,true);
		$criteria->compare('m_zipcode',$this->m_zipcode);
		$criteria->compare('m_city',$this->m_city);
		$criteria->compare('phone',$this->phone);
		$criteria->compare('m_lat',$this->m_lat,true);
		$criteria->compare('m_long',$this->m_long,true);
		$criteria->compare('is_active',0);//$this->is_active
		$criteria->compare('m_state',$this->m_state);
		$criteria->compare('merchant_type',$this->merchant_type);
		//$criteria->compare('created_by',$this->created_by);
		$criteria->compare( 'BevrageCities.city_name', $this->city_search, true );
		$criteria->order='m_name';

		if($session['rid']==2)
			$criteria->compare('created_by', array($session['uid'],23));
		else
			$criteria->compare('created_by',$this->created_by);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'pagination'=>array(
		        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
		    ),
			'sort'=>array(
		        'attributes'=>array(
		            'city_search'=>array(
		                'asc'=>'BevrageCities.city_name',
		                'desc'=>'BevrageCities.city_name DESC',
		            ),
		            '*',
		        ),
		    ),
		));
	}
}
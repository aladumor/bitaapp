<?php

/**
 * This is the model class for table "campaign_master".
 *
 * The followings are the available columns in table 'campaign_master':
 * @property integer $cm_id
 * @property string $campaign_name
 * @property integer $u_id
 * @property integer $b_id
 * @property integer $is_active
 * @property integer $is_delete
 */
class CampaignMaster extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CampaignMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'campaign_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('campaign_name', 'required'),
			array('cm_id, u_id, b_id, is_active, is_delete', 'numerical', 'integerOnly'=>true),
			array('campaign_name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cm_id, campaign_name, u_id, b_id, is_active, is_delete', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserMaster', 'u_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cm_id' => 'Cm',
			'campaign_name' => 'Campaign Name',
			'u_id' => 'Created By',
			'b_id' => 'Brand',
			'is_active' => 'Is Active',
			'is_delete' => 'Is Delete',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$session = new CHttpSession;
		$session->open();

		$criteria=new CDbCriteria;

		if($session["rid"] != '1')
		{
			$criteria->compare('u_id',$session["uid"]);
		}
		else
		{
			$criteria->compare('cm_id',$this->cm_id);
			$criteria->compare('campaign_name',$this->campaign_name,true);
			$criteria->compare('u_id',$this->u_id);
			$criteria->compare('b_id',$this->b_id);
			$criteria->compare('is_active',$this->is_active);
			$criteria->compare('is_delete',$this->is_delete);
		}

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
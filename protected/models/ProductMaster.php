<?php

/**
 * This is the model class for table "product_master".
 *
 * The followings are the available columns in table 'product_master':
 * @property integer $pm_id
 * @property string $product_name
 * @property string $upc_code
 * @property integer $is_active
 * @property integer $is_delete
 * @property integer $created_by
 */
class ProductMaster extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ProductMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_name, upc_code', 'required'),
			array('is_active, is_delete, created_by', 'numerical', 'integerOnly'=>true),
			array('product_name, upc_code', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('pm_id, product_name, upc_code, is_active, is_delete, created_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'pm_id' => 'Pm',
			'product_name' => 'Product Name',
			'upc_code' => 'Code',
			'is_active' => 'Is Active',
			'is_delete' => 'Is Delete',
			'created_by' => 'Created By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$session = new CHttpSession;
		$session->open();

		$criteria=new CDbCriteria;

		if($session["rid"] != '1')
		{
			$criteria->compare('created_by',$session["uid"]);
		}
		else
		{

			$criteria->compare('pm_id',$this->pm_id);
			$criteria->compare('product_name',$this->product_name,true);
			$criteria->compare('upc_code',$this->upc_code,true);
			$criteria->compare('is_active',$this->is_active);
			$criteria->compare('is_delete',$this->is_delete);
			$criteria->compare('created_by',$this->created_by);
		}

		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}
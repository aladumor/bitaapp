<?php

/**
 * This is the model class for table "offer_transaction_master".
 *
 * The followings are the available columns in table 'offer_transaction_master':
 * @property integer $ot_id
 * @property integer $o_id
 * @property string $redeem_by
 * @property string $redeem_date
 * @property integer $total_amount
 * @property integer $u_id
 * @property string $unique_code
 * @property integer $payer_id
 * @property string $type
 * @property integer $merchant_amount
 * @property integer $brand_partner_amount
 * @property integer $customer_amount
 * @property string $payment_status
 * @property integer $transaction_id
 * @property string $transaction_time
 */
class OfferTransactionMaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
        
        public $OfferName;
        public $UserName;
        public $PaymentMethod;
	public function tableName()
	{
		return 'offer_transaction_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('o_id, redeem_by, redeem_date, total_amount, u_id, unique_code, payer_id, type, merchant_amount, brand_partner_amount, customer_amount, transaction_id, transaction_time', 'required'),
			array('o_id, total_amount, u_id, payer_id, merchant_amount, brand_partner_amount, customer_amount, transaction_id', 'numerical', 'integerOnly'=>true),
			array('redeem_by', 'length', 'max'=>8),
			array('unique_code', 'length', 'max'=>10),
			array('type', 'length', 'max'=>7),
			array('payment_status', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('UserName,OfferName,PaymentMethod,ot_id, o_id, redeem_by, redeem_date, total_amount, u_id, unique_code, payer_id, type, merchant_amount, brand_partner_amount, customer_amount, payment_status, transaction_id, transaction_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
                    'OfferMaster' => array(self::BELONGS_TO, 'OfferMaster', 'o_id'),
                    'UserMaster' => array(self::BELONGS_TO, 'UserMaster', 'u_id'),
                    'ScanReceiptsMaster' => array(self::HAS_MANY, 'ScanReceiptsMaster', 'o_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ot_id' => 'Ot',
			'o_id' => 'O',
			'redeem_by' => 'Redeem By',
			'redeem_date' => 'Redeem Date',
			'total_amount' => 'Total Amount',
			'u_id' => 'U',
			'unique_code' => 'Unique Code',
			'payer_id' => 'Payer',
			'type' => 'Type',
			'merchant_amount' => 'Merchant Amount',
			'brand_partner_amount' => 'Brand Partner Amount',
			'customer_amount' => 'Customer Amount',
			'payment_status' => 'Payment Status',
			'transaction_id' => 'Transaction',
			'transaction_time' => 'Transaction Time',
		);
	}
	public function behaviors() {
        return array(
                'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                ),
        );
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$session = new CHttpSession;
		$session->open();
		$cond=empty($_GET['type'])?NULL:$_GET['type'];
		if($cond==1)
		{	
			$criteria->condition="(payment_status=1 OR customer_payment_status=1)";		
			//$criteria->compare('payment_status',1,true);
			//$criteria->compare('customer_payment_status',1,true,'OR');
		}
		else if($cond=='2')
		{
			$criteria->condition="payment_status=2 OR customer_payment_status=2";	
			//$criteria->compare('payment_status',2);
			//$criteria->compare('customer_payment_status',2);
		}
		else if($cond=='3')
		{
			
			$criteria->condition="payment_status=3 OR customer_payment_status=3";
			/*$criteria->compare('payment_status',3);
			$criteria->compare('customer_payment_status',3);*/
		}
		else if($cond=='0' || empty($type))
		{
			
			$criteria->compare('payment_status',$this->payment_status,true);
			$criteria->compare('customer_payment_status',$this->customer_payment_status,true);
		}


               /*
                * For more than two models we have to add multiple models in one array
                */
                
             //    $criteria->with = array('OfferMaster');
 
		if($session["rid"] != '1')
		{
			$idarray='';
			$connection = Yii::app()->db;	   		
	   		$cmd="SELECT o_id from offer_master where u_id=".$session['uid'];
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			if(!empty($rows))
			{
				foreach ($rows as $key => $value) 
				{

					$idarray .=$value['o_id'].',';
				}
			}
			$idarray=trim($idarray,',');
			$criteria->addInCondition('o_id',array($idarray));	
		}
		else
		{
	       $criteria->with = array('UserMaster','OfferMaster');
	       $criteria->compare('o_id',$this->o_id);
			
		}
			$criteria->compare('ot_id',$this->ot_id);
			
			$criteria->compare('redeem_by',$this->redeem_by,true);
			$criteria->compare('redeem_date',$this->redeem_date,true);
			$criteria->compare('total_amount',$this->total_amount);
			$criteria->compare('u_id',$this->u_id);
			$criteria->compare('unique_code',$this->unique_code,true);
			$criteria->compare('payer_id',$this->payer_id);
			$criteria->compare('type',$this->type,true);
			$criteria->compare('merchant_amount',$this->merchant_amount);
			$criteria->compare('brand_partner_amount',$this->brand_partner_amount);
			$criteria->compare('customer_amount',$this->customer_amount);
			$criteria->compare('payment_status',$this->payment_status,true);
			$criteria->compare('transaction_id',$this->transaction_id);
			$criteria->compare('transaction_time',$this->transaction_time,true);
	        $criteria->compare('OfferMaster.offer_name',$this->OfferName,true);
	        $criteria->compare('UserMaster.user_name',$this->UserName,true);
		if($cond=='desc')
		{
			 $criteria->order='redeem_date DESC, transaction_time DESC ';
		}
 
             //   echo '<pre>';print_r($criteria);die;
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
                         'pagination'=>array('pageSize'=>50)
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return OfferTransactionMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	//Payment stauses
    public function getPaymentStatus($status)
    {
        if($status==1)
            $str="Complete";
        else if ($status==2) 
        {
            $str="Pending";
        }
        else
            $str="Cancel";

        return $str;
    }
    //get email
    public function getEmail($code)
    {
    	$connection = Yii::app()->db;	   		
   		$cmd="SELECT email from user_master where unique_code='".$code."'";
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{
			return $rows[0]['email'];
		}
		else
		{
			return false;
		}
    }
    //get payment mehod
     public function getPaymentMethod($code)
    {
        $connection = Yii::app()->db;	   		
   		$cmd="SELECT active_payment_method_id from user_master where unique_code='".$code."'";
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{
			if($rows[0]['active_payment_method_id']==1) //paypal
			{
				return "PayPal";
			}
			else if($rows[0]['active_payment_method_id']==3) //cheque
			{
				return "Cheque";
			}
			else if($rows[0]['active_payment_method_id']==2)
			{
				return "Bank Transfer";
			}
			else
			{
				return "Not Set";
			}
		}
		else
		{
			return "Not Set";
		}
    }
    //get account name
    public function getAccountName($oid)
    {
    	$connection = Yii::app()->db;	   		
   		$cmd="SELECT name from user_master where u_id IN (SELECT u_id from offer_master where o_id=".$oid.")";
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{
			return $rows[0]['name'];
		}
		else
		{
			return "Not available";
		}
    }
}

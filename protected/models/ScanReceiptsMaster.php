<?php

/**
 * This is the model class for table "scan_receipts_master".
 *
 * The followings are the available columns in table 'scan_receipts_master':
 * @property integer $srm_id
 * @property integer $o_id
 * @property integer $u_id
 * @property integer $date
 * @property integer $receipt_image
 * @property integer $product_image
 * @property integer $ot_id
 */
class ScanReceiptsMaster extends CActiveRecord {

    /**
     * @return string the associated database table name
     */
    public function tableName() {
        return 'scan_receipts_master';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules() {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('o_id, u_id, date, receipt_image, product_image, ot_id', 'required'),
            array('o_id, u_id, date, receipt_image, product_image, ot_id', 'numerical', 'integerOnly' => true),
            // The following rule is used by search().
            // @todo Please remove those attributes that should not be searched.
            array('srm_id, o_id, u_id, date, receipt_image, product_image, ot_id', 'safe', 'on' => 'search'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations() {
        // NOTE: you may need to adjust the relation name and the related
        // class name for the relations automatically generated below.
        return array(
            'OfferTransactionMaster' => array(self::BELONGS_TO, 'OfferTransactionMaster', 'ot_id'),
            'OfferMaster' => array(self::BELONGS_TO, 'OfferMaster', 'o_id'),
            'UserMaster' => array(self::BELONGS_TO, 'UserMaster', 'u_id'),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels() {
        return array(
            'srm_id' => 'Srm',
            'o_id' => 'O',
            'u_id' => 'U',
            'date' => 'Date',
            'receipt_image' => 'Receipt Image',
            'product_image' => 'Product Image',
            'ot_id' => 'Ot',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search() {
        // @todo Please modify the following code to remove attributes that should not be searched.

        $criteria = new CDbCriteria;

        $criteria->compare('srm_id', $this->srm_id);
        $criteria->compare('o_id', $this->o_id);
        $criteria->compare('u_id', $this->u_id);
        $criteria->compare('date', $this->date);
        $criteria->compare('receipt_image', $this->receipt_image,true);
        $criteria->compare('product_image', $this->product_image,true);
        $criteria->compare('ot_id', $this->ot_id);
//                echo "<pre>";print_r($criteria);die();
        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
        ));
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return ScanReceiptsMaster the static model class
     */
    public static function model($className = __CLASS__) {
        return parent::model($className);
    }
 
// From Database 
    public function showphoto_from_database($type)
    {
        if($this->receipt_image!='' && $type=='rec'){
            
            $photomodel=ScanReceiptsMaster::model()->findByAttributes(array('receipt_image'=>$this->receipt_image));

            if($photomodel){
                //return "data:image/png;base64,".$photomodel->bitmap;
                
                $img="//".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/receiptImages/".$photomodel->receipt_image;
             // print_r($img);die;
				//return image with link
                return "<a href='".$img."' target='_blank'>". CHtml::image($img,'No Image',array('width'=>100,'height'=>100))."</a>";
            }else{
                $url=Yii::app()->baseUrl."/images/noimage.jpg";  
                return CHtml::image($url,'No Image');
            }
        }
         if($this->product_image!='' && $type=='pro'){
            
            $photomodel=ScanReceiptsMaster::model()->findByAttributes(array('product_image'=>$this->product_image));

            if($photomodel){
                //return "data:image/png;base64,".$photomodel->bitmap;
                
                $img="//".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/scanProducts/".$photomodel->product_image;
             // print_r($img);die;
				//return image with link
                return "<a href='".$img."' target='_blank'>". CHtml::image($img,'No Image',array('width'=>100,'height'=>100))."</a>";
            }else{
                $url=Yii::app()->baseUrl."/images/noimage.jpg";  

                return CHtml::image($url,'No Image');
            }
        }
        
    }
//Payment stauses
    public function getPaymentStatus($status)
    {
        if($status==1)
            $str="Complete";
        else if ($status==2) 
        {
            $str="Pending";
        }
        else
            $str="Cancel";

        return $str;
    }
}

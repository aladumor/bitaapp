<?php

/**
 * This is the model class for table "user_master".
 *
 * The followings are the available columns in table 'user_master':
 * @property integer $u_id
 * @property string $unique_code
 * @property string $login_type
 * @property integer $ur_id
 * @property integer $ut_id
 * @property string $email
 * @property string $user_name
 * @property string $user_pass
 * @property string $addressline_1
 * @property string $addressline_2
 * @property integer $city_id
 * @property integer $state_id
 * @property integer $country_id
 * @property string $zipcode
 * @property integer $phone_no
 * @property string $name
 * @property string $profile_picture
 * @property integer $dob
 * @property integer $gps_flag
 * @property integer $email_flag
 * @property integer $notification_flag
 * @property integer $reward_points
 * @property integer $active_payment_method_id
 * @property string $venmo_email
 * @property string $paypal_email
 * @property integer $is_active
 * @property integer $is_delete
 * @property string $created_on
 * @property integer $created_by
 * @property string $modified_on
 * @property integer $modified_by
 */
class UserDetailMaster extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return UserDetailMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_name,deposit_amount', 'required'),
			array('ur_id, ut_id, phone_no, dob, gps_flag, email_flag, notification_flag, reward_points, active_payment_method_id, is_active, is_delete, created_by, modified_by', 'numerical', 'integerOnly'=>true),
			array('unique_code, login_type', 'length', 'max'=>11),
			array('email, user_name, user_pass, name, paypal_email', 'length', 'max'=>100),
			array('addressline_1, addressline_2', 'length', 'max'=>200),
			array('zipcode', 'length', 'max'=>10),
			array('profile_picture', 'length', 'max'=>50),
			array('created_on, modified_on', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('u_id, unique_code, login_type, ur_id, ut_id, email, user_name, user_pass, addressline_1, addressline_2, zipcode, phone_no, name, profile_picture, dob, gps_flag, email_flag, notification_flag, reward_points, active_payment_method_id,  paypal_email, is_active, is_delete, created_on, created_by, modified_on, modified_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'u_id' => 'U',
			'unique_code' => 'Unique Code',
			'login_type' => 'Login Type',
			'ur_id' => 'Ur',
			'ut_id' => 'Ut',
			'email' => 'Email',
			'user_name' => 'User Name',
			'user_pass' => 'Password',
			'addressline_1' => 'Addressline 1',
			'addressline_2' => 'Addressline 2',
			//'city_id' => 'City',
			//'state_id' => 'State',
			//'country_id' => 'Country',
			'zipcode' => 'Zipcode',
			'phone_no' => 'Phone No',
			'name' => 'Account Name',
			'profile_picture' => 'Profile Picture',
			'dob' => 'Dob',
			'gps_flag' => 'Gps Flag',
			'email_flag' => 'Email Flag',
			'notification_flag' => 'Notification Flag',
			'reward_points' => 'Reward Points',
			'active_payment_method_id' => 'Active Payment Method',
			//'venmo_email' => 'Venmo Email',
			'paypal_email' => 'Paypal Email',
			'is_active' => 'Is Active',
			'is_delete' => 'Is Delete',
			'created_on' => 'Created On',
			'created_by' => 'Created By',
			'modified_on' => 'Modified On',
			'modified_by' => 'Modified By',
			'deposit_amount' => 'Deposited Amount',
		);
	}
	public function behaviors() {
        return array(
                'ERememberFiltersBehavior' => array(
                'class' => 'application.components.ERememberFiltersBehavior',
                ),
        );
	}
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$session = new CHttpSession;
		$session->open();

		if(!empty($session["uid"]))
		{	
			$connection = Yii::app()->db;
			$sql = "select ur_id from user_master where u_id = ".$session["uid"];
			$user = $connection->createCommand($sql)->queryAll();
			$user_role_id = empty($user[0]['ur_id']) ? null : $user[0]['ur_id'];
		}

		$criteria=new CDbCriteria;

		if($user_role_id == '2')
		{
			$criteria->compare('u_id',$session["uid"]);
		}
		else
		{
			$criteria->compare('u_id',$this->u_id);
			$criteria->compare('unique_code',$this->unique_code,true);
			$criteria->compare('login_type',$this->login_type,true);
			$criteria->compare('ur_id',2);
			$criteria->compare('ut_id',$this->ut_id);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('user_name',$this->user_name,true);
			$criteria->compare('user_pass',$this->user_pass,true);
			$criteria->compare('addressline_1',$this->addressline_1,true);
			$criteria->compare('addressline_2',$this->addressline_2,true);
			//$criteria->compare('city_id',$this->city_id);
			//$criteria->compare('state_id',$this->state_id);
			//$criteria->compare('country_id',$this->country_id);
			$criteria->compare('zipcode',$this->zipcode,true);
			$criteria->compare('phone_no',$this->phone_no);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('profile_picture',$this->profile_picture,true);
			$criteria->compare('dob',$this->dob);
			$criteria->compare('gps_flag',$this->gps_flag);
			$criteria->compare('email_flag',$this->email_flag);
			$criteria->compare('notification_flag',$this->notification_flag);
			$criteria->compare('reward_points',$this->reward_points);
			$criteria->compare('active_payment_method_id',$this->active_payment_method_id);
			//$criteria->compare('venmo_email',$this->venmo_email,true);
			$criteria->compare('paypal_email',$this->paypal_email,true);
			$criteria->compare('is_active',"0");
			$criteria->compare('is_delete',$this->is_delete);
			$criteria->compare('created_on',$this->created_on,true);
			$criteria->compare('created_by',$this->created_by);
			$criteria->compare('modified_on',$this->modified_on,true);
			$criteria->compare('modified_by',$this->modified_by);
			$criteria->compare('deposit_amount',$this->deposit_amount);
		}

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
	}
	public function customersearch()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
		
		$session = new CHttpSession;
		$session->open();


		$criteria=new CDbCriteria;

			$criteria->compare('u_id',$this->u_id);
			$criteria->compare('unique_code',$this->unique_code,true);
			$criteria->compare('login_type',$this->login_type,true);
			$criteria->AddInCondition('ur_id',array(4));
			$criteria->compare('ut_id',$this->ut_id);
			$criteria->compare('email',$this->email,true);
			$criteria->compare('user_name',$this->user_name,true);
			$criteria->compare('user_pass',$this->user_pass,true);
			$criteria->compare('addressline_1',$this->addressline_1,true);
			$criteria->compare('addressline_2',$this->addressline_2,true);
			//$criteria->compare('city_id',$this->city_id);
			//$criteria->compare('state_id',$this->state_id);
			//$criteria->compare('country_id',$this->country_id);
			$criteria->compare('zipcode',$this->zipcode,true);
			$criteria->compare('phone_no',$this->phone_no);
			$criteria->compare('name',$this->name,true);
			$criteria->compare('profile_picture',$this->profile_picture,true);
			$criteria->compare('dob',$this->dob);
			$criteria->compare('gps_flag',$this->gps_flag);
			$criteria->compare('email_flag',$this->email_flag);
			$criteria->compare('notification_flag',$this->notification_flag);
			$criteria->compare('reward_points',$this->reward_points);
			$criteria->compare('active_payment_method_id',$this->active_payment_method_id);
			//$criteria->compare('venmo_email',$this->venmo_email,true);
			$criteria->compare('paypal_email',$this->paypal_email,true);
			$criteria->compare('is_active',"0");
			$criteria->compare('is_delete',$this->is_delete);
			$criteria->compare('created_on',$this->created_on,true);
			$criteria->compare('created_by',$this->created_by);
			$criteria->compare('modified_on',$this->modified_on,true);
			$criteria->compare('modified_by',$this->modified_by);
			$criteria->compare('deposit_amount',$this->deposit_amount);

			return new CActiveDataProvider($this, array(
				'criteria'=>$criteria,
			));
	}
	//redeem count
	public function getaddress($unique_code)
	{
		$connection = Yii::app()->db;
		$cmd="SELECT CONCAT (addressline_1,',',zipcode) AS 'address' FROM user_master WHERE unique_code= '".$unique_code."'";
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		
		if(!empty($rows))
		{
			return $rows[0]['address'];
		}
		else
		{
			return "Not available";
		}
		
	}
	
}
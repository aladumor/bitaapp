<?php

/**
 * This is the model class for table "information_master".
 *
 * The followings are the available columns in table 'information_master':
 * @property integer $im_id
 * @property string $about_us_title
 * @property string $about_us
 * @property string $privacy_policy_title
 * @property string $privacy_policy
 * @property string $t_and_c_title
 * @property string $t_and_c
 */
class InformationMaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'information_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('about_us_title, about_us, privacy_policy_title, privacy_policy, t_and_c_title', 'required'),
			array('t_and_c', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('im_id, about_us_title, about_us, privacy_policy_title, privacy_policy, t_and_c_title, t_and_c', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'im_id' => 'Im',
			'about_us_title' => 'About Us Title',
			'about_us' => 'About Us',
			'privacy_policy_title' => 'Privacy Policy Title',
			'privacy_policy' => 'Privacy Policy',
			't_and_c_title' => 'Terms And Conditions Title',
			't_and_c' => 'Terms And Conditions',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('im_id',$this->im_id);
		$criteria->compare('about_us_title',$this->about_us_title,true);
		$criteria->compare('about_us',$this->about_us,true);
		$criteria->compare('privacy_policy_title',$this->privacy_policy_title,true);
		$criteria->compare('privacy_policy',$this->privacy_policy,true);
		$criteria->compare('t_and_c_title',$this->t_and_c_title,true);
		$criteria->compare('t_and_c',$this->t_and_c,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InformationMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}

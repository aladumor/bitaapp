<?php

/**
 * This is the model class for table "merchandise_master".
 *
 * The followings are the available columns in table 'merchandise_master':
 * @property integer $m_id
 * @property string $name
 * @property integer $point_required
 * @property integer $is_active
 * @property integer $bp_id
 */
class MerchandiseMaster extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MerchandiseMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'merchandise_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, point_required', 'required'),
			array('point_required, is_active, bp_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>100),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('m_id, name, point_required, is_active, bp_id,image', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'm_id' => 'M',
			'name' => 'Name',
			'point_required' => 'Point Required',
			'is_active' => 'Is Active',
			'bp_id' => 'Bp',
			'image'=>'Merchandise Image',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('m_id',$this->m_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('point_required',$this->point_required);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('bp_id',$this->bp_id);
		$criteria->compare('image',$this->image,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getimage($image)
    {              
       return '<img src="'.Yii::app()->request->baseUrl.'/images/merchandise_images/'.$image.'" height="25" width="25">';   
    }
}
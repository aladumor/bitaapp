<?php

/**
 * This is the model class for table "activity_log".
 *
 * The followings are the available columns in table 'activity_log':
 * @property integer $logid
 * @property integer $ur_id
 * @property integer $ut_id
 * @property integer $u_id
 * @property string $action
 * @property string $description
 * @property integer $log_time
 */
class ActivityLog extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return ActivityLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'activity_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ur_id, ut_id, action, description, log_time', 'required'),
			array('ur_id, ut_id, u_id, log_time', 'numerical', 'integerOnly'=>true),
			array('action', 'length', 'max'=>50),
			array('description', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('logid, ur_id, ut_id, u_id, action, description, log_time', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'logid' => 'Logid',
			'ur_id' => 'Ur',
			'ut_id' => 'Ut',
			'u_id' => 'U',
			'action' => 'Action',
			'description' => 'Description',
			'log_time' => 'Log Time',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('logid',$this->logid);
		$criteria->compare('ur_id',$this->ur_id);
		$criteria->compare('ut_id',$this->ut_id);
		$criteria->compare('u_id',$this->u_id);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('log_time',$this->log_time);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        /*
        * @desc : Insert Activity log
        */
        public function addLog($ur_id, $ut_id, $u_id, $action, $description, $log_time, $o_id) {
            $sql = "INSERT INTO activity_log (ur_id, ut_id, u_id, action, description, log_time, o_id)VALUES ('" . $ur_id . "','".$ut_id."','" . $u_id . "','" . $action . "','" . $description . "','" . $log_time . "','".$o_id."')";
            $connection = Yii::app()->db;
            $dataReader=$connection->createCommand($sql);
            $dataReader->execute();
        }
}
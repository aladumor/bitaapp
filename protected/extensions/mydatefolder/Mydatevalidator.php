<?php
class Mydatevalidator extends CValidator

{

    public $date_first;

    public $date_second;



    public function validateAttribute( $model, $attribute )

    {

        $value  = $model -> $attribute;



        if( $attribute == 'start_date' )

        {

            $this -> date_first = $value;

        }



        if( $attribute == 'end_date' )

        {

            $this -> date_second = $value;

        }



        if( $this -> date_first !== NULL && $this -> date_second !== NULL )

        {

            if( $this -> date_first > $this -> date_second )

            {

                $model -> addError( $attribute, 'end_date needs to be after date1' );

            }

        }

    }

}

?>
<?php

/**
 * This is the model class for table "offer_master".
 *
 * The followings are the available columns in table 'offer_master':
 * @property integer $o_id
 * @property integer $cm_id
 * @property integer $u_id
 * @property string $offer_name
 * @property integer $city_id
 * @property string $offer_desc
 * @property string $start_date
 * @property string $end_date
 * @property integer $maximum_redemption
 * @property string $offer_image
 * @property string $code_redeem_file
 * @property integer $is_active
 * @property integer $is_delete
 * @property integer $created_by
 * @property integer $modified_by
 * @property string $created_on
 * @property string $modified_on
 * @property integer $loyalty_offer_id
 */
class OfferMaster extends UserMaster
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OfferMaster the static model class
	 */
	
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'offer_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_name, offer_name,  offer_desc, start_date, end_date, maximum_redemption, offer_image', 'required','on'=>'inser,update'),
			array('cm_id, u_id,  maximum_redemption, is_active, is_delete, created_by, modified_by, loyalty_offer_id', 'numerical', 'integerOnly'=>true),
			array('offer_name, offer_image, code_redeem_file', 'length', 'max'=>200),
			array('offer_desc', 'length', 'max'=>500),
			array('offer_image', 'file','types'=>'jpg, gif, png', 'allowEmpty'=>true, 'on'=>'update'), 
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('o_id, cm_id, u_id, offer_name,  offer_desc, start_date, end_date, maximum_redemption, offer_image, code_redeem_file, is_active, is_delete, created_by, modified_by, created_on, modified_on, loyalty_offer_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'user' => array(self::BELONGS_TO, 'UserMaster', 'u_id'),
			'campaign' => array(self::BELONGS_TO, 'CampaignMaster', 'cm_id'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'o_id' => 'O',
			'cm_id' => 'Campaign',
			'u_id' => 'U',
			'offer_name' => 'Offer Name',
			'product_name' => 'Beverage Name',
			
			'offer_desc' => 'Offer Description',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'maximum_redemption' => 'Maximum Redemption',
			'offer_image' => 'Offer Image',
			'code_redeem_file' => 'QR Code',
			'is_active' => 'Is Active',
			'is_delete' => 'Is Delete',
			'created_by' => 'Created By',
			'modified_by' => 'Modified By',
			'created_on' => 'Created On',
			'modified_on' => 'Modified On',
			'loyalty_offer_id' => 'Loyalty Offer',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('o_id',$this->o_id);
		$criteria->compare('cm_id',$this->cm_id);
		$criteria->compare('u_id',$this->u_id);
		$criteria->compare('offer_name',$this->offer_name,true);
		$criteria->compare('offer_desc',$this->offer_desc,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('maximum_redemption',$this->maximum_redemption);
		$criteria->compare('offer_image',$this->offer_image,true);
		$criteria->compare('code_redeem_file',$this->code_redeem_file,true);
		$criteria->compare('is_active',$this->is_active);
		$criteria->compare('is_delete',$this->is_delete);
		$criteria->compare('created_by',$this->created_by);
		$criteria->compare('modified_by',$this->modified_by);
		$criteria->compare('created_on',$this->created_on,true);
		$criteria->compare('modified_on',$this->modified_on,true);
		$criteria->compare('loyalty_offer_id',$this->loyalty_offer_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	public function getimage($image)
    {              
       return '<img src="'.Yii::app()->request->baseUrl.'/images/offer_images/'.$image.'" height="25" width="25">';   
    }
}
<?php

class UserDetailMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */


	public function accessRules()
	{
                $session=new CHttpSession;
                $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','userDashboard','exporttoexcel','sendPush','view','create','update','admin','delete','customer','accountHistory','ajaxgetRedeemHistory','ajaxgetshareHistory'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new UserDetailMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserDetailMaster']))
		{
			$model->login_type = 1;
			$model->ur_id = 2;
			$model->ut_id = 1;
			$password = base64_encode("Bevrage@2015");
			$model->user_pass = base64_encode($password);
			$model->attributes=$_POST['UserDetailMaster'];
			if($model->save())
			{
				/*Last inserted id of */
				$Id = Yii::app()->db->getLastInsertId();

				$brand_name=empty($_POST['brand_name'])? null :$_POST['brand_name'];
				if(!empty($brand_name))
				{
					$connection = Yii::app()->db;
					$cmd="INSERT INTO `brand_master`(`u_id`,`brand_name`) VALUES ('".$Id."','".$brand_name."')";
					$dataReader = $connection->createCommand($cmd)->query();
				}

				/*For Add Brand Details*/
				$product_name=empty($_POST['product_name'])? null :$_POST['product_name'];
				$upc_code=empty($_POST['upc_code'])? null :$_POST['upc_code'];
				if(!empty($product_name) && !empty($upc_code))
				{
					$connection = Yii::app()->db;
					$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$product_name.'","'.$upc_code.'","'.$Id.'")';
					$dataReader = $connection->createCommand($cmd)->query();
				}
				$new_product=empty($_POST['new_product'])? null :$_POST['new_product'];
				$new_upc=empty($_POST['new_upc'])? null :$_POST['new_upc'];
				if($new_product != null)
				{
					foreach($new_product as $key => $value2)
					{
						$p_name = $value2;
						$upc_code = $new_upc[$key];
						$connection = Yii::app()->db;
						$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$p_name.'","'.$upc_code.'","'.$Id.'")';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/*For Add Brand Details*/

				/*
				BRAND PARTNERS -> PAYPAL PAYMENTS				
				*/
				//$resultPay=$this->actionRequestPayment($model->id);
				//if($resultPay=="1")
				//{
					//success
				//}
				//else
				//{
					//fail
				//}
				/* send email */
				/*$message = new YiiMailMessage;  
		        $msg= "Thanks For joining BEVRAGE Team. 
		       	\n Following is your Login Credentials for Brand Partner Portal :
		       	\n Username: ".$_POST['UserDetailMaster']['email']."
		       	\n Pasword: ".$password."\n
		        \n Best regrads,
		        \n Dounya Irrgang
		        \n CEO 
		        \n BEVRAGE CO.," ;   
		        $message->setBody($msg, 'text/html');       
		        $message->setsubject('Welcome to BEVRAGE');      
		        $message->addTo($email);         
		        $message->from = "service@bevRage.com";        
		        Yii::app()->mail->send($message); */
				/* end */
			}
				
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	public function actionRequestPayment()
      {
          $e=new ExpressCheckout;
 
          $products=array(
 
                '0'=>array(
                      'NAME'=>'p1',
                      'AMOUNT'=>'250.00',
                      'QTY'=>'2'
                      ),
                '1'=>array(
                      'NAME'=>'p2',
                      'AMOUNT'=>'300.00',
                      'QTY'=>'2'
                      ),
                '2'=>array(
                      'NAME'=>'p3',
                      'AMOUNT'=>'350.00',
                      'QTY'=>'2'
                      ),
 
                );
                         /*Optional */
                   $shipping_address=array(
 
            'FIRST_NAME'=>'Sirin',
            'LAST_NAME'=>'K',
            'EMAIL'=>'sirinibin2006@gmail.com',
            'MOB'=>'0918606770278',
            'ADDRESS'=>'mannarkkad', 
            'SHIPTOSTREET'=>'mannarkkad',
            'SHIPTOCITY'=>'palakkad',
            'SHIPTOSTATE'=>'kerala',
            'SHIPTOCOUNTRYCODE'=>'IN',
            'SHIPTOZIP'=>'678761'
                                          ); 
 
            $e->setShippingInfo($shipping_address); // set Shipping info Optional
 
            $e->setCurrencyCode("EUR");//set Currency (USD,HKD,GBP,EUR,JPY,CAD,AUD)
 
            $e->setProducts($products); /* Set array of products*/
 
            $e->setShippingCost(5.5);/* Set Shipping cost(Optional) */
 
 
            $e->returnURL=Yii::app()->createAbsoluteUrl("site/PaypalReturn");
 
                    $e->cancelURL=Yii::app()->createAbsoluteUrl("site/PaypalCancel");
 
            $result=$e->requestPayment(); 
 
            /*
              The response format from paypal for a payment request
            Array
        (
            [TOKEN] => EC-9G810112EL503081W
            [TIMESTAMP] => 2013-12-12T10:29:35Z
            [CORRELATIONID] => 67da94aea08c3
            [ACK] => Success
            [VERSION] => 65.1
            [BUILD] => 8725992
        )
                */
 
 
        if(strtoupper($result["ACK"])=="SUCCESS")
          {
            /*redirect to the paypal gateway with the given token */
            header("location:".$e->PAYPAL_URL.$result["TOKEN"]);
          } 
 
 
 
         }          
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		/*Fetch brand_name from brand_master*/
		$connection = Yii::app()->db;
		$brand_name = "select brand_name from brand_master where u_id = ".$id;
		$brand_namerows = $connection->createCommand($brand_name)->queryAll();
		$BrandName = empty($brand_namerows[0]['brand_name']) ? null : $brand_namerows[0]['brand_name'];

		/*Fetch product_details from product_master*/
		$product_name = "select product_name,upc_code,pm_id from product_master where created_by = ".$id;
		$product_namerows = $connection->createCommand($product_name)->queryAll();

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['UserDetailMaster']))
		{
			/*echo "<pre>";
			print_r($_POST);
			
			die();*/
			$model->attributes=$_POST['UserDetailMaster'];
			if(empty($_POST['user_pass']))
				$model->user_pass=$model->user_pass;
			else
				$model->user_pass=base64_encode($_POST['user_pass']);
			$model->phone_no=$_POST['UserDetailMaster']['phone_no'];
			
			if($model->save())
				$session=new CHttpSession;
                $session->open();
                $session['uname']=$model->user_name;
				/*Any new product Added */
				$new_product=empty($_POST['new_product'])? null :$_POST['new_product'];
				$new_upc=empty($_POST['new_upc'])? null :$_POST['new_upc'];
				if($new_product != null)
				{
					foreach($new_product as $key => $value2)
					{
						$p_name = $value2;
						$upc_code = $new_upc[$key];
						$cmd='INSERT INTO `product_master`(`product_name`,`upc_code`,`created_by`) VALUES ("'.$p_name.'","'.$upc_code.'","'.$id.'")';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/* end */
				/* update old product */
				$old_product_name=empty($_POST['old_product_name'])? null :$_POST['old_product_name'];
				$old_upc_code=empty($_POST['old_upc_code'])? null :$_POST['old_upc_code'];
				$pm_id=empty($_POST['pm_id'])? null :$_POST['pm_id'];
				
				if($old_product_name != null)
				{
					if(!empty($_POST['deletedPids']))
					{
						$deletedPids=ltrim ($_POST['deletedPids'], ',');
						$cmd="DELETE FROM `product_master` where pm_id IN (".$deletedPids.")";
						$dataReader = $connection->createCommand($cmd)->query();
					}
					
					foreach($old_product_name as $key => $value3)
					{
						$product_name = $value3;
						$upc_code = $old_upc_code[$key];
						$cmd='UPDATE `product_master` SET product_name = "'.$product_name.'",upc_code = "'.$upc_code.'" where pm_id ="'.$key.'"';
						$dataReader = $connection->createCommand($cmd)->query();
					}
				}
				/* end*/
				$brand_name=empty($_POST['brand_name'])? null :$_POST['brand_name'];
				if($brand_name != null)
				{
					$cmd="UPDATE `brand_master` SET brand_name = '".$brand_name."' where u_id ='".$id."'";
					$dataReader = $connection->createCommand($cmd)->query();
				}

				$model->email=empty($_POST['email'])? null :$_POST['email'];
				$model->phone_no=empty($_POST['phone_no'])? null :$_POST['phone_no'];
				

				$this->redirect(array('admin'));
		}

		$this->render('update',array(
			'model'=>$model,'BrandName'=>$BrandName,'product_namerows'=>$product_namerows,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE user_master SET is_active=1 ,is_delete=1 where u_id=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();

		$cmd="UPDATE product_master SET is_active=1 ,is_delete=1 where created_by=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();

		$cmd="UPDATE offer_master SET is_active=1 ,is_delete=1 where u_id=".$id;		
		$dataReader = $connection->createCommand($cmd)->query();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('UserDetailMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new UserDetailMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserDetailMaster']))
			$model->attributes=$_GET['UserDetailMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	public function actioncustomer()
	{
		$model=new UserDetailMaster('customersearch');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['UserDetailMaster']))
			$model->attributes=$_GET['UserDetailMaster'];

		$this->render('customer',array(
			'model'=>$model,
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return UserDetailMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=UserDetailMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param UserDetailMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-detail-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	// GET ACCOUNT HISTORY
	public function actionaccountHistory()
	{
		/*
		* 	Account History should include:
		*	Transaction Overview (number of redemptions, total redemption amount, top products, top redemption locations, category type (e.g., spirits)
		*	Current/Pending/Recent Transactions
		*	Offers Redeemed (which should link to a list of Transaction records)
		*	Offers Shared (and with whom)
		*	Rewards Points
		*/

		/*
		* Transaction Overview Data
		*/

		/* number of redemptions */
		$connection = Yii::app()->db;
		$cmd="SELECT count(ot_id) AS 'total_redemption' from offer_transaction_master where unique_code='".$_REQUEST['code']."'";		
		$dataReader = $connection->createCommand($cmd)->query();
		$redemptionRows=$dataReader->readAll();
		$total_redemption=$redemptionRows[0]['total_redemption'];
		
		/* number of redemptions */
		$connection = Yii::app()->db;
		$cmd="SELECT sum(total_amount) AS 'total_amount' from offer_transaction_master
		 where unique_code='".$_REQUEST['code']."' and (customer_payment_status=1 or payment_status=1)";		
		$dataReader = $connection->createCommand($cmd)->query();
		$amountRows=$dataReader->readAll();
		$total_amount=$amountRows[0]['total_amount'];

		$this->render('accountHistory',array('total_amount'=>$total_amount,'total_redemption'=>$total_redemption));
	}

	//getRedeemHistory
	public function actionajaxgetRedeemHistory()
	{
		$unique_code=$_REQUEST['code'];
		$connection = Yii::app()->db;
		$cmd="SELECT offer_name,redeem_date,total_amount,customer_payment_status,payment_status,type from offer_transaction_master,offer_master 
				where offer_transaction_master.unique_code='".$unique_code."'
				and offer_transaction_master.o_id=offer_master.o_id";		
		$dataReader = $connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		$str="";
		
		if(!empty($rows))
		{
			$str .= "<h3><i class='fa fa-history'></i> Redemption History</h3><table>";
			$str .= "<tr><th>Offer Name</th><th>Redeem Date</th><th>Amount</th><th>status</th></tr>";
			foreach ($rows as $key => $value) 
			{
				if($value['type']=='off_pre')
				{
					if($value['customer_payment_status']==2)
						$status="Pending";
					elseif ($value['customer_payment_status']==3)
						$status="Cancel";
					elseif ($value['customer_payment_status']==1)
						$status="Completed";
				}
				else
				{
					if($value['payment_status']==2)
						$status="Pending";
					elseif ($value['payment_status']==3)
						$status="Cancel";
					elseif ($value['payment_status']==1)
						$status="Completed";
				}
				$str .="<tr><td>".$value['offer_name']."</td><td>".$value['redeem_date']."</td><td>".$value['total_amount']."</td><td>".$status."</td></tr>";
			}
			$str .= "</table>";
		}
		else
		{
			$str= "<h3><i class='fa fa-history'></i> Redemption History<br><br> <span> Data Not Available ! </span></h3>";
		}
		echo  $str;
		
	}

	//get offers shared
	public function actionajaxgetShareHistory()
	{
		$unique_code=$_REQUEST['code'];
		$uid=$_REQUEST['id'];
		$connection = Yii::app()->db;
		$cmd="SELECT offer_name from share_record,offer_master
				where share_record.u_id =".$uid."
				and share_record.o_id=offer_master.o_id";		
		$dataReader = $connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		$str="";
		
		if(!empty($rows))
		{
			$str .= "<h3><i class='fa fa-share-alt'>  Offer Share History</i></h3><table>";
			$str .= "<tr><th>Offer Name</th></tr>";
			foreach ($rows as $key => $value) 
			{
				$str .="<tr><td>".$value['offer_name']."</td></tr>";
			}
			$str .= "</table>";
		}
		else
		{
			$str= "<h3><i class='fa fa-share-alt'>  Offer Share History</i> <br> <br><span> Data Not Available ! </span></hr>";
		}
		echo  $str;
		
	}

	//userDashboard
	public function actionuserDashboard()
	{
		$dataProvider=new CActiveDataProvider('UserDetailMaster');
		// get tootal app users
		$connection = Yii::app()->db;
		$cmd="SELECT COUNT(u_id) AS 'users' FROM user_master
				WHERE ur_id=4";
		$dataReader=$connection->createCommand($cmd)->query();
		$userRows=$dataReader->readAll();
		$totalUsers=$userRows[0]['users'];
		
		//Get Age Data		
		$cmd="SELECT
					CASE
					WHEN age < 20 THEN 'Under 20'
					WHEN age BETWEEN 20 AND 29 THEN '21 - 29'
					WHEN age BETWEEN 30 AND 39 THEN '30 - 39'
					WHEN age BETWEEN 40 AND 49 THEN '40 - 49'
					WHEN age BETWEEN 50 AND 59 THEN '50 - 59'										        
					WHEN age >= 60 THEN 'Over 60'
					WHEN age IS NULL THEN 'Others'				
					END AS option_value,COUNT(*) as cnt
					FROM (SELECT TIMESTAMPDIFF(YEAR, `dob`, CURDATE()) AS age from user_master where ur_id=4) AS derived
				    GROUP BY option_value
				    ORDER BY option_value";
		$dataReader=$connection->createCommand($cmd)->query();
		$agerows=$dataReader->readAll();
		if(!empty($agerows))
		{
			$data['ageData']=$agerows;
		}
		else
		{
			$data['ageData']='';
		}

		//fetch device type counts
		$cmd="SELECT type_name as 'option_value',COUNT(u_id) AS cnt FROM device_type_master ,user_device_master 
				WHERE device_type_master.dt_id=user_device_master.dt_id and u_id IN (select u_id from user_master where ur_id=4)
				GROUP BY type_name";
		$dataReader=$connection->createCommand($cmd)->query();
		$devicerows=$dataReader->readAll();
		if(!empty($devicerows))
		{
			$data['deviceData']=$devicerows;
		}
		else
		{
			$data['deviceData']='';
		}

		//Gender Data
		$cmd="SELECT count(*) as 'cnt',gender  as 'option_value' FROM `user_master` group by gender";
		$dataReader=$connection->createCommand($cmd)->query();
		$genderrows=$dataReader->readAll();
				
		if(!empty($genderrows))
		{
			$data['genderData']=$genderrows;
		}
		else
		{
			$data['genderData']='';
		}


		$this->render('userDashboard',array(
			'dataProvider'=>$dataProvider,'data'=>$data,'users'=>$totalUsers,
		));
	}

	//Send mass push notofication
	public function actionsendPush()
	{
		$model=new UserDetailMaster('search');
		if(isset($_REQUEST['pushmsg']))
		{
			//send push notofication to app users
			$connection = Yii::app()->db;
			$cmd="SELECT Distinct (u_id),dt_id,token,badge from  user_device_master 
				  where u_id in (select u_id from user_master 
				  				where ur_id=4 and notification_flag==0)";		
			$dataReader = $connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			if(!empty($rows))
			{
				foreach ($rows as $key => $value) 
				{
					if(!empty($value['token']))
					{
						if($value['dt_id']==1)
						{
							//android							
							//$this->sendAndroidNotification($token,$key,$msg);
						}
						else
						{
							//ios
							//$this->sendiosNotification($token,$msg,$badge);
						}
					}
				}
				$this->render('sendPush',array(
				'model'=>$model,'status'=>'1',
			));
			}

		}
		$this->render('sendPush',array(
			'model'=>$model,
		));
	}

	//snd anroid mass push notofication
	public function sendAndroidNotification($token,$key,$message)
	{
		$url = 'https://android.googleapis.com/gcm/send';        
        $fields = array(
            'registration_ids' => array($token),
            'data' => array( "message" => $message,'flag'=>"mass" ),
            
        );

        
        $headers = array(
            'Authorization: key='.$key,
            'Content-Type: application/json'
        );
        // Open connection
        $ch = curl_init();

        // Set the url, number of POST vars, POST data
        curl_setopt($ch, CURLOPT_URL, $url);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        // Disabling SSL Certificate support temporarly
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) {
            die('Curl failed: ' . curl_error($ch));
        }

        // Close connection
        curl_close($ch);
        $res = json_decode($result, true);
        if ($res["success"] == '1') {
            return true;
        } else {
            return false;
        }
	}

	//snd apple mass push notofication
	public function sendiosNotification($token,$message,$badge)
	{
		if ($token != '0') 
		{
			$certificate="BevRageck.pem";
            $ctx = stream_context_create();
            $passphrase = '1234';
            stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            $body['aps'] = array(
                'badge' => $badge,
                'alert' => $message,
                'sound' => 'default',
                'flag'=> "mass"
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $token) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            $flag = 0;
            if (!$result) {
                $flag = 1;
            }
            fclose($fp);
            if ($flag == 0) {
                return true;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
		
	}
	//export to excel
	public function actionexporttoexcel()
	{
		$connection = Yii::app()->db;
		$cmd="SELECT name,email from user_master where ur_id=4";		
		$dataReader = $connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{
			$data[]=array('Name','Email');
			foreach ($rows as $key => $value) 
			{
				$data[]=array($value['name'],$value['email']);
			}
		}
		
		Yii::import('application.extensions.phpexcel.JPhpExcel');
		$xls = new JPhpExcel('UTF-8', false, 'Bevrage App User Details');
		$xls->addArray($data);
		$xls->generateXML('Bevrage_App_User_data');
	}
}

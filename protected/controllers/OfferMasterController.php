<?php

class OfferMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $session=new CHttpSession;
        $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','ReportDashboard','SendPush','getajaxOffers','generate','changeStatus','GenerateOfferReports','GenerateCampaignReports','GenerateLocationReports','getMerchants','getcities'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
            
		$model=new OfferMaster;
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferMaster']))
		{
			$fileext = $_FILES['OfferMaster']['name']['offer_image'];
                        $fileext = explode(".", strtolower($fileext));
                        $fileext = $fileext[1];
                        
                        $codefilename='qr'.date('YmdHis').'.'.$fileext;		
			/* save offer image */
				$offerImage=CUploadedFile::getInstance($model,'offer_image');
		        if($offerImage)
		        {  		        	
		            $rnd = rand(0,9999);
		            $f1=str_replace(' ','_', $offerImage);		        
                            $fileName = "{$rnd}-".date('YmdHis').".".$fileext;  
		            $model->offer_image=$fileName;	           
	                $imagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$fileName;
	                $offerImage->saveAs($imagepath);                 
                        /////////////////////////////
                        $imagethumb=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR. "thumb" . DIRECTORY_SEPARATOR; 
                        $newfilename = $_FILES['OfferMaster']['name']['offer_image'];
                        $ext = explode(".", strtolower($newfilename));
                        $src =  $imagepath;
                        if ($ext[1] == 'gif')
                            $resource = imagecreatefromgif($src);
                        else if ($ext[1] == 'png')
                            $resource = imagecreatefrompng($src);
                        else if ($ext[1] == 'PNG')
                            $resource = imagecreatefrompng($src);
                        else if ($ext[1] == 'jpg' || $ext[1] == 'jpeg')
                            $resource = imagecreatefromjpeg($src);
                        $width = imagesx($resource);
                        $height = imagesy($resource);
                        $desired_width = 500;
                        $desired_height = 300;
                        $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
                        imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
                        imagejpeg( $virtual_image, "{$imagepath}" );
                        //@desc: convert thumb images
                        $desired_width = 200;
                        $desired_height = 200;
                        $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
                        imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
                        imagejpeg( $virtual_image, "{$imagethumb}{$fileName}" );                        
                        
                        }else{
                            $fileName = "";
                        }	
                        //////////////////////////////		        
	        /* end of image save code */

			$model->attributes=$_POST['OfferMaster'];
			$model->code_redeem_file=$codefilename;
			$model->start_date=date('Y-m-d',strtotime(str_replace('-','/',$_POST['OfferMaster']['start_date'] )));
			$model->end_date=date('Y-m-d',strtotime(str_replace('-','/',$_POST['OfferMaster']['end_date'] )));
			$model->created_on=gmdate('Y-m-d H:i:s');
			$model->modified_on=gmdate('Y-m-d H:i:s');
			$model->redeem_type=$_POST['OfferMaster']['redeem_type'];
			//$model->product_price=empty($_POST['OfferMaster']['product_price'])? null :$_POST['OfferMaster']['product_price'];
			$model->discounted_price=empty($_POST['OfferMaster']['discounted_price'])? null :$_POST['OfferMaster']['discounted_price'];
			$session=new CHttpSession;
                         $session->open();
			$model->u_id=$session['uid'];
			//$model->city_id="26318";
			$model->offer_image=$fileName;
			
			if($model->save())
			{
				//$desc: Add activity log;
                                $activity=new ActivityLog;                                
                                $activity->addLog($session["rid"], 1, $session['uid'], 'create', 'create offer by '.$session["uname"], time(),$model->o_id);				/* GENERATE QR CODE */
					$urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice/offer_detail_V2?o_id=".$model->o_id;
					$code=new QRCode($urlpath);		 
					
					// to flush the code directly
					//$code->create();

					// to save the code to file				
					$codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_codes" . DIRECTORY_SEPARATOR.$codefilename;
					$code->create($codepath);
				/* END OF QR CODE */

				/* save the offer cities */
                            
                    if($_POST['city'] != "")
                    {
                        foreach ($_POST['city'] as $value) 
						{
							$connection = Yii::app()->db;
							$cmd="INSERT INTO `offer_location_assoc`(`ol_id`, `o_id`, `city_id`) 
															VALUES (NUll,'".$model->o_id."','".$value."')";
							$dataReader=$connection->createCommand($cmd);
							$num=$dataReader->execute();
						}
                	} 
					
				/* end of code of save city*/

				/* save the offer merchants */
                            
                if($_POST['merchants'] != "")
                {
                    foreach ($_POST['merchants'] as $value) 
					{
						$connection = Yii::app()->db;
						$cmd="INSERT INTO `offer_merchant_assoc`(`om_id`, `o_id`, `m_id`) 
														VALUES (NUll,'".$model->o_id."','".$value."')";
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
					}
                } 
					
				/* end of code of save merchants*/

				$this->redirect(array('admin'));
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	*Generate QR code for Offers
	**/
  	 public function actiongenerate()//$id,$ctime
  	{
	   /*$this->widget('application.extensions.qrcode_google.QRCode', array(
	   'content' => 'http://localhost/bevrage/index.php/site/publicbinarytree?obj_id='.$id, // qrcode data content
	   'filePath' => YiiBase::getPathOfAlias('webroot.images/google'),//$path_to_img
	   'filename' => $ctime.'.png', // image name (make sure it should be .png)
	   'width' => '150', // qrcode image height
	   'height' => '150', // qrcode image width
	   'encoding' => 'UTF-8', // qrcode encoding method
	   'correction' => 'H', // error correction level (L,M,Q,H)
	   'watermark' => 'No' // set Yes if you want watermark image in Qrcode else 'No'. for 'Yes', you need to set watermark image $stamp in QRCode.php
	   ));*/
	   //$this->redirect(array('index','id'=>$id));
		//Yii::import('ext.qrcode.QRCode');
		$code=new QRCode("http://dev.credencys.com/bevrage/index.php/webservice/offer_detail?o_id=3");
		 
		// to flush the code directly
		//$code->create();
		 
		// to save the code to file
		$code->create('D:/xampp/htdocs/bevrage/qr20150209234117.png');
  	}
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferMaster']))
		{
			
			/*echo "<pre>";
		print_r($_POST);	
		die();*/
			$uid=$model->u_id;
			$session=new CHttpSession;
            $session->open();
			$model->attributes=$_POST['OfferMaster'];
			$model->u_id=$uid;
			$model->start_date=date('Y-m-d',strtotime(str_replace('-','/',$_POST['OfferMaster']['start_date'] )));
			$model->end_date=date('Y-m-d',strtotime(str_replace('-','/',$_POST['OfferMaster']['end_date'] )));
			$model->modified_on=gmdate('Y-m-d H:i:s');
			$model->redeem_type=$_POST['OfferMaster']['redeem_type'];
			//$model->product_price=empty($_POST['OfferMaster']['product_price'])? null :$_POST['OfferMaster']['product_price'];
			$model->discounted_price=empty($_POST['OfferMaster']['discounted_price'])? null :$_POST['OfferMaster']['discounted_price'];
			

			/* GENERATE QR CODE */
					$codefilename='qr'.date('YmdHis').'.png';	
					$urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice/offer_detail_V2?o_id=".$model->o_id;
					$code=new QRCode($urlpath);		 
					
					// to flush the code directly
					//$code->create();

					// to save the code to file				
					$codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_codes" . DIRECTORY_SEPARATOR.$codefilename;
					$code->create($codepath);
					$model->code_redeem_file=$codefilename;
				/* END OF QR CODE */

			/*save ne Image */						
				$offerImage=CUploadedFile::getInstance($model,'offer_image');
		        if($offerImage)
		        {  	
		        	/* delete old image */
		        	$oldimagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$model->offer_image;
		        	@unlink($oldimagepath);

		        	/*Save new image*/   
                            /////////////////
                            $fileext = $_FILES['OfferMaster']['name']['offer_image'];
                            $fileext = explode(".", strtolower($fileext));
                            $fileext = $fileext[1];    
                            
                            ////////////////
		            $rnd = rand(0,9999);
		            $f1=str_replace(' ','_', $offerImage);		            
                            $fileName = "{$rnd}-".date('YmdHis').".".$fileext;  
		            $model->offer_image=$fileName;	           
                            $imagepath=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR.$fileName;
	                $offerImage->saveAs($imagepath);   
                        /////////////////////////////
                            $imagethumb=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_images" . DIRECTORY_SEPARATOR. "thumb" . DIRECTORY_SEPARATOR; 
                            $newfilename = $_FILES['OfferMaster']['name']['offer_image'];
                            $ext = explode(".", strtolower($newfilename));
                           
                            $src =  $imagepath;
                            if ($ext[1] == 'gif')
                                $resource = imagecreatefromgif($src);
                            else if ($ext[1] == 'png')
                                $resource = imagecreatefrompng($src);
                            else if ($ext[1] == 'PNG')
                                $resource = imagecreatefrompng($src);
                            else if ($ext[1] == 'jpg' || $ext[1] == 'jpeg')
                                $resource = imagecreatefromjpeg($src);
                           
                            $width = imagesx($resource);
                            $height = imagesy($resource);
                            $desired_width = 500;
                            $desired_height = 300;
                            $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
                            imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
                            imagejpeg( $virtual_image, "{$imagepath}" );
                            //@desc: convert thumb images
                            $desired_width = 200;
                            $desired_height = 200;
                            $virtual_image = imagecreatetruecolor($desired_width,$desired_height);
                            imagecopyresized($virtual_image,$resource,0,0,0,0,$desired_width,$desired_height,$width,$height);
                            imagejpeg( $virtual_image, "{$imagethumb}{$fileName}" );                        
                            //////////////////////////////
                        
		        }
                        
			/* end of save image*/

			
			if($model->save())
			{
				$connection = Yii::app()->db;
				/* DELETE OLD ENTRIES*/
					$cmd="DELETE FROM `offer_location_assoc` where `o_id`= ".$id;
					$dataReader=$connection->createCommand($cmd);
					$num=$dataReader->execute();
                                        
                                //$desc: Add activity log
                                $activity=new ActivityLog;
                                $activity->addLog($session["rid"], 1, $session['uid'], 'update', 'update offer by '.$session["uname"], time(),$id);        
				/* update the offer cities */
                                    if($_POST['city'] != "")
                                    {
					foreach ($_POST['city'] as $value) 
					{
						
						
						
						/*INSERT NEW ENTRIES*/
						$cmd="INSERT INTO `offer_location_assoc`(`ol_id`, `o_id`, `city_id`) 
														VALUES (NUll,'".$model->o_id."','".$value."')";
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
					}
                                    }
				/* end of code of update city*/

				/* save the offer merchants */
                            
                if($_POST['merchants'] != "")
                {
                	/* DELETE OLD ENTRIES*/
					$cmd="DELETE FROM `offer_merchant_assoc` where `o_id`= ".$id;
					$dataReader=$connection->createCommand($cmd);
					$num=$dataReader->execute();
                    foreach ($_POST['merchants'] as $value) 
					{
						$connection = Yii::app()->db;

						

						$cmd="INSERT INTO `offer_merchant_assoc`(`om_id`, `o_id`, `m_id`) 
														VALUES (NUll,'".$model->o_id."','".$value."')";
						$dataReader=$connection->createCommand($cmd);
						$num=$dataReader->execute();
					}
                } 
					
				/* end of code of save merchants*/

				//$this->redirect(array('view','id'=>$model->o_id));
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/*
	* change the status tof offer
	*/
	public function actionchangeStatus()
	{
		$status=$_GET['status'];
		if($status==0)
			$status=1;
		else
			$status=0;
		$o_id=$_GET['id'];
		$connection = Yii::app()->db;
		$cmd="UPDATE  `offer_master` SET is_active=".$status." WHERE o_id=".$o_id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();
                //@Desc: add admin site log
                $session=new CHttpSession;
                $session->open();
                $activity=new ActivityLog;
                $statuslog = ($status == 0) ? "Active" : "Inactive";
                $activity->addLog($session["rid"], 1, $session['uid'], 'update', 'status changed to '.$statuslog.'  by '.$session["uname"], time(), $o_id);
		$this->redirect(array('admin'));
	}
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE  `offer_master` SET is_active='1',is_delete='1' WHERE o_id=".$id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OfferMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$searchOrder=0;//$_REQUEST['order'];
		$model=new OfferMaster('search',$searchOrder);
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OfferMaster']))
			$model->attributes=$_GET['OfferMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	/*
	* SEND MASS PUSH NOTIFICATIONS
	*/
	public function actionSendPush()
	{
		$model=new OfferMaster;
		if(isset($_REQUEST['pushmsgbox']) && !empty($_REQUEST['pushmsgbox']))
		{
			//send push
			$connection = Yii::app()->db;
			$cmd="SELECT distinct token,dt_id,badge from user_device_master where (token is not null or token!='null')  and u_id IN (6,87)";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			//print_r($rows); 
			//die();
			if(!empty($rows))
			{
				foreach ($rows as $key => $value) 
				{
					if($value['dt_id']==1)
					{
						//apple
						if ($value['token'] != '0') 
						{
							$message=$_REQUEST['pushmsgbox'];
		                    $ctx = stream_context_create();            
		                    $passphrase = '1234';
		                    $certificate= 'BevRageck.pem';
		                    stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
		                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		                    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
		                    $body['aps'] = array(
		                        'badge' => 1,
		                        'alert' => $message,
		                        'sound' => 'default',
		                        'flag'=>3 
		                    );
		                    $payload = json_encode($body);
		                    $msg = chr(0) . pack('n', 32) . pack('H*', $value['token']) . pack('n', strlen($payload)) . $payload;
		                    $result = fwrite($fp, $msg, strlen($msg));
		                    $flag = 0;

		                    if (!$result) {               
		                        $flag = 1;
		                    }
		                    
		                    fclose($fp);
		                    if ($flag == 0) {
		                       //echo "true";
		                        //return true;
		                    } else {
		                       //echo "false";
		                        //return FALSE;
		                    }
		                }
					}
					else
					{
						//android
						
						$url = 'https://android.googleapis.com/gcm/send';
				        $messages = $_REQUEST['pushmsgbox'];
				        $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4";
				        $registatoin_ids =array($value['token']);   
				        $fields = array(
				            'registration_ids' => $registatoin_ids,
				            'data' => array( "data" => $messages ),
				            
				        );
				        
				        
				        $headers = array(
				            'Authorization: key='.$apiKey,
				            'Content-Type: application/json'
				        );
				        // Open connection
				        $ch = curl_init();

				        // Set the url, number of POST vars, POST data
				        curl_setopt($ch, CURLOPT_URL, $url);

				        curl_setopt($ch, CURLOPT_POST, true);
				        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
				        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

				        // Disabling SSL Certificate support temporarly
				        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

				        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));

				        // Execute post
				        $result = curl_exec($ch);
				        if ($result === FALSE) 
				        {
				            die('Curl failed: ' . curl_error($ch));						
						}
						
					}	
				}
			}
			
			$this->render('SendPush',array(
				'model'=>$model,"status"=>"1",
			));
			exit;
		}
		$this->render('SendPush',array(
			'model'=>$model,"status"=>"0"
		));
	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OfferMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OfferMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OfferMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offer-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	/* 
	* -------------------REPORTS ---------------------------------------
	*/
	


	/*
	*Individual Offer Reports
	*/
	public function actionGenerateOfferReports()
	{
		
		/* REPORT 1: 
		* Individual Offer Report 
		*/

		if(isset($_REQUEST['o_id']))
		{
			
			/*
			* offer details
			*/
			$offer_name=$_REQUEST['offer_name'];
			$offer_type=($_REQUEST['offer_type']=='1')?'Bars & Restaurant':'Liquor Store';
			$duration=date( 'd/m/Y',strtotime($_REQUEST['start_date']))." - ".date( 'd/m/Y',strtotime($_REQUEST['end_date'])) ;

			/* GET TOTAL FACEBOOK SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT product_name  FROM `product_master` WHERE pm_id IN (SELECT pm_id from offer_master where o_id=".$_REQUEST['o_id'].")";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			$product_name=empty($rows[0]['product_name'])?'':$rows[0]['product_name'];

			

			/* GET TOTAL FACEBOOK SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'fb_count' FROM `share_record` WHERE `media_type`='fb' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query();
			$fbrows=$dataReader->readAll();
			$fbshare=empty($fbrows[0]['fb_count'])?0:$fbrows[0]['fb_count'];


			/* GET TOTAL TWITTER SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'tw_count' FROM `share_record` WHERE `media_type`='twitter' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query();
			$twrows=$dataReader->readAll();
			$twshare=empty($twrows[0]['tw_count'])?0:$twrows[0]['tw_count'];

			/* GET TOTAL gplus SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'gp_count' FROM `share_record` WHERE `media_type`='gplus' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query();
			$gprows=$dataReader->readAll();
			$gpshare=empty($gprows[0]['gp_count'])?0:$gprows[0]['gp_count'];

			
			/* GET TOTAL sms SHARE ON OFFER */
			$connection = Yii::app()->db;
			$cmd="SELECT count( `o_id`) as 'sms_count' FROM `share_record` WHERE `media_type`='sms' and o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query();
			$textrows=$dataReader->readAll();
			$textshare=empty($textrows[0]['sms_count'])?0:$textrows[0]['sms_count'];

			/* GET TOTAL scans of OFFER */
			$connection = Yii::app()->db;
			//$cmd="SELECT count( `o_id`) as 'scan_count' FROM `share_record` WHERE `media_type`='scan' and o_id=".$_REQUEST['o_id'];
			$cmd="SELECT count( `o_id`) as 'scan_count' FROM `offer_transaction_master` WHERE o_id=".$_REQUEST['o_id'];
			$dataReader=$connection->createCommand($cmd)->query();
			$scanrows=$dataReader->readAll();
			$scans=empty($scanrows[0]['scan_count'])?0:$scanrows[0]['scan_count'];

			
			/* 
			*Top TWO Table DATA 
			*/
			$data=array('fb'=>$fbshare,'tw'=>$twshare,'gplus'=>$gpshare,'sms'=>$textshare,'scan'=>$scans,
						'offer_name'=>$offer_name,'offer_type'=>$offer_type,'product_name'=>$product_name,'duration'=>$duration);
			
			
			/* ********************************
			 Date wise redemption data 
			 *********************************/
			$cmd="SELECT COUNT(  `o_id` ) AS  'totalcount' ,CONCAT(' ', DATE_FORMAT(`redeem_date`,'%Y'),',', 
								               DATE_FORMAT(`redeem_date`,'%c') -1,',' ,
								               DATE_FORMAT(`redeem_date`,'%e')) AS dateIndex 
				FROM  `offer_transaction_master` 
				WHERE o_id =".$_REQUEST['o_id']."
				GROUP BY redeem_date";
			$dataReader=$connection->createCommand($cmd)->query();
			$daterows=$dataReader->readAll();

			//assign it to final data Array
				$data['dateData']=$daterows;
			/* END of Date wise redemption data */

			/* ********************************
			 Time wise redemption data
			 ******************************** */
			$cmd="SELECT
					    CASE						
						WHEN time_slot BETWEEN 00 AND 12 THEN '1AM - 12PM'
						WHEN time_slot BETWEEN 12 AND 15 THEN '12PM - 3PM'
						WHEN time_slot BETWEEN 15 AND 18 THEN '3PM - 6PM'
						WHEN time_slot BETWEEN 18 AND 21 THEN '6PM - 9PM'										        
						WHEN time_slot BETWEEN 21 AND 23 THEN '9PM - 11PM'
						WHEN time_slot BETWEEN 22 AND 24 THEN '10 - 12AM'						
						WHEN time_slot >= 00 THEN 'After 12PM'					
					    END AS option_value,
					    COUNT(*) AS 'totalcount'
					    FROM (SELECT DATE_FORMAT(transaction_time,'%H') AS time_slot FROM offer_transaction_master
					     		WHERE o_id=".$_REQUEST['o_id']." 										     		
					    ) AS derived

					    GROUP BY option_value

					    ORDER BY option_value";
			$dataReader=$connection->createCommand($cmd)->query();
			$timerows=$dataReader->readAll();
			
			//assign it to final data Array
				$data['timeData']=$timerows;
			
			/* END of Time wise redemption data */
			

			/* ********************************
			 Age wise redemption data 
			 ******************************** */
			$cmd="SELECT
					CASE
					WHEN age < 20 THEN 'Under 20'
					WHEN age BETWEEN 20 AND 29 THEN '21 - 29'
					WHEN age BETWEEN 30 AND 39 THEN '30 - 39'
					WHEN age BETWEEN 40 AND 49 THEN '40 - 49'
					WHEN age BETWEEN 50 AND 59 THEN '50 - 59'										        
					WHEN age >= 60 THEN 'Over 60'
					WHEN age IS NULL	THEN 'Others'				
					END AS option_value,
					COUNT(*) AS 'totalcount' FROM (SELECT TIMESTAMPDIFF(YEAR, `dob`, CURDATE()) AS age from user_master where unique_code in(select unique_code from offer_transaction_master where o_id=".$_REQUEST['o_id'].") 
					) AS derived
				    GROUP BY option_value
				    ORDER BY option_value";
			$dataReader=$connection->createCommand($cmd)->query();
			$agerows=$dataReader->readAll();
			
			//assign it to final data Array
				$data['ageData']=$agerows;
			
			/* END of Age wise redemption data */

			/* ********************************
			 Location wise redemption data 
			 *************************************/
			$cmd="SELECT count(*) as cnt , location as 'option_value' FROM `offer_transaction_master` 
				 where o_id=".$_REQUEST['o_id']." GROUP BY location";
			$dataReader=$connection->createCommand($cmd)->query();
			$locationrows=$dataReader->readAll();
			
				//assign it to final data Array
				$data['locationData']=$locationrows;
			
			/* END of Age wise redemption data */

			/*********************************
			GENDER WISE REDEMPTION REPORT
			******************************** */
			$cmd="SELECT count(*) as cnt , gender as 'option_value' FROM `user_master` 
				where unique_code in (select unique_code from offer_transaction_master 
										where o_id=".$_REQUEST['o_id'].") group by gender";
			$dataReader=$connection->createCommand($cmd)->query();
			$genderrows=$dataReader->readAll();
			//print_r($gender);die();
				//assign it to final data Array
				$data['genderData']=$genderrows;
			
			/* END of Age wise redemption data */






			$this->render('GenerateOfferReports',array('data'=>$data));
			exit;

		}
		$this->render('admin');
	}


	/*
	*Campaign Reports
	*/
	public function actionGenerateCampaignReports()
	{
		
		/* REPORT 1: 
		* Campaign Report 
		*/
		if(isset($_REQUEST['uid']))
		{
			$resultArray='';
			if($_REQUEST['rid']==1) //Super Admin Report
			{
				$connection = Yii::app()->db;
				/* Fetch all campaigns of Brand Partner */
				$TopOffers="SELECT o_id, count(o_id) as count from offer_transaction_master group by o_id order by count";
				$dataReader=$connection->createCommand($TopOffers)->query();
				$TopOfferrows=$dataReader->readAll();
				$FinalResult=array();
				if(!empty($TopOfferrows))
				{
					$offerid='';
					foreach ($TopOfferrows as $key => &$value) 
					{						
						$cmd="SELECT campaign_name FROM `campaign_master` where
								cm_id in (SELECT cm_id from offer_master 
							where o_id =".$value['o_id'].")  ";
						$dataReader=$connection->createCommand($cmd)->query();
						$Camprows=$dataReader->readAll();
						$value['campaign_name']=empty($Camprows[0]['campaign_name'])?'Individual':$Camprows[0]['campaign_name'];

						if(!empty($FinalResult) && array_key_exists($value['campaign_name'], $FinalResult))
						{
							
							$FinalResult[$value['campaign_name']]=$FinalResult[$value['campaign_name']]+$value['count'];							
						}
						else
						{

							$FinalResult[$value['campaign_name']]=$value['count'];

						}
					}

					$this->render('GenerateCampaignReports',array('campaigns'=>$FinalResult));
					die();
				}
				else
				{
					$this->render('GenerateCampaignReports',array('campaigns'=>$FinalResult));
					die();
				}
			}
			else //Brand Partner Report
			{

				$connection = Yii::app()->db;
				/* Fetch all campaigns of Brand Partner */
				$TopOffers="SELECT o_id, count(o_id) as count from offer_transaction_master  where o_id in(SELECT o_id from offer_master where created_by=".$_REQUEST['uid'].") group by o_id order by count";
				$dataReader=$connection->createCommand($TopOffers)->query();
				$TopOfferrows=$dataReader->readAll();
				$FinalResult=array();
				if(!empty($TopOfferrows))
				{
					$offerid='';
					foreach ($TopOfferrows as $key => &$value) 
					{						
						$cmd="SELECT campaign_name FROM `campaign_master` where
								cm_id in (SELECT cm_id from offer_master 
							where o_id =".$value['o_id'].")  ";
						$dataReader=$connection->createCommand($cmd)->query();
						$Camprows=$dataReader->readAll();
						$value['campaign_name']=empty($Camprows[0]['campaign_name'])?'Individual':$Camprows[0]['campaign_name'];

						if(!empty($FinalResult) && array_key_exists($value['campaign_name'], $FinalResult))
						{
							
							$FinalResult[$value['campaign_name']]=$FinalResult[$value['campaign_name']]+$value['count'];							
						}
						else
						{

							$FinalResult[$value['campaign_name']]=$value['count'];

						}
					}
					
					$this->render('GenerateCampaignReports',array('campaigns'=>$FinalResult));
					die();
				}
				else
				{

					$this->render('GenerateCampaignReports',array('campaigns'=>$FinalResult));
					die();
				}
					
			}
		}
		$this->render('GenerateCampaignReports');
	}
	/* 
	*Campaign Wise offer Reports - ajax function call "getajaxOffers"
	*/
	public function actiongetajaxOffers()
	{
		$cm=$_REQUEST['campaign'];
		$connection = Yii::app()->db;
		if($cm=='Individual')
		{
			$cm=0;
			$Offers="SELECT offer_name, count(offer_transaction_master.o_id) as count from offer_transaction_master ,offer_master
				where offer_transaction_master.o_id in (SELECT o_id from offer_master 
								where cm_id ='".$cm."') 
				and offer_transaction_master.o_id=offer_master.o_id
				group by offer_transaction_master.o_id order by count";
			$dataReader=$connection->createCommand($Offers)->query();
			$OfferRows=$dataReader->readAll();
		}
		else //fetch campaign id
		{
			$Offers='SELECT offer_name, count(offer_transaction_master.o_id) as count from offer_transaction_master ,offer_master
				where offer_transaction_master.o_id in (SELECT o_id from offer_master 
								where cm_id in (SELECT cm_id from campaign_master where campaign_name ="'.$cm.'")) 
				and offer_transaction_master.o_id=offer_master.o_id
				group by offer_transaction_master.o_id order by count';
			$dataReader=$connection->createCommand($Offers)->query();
			$OfferRows=$dataReader->readAll();
		}
		$str='';
		foreach ($OfferRows as $key => $value) 
		{
			$str .= '["'.$value["offer_name"].'",  '. $value["count"].'],';
		}
		$str=trim($str,",");
		echo $str;

	}


	/* 
	* Report Dashboard
	*/
	public function actionReportDashboard()
	{
		$this->render('ReportDashboard');
	}	

	public function actionGenerateLocationReports()
	{
		$this->render('GenerateLocationReports');
	}



	//getMerchants
    public function actiongetMerchants()
    {
    	$cities=$_POST['cid'];    	
    	$oid=$_POST['oid'];
    	if(!empty($cities))
    	{
    		$citiesStr='';
    		foreach ($cities as $key => $value) {
    			$citiesStr .=$value.",";
    		}
    		$citiesStr=trim($citiesStr,',');
    	}
    	if(!empty($citiesStr))
    	{
    		$session=new CHttpSession;
        	$session->open();
    		if($session['rid']==2)
				$str='and created_by In ('.$session['uid'].',23)';
			else
				$str='';
    		$connection = Yii::app()->db;
	   		$cmd="SELECT m_id,establishment_name from merchant_master where m_city in (".$citiesStr.") and is_active=0 " .$str." order by establishment_name ";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();	
			$str='';
			if(!empty($rows))
			{		
				if(!empty($oid))
				{
					$array=array();
					$cmd1="SELECT m_id from offer_merchant_assoc where `o_id`=".$oid;
					$dataReader=$connection->createCommand($cmd1)->query();
					$rows2=$dataReader->readAll();
					
					if(!empty($rows2)){
						foreach ($rows2 as $key => $value) {
							$array[]=$value['m_id'];
						}						
					}
				}
				$str='<select id="merchant_dd" name="merchants[]" multiple="multiple">';
				foreach ($rows as $key => $value) 
				{
					if(!empty($array))
					{
						if(in_array($value['m_id'],$array))
							$str.= '<option value='.$value['m_id'].' selected>'.$value['establishment_name'].'</option>';
						else
							$str.= '<option value='.$value['m_id'].'>'.$value['establishment_name'].'</option>';
					}
					else
						$str.= '<option value='.$value['m_id'].'>'.$value['establishment_name'].'</option>';
				}
				$str.='</select>';
			}
			echo $str;	
    	}
    	
    }

    //get cities
    public function actiongetcities()
    {
    	$type=$_POST['type'];    	
    	$oid=$_POST['oid'];
    	$state=$_POST['state'];
    	
    	if(!empty($type))
    	{
    		$connection = Yii::app()->db;
	   		$cmd="SELECT bc_id,city_name from bevrage_cities where (type=".$type." or type=3) and state_code='".$state."'  order by city_name";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();			
			$str='';
			if(!empty($rows))
			{	
				if(!empty($oid))
				{
					
					$array=array();
					$cmd1="SELECT city_id from offer_location_assoc where `o_id`=".$oid;
					$dataReader=$connection->createCommand($cmd1)->query();
					$rows2=$dataReader->readAll();
					
					if(!empty($rows2)){
						//echo "here3";die;
						foreach ($rows2 as $key => $value) {
							$array[]=$value['city_id'];
						}						
					}
				}
				//print_r($rows);

				//print_r($array);
				$str='<select id="city_dd" name="city[]" multiple="multiple" onchange="merchantDropdwn()">';
				foreach ($rows as $key => $value) 
				{
					if(!empty($array))
					{
						if(in_array($value['bc_id'],$array))
							$str.= '<option value='.$value['bc_id'].' selected="selected">'.$value['city_name'].'</option>';
						else
							$str.= '<option value='.$value['bc_id'].'>'.$value['city_name'].'</option>';
					}
					else
						$str.= '<option value='.$value['bc_id'].'>'.$value['city_name'].'</option>';
				}
				$str.='</select>';
			}
			echo $str;	
			die();
    	}
    	
    }

}

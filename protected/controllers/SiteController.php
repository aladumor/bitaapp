<?php
class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			// captcha action renders the CAPTCHA image displayed on the contact page
			'captcha'=>array(
				'class'=>'CCaptchaAction',
				'backColor'=>0xFFFFFF,
			),
			// page action renders "static" pages stored under 'protected/views/site/pages'
			// They can be accessed via: index.php?r=site/page&view=FileName
			'page'=>array(
				'class'=>'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
                $session=new CHttpSession;
                $session->open();
		// renders the view file 'protected/views/site/index.php'
		// using the default layout 'protected/views/layouts/main.php'
		$this->render('index',array('username' => $session["fullname"]));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		if($error=Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

	/**
	 * Displays the contact page
	 */
	public function actionContact()
	{   
		$model=new ContactForm;
		if(isset($_POST['ContactForm']))
		{
			$model->attributes=$_POST['ContactForm'];
			if($model->validate())
			{
				$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
				$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
				$headers="From: $name <{$model->email}>\r\n".
					"Reply-To: {$model->email}\r\n".
					"MIME-Version: 1.0\r\n".
					"Content-type: text/plain; charset=UTF-8";

				mail(Yii::app()->params['adminEmail'],$subject,$model->body,$headers);
				Yii::app()->user->setFlash('contact','Thank you for contacting us. We will respond to you as soon as possible.');
				$this->refresh();
			}
		}
		$this->render('contact',array('model'=>$model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
		$model=new LoginForm;
		// if it is ajax validation request
		$session=new CHttpSession;
        $session->open();
        if(!empty($session['uid']))
        {
        	$this->render('index');
        	die();
        }
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/index");
		}
		// display the login form
		$this->render('login',array('model'=>$model));
	}
	public function actionLogout()
	{
		Yii::app()->user->logout();
                $this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
	}

	/**
	 * Displays the login page
	 */
	public function actionForgotPassword()
	{
		$model=new LoginForm;
		// collect user input data
		if(isset($_POST) && $_POST != '' && count($_POST)>0)
		{
			/*Reset Password link to be sent to User*/
			$connection = Yii::app()->db;
			$sql = "SELECT * from  `user_master` WHERE email = '".$_POST['email']."' and ur_id=2";
			$dataReader = $connection->createCommand($sql)->query();
			$rows=$dataReader->readAll();
			if(count($rows)>0)
			{
				/*$newPassword=rand(00000,999999);
				$newPassword=base64_encode($newPassword);
				*/
				$Password=base64_decode($rows[0]['user_pass']);
				$body1 =" Forgot Password Request :<br>"."Your Bevrage Account Password :".$Password.".";
				/*$connection = Yii::app()->db;
				$sql = "UPDATE `user_master` SET `user_pass`='".$Password."' WHERE u_id = '".$rows[0]['u_id']."'";
				$dataReader = $connection->createCommand($sql)->query();*/
				$email=empty($_POST['email'])?0:$_POST['email'];
				$message = new YiiMailMessage;
				$message->setBody($body1, 'text/html');	
				$message->setsubject('Reset Password');
				$message->addTo($email);
				$message->from ='no-reply@bevrage.com';// Yii::app()->params['adminEmail'];
				Yii::app()->mail->send($message);

			 	$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
			}
			else
			{
				$this->render('ForgotPassword',array('model'=>$model,'msg'=>'Please Enter Registered Email.'));
				exit;
			}
			
		}
		$this->render('ForgotPassword',array('model'=>$model));
	}

	/**
	 * Reset Password
	 */
	public function actionResetPassword()
	{
		$model=new LoginForm;
		$u_id = $_REQUEST['id'];
		// collect user input data
		if(isset($_POST) && $_POST != '' && count($_POST)>0)
		{
			$password=empty($_POST['password'])?0:$_POST['password'];
			$user_pass = base64_encode($password);
			//$confirm_password=empty($_POST['confirm_password'])?0:$_POST['confirm_password'];

			/* @desc :update password data in user master.*/
			$connection = Yii::app()->db;
			$sql = "UPDATE `user_master` SET `user_pass`='".$user_pass."' WHERE u_id = '".$u_id."'";
			$dataReader = $connection->createCommand($sql)->query();

		 	$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/login");
		}
		$this->render('ResetPassword',array('model'=>$model));
	}

	/*
	* ComingSoonPage
	*/
	public function actionComingSoonPage()
	{
		$this->render('ComingSoonPage');
	}

	//MARKETING SITE OFFERS
    public function actionajaxmarketingOffers()
    {
    	$filter=empty($_REQUEST['filterby'])?'':$_REQUEST['filterby'];
    	if($filter==3)
    	{
    		//Fetch all the latest 50 offers of on pre/ Off Pre
	    	$connection = Yii::app()->db;
			$cmd="SELECT * from offer_master where is_active=0 and is_delete=0 ORDER BY o_id DESC";
			$dataReader=$connection->createCommand($cmd)->query();
			$offerList=$dataReader->readAll();
			if(!empty($offerList))	
			{
				//find all merchants of each offers
				foreach ($offerList as $key => &$value) 
				{
					$cmd="SELECT offer_merchant_assoc.*,merchant_master.establishment_name from offer_merchant_assoc,merchant_master 
							where o_id=".$value['o_id']."
							and merchant_master.m_id=offer_merchant_assoc.m_id";
					$dataReader=$connection->createCommand($cmd)->query();
					$merchantList=$dataReader->readAll();
					if(!empty($merchantList))	
						$value['merchantList']=count($merchantList);
					else
						unset($offerList[$key]);					
				}			
			}
    	}
    	else
    	{
    		if($filter=='1')//filter by city
    		{
				$cityid=$_REQUEST['val'];
    			//Fetch all the latest 50 offers of on pre/ Off Pre
		    	$connection = Yii::app()->db;
				$cmd="SELECT * from offer_master where is_active=0 and is_delete=0
						and o_id in (select o_id from offer_location_assoc where city_id=".$cityid.")  ORDER BY o_id DESC";
				$dataReader=$connection->createCommand($cmd)->query();
				$offerList=$dataReader->readAll();
				if(!empty($offerList))	
				{
					//find all merchants of each offers
					foreach ($offerList as $key => &$value) 
					{
						$cmd="SELECT offer_merchant_assoc.*,merchant_master.establishment_name from offer_merchant_assoc,merchant_master 
								where o_id=".$value['o_id']."
								and merchant_master.m_id=offer_merchant_assoc.m_id
								and merchant_master.m_city=".$cityid;
						$dataReader=$connection->createCommand($cmd)->query();
						$merchantList=$dataReader->readAll();
						if(!empty($merchantList))	
							$value['merchantList']=count($merchantList);
						else
							unset($offerList[$key]);					
					}			
				}
    		}
    		else if($filter=='2')//filter by offer type
    		{
    			$type=$_REQUEST['val'];
    			//Fetch all the latest 50 offers of on pre/ Off Pre
		    	$connection = Yii::app()->db;
				$cmd="SELECT * from offer_master where is_active=0 and is_delete=0
						and offer_type='".$type."'  ORDER BY o_id DESC";
				$dataReader=$connection->createCommand($cmd)->query();
				$offerList=$dataReader->readAll();
				if(!empty($offerList))	
				{
					//find all merchants of each offers
					foreach ($offerList as $key => &$value) 
					{
						$cmd="SELECT offer_merchant_assoc.*,merchant_master.establishment_name from offer_merchant_assoc,merchant_master 
								where o_id=".$value['o_id']."
								and merchant_master.m_id=offer_merchant_assoc.m_id";
						$dataReader=$connection->createCommand($cmd)->query();
						$merchantList=$dataReader->readAll();
						if(!empty($merchantList))	
							$value['merchantList']=count($merchantList);
						else
							unset($offerList[$key]);					
					}			
				}
    		}	
    	}

    	//display offers 
    	$str='';
    	if(!empty($offerList))
		{
			
			$imagepath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images";
			foreach ($offerList as $key => $value) 
			{
				$image=$imagepath."/".$value['offer_image'];
				$str.="<div class='offerlist' id='{$value['o_id']}'>";
				$str.="<div class='offer-list-img'><img src='{$image}' alt=''  /> </div>";
				$str.="<div class='offer-dec'><div class='offer-lbl'>{$value['offer_name']} </div>";				
				$str.="<div class='offer_desc'>{$value['offer_desc']} </div>";

				$str.="<div class='clearfix'><label class='offer-date'><i class='fa fa-calendar'></i> {$value['end_date']} </label>";	
				$str.="<label class='offer-location'><i class='fa fa-map-marker'></i> {$value['merchantList']} locations </label></div>";							

				$str.="<a class='view-btn' href='offerDetails?id={$value['o_id']}'>View Details</a><br>";
				$str.="</div></div>";
			}
		}
		echo $str;
    }
    public function actionOffersList()
    {
    	$this->render('OffersList');
    }

    //Marketing site offer details
    public function actionofferDetails()
    {
    	/*$session=new CHttpSession;
        $session->open();        
        if($session['uid']!='' && $session['rid']==4)
        {
        	*/$o_id=$_REQUEST['id'];
	    	//Fetch offers details
		    	$connection = Yii::app()->db;
				$cmd="SELECT * from offer_master where is_active=0 and is_delete=0 and o_id=".$o_id;
				$dataReader=$connection->createCommand($cmd)->query();
				$offerList=$dataReader->readAll();
				if(!empty($offerList))	
				{
						$cmd="SELECT offer_merchant_assoc.*,merchant_master.establishment_name,phone,m_street,m_zipcode,m_state,city_name 
							from offer_merchant_assoc,merchant_master ,bevrage_cities
								where o_id=".$o_id."
								and merchant_master.m_id=offer_merchant_assoc.m_id
								and merchant_master.m_city=bevrage_cities.bc_id";
						$dataReader=$connection->createCommand($cmd)->query();
						$merchantList=$dataReader->readAll();
						if(!empty($merchantList))	
							$offerList[0]['merchantList']=$merchantList;
						else
							unset($offerList[$key]);						
				}
	    		$this->render('offerDetails',array('offerList'=>$offerList));
        /*}
        else
        {
        	$this->render('Marketinglogin');
        }*/
    	
    }
    public function actionMarketinglogin()
    {
    	$model=new LoginForm;
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='login-form')
		{
			echo "here";die();
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}

		// collect user input data
		if(isset($_POST['LoginForm']))
		{
			$model->attributes=$_POST['LoginForm'];
			echo "here2";die();
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(Yii::app()->getBaseUrl(true)."/index.php/site/OffersList");
		}
		// display the login form
		$this->render('Marketinglogin',array('model'=>$model));
    }

    public function actionajaxgetQr()
    {
    	$email=$_REQUEST['email'];
    	//Register User in DB and get unique code and 

    	/* GENERATE QR CODE */
			$urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice/offer_detail?o_id=".$model->o_id."&origin=Web&u_id=";
			$code=new QRCode($urlpath);		 
			
			// to flush the code directly
			//$code->create();

			// to save the code to file				
			$codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "offer_codes" . DIRECTORY_SEPARATOR.$codefilename;
			$code->create($codepath);
		/* END OF QR CODE */

    }
}
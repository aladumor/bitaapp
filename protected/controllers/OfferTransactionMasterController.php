<?php

class OfferTransactionMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
       

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
                $session=new CHttpSession;
                $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','Payapi','checkPaymentMethod','CustomDelete','androidPush','Pushios','ajaxPendingRecords','payCheque'),
				'users'=>array($session["uname"]),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','Payapi'),
				'users'=>array($session["uname"]),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array($session["uname"]),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actioncheckPaymentMethod()
	{
		
		$o_id=$_REQUEST['o_id'];
		$ot_id=$_REQUEST['ot'];
		$error_flag=0;
		$error_msg='';
		if($_REQUEST['type']=='on_pre')
			$certificate='BevRageMerchantck.pem';
		else
			$certificate='BevRageck.pem';
	   	if(!empty($o_id))
	   	{

	   		/*
	   		* fecth order details and user details
	   		*/
	   		$connection = Yii::app()->db;
	   		$payer_id=$_REQUEST['payer_id'];
	   		$cmd="SELECT `dt_id`,`token`,badge,user_master.* FROM `user_device_master`,user_master WHERE user_master.u_id in (select u_id from user_master where unique_code in(select unique_code from offer_transaction_master where ot_id=".$ot_id."))
					and user_master.`u_id`=user_device_master.`u_id`  and user_master.is_active=0";
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			if(!empty($rows))
			{

				$uid=$rows[0]['u_id'];
				$type="redemeed";			
				$deviceToken=$rows[0]['token'];
				//chk is deposit amount is suffecient for payment
				
				$cmd2="SELECT * FROM user_master WHERE user_master.u_id IN (SELECT u_id from offer_master where o_id=".$o_id.")";
				$dataReader=$connection->createCommand($cmd2)->query();
				$rows2=$dataReader->readAll();
				
				if(!empty($rows2))
				{ 
					
					/*if($rows2[0]['deposit_amount']!='' && ($rows2[0]['deposit_amount']>=$_REQUEST['amount']))
					{*/
						
						if($rows[0]['active_payment_method_id']==1 )
						{

							//PAYPAL Transaction
							//$receiver_email='pooja.shah@credencys.com';

							$receiver_email=$rows[0]['paypal_email'];//'dounyad@bevrage.com';
							$amount=$_REQUEST['amount'];
							$response= $this->actionPayapi($receiver_email,$amount);
							//print_r($response);die();
							if(!empty($response))
							{

								if($response["responseEnvelope.ack"] == "Success")
								{
							   		$error_flag=0;
							   		$pay_key_id=$response["paymentInfoList.paymentInfo(0).transactionId"];
								}
								else
								{
									$error_flag=1;
									$status=4;
									$error_msg=urldecode($kArray["error(0).message"]);
								}
							}
						}
						else if($rows[0]['active_payment_method_id']==3 )
						{	
							//Take to their pending cheque amount details.							
							$this->actionChequeDeatils($ot_id);	
							exit;						
						}
						else
						{
						
							$error_flag==1;
						}
					/*}
					else
					{
						//echo "here";die();
						//amount not sufficient to pay customers
						$error_flag=1;
						$error_msg="Brand Partner Deposit Amount Limit Recahed! ";
					}*/

					//SEND PUSH NOTIFICATIONs
					if($error_flag==0 && $deviceToken!='')
					{
						$status=1;
						$msg="Your Reimbursement is Successfull,Cheers!";
						if($rows[0]['dt_id']==1)
						{	
							$badge=$rows[0]['badge'];
							$this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
							$cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
							$dataReader=$connection->createCommand($cmd);
							$rows=$dataReader->execute();
							
						}
						else
						{
							$this->androidPush($uid,$type,$msg,$deviceToken);
						}
					}
					else
					{	
						if($status!=4)
							$status=3;		
						$msg="Please link your valid Paypal Account for Rebates!";
						if($rows[0]['dt_id']==1)
						{			
							$badge=$rows[0]['badge'];		
							$this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
							$cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
							$dataReader=$connection->createCommand($cmd);
							$rows=$dataReader->execute();
						}
						else
						{
							$this->androidPush($uid,$type,$msg,$deviceToken);
						}
					}
				}
				else
				{
					if($_REQUEST['type']=="off_pre")	
					{						
						$this->redirect(array('../index.php/scanReceiptsMaster/otView?error=Error Occured.Please try again later.type=off_pre&ot='.$ot_id));					
					}
					else
					{
						$this->redirect(array('../index.php/OfferTransactionMaster/view?error=Error Occured.Please try again later.&id='.$ot_id));	
					}
					exit;
				}
				
					
			}
			else
			{
				$status=2;	
				//user id not found
				$error_flag=1;
				$error_msg="Merchant Not Active Any More! ";
				if($_REQUEST['type']=="off_pre")	
				{						
					$this->redirect(array('../index.php/scanReceiptsMaster/otView?error='.$error_msg.'&type=off_pre&ot='.$ot_id));					
				}
				else
				{
					$this->redirect(array('../index.php/OfferTransactionMaster/view?error='.$error_msg.'&id='.$ot_id));	
				}
				exit;
			}


			if($error_flag==1)
			{
				$status=2;
				if($_REQUEST['type']=="off_pre")	
				{						
					$this->redirect(array('../index.php/scanReceiptsMaster/otView?error=Error Occured.Please try again later.&type=off_pre&ot='.$ot_id));					
				}
				else
				{
					$this->redirect(array('../index.php/OfferTransactionMaster/view?error=Error Occured.Please try again later.&id='.$ot_id));	
				}
				exit;

			}
			else
			{
				if(!empty($pay_key_id))
				{
					if($_REQUEST['type']=='off_pre')
						$cmd="UPDATE `offer_transaction_master` SET customer_payment_status=".$status." ,transaction_id='".$pay_key_id."' where ot_id=".$_REQUEST['ot'];
					else
						$cmd="UPDATE `offer_transaction_master` SET payment_status=".$status.",transaction_id='".$pay_key_id."' where ot_id=".$_REQUEST['ot'];
					
					$dataReader=$connection->createCommand($cmd);
					$rows=$dataReader->execute();
					$error_msg="Payment Successfully done!";				
				}
				else
				{
					if($_REQUEST['type']=='off_pre')
						$cmd="UPDATE `offer_transaction_master` SET customer_payment_status=".$status." where ot_id=".$_REQUEST['ot'];
					else
						$cmd="UPDATE `offer_transaction_master` SET payment_status=".$status." where ot_id=".$_REQUEST['ot'];
					
					$dataReader=$connection->createCommand($cmd);
					$rows=$dataReader->execute();
					$error_msg="Payment Failed!";	
				}				
			}

			if($_REQUEST['type']=="off_pre")	
			{						
				$this->redirect(array('../index.php/scanReceiptsMaster/otView?error='.$error_msg.'&type=off_pre&ot='.$ot_id));					
			}
			else
			{
				$this->redirect(array('../index.php/OfferTransactionMaster/view?error='.$error_msg.'&id='.$ot_id));	
			}	
		}
		else
		{
			if($_REQUEST['type']=="off_pre")	
			{						
				$this->redirect(array('../index.php/scanReceiptsMaster/otView?error='.$error_msg.'&type=off_pre&ot='.$ot_id));					
			}
			else
			{
				$this->redirect(array('../index.php/OfferTransactionMaster/view?error='.$error_msg.'&id='.$ot_id));	
			}
			
		}
	}

	public function actionPayapi($receiver_email,$amount)
	{
		   //turn php errors on
		   ini_set("track_errors", true);

		    //set PayPal Endpoint to sandbox
		  	$url = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");

		  
		   

		   //PayPal SANDBOX API Credentials
		   $app_id = 'APP-80W284485P519543T';//  sandbox
		   $api_username = "mitchb-facilitator_api1.bevrage.com";
		   $api_password = "AYKGUWNDNNHMFXVP";
		   $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AazG6HsiIpUp3NxmRNO-4NaxSnkT";
		   


		   //PayPal LIVE API Credentials
		   
		   //set PayPal Endpoint to live
		   // $url = trim("https://svcs.paypal.com/AdaptivePayments/Pay");
		   /* $app_id = 'APP-2RM4658855985480F';//live;
		   $api_username = "mitchb_api1.bevrage.com";
		   $api_password = "55PSXR9XQTZTY93E";
		   $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AbJpKwnQGTIHON6MQofMMvBIaggN";*/
		   

		   //$amount =1 ;//$amount; //TODO
		   $API_RequestFormat = "NV";
		   $API_ResponseFormat = "NV";

		  	$receiver_email = $receiver_email; //TODO
		  	// die();

		   //Create request payload with minimum required parameters
		   $bodyparams = array ("requestEnvelope.errorLanguage" => "en_US",
		                     "actionType" => "PAY",
		                     "cancelUrl" => "http://localhost/beverage/index.php/site/index",
		                     "returnUrl" => "http://localhost/commonadmin/index.php/CategoryMaster/admin",
		                     "currencyCode" => "USD",
		                     "receiverList.receiver(0).email" =>"mitchb-buyer@bevrage.com",// 'mitchb-buyer@bevrage.com',//$receiver_email,(live)
		                     "receiverList.receiver(0).amount" => 1,//$amount,
		                     "senderEmail"=>"mitchb-facilitator@bevrage.com",// 'mitchb-facilitator@bevrage.com',//"mitchb@bevrage.com", (live)
		    );

		   // convert payload array into url encoded query string
		   $body_data = http_build_query($bodyparams, "", chr(38));


		   	try
		   	{

			   	//create request and add headers
			   	$params = array("http" => array(
			       "method" => "POST",                                                 
			       "content" => $body_data,                                             
			       "header" =>  "X-PAYPAL-SECURITY-USERID: " . $api_username . "\r\n" .
			                    "X-PAYPAL-SECURITY-SIGNATURE: " . $api_signature . "\r\n" .
			                    "X-PAYPAL-SECURITY-PASSWORD: " . $api_password . "\r\n" .
			                    "X-PAYPAL-APPLICATION-ID: " . $app_id . "\r\n" .
			                    "X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
			                    "X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n"
			   ));


			   	//create stream context
			    $ctx = stream_context_create($params);


			   	//open the stream and send request
			    $fp = @fopen($url, "r", false, $ctx);

			   	//get response
			    $response = stream_get_contents($fp);

			   	//check to see if stream is open
			    if ($response === false) 
			    {
			       throw new Exception("php error message = " . "$php_errormsg");
			    }

			   	//close the stream
			   	fclose($fp);

			   	//parse the ap key from the response

			   	$keyArray = explode("&", $response);

			   	foreach ($keyArray as $rVal)
			   	{
			       	list($qKey, $qVal) = explode ("=", $rVal);
			       	$kArray[$qKey] = $qVal;
			   	}
			   	//echo "<pre>";
			   	//print_r($kArray["paymentInfoList.paymentInfo(0).transactionId"]);
			   //die();
			   	//print the response to screen for testing purposes
			   	If( $kArray["responseEnvelope.ack"] == "Success") 
			   	{

			       /* foreach ($kArray as $key =>$value)
			        {
			       		echo $key . ": " .$value . "<br/>";
			   		}*/
			   		
			   		return $kArray;
			    }
			   	else 
			   	{
			   		return $kArray;
			       	//echo 'ERROR Code: ' .  $kArray["error(0).errorId"] . " <br/>";
			     	//echo 'ERROR Message: ' .  urldecode($kArray["error(0).message"]) . " <br/>";
			     	//die();
			   	}

		   	}
		   	catch(Exception $e) 
		   	{
		   		echo "Message: ||" .$e->getMessage()."||";
		   	}

		
	}


	public function actionView($id)
	{

		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new OfferTransactionMaster;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferTransactionMaster']))
		{
			$model->attributes=$_POST['OfferTransactionMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ot_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['OfferTransactionMaster']))
		{
			$model->attributes=$_POST['OfferTransactionMaster'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->ot_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE `offer_transaction_master` SET payment_status=3 where ot_id=".$id;		
		$dataReader=$connection->createCommand($cmd);
		$rows=$dataReader->execute();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionCustomDelete($id)
	{
		//$this->loadModel($id)->delete();
		$connection = Yii::app()->db;
		$cmd="UPDATE `offer_transaction_master` SET customer_payment_status=3 where ot_id=".$id;		
		$dataReader=$connection->createCommand($cmd);
		$rows=$dataReader->execute();
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		/*if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));*/
		$this->redirect(array('view/'.$id));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OfferTransactionMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new OfferTransactionMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['OfferTransactionMaster']))
			$model->attributes=$_GET['OfferTransactionMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/*
	* Fetch details of particular users for writting them a cheque  for OFF PREMISE .
	*/

	function actionChequeDeatils($ot_id)
	{
		//fetch att transactions of customer for whivh cheque has to be issued.
		$connection = Yii::app()->db;
   		$error=0;
   		//Fetch customer details
		$cmd="SELECT * from user_master where unique_code in 
   				(SELECT unique_code from offer_transaction_master where ot_id=".$ot_id.")";   		
		$dataReader=$connection->createCommand($cmd)->query();
		$userrows=$dataReader->readAll();
		if(!empty($userrows))
		{
			$uid=$userrows[0]['u_id'];
			$code=$userrows[0]['unique_code'];
			$customerName=$userrows[0]['fullname'];
			$address=$userrows[0]['addressline_1'];
		}
		else
		{
			$error=1;
		}	

		//Fetch last Payout Date
   		$cmd="SELECT * from user_cheque_payout_assoc where u_id=".$uid." order by payout_date DESC LIMIT 0,1"; 		
		$dataReader=$connection->createCommand($cmd)->query();
		$Payoutrows=$dataReader->readAll();
		$lastDate='';
		$lastPaid='';
		if(!empty($Payoutrows))
		{
			$lastDate=$Payoutrows[0]['payout_date'];
			$lastPaid=$Payoutrows[0]['payout_amount'];
		}

		//Total Outstanding Amount 
		$cmd="SELECT SUM(customer_amount) as 'totalamnt' from offer_transaction_master where unique_code='".$code."' and (customer_payment_status=2 or payment_status=2) "; 		
		$dataReader=$connection->createCommand($cmd)->query();
		$AmountRows=$dataReader->readAll();
		$outstandingAmt=empty($AmountRows[0]['totalamnt'])?'':$AmountRows[0]['totalamnt'];

		$this->render('chequePayments',array('uid'=>$uid,'ot_id'=>$ot_id,'customerName'=>$customerName,
			'address'=>$address,'lastDate'=>$lastDate,'lastPaid'=>$lastPaid,'error'=>$error,'code'=>$code,'outstandingAmt'=>$outstandingAmt,)
		);

	}
	public function actionchequePayments()
	{
		$model=new OfferTransactionMaster('search');
		$this->render('chequePayments');
	}



	
	/*
	* ajax call to get pending cheque payments transaction of customer
	*/
	public function actionajaxPendingRecords()
	{
		$connection = Yii::app()->db;
		$status=$_POST['status'];
   		$error=0;
   		//Fetch customer details
   		$code=$_POST['code'];
		$cmd="SELECT offer_transaction_master.*,offer_name,redeem_type from offer_transaction_master,offer_master where unique_code='".$code."' 
		and offer_master.o_id=offer_transaction_master.o_id
		and customer_payment_status =".$status;   		   	
		$dataReader=$connection->createCommand($cmd)->query();
		$rows=$dataReader->readAll();
		if(!empty($rows))
		{	$str='';
			$str.="</br><table class='table table-striped table-bordered responsive'>";
			$str.="<tr>
		   			<th>Offer Name</th>
		    		<th>Redeem Date</th> 
		    		<th>Amount</th>
		    		<th>Transaction Status</th>	
		    		<th>Pay</th>		    		
		  		</tr>";
			
			foreach ($rows as $key => $value) 
			{
				if($value['redeem_type']==2)
					$status=$value['customer_payment_status'];
				else
					$status=$value['payment_status'];
				$str.="<tr><td>".$value['offer_name']."</td><td>".date("m/d/Y",strtotime($value['redeem_date']))."</td><td>".$value['customer_amount']."</td><td>".$status."</td>

				<td><input class='btn btn-success'  id='pay' name='Pay' value='Pay' type='button'  onclick='paytransaction(".$value['ot_id'].")'></td></tr>";
			}
			$str.="</table>";
			echo $str;
		}
	}

	/*
	* UPDATE THE cheque payment details and transaction statuses for off Premise Transactions
	*/
	public function actionpayCheque()
	{

		$filter=$_REQUEST['payfilter'];	
		$date=gmdate('Y-m-d');
		//update user_cheque_payout table
		$connection = Yii::app()->db;
		if($filter=='date')
		{
			$start_date =date('Y-m-d',strtotime($_REQUEST['st_date']));
			$end_date =date('Y-m-d',strtotime($_REQUEST['end_date']));
			$connection = Yii::app()->db;
			if(strtotime($start_date)==strtotime($end_date))
				$cond="redeem_date='".$start_date."'";
			else
				$cond="redeem_date BETWEEN '".$start_date." AND ".$end_date."'";
			//get payout Amount
			$cmd="SELECT SUM(total_amount) as total from offer_transaction_master where 
				".$cond."
				and customer_payment_status=2 or payment_status=2
				and unique_code='".$_REQUEST['code']."' ";   		
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			$payoutAmount=$rows[0]['total'];


	   		//insert record user_cheque_payout
			$cmd="INSERT INTO `user_cheque_payout_assoc`(`ucp_id`, `u_id`, `payout_amount`, `payout_date`, `start_date`, `end_date`,`cheque_no`,`cheque_issue_date`) 
				VALUES (NULL,'".$_REQUEST['u_id']."','".$payoutAmount."','".$date."','".$start_date."','".$end_date."','".$_REQUEST['chq_no']."','".$_REQUEST['chq_issue_date']."')";   		
			$dataReader=$connection->createCommand($cmd);
			$rows=$dataReader->execute();

			//update payement status of offer transaction master
			$cmd="SELECT * from offer_transaction_master where 
				redeem_date BETWEEN '".$start_date." AND ".$end_date."'
				and customer_payment_status=2 or payment_status=2
				and unique_code='".$_REQUEST['code']."' ";   		
			$dataReader=$connection->createCommand($cmd)->query();
			$Allrows=$dataReader->readAll();
			if(!empty($Allrows))
			{
				foreach ($Allrows as $key => $value) 
				{
					if($value['type']=="off_pre")	
					{
						$cmd="UPDATE offer_transaction_master SET payment_status=1 where ot_id=".$value['ot_id'];
								/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
								and customer_payment_status=2
								and unique_code='".$_REQUEST['code']."' "; */	
					}
					else
					{
						$cmd="SELECT * from offer_master where o_id=".$value['o_id'];   		
						$dataReader=$connection->createCommand($cmd)->query();
						$offerData=$dataReader->readAll();
						if($offerData[0]['redeem_type']==1)
						{
							$cmd="UPDATE offer_transaction_master SET payment_status=1  where ot_id=".$value['ot_id'];
								/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
								and payment_status=2
								and unique_code='".$_REQUEST['code']."' ";*/
						}
						else
						{
							$cmd="UPDATE offer_transaction_master SET customer_payment_status=1  where ot_id=".$value['ot_id'];
								/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
								and customer_payment_status=2
								and unique_code='".$_REQUEST['code']."' ";*/
						}
					}
					$dataReader=$connection->createCommand($cmd);
					$rows=$dataReader->execute();
				}
				
			}
			
			$this->redirect(array('../index.php/OfferTransactionMaster/view/'.$value['ot_id']));
			
		}
		elseif ($filter=='single') 
		{
			$cmd="SELECT * from offer_transaction_master where ot_id=".$_REQUEST['ot_id'];   		
			$dataReader=$connection->createCommand($cmd)->query();
			$results=$dataReader->readAll();
			$payoutAmount=$results[0]['total'];
			//insert record user_cheque_payout
			$cmd="INSERT INTO `user_cheque_payout_assoc`(`ucp_id`, `u_id`, `payout_amount`, `payout_date`, `start_date`, `end_date`,`cheque_no`,`cheque_issue_date`) 
				VALUES (NULL,'".$_REQUEST['u_id']."','".$payoutAmount."','".$date."','".$date."','".$date."','".$_REQUEST['chq_no']."','".$_REQUEST['chq_issue_date']."')";   		
			$dataReader=$connection->createCommand($cmd);
			$rows=$dataReader->execute();
		
			if($results[0]['type']=="off_pre")	
			{
				
				$cmd="UPDATE offer_transaction_master SET customer_payment_status=1 where ot_id=".$_REQUEST['ot_id'];
				$dataReader=$connection->createCommand($cmd)->execute();	
				$this->redirect(array('../index.php/scanReceiptsMaster/otView?error=Payment Successfull&type=off_pre&ot='.$_REQUEST['ot_id']));					
			}
			else
			{
				$cmd="SELECT * from offer_master where o_id=".$results[0]['o_id'];  	
				$dataReader=$connection->createCommand($cmd)->query();
				$offerData=$dataReader->readAll();
				if($offerData[0]['redeem_type']==1)
				{
					$cmd="UPDATE offer_transaction_master SET payment_status=1  where ot_id=".$_REQUEST['ot_id'];
					$dataReader=$connection->createCommand($cmd)->execute();
					$this->redirect(array('../index.php/OfferTransactionMaster/view?error=Payment Successfull&id='.$_REQUEST['ot_id']));
					
				}
				else
				{
					$cmd="UPDATE offer_transaction_master SET customer_payment_status=1  where ot_id=".$value['ot_id'];
					$dataReader=$connection->createCommand($cmd)->execute();
					$this->redirect(array('../index.php/scanReceiptsMaster/otView?error=Payment Successfull&type=off_pre&ot='.$_REQUEST['ot_id']));
				}
			}
			
			
		}
		else
		{
			$connection = Yii::app()->db;
			//get payout Amount
			$cmd="SELECT SUM(customer_amount) as total from offer_transaction_master where 				
				customer_payment_status=2
				and unique_code='".$_REQUEST['code']."' ";   		
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			$payoutAmount=$rows[0]['total'];


	   		//insert record user_cheque_payout
			$cmd="INSERT INTO `user_cheque_payout_assoc`(`ucp_id`, `u_id`, `payout_amount`, `payout_date`) 
				VALUES (NULL,'".$_REQUEST['u_id']."','".$payoutAmount."','".$date."')";   		
			$dataReader=$connection->createCommand($cmd);
			$rows=$dataReader->execute();

			
			//update payement status of offer transaction master
			$cmd="SELECT * from offer_transaction_master where 				
				and customer_payment_status=2 or payment_status=2
				and unique_code='".$_REQUEST['code']."' ";   		
				$dataReader=$connection->createCommand($cmd)->query();
				$Allrows=$dataReader->readAll();
				if(!empty($Allrows))
				{
					foreach ($Allrows as $key => $value) 
					{
						if($value['type']=="off_pre")	
						{
							$cmd="UPDATE offer_transaction_master SET payment_status=1 where ot_id=".$value['ot_id'];
									/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
									and customer_payment_status=2
									and unique_code='".$_REQUEST['code']."' "; */	
						}
						else
						{
							$cmd="SELECT * from offer_master where o_id=".$value['o_id'];   		
							$dataReader=$connection->createCommand($cmd)->query();
							$offerData=$dataReader->readAll();
							if($offerData[0]['redeem_type']==1)
							{
								$cmd="UPDATE offer_transaction_master SET payment_status=1  where ot_id=".$value['ot_id'];
									/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
									and payment_status=2
									and unique_code='".$_REQUEST['code']."' ";*/
							}
							else
							{
								$cmd="UPDATE offer_transaction_master SET customer_payment_status=1  where ot_id=".$value['ot_id'];
									/*redeem_date BETWEEN '".$start_date."' AND '".$end_date."'
									and customer_payment_status=2
									and unique_code='".$_REQUEST['code']."' ";*/
							}
						}
						$dataReader=$connection->createCommand($cmd);
						$rows=$dataReader->execute();
					}
					
				}
			$this->redirect(array('view/395'));

		}

	}
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OfferTransactionMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OfferTransactionMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OfferTransactionMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='offer-transaction-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	/*
	*send push notification of return cashback 
	*/
	public function Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate)
	{
				
		if ($deviceToken != '0') 
		{
            $ctx = stream_context_create();            
            $passphrase = '1234';
            $certificate= $certificate;//'BevRageMerchant_Developer.pem';//'BevRageck.pem';            
            stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            $body['aps'] = array(
                'badge' => $badge,
                'alert' => $msg,
                'sound' => 'default',
                'flag'=>1,
                
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            $flag = 0;
            if (!$result) {
                $flag = 1;
            }
            fclose($fp);
            if ($flag == 0) {

                return true;
            } else {
                return FALSE;
            }
        } 
        else 
        {
            return FALSE;
        }
	}

	//android push on successfull cashback  
    public function androidPush($uid,$type,$msg,$deviceToken)
    {
      
        // Replace with real BROWSER API key from Google APIs
        $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4";       
        // Replace with real client registration IDs 
        $registrationIDs =array($deviceToken);       
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
        'registration_ids'  => $registrationIDs,
        'data'              => array( "message" => $msg,'flag'=>1),
        );
        
        $headers = array( 
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json',
        );
      
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) 
        {
                die('Problem occurred: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo $result;die();
    }

}
<?php
/**
 *@desc : This controller is example of varios methods includes in JsonWebservice Extentions.
 * Following are the steps to user this extention into you Yii Project. 
 * 1) Copy JsonWebserivice Folder into /root/protected/extensions/ Directory into your project
 * 2) Add this extention into  /root/protected/config/mail.php by following way .
 *  'components'=>array(
        'user'=>array(
            // enable cookie-based authentication
            'allowAutoLogin'=>true,
        ),
                // This is the syntaxt to include extentions in yii
                'JsonWebservice'=>array(
                        'class'=>'application.extensions.JsonWebservice.JsonWebservice',
                ),
                ...............
 * 3) Now Your Extention is ready to use for project 
 * 4) This extention inlude following public method to use :
 * ------------------------------------------------------------------------------------------
 *      a) addData : This methoad is use to insert data into database.
 *      b) updateData : This method is use to update data into database.
 *      c) userValidate : Validate User form exiting database
 *      c) fetchData : Listing Data from database
 *      d) getTotalRecords : Count total nubers of records from mysql query
 *      e) getDistanceFromLatLng : Get Distance between two latitude and longitude
 *      f) executeQuery : Execute Mysql custom query
 *      g) getRowData : Get Single Row Data from custom Mysql query 
 *      h) p: User to print array 
 *      i) sendPushNotificationToIos : Use to send push notification to IOS device 
 *      j) sendPushNotificationToAndroid : Use to send push notification to Android device 
 *      k) response : send response into json format
 * ------------------------------------------------------------------------------------------
 * Examples Urls : Please change you directory name into below url 
 * ------------------------------------------------------------------------------------------
 * 1) Insert : http://localhost/commanadmin/index.php/webservice/adddata?user_name=abc31&user_pass=abc&u_id=20&user_name_old=abc2
 * 2) Update: http://localhost/commanadmin/index.php/webservice/updatedata?user_name=abc31&user_pass=abc&u_id=20&user_name_old=abc2
 * 3) Fetch : http://localhost/commanadmin/index.php/webservice/fetchData
 * 4) UserVAlidation : http://localhost/commanadmin/index.php/webservice/uservalidate?user_name=abc31&user_pass=abc
 * 5) Get Single Row Data : http://localhost/commanadmin/index.php/webservice/getRowData
 */
class Webservice_v3Controller extends Controller {
//REGISTER END USER
    public function actionregisterUser()
    {
        $post = $_REQUEST;
        $validate = array("user_name" => array("require" => 1 ,"msg" => "Username is require."),
                         "user_pass" => array("require" => 1 ,"msg" => "Password is require."),
                         "ur_id" => array("require" => 1 ,"msg" => "user role id  is require."), 
                         //"login_type" => array("require" => 1 ,"msg" => "login type is require."),                        
                         //"zipcode" => array("require" => 1 ,"msg" => "zipcode  is require."), 
                         //"dob" => array("require" => 1 ,"msg" => "birth date  is require."), 
                         //"email" => array("require" => 1 ,"msg" => "email  is require."),
                         "dt_id" => array("require" => 1 ,"msg" => "device type  is require."),
                         "token" => array("require" => 1 ,"msg" => "device token  is require.")
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
              $res = array("status" => '-4',"error_msg" => $errorMsg) ;
             echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $username=empty($_REQUEST['user_name'])?null:$_REQUEST['user_name'];
            $password=empty($_REQUEST['user_pass'])?null:$_REQUEST['user_pass'];
            $email=$_REQUEST['email'];
            $password = base64_encode($post["user_pass"]);
            $ur_id=$_REQUEST['ur_id'];
            $zipcode=$_REQUEST['zipcode'];
            $picture=empty($_REQUEST['picture'])?NULL:$_REQUEST['picture'];
            $dob=empty($_REQUEST['dob'])?NUll:$_REQUEST['dob'];
            $device_type=$_REQUEST['dt_id'];
            $token=$_REQUEST['token'];
            $login_type=empty($_REQUEST['login_type'])?null:$_REQUEST['login_type'];
            $table = "user_master";
            $where = "user_name = '{$post["user_name"]}' or email = '{$post["user_name"]}' LIMIT 1";        
            $response = Yii::app()
                    ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
            if($response['status']!=1 )
            {   
                $connection = Yii::app()->db;    
                $codeflag=0;   
                
                while($codeflag==0)
                {
                    $rnd=rand(0000,9999);
                    if($ur_id==4)
                        $uniqueCode='cust_'.$rnd;
                    else
                        $uniqueCode='merc_'.$rnd;
                    $chkSql='SELECT u_id from user_master where unique_code="'.$uniqueCode.'"';
                    $dataReader=$connection->createCommand($chkSql)->query();
                    $rows=$dataReader->readAll();
                    if(empty($rows))
                        $codeflag=1;
                }                            
                $sql="INSERT INTO `user_master`(`u_id`, `unique_code`, `login_type`,`ur_id`, `ut_id`,email, `user_name`, `user_pass`, `addressline_1`, `addressline_2`, `city_id`, `state_id`, `country_id`, `zipcode`, `phone_no`, `fullname`, `profile_picture`, `dob`,`reward_points`, `active_payment_method_id`, `created_on` ) VALUES (NULL,'".$uniqueCode."','".$login_type."','".$ur_id."','2','".$email."','".$username."','".$password."',NULL,NULL,NULL,NULL,NULL,
                    '".$zipcode."',NULL,NULL,'".$picture."','".$dob."',0, NULL,'".date('Y-m-d')."')";
                $dataReader=$connection->createCommand($sql);
                $num= $dataReader->execute();
                $id= Yii::app()->db->getLastInsertId();
                if($num >0)
                {
                    $sql1="INSERT INTO `user_device_master`(`ud_id`, `dt_id`, `token`, `u_id`) 
                                    VALUES (NULL,'".$device_type."','".$token."','".$id."')";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();
                    if($ur_id==4)
                        $res = array("status" => '1','id'=>$id,'code'=>$uniqueCode,'name'=>$username);
                    else
                        $res = array("status" => '1','id'=>$id,'name'=>$username);
                    echo Yii::app()->JsonWebservice->response($res);
                }
                else
                {
                    $errorMsg[]='Error.Registration failed.';
                    $res = array("status" => '-4',"error_msg" => $errorMsg) ;
                    echo Yii::app()->JsonWebservice->response($res);
                }
                  
            }
            else
            {
                $errorMsg[]="Username Exists.";
                $res = array("status" => '-2',"error_msg" => $errorMsg) ;
                echo Yii::app()->JsonWebservice->response($res);
            }
        }        
    }
// USER  LOGIN 
    public function actionUserValidate()
    {
        $post = $_REQUEST ;
        $validate = array("user_name" => array("require" => 1 ,"msg" => "Username is require."),
                          "user_pass" => array("require" => 1 ,"msg" => "Password is require."),  
                          "token" => array("require" => 1 ,"msg" => "device token  is require."),
                          "dt_id" => array("require" => 1 ,"msg" => "device type  is require."),
                          "role_id" => array("require" => 1 ,"msg" => "role type  is require."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $role=$_REQUEST['role_id'];
            $connection = Yii::app()->db;  
            $password = base64_encode($post["user_pass"]);
            $table = "user_master";
            $where = "(user_name = '{$post["user_name"]}' OR email='{$post["user_name"]}') AND user_pass = '{$password}' AND ur_id='{$role}' LIMIT 1";        
            $response = Yii::app()
                    ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
            if($response['status']!=-1)
            {
                $post["table"] = 'user_master';
                $post["fields"] = 'u_id,unique_code,ur_id,user_name,phone_no,profile_picture,active_payment_method_id';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "(user_name = '{$post["user_name"]}' OR email='{$post["user_name"]}') AND user_pass = '{$password}' AND ur_id='{$role}' LIMIT 1";
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');  
                
                $post["table"] = 'user_device_master';
                $post["fields"] = 'dt_id';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "u_id='".$response['data'][0]['u_id']."'";
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db'); 
                //update token
                $dt_id=$_REQUEST['dt_id'];
                if($response2['status']==1 && $dt_id==$response2['data'][0]['dt_id'])
                {
                    $sql1="UPDATE `user_device_master` SET  dt_id='".$dt_id."', `token`='".$_REQUEST['token']."'  WHERE `u_id`='".$response['data'][0]['u_id']."'";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();    
                }
                else
                {
                   $sql1="INSERT INTO `user_device_master`(`ud_id`, `dt_id`, `token`, `u_id`) 
                                    VALUES (NULL,'".$dt_id."','".$_REQUEST['token']."','".$response['data'][0]['u_id']."')";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();
                }
                $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/profileImages/";
                $pic=$imagePath.$response['data'][0]['profile_picture'];
                if(empty($response['data'][0]['active_payment_method_id']) )
                        $active_payment_method_id='';
                    else
                        $active_payment_method_id=$response['data'][0]['active_payment_method_id'];
                if($response['data'][0]['ur_id']==4)
                {

                    //$data=array('id'=>$response['data'][0]['u_id'],'code'=>$response['data'][0]['unique_code'],'name'=>$response['data'][0]['user_name']);
                   
                    $response=array('status'=>'1','profile_picture'=>$pic,'payment_flag'=>$active_payment_method_id,'phone_no'=>$response['data'][0]['phone_no'],'id'=>$response['data'][0]['u_id'],'code'=>$response['data'][0]['unique_code'],'name'=>$response['data'][0]['user_name']);    
                }
                else
                {
                    //$data=array('id'=>$response['data'][0]['u_id'],'name'=>$response['data'][0]['user_name']);
                   $response=array('status'=>'1','profile_picture'=>$pic,'payment_flag'=>$active_payment_method_id,'phone_no'=>$response['data'][0]['phone_no'],'id'=>$response['data'][0]['u_id'],'name'=>$response['data'][0]['user_name']);     
                }            
                echo Yii::app()->JsonWebservice->response($response);
                
            }
            else
            {
                echo Yii::app()->JsonWebservice->response($response); 
            }
            
        }      
        
    }
//FORGOT PASSWORD
    public function actionForgotPassword()
    {
        $post = $_REQUEST ;
        $validate = array("user_name" => array("require" => 1 ,"msg" => "user_name is required."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $table = "user_master";
            $where = "(user_name = '{$post["user_name"]}' or email='{$post["user_name"]}') and ur_id={$post["ur_id"]} LIMIT 1";     
            $response = Yii::app()
                    ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
            if($response['status']==1)
            {
                $username = $post["user_name"];
                $post["table"] = 'user_master';
                $post["fields"] = 'user_pass,email';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "(user_name='".$username."' or email='".$post['user_name']."') and ur_id=".$post["ur_id"];
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                if($response2['status']==1)
                {
                    $password=base64_decode($response2['data'][0]['user_pass']);
                    $msg="Your Password: ".$password;
                    $message = new YiiMailMessage;      
                    $message->setBody($msg, 'text/html');       
                    $message->setsubject('BevRage: Forgot Password!');      
                    $message->addTo($response2['data'][0]['email']);         
                    $message->from = "service@bevRage.com";        
                    Yii::app()->mail->send($message); 
                    $res=array('status'=>'1');
                    
                }
                else
                {
                   $res=array('status'=>'-1'); 
                }
                echo Yii::app()->JsonWebservice->response($res); 
               
            }
            else
            {
                echo Yii::app()->JsonWebservice->response($response); 
            } 
        }
    }    

//LIST OF OFFERS
    public function actionListOffers()
    {
        $location=empty($_REQUEST['location_id'])?NUll:$_REQUEST['location_id'];
        $code=empty($_REQUEST['code'])?NUll:$_REQUEST['code'];
        $upc_code=empty($_REQUEST['upc_code'])?NULL : $_REQUEST['upc_code'];
        $timestamp=empty($_REQUEST['timestamp'])?NUll: $_REQUEST['timestamp'];
        if($location)//get offer list based on location
        {
            if($upc_code)//upc code scan based offer list 
            {

            }
            else//get offer list based on location
            {

            }
            /*if(empty($response['data'])){
                unset($response['data']);
                $response['status']='-3';
            }*/
        }
        else if($upc_code) //upc code scan- get offer list 
        {
            if($location)//get offer list based on location
            {
                $whereCon=" ABS(DATEDIFF(now(),`end_date`)) >= 0 
                                    and o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
                                    and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id  in(SELECT u_id from user_master where unique_code='".$code."'))
                                    and offer_master.is_active=0 
                                    and offer_master.is_delete=0
                                    and product_master.pm_id in (SELECT pm_id from product_master where upc_code='".$upc_code."')
                                    and offer_master.pm_id=product_master.pm_id";
            }
            else//get all offer list for upc code
            {
                //and o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
                $whereCon=" ABS(DATEDIFF(now(),`end_date`)) >= 0 
                                    and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id  in(SELECT u_id from user_master where unique_code='".$code."'))
                                    and offer_master.is_active=0 
                                    and offer_master.is_delete=0
                                    and offer_master.pm_id in (SELECT pm_id from product_master where upc_code='".$upc_code."')
                                    and offer_master.pm_id=product_master.pm_id";
            }
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'DISTINCT o_id,offer_name,"Texas City" as location,offer_type, maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = $whereCon;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
           
            //Get Redemption count of the offers
            if($response['status']==1)       
            {
                    foreach ($response['data'] as  $key=>&$value) 
                    {
                        $post["table"] = 'offer_transaction_master';
                        $post["fields"] = 'count(*) as count';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "o_id=".$value['o_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        //chk if count less then max limit or not
                        if($response2['status']==1)
                        {
                            if($response2['data'][0]['count'] < $value['maximum_redemption'])
                                $value['left_count']=$value['maximum_redemption'] - $response2['data'][0]['count'];
                            else{
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']);
                            }
                        }
                        else
                        {
                            $value['left_count']=$value['maximum_redemption'];
                        }
                    }
            }
            if(empty($response['data'])){
                unset($response['data']);
                $response['status']='-3';
            }

            echo Yii::app()->JsonWebservice->response($response); 
        }
        else //fetch all offers of app
        {   
            $whereCon='';
            if($timestamp)
                $whereCon=" and modified_on <'".$timestamp."'";
            // /and o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'o_id,offer_name,"Texas City" as location, offer_type, maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = " ABS(DATEDIFF(now(),`end_date`)) >= 0                                     
                                    and offer_master.is_active=0 
                                    and offer_master.is_delete=0
                                    and product_master.pm_id=offer_master.pm_id ".$whereCon." 
                                    order by modified_on DESC limit 0,3";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            /*echo "<pre>";
            print_r($response);
            die();*/
             //echo Yii::app()->JsonWebservice->response($response); die();
          
            //Get Redemption count of the offers
            if($response['status']==1)       
            {
                    foreach ($response['data'] as $key=>&$value) 
                    {
                        $post["table"] = 'offer_transaction_master';
                        $post["fields"] = 'count(*) as count';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "o_id=".$value['o_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        //chk if count less then max limit or not
                        if($response2['status']==1)
                        {
                            if($response2['data'][0]['count'] < $value['maximum_redemption'])
                                $value['left_count']=$value['maximum_redemption'] - $response2['data'][0]['count'];
                            else{
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']);
                            }
                        }
                        else
                        {
                            $value['left_count']=$value['maximum_redemption'];
                        }
                    }
            }

            if(empty($response['data'])){
                unset($response['data']);
                $response['status']='-3';
            }
            
            echo Yii::app()->JsonWebservice->response($response); 
        }

    }
//List of Cities
    public function actionCityList()
    {
        $post["table"] = 'bevrage_cities,offer_location_assoc';
        $post["fields"] = 'distinct bevrage_cities.bc_id,bevrage_cities.city_name';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = "bevrage_cities.bc_id=offer_location_assoc.city_id  order by city_name";
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');       
        
        echo Yii::app()->JsonWebservice->response($response); 


    }
//Details of Offers after QR CODE scan
    public function actionoffer_detail()
    {
        $o_id=$_REQUEST['o_id'];
        $code=empty($_REQUEST['cust_code'])?0:$_REQUEST['cust_code'];
        $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
        $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
        $post["table"] = 'offer_master,product_master';
        $post["fields"] = 'o_id,offer_name,redeem_type, "Texas City" as location ,maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = " ABS(DATEDIFF(now(),`end_date`)) > 0 
                                and offer_master.is_active=0 
                                and offer_master.is_delete=0
                                and end_date >='".$today."'
                                and offer_master.pm_id=product_master.pm_id
                                and o_id  NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$cust_code."')
                                and o_id=".$o_id;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        //**********Get Redemption count of the offers
            if($response['status']==1)       
            {               
                $post["table"] = 'offer_transaction_master';
                $post["fields"] = 'count(*) as count';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "o_id=".$o_id;
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                //*********chk if count less then max limit or not
                if($response2['status']==1)
                {
                    $cnt=$response2['data'][0]['count'];
                    $max=$response['data'][0]['maximum_redemption'];
                    if($cnt< $max)
                        $response['data'][0]['left_count']=$response['data'][0]['maximum_redemption'] - $response2['data'][0]['count'];
                    else{
                        $re[]="Maximum redeem limit reached.";
                        $response = array("status" => '-4',"error_msg" => $re) ;
                    }
                        
                }
                else
                {
                    $response['data'][0]['left_count']=$response['data'][0]['maximum_redemption'];
                }
            }
            echo Yii::app()->JsonWebservice->response($response); 
    }
//MERCHNAT SIDE : redeem offer ON-PREMISE from Merchant App
    public function actionRedeemOfferMerchant()
    {
        $o_id=$_REQUEST['o_id'];
        $cust_code=$_REQUEST['code'];
        $merchant_id=empty($_REQUEST['merchant_id'])? NULL:$_REQUEST['merchant_id'];
        $redeem_by=$_REQUEST['redeem_by'];//Customer,Merchant
        $source_uid=empty($_REQUEST['source_uid'])? NULL:$_REQUEST['source_uid'];
        $type=$_REQUEST['type'];//on_pr,off_pre
        
        /*
        *check last redemption time of customer 
        * if time less than 24 hrs -no redemption  else allow redemption
        */
        $currentTimestamp=gmdate('Y-m-d H:i:s');
        $post["table"] = 'offer_transaction_master';
        $post["fields"] = "TIMESTAMPDIFF(HOUR, CONCAT(`redeem_date`,' ' ,`transaction_time`), '".$currentTimestamp."') as 'timediff'";
        $post["beforeWhere"] = '';
        $post["afterWhere"] = "unique_code='".$cust_code."' order by transaction_time DESC LIMIT 0,1";
        $post["r_p_p"] = '';
        $post["start"] = '';

        $Timeresponse = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        //echo $Timeresponse['data'][0]['timediff'];die();
        if($Timeresponse['status']==1 && $Timeresponse['data'][0]['timediff']<=24)
        {
            $re[]="You have already redeemed an offer in 24 hours. Please check later.";
            $response = array("status" => '-5',"error_msg" => $re) ;
            echo Yii::app()->JsonWebservice->response($response); 
            exit;
        }

        //*******Check if already redeemed or not.
        $post["table"] = 'offer_transaction_master';
        $post["fields"] = '*';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = "o_id=".$o_id." and unique_code='".$cust_code."'";
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
       
        if($response['status']==1)
        {
            $re[]="Already Redeemed.";
            $response = array("status" => '-4',"error_msg" => $re) ;
            echo Yii::app()->JsonWebservice->response($response); 
        }  
        else
        {
            //*******Check if offer active or not.
            $post["table"] = 'offer_master';
            $post["fields"] = '*';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "o_id=".$o_id;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            if($response2['status']==1 && $response2['data'][0]['is_active']==0 && $response2['data'][0]['is_delete']==0 && strtolower($redeem_by)=='merchant') 
            { 
                //SEND PUSH NOTIFICTAION 
                $post["table"] = 'user_device_master';
                $post["fields"] = '*';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "u_id in (SELECT u_id from user_master WHERE unique_code='".$cust_code."' and is_login=0) order by ud_id DESC limit 1";
                $post["r_p_p"] = '';
                $post["start"] = '';
                $responseToken = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                if($responseToken['status']==1)
                {
                    
                    $token=$responseToken['data'][0]['token'];
                    $badge=$responseToken['data'][0]['badge'];
                    $ud_id=$responseToken['data'][0]['ud_id'];
                    $newBadge=$badge+1;

                    $message="Offer redeemed successfully. Drink responsibly.";
                    $imageurl= $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/".$response2['data'][0]['offer_image'];
                    if($responseToken['data'][0]['dt_id']==1)
                    {
                        
                        //Update Badge Count
                        $connection = Yii::app()->db;  
                        $sql="UPDATE user_device_master SET badge=".$newBadge." WHERE ud_id=".$ud_id;
                        $dataReader=$connection->createCommand($sql);
                        $num= $dataReader->execute();

                        //IOS PUSH
                        if(!empty($token))
                        {
                           
                            $this->iosPush($token,$message,$imageurl,$newBadge);
                        }
                    }
                    else
                    {
                        //Android PUSH
                        if(!empty($token))
                            $this->androidPush($token,$message,$imageurl);
                    }
                   
                }
                $today=gmdate('Y-m-d');
                $time=gmdate('H:i:s');
                $connection = Yii::app()->db;    
               
                $discount_price=empty($response2['data'][0]['discounted_price'])?0:$response2['data'][0]['discounted_price'];
                
               /* $merchant_amount=$discount_price;
                $bp_pay=$product_price-$discount_price;  

                $merchant_gets=$product_price - $discount_price;
                $discount_price=empty($response2['data'][0]['discount_price'])?0:$response2['data'][0]['discount_price'];
                $bp_pay=$response2['data'][0]['product_price']-$response2['data'][0]['discount_price'];  */      
                
                $sql1="INSERT INTO `offer_transaction_master`(`ot_id`, `o_id`, `redeem_by`, `redeem_date`, `total_amount`, `u_id`, 
                    `unique_code`, `payer_id`, `type`, `merchant_amount`, `brand_partner_amount`, `customer_amount`, `payment_status`,
                     `transaction_id`, `transaction_time`,customer_payment_status) 
                VALUES (NULL,".$o_id.",'".$redeem_by."','".$today."',".$discount_price.",0,'".$cust_code."','".$merchant_id."','".$type."',
                    ".$discount_price.",".$discount_price.",0,'2',null,'".$time."','0')";
                $dataReader=$connection->createCommand($sql1);
                $num1= $dataReader->execute();

                /*
                * Automatic Payouts
                */
               /* $ot_id=Yii::app()->db->getLastInsertId();
                $payResult=$this->actioncheckPaymentMethod($o_id,$ot_id,$discount_price);*/
                

                /*
                * reward points to customer based on redemption
                */
                $reward=1;
                $sql2="UPDATE user_master SET reward_points=reward_points + ".$reward." where unique_code='".$cust_code."'";
                $dataReader=$connection->createCommand($sql2);
                $num2= $dataReader->execute();
                /* END OF REWARDS CODE */

                /*insert scan record in db */
                if($responseToken['status']==1) 
                {
                    $datetime=gmdate('Y-m-d H:i:s');
                    $sql1="INSERT INTO `share_record`(`s_id`, `u_id`, `media_type`, `share_type`, `o_id`, `datetime`) 
                       VALUES (NULL,".$responseToken['data'][0]['u_id'].",'scan','','".$o_id."','".$datetime."')";
                    $dataReader=$connection->createCommand($sql1);
                    $num= $dataReader->execute();

                     //check the source of offer id qr code
                    if(!empty($source_uid))
                    {
                        
                        $sql2="UPDATE user_master SET reward_points=reward_points + 3 where u_id='".$source_uid."'";
                        $dataReader=$connection->createCommand($sql2);
                        $num2= $dataReader->execute();
                    }
                }
                
                /*---------------------------------------*/


               /* $codefilename=$o_id."_".$cust_code.'.png';
                $deleteQR=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "OneTimeQR" . DIRECTORY_SEPARATOR.$model->offer_image;
                unlink($deleteQR);*/

                $response = array("status" => '1');
                echo Yii::app()->JsonWebservice->response($response);
            }
            else 
            {
                /*$codefilename=$o_id."_".$cust_code.'.png';
                $deleteQR=getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "OneTimeQR" . DIRECTORY_SEPARATOR.$model->offer_image;
                unlink($deleteQR);*/
                
                $re[]="Offer Expired.";
                $response = array("status" => '-4',"error_msg" => $re) ;
                echo Yii::app()->JsonWebservice->response($response);
            }
             
        }    
    }
//android push on successfull offer redeem     
    public function androidPush($token,$message,$imageurl)
    {
      
        // Replace with real BROWSER API key from Google APIs
        $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4";       
        // Replace with real client registration IDs 
        $registrationIDs =array($token);       
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
        'registration_ids'  => $registrationIDs,
        'data'              => array( "message" => $message,'url'=>$imageurl,'flag'=>2 ),
        );
        
        $headers = array( 
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json',
        );
      
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) 
        {
                die('Problem occurred: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
       // echo $result;
    }
//ios push on successfull offer redeem    
    public function iosPush($deviceToken,$message,$imageurl,$badge=1)
    {
        
        if ($deviceToken != '0') {
           
            $ctx = stream_context_create();            
            $passphrase = '1234';
            $certificate= 'BevRageck.pem';
            stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            $body['aps'] = array(
                'badge' => $badge,
                'alert' => $message,
                'sound' => 'default',
                'url'=>$imageurl,
                'flag'=>2 
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            $flag = 0;

            if (!$result) {               
                $flag = 1;
            }

            fclose($fp);
            if ($flag == 0) {
               
                return true;
            } else {
              
                return FALSE;
            }
        } else {
           
            return FALSE;
        }
    }
//check social login registered or not
    public function actioncheck()
    {
        $post=$_REQUEST;
        $table = "user_master";
        $where = "user_name = '{$post["user_name"]}' LIMIT 1";        
        $response = Yii::app()
                ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
        if($response['status']==1)
        {
            $post["table"] = 'user_master';
            $post["fields"] = 'u_id as id,unique_code as code,user_name as name,active_payment_method_id as "payment_flag"';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "user_name = '{$post["user_name"]}' LIMIT 1";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        }       
        echo Yii::app()->JsonWebservice->response($response);
    }
//SEarch services
    public function actionSearch()
    {
        $is_advance=$_REQUEST['is_advance'];
        $cust_code=$_REQUEST['code'];
        $searchterm=empty($_REQUEST['searchterm'])?NUll:$_REQUEST['searchterm'];
        $location=empty($_REQUEST['city_id'])? NUll:$_REQUEST['city_id'];
        $discount=empty($_REQUEST['discount'])? NULL:$_REQUEST['discount'];
        $category=empty($_REQUEST['category_id'])? NULL: $_REQUEST['category_id'];
        $today=gmdate('Y-m-d');
        $table="offer_master,product_master";
        $whereCondition="";

        if($is_advance==1)//id advance search 
        {
           // echo "in advance";
            //if location eg: new York,New Jersey,LA,etc
            if(!empty($location))
            {
               // echo $location;die();
                $whereCondition.=" offer_master.o_id IN (select o_id from offer_location_assoc where city_id=".$location.")";
            }
            //if on-premise,off-premise
            if(!empty($category))
            {
                $category_id="3,".$category;
                if(!empty($whereCondition))
                    $whereCondition .=" and offer_type IN(".$category_id.")";
                else
                    $whereCondition .=" offer_type IN (".$category_id.")";
            }
            //if discount eg:10%,20% etc
            if(!empty($discount))
            {
                if(!empty($whereCondition))
                    $whereCondition .=" and (offer_name like '%".$discount."%' 
                                            or offer_desc like '%".$discount."%' )";//discount=".$discount."  or 
                else
                    $whereCondition .=" ( offer_name like '%".$discount."%' 
                                            or offer_desc like '%".$discount."%' )";//discount=".$discount."  or

            }
            //if serach ter, eg :vodka,gin,absolute
            if(!empty($searchterm))
            {
                //$table .=",product_master";
                if(!empty($whereCondition))
                    $whereCondition .=" and (offer_name like '%".$searchterm."%' 
                                    or offer_desc like '%".$searchterm."%' 
                                    or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%'))";
                else
                    $whereCondition .=" (offer_name like '%".$searchterm."%' 
                                    or offer_desc like '%".$searchterm."%' 
                                    or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%'))";
            }
        }
        else //if normal search term
        {
            //$table .=",product_master";
             $whereCondition .=" (offer_name like '%".$searchterm."%' 
                                or offer_desc like '%".$searchterm."%' 
                                or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%'))";

        }

        //fetch offer based on search criteria
        $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
        $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
        
        $post["table"] = $table;
        $post["fields"] = 'offer_master.o_id,offer_name, "Texas City" as location ,maximum_redemption,product_name,offer_type,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = $whereCondition ." and ABS(DATEDIFF(now(),`end_date`)) > 0 
                                and offer_master.is_active=0 
                                and offer_master.is_delete=0
                                and end_date >='".$today."'
                                and product_master.pm_id=offer_master.pm_id " ;
                                // /
                                //and o_id  NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$cust_code."')
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');       
       
        //Get Redemption count of the offers
            if($response['status']==1)       
            {
                    foreach ($response['data'] as $key=>&$value) 
                    {
                        $post["table"] = 'offer_transaction_master';
                        $post["fields"] = 'count(*) as count';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "o_id=".$value['o_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        //chk if count less then max limit or not
                        if($response2['status']==1)
                        {
                            if($response2['data'][0]['count'] < $value['maximum_redemption'])
                                $value['left_count']=$value['maximum_redemption'] - $response2['data'][0]['count'];
                            else{
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']);
                            }
                        }
                        else
                        {
                            $value['left_count']=$value['maximum_redemption'];
                        }
                    }
            }
            echo Yii::app()->JsonWebservice->response($response);
    }
//record share information
    public function actionsaveShareInfo()
    {
        $post = $_REQUEST ;
        $validate = array("media_type" => array("require" => 1 ,"msg" => "media_type is required."),
                            "share_type" => array("require" => 1 ,"msg" => "media_type is required."),
                            "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $datetime=gmdate('Y-m-d H:i:s');
            $connection = Yii::app()->db;    
            $sql1="INSERT INTO `share_record`(`s_id`, `u_id`, `media_type`, `share_type`, `o_id`, `datetime`) 
                   VALUES (NULL,".$post['u_id'].",'".$post['media_type']."','".$post['share_type']."','".$post['o_id']."','".$datetime."')";
            $dataReader=$connection->createCommand($sql1);
            $num= $dataReader->execute();
            /*
            * reward points to customer based on share
            */
            if($post['share_type']=='offer')
                $reward=2;
            else if($post['share_type']=='app')
                $reward=2;
            else
                $reward=0;
            $sql2="UPDATE user_master SET reward_points=reward_points + ".$reward." where u_id=".$post['u_id'];
            $dataReader=$connection->createCommand($sql2);
            $num2= $dataReader->execute();
            /* END OF REWARDS CODE */
            if($num>0)
            {
                $res = array("status" => '1') ;
                echo Yii::app()->JsonWebservice->response($res);
            }
            else
            {
                $res = array("status" => '-1') ;
                echo Yii::app()->JsonWebservice->response($res);
            }

        }
    }    
//MERCHNAT SIDE : my account detail service
public function actionmerchantAccountDetail()
    {

        $post = $_REQUEST;

        $validate = array("u_id" => array("require" => 1 ,"msg" => "user id is require."),
                        
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $u_id=$_REQUEST['u_id'];
            //$imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/profileImages/";
            $post["table"] = 'user_master';
            $post["fields"] = '`u_id`,  `email`,`email_flag`, `user_name`,email,user_pass,`addressline_1`,
             `addressline_2`, `zipcode`, `phone_no`, `fullname`,`profile_picture`, `dob`, `gps_flag`, `email_flag`, `notification_flag`, `reward_points`, `active_payment_method_id`, `is_active`, `is_delete`';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "u_id=".$u_id;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            if($response['status']==1){

                $response['data'][0]['user_pass']=base64_decode($response['data'][0]['user_pass']);
                //$response['data'][0]['profile_picture']=$imagePath.$response['data'][0]['profile_picture'];
            }

            echo Yii::app()->JsonWebservice->response($response);

        }
    }
//store payment information
    public function actionsavePaymentInfo()
    {
        $u_id=$_REQUEST['u_id'];
        $payType=$_REQUEST['pay_type'];//1.paypal ,2.venmo
        $email=$_REQUEST['payemail'];
        
       // if($payType==1)
            $str="UPDATE `user_master` SET  `active_payment_method_id`=".$payType.",`paypal_email`='".$email."'  WHERE `u_id`=".$u_id;
       /* else
            $str="UPDATE `user_master` SET  `active_payment_method_id`=".$payType.",`venmo_email`='".$email."'  WHERE `u_id`=".$u_id;*/

        $connection = Yii::app()->db;    
        $sql1=$str;
        $dataReader=$connection->createCommand($sql1);
        $num= $dataReader->execute();
        
        if($num>0)
        {
            $res = array("status" => '1') ;
            echo Yii::app()->JsonWebservice->response($res);
        }
        else
        {
            $res = array("status" => '-1') ;
            echo Yii::app()->JsonWebservice->response($res);
        }
    }
//update active payment type flag
    public function actioncahngePaymentMethod()
    {
        $u_id=$_REQUEST['u_id'];
        $payType=$_REQUEST['pay_type'];//1.paypal ,2.venmo
        
        
        //if($payType==1)
            $str="UPDATE `user_master` SET  `active_payment_method_id`=".$payType." WHERE `u_id`=1";
       /* else
            $str="UPDATE `user_master` SET  `active_payment_method_id`=".$payType." WHERE `u_id`=1";
*/
        $connection = Yii::app()->db;    
        $sql1=$str;
        $dataReader=$connection->createCommand($sql1);
        $num= $dataReader->execute();
        
        if($num>0)
        {
            $res = array("status" => '1') ;
            echo Yii::app()->JsonWebservice->response($res);
        }
        else
        {
            $res = array("status" => '-1') ;
            echo Yii::app()->JsonWebservice->response($res);
        }
    }
//save offers to wallet
    public function actionSaveOffer()
    {
        $post = $_REQUEST ;
        $validate = array("o_id" => array("require" => 1 ,"msg" => "offer id is required."),                           
                            "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $datetime=gmdate('Y-m-d H:i:s');
            $u_id=$_REQUEST['u_id'];
            $o_id=$_REQUEST['o_id'];
            $connection = Yii::app()->db;  
            
            //if already in list or not  
            $post["table"] = 'user_wallet_list';
            $post["fields"] = '*';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "u_id=".$u_id." and o_id=".$o_id;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');

            if($response['status']!=1)
            {
                $sql1="INSERT INTO `user_wallet_list`(`uw_id`, `u_id`, `o_id`, `status`) VALUES (NULL,'".$u_id."','".$o_id."',1)";
                $dataReader=$connection->createCommand($sql1);
                $num= $dataReader->execute();
                
                if($num>0)
                {
                    $res = array("status" => '1') ;
                    echo Yii::app()->JsonWebservice->response($res);
                }
                else
                {
                    $res = array("status" => '-1') ;
                    echo Yii::app()->JsonWebservice->response($res);
                }
            }
            else
            {                
                $data=array('Already in Wallet');
                $res = array("status" => '-3','error_msg'=>$data) ;
                echo Yii::app()->JsonWebservice->response($res);
            }

        }
    }
//fetch saved offers/redeemed offers
  /*  public function actionfetchOfferWallet()
    {
        $post = $_REQUEST ;
        $validate = array("code" => array("require" => 1 ,"msg" => "code  is required."),                           
                            "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $u_id=$_REQUEST['u_id'];
            $code=$_REQUEST['code'];
            $today=gmdate('Y-m-d');
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            //fetch saved offers
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'offer_master.o_id,offer_name,offer_type, start_date,end_date,"Texas City" as location ,
                                maximum_redemption,product_name,offer_desc,start_date,end_date,
                                ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                                CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where  unique_code='".$code."')
                                   and offer_master.o_id IN (SELECT o_id from user_wallet_list where  u_id=".$u_id.")";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response1 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            if($response1['status']==1)
            {
                foreach ($response1['data'] as $key => &$value) 
                {
                    //$value['offer_cat']='save';
                    //Scenario 1
                    if( ($value['end_date'] >$today) || ($value['is_active']==1) || ($value['is_delete']==1))
                        $value['offer_status']='-4';
                    else
                        $value['offer_status']='1';//offer still available

                    
                    //Scenario 2
                    //check offer available 
                    $post["table"] = 'offer_transaction_master';
                    $post["fields"] = 'count(*) as count';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = "o_id=".$value['o_id'];
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    
                    //*********chk if count less then max limit or not
                    if($response2['status']==1)
                    {
                        $cnt=$response2['data'][0]['count'];
                        $max=$response1['data'][0]['maximum_redemption'];
                        if($cnt>=$max){
                           $value['offer_status']='-4';
                           $value['left_count']=$max-$cnt;
                        }
                       else{
                            $value['offer_status']='1';//offer still available
                            $value['left_count']=$max-$cnt;
                       }
                    }                 
                }
            }
            //fetch redeemed offers
            $post["table"] = 'offer_master,product_master,offer_transaction_master';
            $post["fields"] = 'offer_master.o_id,offer_name,redeem_date ,offer_type,start_date,end_date, "Texas City" as location ,maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image, offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_master.o_id=offer_transaction_master.o_id
                                   and offer_master.o_id IN (SELECT o_id from offer_transaction_master where   unique_code='".$code."')";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response3 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            if($response3['status']==1)
            {
                foreach ($response3['data'] as $key => &$value) 
                {
                    $value['offer_status']='-4';
                }
            }
            if($response1['status']==1)
                $finalArray['save']=$response1['data'];
            else
                $finalArray['save']=array();

            if($response3['status']==1)
                $finalArray['redeem']=$response3['data'];
            else
                $finalArray['redeem']=array();
            if(empty($finalArray['save']) && empty($finalArray['redeem']))
                $finalArray = array("status" => '-1') ;
            echo Yii::app()->JsonWebservice->response($finalArray);
        }
    }*/

//fetch saved offers/redeemed offers
    public function actionfetchOfferWallet()
    {
        $post = $_REQUEST ;
        $validate = array("code" => array("require" => 1 ,"msg" => "code  is required."),                           
                            "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $u_id=$_REQUEST['u_id'];
            $code=$_REQUEST['code'];
            $today=gmdate('Y-m-d');
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            $recImagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/receiptImages/";//sd1.jpg
            $proImagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/productImage/";//sd1.jpg
            //fetch saved offers
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'offer_master.o_id,offer_name,offer_type, start_date,end_date,"Texas City" as location ,
                                maximum_redemption,product_name,offer_desc,start_date,end_date,
                                ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                                CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where  unique_code='".$code."')
                                   and offer_master.o_id IN (SELECT o_id from user_wallet_list where  u_id=".$u_id.")";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response1 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            if($response1['status']==1)
            {
                foreach ($response1['data'] as $key => &$value) 
                {
                    //$value['offer_cat']='save';
                    //Scenario 1
                    if( ($value['end_date'] >$today) || ($value['is_active']==1) || ($value['is_delete']==1))
                        $value['offer_status']='-4';
                    else
                        $value['offer_status']='1';//offer still available

                    
                    //Scenario 2
                    //check offer available 
                    $post["table"] = 'offer_transaction_master';
                    $post["fields"] = 'count(*) as count';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = "o_id=".$value['o_id'];
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    
                    //*********chk if count less then max limit or not
                    if($response2['status']==1)
                    {
                        $cnt=$response2['data'][0]['count'];
                        $max=$response1['data'][$key]['maximum_redemption'];
                        if($cnt>=$max){
                           $value['offer_status']='-4';
                           $value['left_count']=$max-$cnt;
                        }
                       else{
                            $value['offer_status']='1';//offer still available
                            $value['left_count']=$max-$cnt;
                       }
                    }                 
                }
            }
            //fetch redeemed offers
            $post["table"] = 'offer_master,product_master,offer_transaction_master';
            $post["fields"] = 'offer_transaction_master.ot_id,offer_master.o_id,offer_name,redeem_date ,offer_type,start_date,end_date,
                             "Texas City" as location ,maximum_redemption,product_name,offer_desc,start_date,end_date,
                             ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                             CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_transaction_master.type as "offer_redeem_type",
                             offer_transaction_master.customer_payment_status as payment_status,offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_master.o_id=offer_transaction_master.o_id 
                                    and offer_transaction_master.unique_code='".$code."' order by redeem_date DESC ";                           
                                   
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response3 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            //and offer_master.o_id IN (SELECT o_id from offer_transaction_master where   unique_code='".$code."')";
            /*         $post["table"] = 'offer_master,product_master,offer_transaction_master';
            $post["fields"] = 'offer_transaction_master.ot_id,offer_master.o_id,offer_name,redeem_date ,offer_type,start_date,end_date,
                             "Texas City" as location ,maximum_redemption,product_name,offer_desc,start_date,end_date,
                             ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                             CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_transaction_master.type as "offer_redeem_type",
                             offer_transaction_master.customer_payment_status as payment_status,offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_transaction_master.o_id=offer_master.o_id
                                   and type='off_pre'  
                                   and offer_transaction_master.unique_code='".$code."'";                                                       
                                   
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response4 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            print_r($response4);die();*/
            if($response3['status']==1)
            {
                foreach ($response3['data'] as $key => &$value) 
                {
                    $value['offer_status']='-4';
                }
            }
            if($response1['status']==1)
                $finalArray['save']=$response1['data'];
            else
                $finalArray['save']=array();

            if($response3['status']==1){
                foreach ($response3['data'] as $key => &$value) 
                {

                    if($value['offer_redeem_type']=="off_pre")
                    {
                        $post["table"] = 'scan_receipts_master';
                        $post["fields"] = 'CONCAT("'.$recImagePath.'",`receipt_image`) as receipt_image ,CONCAT("'.$proImagePath.'",`product_image`) as product_image ';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "ot_id=".$value['ot_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $responseImages = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');



                        $post["table"] = 'scan_receipts_master';
                        $post["fields"] = 'CONCAT("'.$recImagePath.'",`receipt_image`) as receipt_image ,CONCAT("'.$proImagePath.'",`product_image`) as product_image ';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "ot_id=".$value['ot_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $responseImages = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($responseImages['status']==1)
                        {
                            $value['receipt_image']=$responseImages['data'][0]['receipt_image'];
                            $value['product_image']=$responseImages['data'][0]['product_image'];
                        }
                        else
                        {
                            $value['receipt_image']="";
                            $value['product_image']="";
                        }                        
                    }
                    else
                    {
                        $value['receipt_image']='';
                        $value['product_image']='';
                    }
                }
                $finalArray['redeem']=$response3['data'];
            }
            else
                $finalArray['redeem']=array();
            if(empty($finalArray['save']) && empty($finalArray['redeem']))
                $res = array("status" => '-1') ;
            else
                $res = array("status" => '1','data'=>$finalArray) ;

            echo Yii::app()->JsonWebservice->response($res);
        }
    }
//MERCHNAT SIDE :transaction Details service
    public function actionmerchantTransactionDetails()
    {

        $post = $_REQUEST;

        $validate = array("u_id" => array("require" => 1 ,"msg" => "user id is require."),
          );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 

            $u_id=$_REQUEST['u_id'];
            $post["table"] = 'offer_transaction_master,offer_master';
            $post["fields"] = 'offer_transaction_master.ot_id,offer_transaction_master.o_id,offer_name,offer_master.discounted_price as "offer_value",redeem_date,merchant_amount,payment_status';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.o_id=offer_transaction_master.o_id and payer_id=".$u_id;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            if($response['status']==1)
            {
                $totalReceived=0;
                $totalAwaiting=0;
                $AwaitingArray=array();
                $completeArray=array();
                foreach ($response['data'] as $key => $value) 
                {
                    if($value['payment_status']=='1')
                    {                    
                        @array_push($completeArray,$value);
                        $totalReceived=$totalReceived+$value['merchant_amount'];
                    }
                    else
                    {
                        @array_push($AwaitingArray,$value);
                        $totalAwaiting=$totalAwaiting+$value['merchant_amount'];
                    }
                }
              
                $finalArray['amountAwaiting']=$totalAwaiting;
                $finalArray['amountReceived']=$totalReceived;
                if(isset($completeArray))
                    $finalArray['Complete']=$completeArray;
                if(isset($AwaitingArray))
                    $finalArray['AwaitingArray']=$AwaitingArray;

                if(isset($finalArray))
                    $response['data']=$finalArray;
            }
            
            echo Yii::app()->JsonWebservice->response($response);
        }
    }  

//MERCHANT SIDE: UPDATE MY ACCOUNT
public function actionmerchantAccountUpdate()
    {

        $post = $_REQUEST;

        $validate = array("u_id" => array("require" => 1 ,"msg" => "user id is require."),
                        
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $updateFields='';
            $u_id=$_REQUEST['u_id'];
            if(!empty($_REQUEST["user_name"]))
            {
                $updateFields .= 'user_name="'.$_REQUEST["user_name"].'",';
            }
            if(!empty($_REQUEST["email"]))
            {
                $updateFields .= 'email="'.$_REQUEST["email"].'",';
            }
            if(!empty($_REQUEST["addressline_1"]))
            {
                $updateFields .= 'addressline_1="'.$_REQUEST["addressline_1"].'",';
            }
            if(!empty($_REQUEST["addressline_2"]))
            {
                $updateFields .= 'addressline_2="'.$_REQUEST["addressline_2"].'",';
            }
            if(!empty($_REQUEST["zipcode"]))
            {
                 $updateFields .= 'zipcode="'.$_REQUEST["zipcode"].'",';
            }
            if(!empty($_REQUEST["phone_no"]))
            {
                $updateFields .= 'phone_no='.$_REQUEST["phone_no"].',';
            }
            if(!empty($_REQUEST["name"]))
            {
                $updateFields .= 'fullname="'.$_REQUEST["name"].'",';
            }
            if(!empty($_REQUEST["password"]))
            {
                $updateFields .= 'user_pass="'.base64_encode($_REQUEST["password"]).'",';
            }
            if($_REQUEST["email_flag"]=='0' || !empty($_REQUEST["email_flag"]))
            {

                  $updateFields .= 'email_flag="'.$_REQUEST["email_flag"].'",';
            }
            if($_REQUEST["gps_flag"]=='0' || !empty($_REQUEST["gps_flag"]))
            {

                    $updateFields .= 'gps_flag="'.$_REQUEST["gps_flag"].'",';
            }
             if($_REQUEST["push_flag"]=='0' || !empty($_REQUEST["push_flag"]))
            {
                    $updateFields .= 'notification_flag="'.$_REQUEST["push_flag"].'",';
            }
            if(!empty($_REQUEST["profilepic"]))
            {   
                $imagepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "profileImages" . DIRECTORY_SEPARATOR;                                                   
                /*$imageData=base64_decode($_REQUEST["profilepic"]);
                $new_image_name = date('ymd')."_".time().'.png';
                $img = $imagepath.$new_image_name;
                @file_put_contents($img, @file_get_contents($imageData));
                $imageName=$new_image_name;
                $updateFields .= 'profile_picture="'.$new_image_name.'",';*/

                /*TANVI*/
                $error_upload1 = 0;
                $final_image1 = "";
                $image1=$_REQUEST["profilepic"];
                if($image1 !== '') 
                {
                    $current_time = time();
                    $output_file1 = $imagepath.$current_time.'.png';
                    $decodedData = base64_decode($image1);
                    if(!$fp = fopen($output_file1, 'w')){
                        $error_upload1 = 1;
                    }
                    if(fwrite($fp, $decodedData)===false){
                        $error_upload1 = 1;
                    }
                    fclose($fp);
                    if($error_upload1 == 0)
                    {
                        $final_image1 = $current_time.'.png';
                    }
                    $image_data1 = file_get_contents($output_file1);                       
                    if($image_data1) 
                    {
                        $updateFields .= 'profile_picture="'.$final_image1.'",';
                    }
                   }
                /* END of tanvi CODE*/
            }
            if(!empty($_REQUEST["dob"]))
            {
                $updateFields .= "dob='".$_REQUEST["dob"]."',";
            }            

            if(!empty($updateFields))
            {
                $updateFields=trim($updateFields,",");
                $connection = Yii::app()->db;
                $sql1="UPDATE user_master SET ".$updateFields ." where  u_id=".$u_id;
                $dataReader=$connection->createCommand($sql1);
                $num= $dataReader->execute();
                
                if($num>0)
                {
                        $sql1="SELECT profile_picture from user_master where u_id=".$u_id;
                        $dataReader=$connection->createCommand($sql1)->query();
                        $row= $dataReader->readAll();
                        if(!empty($final_image1))
                            $url= 'http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/profileImages/". $final_image1;
                        else
                            $url='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/profileImages/". $row[0]['profile_picture'];
                        $imagurl[]=array('profile_picture'=>$url);
                        $res=array("status" =>'1','data'=>$imagurl);
                        echo Yii::app()->JsonWebservice->response($res);
                   
                }
                else
                {
                    $res = array("status" => '-1') ;
                    echo Yii::app()->JsonWebservice->response($res);
                }
            }
        }

    }    
//SCAN OFF PREMISE
    public function actionScanOffPremise()
    {
        $post = $_REQUEST;

        $validate = array("u_id" => array("require" => 1 ,"msg" => "user id is require."),
                         "o_id" => array("require" => 1 ,"msg" => "offer id is require."),
                         "receipt_image" => array("require" => 1 ,"msg" => "receipt_image is require."),
                         //"product_image" => array("require" => 1 ,"msg" => "product_image is require."),
                         "unique_code" => array("require" => 1 ,"msg" => "unique_code is require."),                                                 
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $cust_code=$_REQUEST['code'];
            $o_id=$_REQUEST['o_id'];
            $brand_id=1;
            $u_id=$_REQUEST['u_id'];
            $source_uid=empty($_REQUEST['source_uid'])? NULL:$_REQUEST['source_uid'];
            $productImage='';

            //*******Check if already redeemed or not.
            $post["table"] = 'offer_transaction_master';
            $post["fields"] = '*';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "o_id=".$o_id."  and unique_code='".$cust_code."'";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
           
            if($response['status']==1)
            {
                $re[]="Already Redeemed.";
                $response = array("status" => '-4',"error_msg" => $re) ;
                echo Yii::app()->JsonWebservice->response($response); 
            }  
            else
            {
                 /*
                *check last redemption time of customer 
                * if time less than 24 hrs -no redemption  else allow redemption
                */
                $currentTimestamp=gmdate('Y-m-d H:i:s');
                $post["table"] = 'offer_transaction_master';
                $post["fields"] = "TIMESTAMPDIFF(HOUR, CONCAT(`redeem_date`,' ' ,`transaction_time`), '".$currentTimestamp."') as 'timediff'";
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "unique_code='".$cust_code."' order by transaction_time DESC LIMIT 0,1";
                $post["r_p_p"] = '';
                $post["start"] = '';

                $Timeresponse = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                if($Timeresponse['status']==1 && $Timeresponse['data'][0]['timediff']<=24)
                {
                    $re[]="You have already redeemed an offer in 24 hours. Please check later.";
                    $response = array("status" => '-5',"error_msg" => $re) ;
                    echo Yii::app()->JsonWebservice->response($response); 
                    exit;
                }
                   //*******Check if offer active or not.
                $post["table"] = 'offer_master';
                $post["fields"] = '*';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "o_id=".$o_id;
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');

                if($response2['status']==1 && $response2['data'][0]['is_active']==0 && $response2['data'][0]['is_delete']==0) 
                { 


                     /*store receipts*/
                    $Recimagepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "receiptImages" . DIRECTORY_SEPARATOR;                    
                    $imageName=date('ymd')."_".time().'.png';
                    
                    /*TANVI*/
                        $error_upload1 = 0;
                        $final_image1 = "";
                        $image1=$_REQUEST["receipt_image"];
                        if($image1 !== '') 
                        {
                            $current_time = time();
                            $output_file1 = $Recimagepath.$imageName;
                            $decodedData = base64_decode($image1);
                            if(!$fp = fopen($output_file1, 'w')){
                                $error_upload1 = 1;
                            }
                            if(fwrite($fp, $decodedData)===false){
                                $error_upload1 = 1;
                            }
                            fclose($fp);
                            if($error_upload1 == 0)
                            {
                                $final_image1 = $current_time.'.png';
                                $receiptImage=$imageName;
                            }
                            $image_data1 = file_get_contents($output_file1);                       
                            
                           }
                    /* END of tanvi CODE*/
                    /* end of receipt image code*/

                    /*store product image*/
                    $productimagepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "scanProducts" . DIRECTORY_SEPARATOR;                    
                    $imageName=date('ymd')."_".time().'.png';
                    
                     /*TANVI*/
                        $error_upload1 = 0;
                        $final_image1 = "";
                        $image1=empty($_REQUEST["product_image"])?'':$_REQUEST["product_image"];
                        if($image1 !== '') 
                        {
                            $current_time = time();
                            $output_file1 = $productimagepath.$imageName;
                            $decodedData = base64_decode($image1);
                            if(!$fp = fopen($output_file1, 'w')){
                                $error_upload1 = 1;
                            }
                            if(fwrite($fp, $decodedData)===false){
                                $error_upload1 = 1;
                            }
                            fclose($fp);
                            if($error_upload1 == 0)
                            {
                                $final_image1 = $current_time.'.png';
                                $productImage=$imageName;
                            }
                            $image_data1 = file_get_contents($output_file1);                       
                            
                        }
                    /* END of tanvi CODE*/                 
                    /*end of product image code*/

                
                    $today=gmdate('Y-m-d');
                    $time=gmdate('H:i:s');
                    $connection = Yii::app()->db;    
                    $discount_price=empty($response2['data'][0]['discounted_price'])?0:$response2['data'][0]['discounted_price'];
                    
                    /*get redeem type  type and amount*/
                    if($response2['data'][0]['offer_type']==1)
                    {                                        
                        $type='on_pre';
                      
                    }
                    else{
                        $type='off_pre';
                    }
                        

                    $sql1="INSERT INTO `offer_transaction_master`(`ot_id`, `o_id`, `redeem_by`, `redeem_date`, `total_amount`, `u_id`, 
                        `unique_code`, `payer_id`, `type`, `merchant_amount`, `brand_partner_amount`, `customer_amount`, `payment_status`,
                         `transaction_id`, `transaction_time`,customer_payment_status) 
                    VALUES (NULL,".$o_id.",'Customer','".$today."',".$discount_price.",'".$u_id."','".$cust_code."','".$u_id."','".$type."',
                        0,".$discount_price.",".$discount_price.",0,NUll,'".$time."','2')";
                    $dataReader=$connection->createCommand($sql1);
                    $num= $dataReader->execute();
                    $ot_id= Yii::app()->db->getLastInsertId(); 
                    /*
                    * reward points to customer based on redemption
                    */
                    $reward=1;
                    $sql2="UPDATE user_master SET reward_points=reward_points + ".$reward." where unique_code='".$cust_code."'";
                    $dataReader=$connection->createCommand($sql2);
                    $num2= $dataReader->execute();

                     //check the source of offer id qr code
                    if(!empty($source_uid))
                    {
                        
                        $sql2="UPDATE user_master SET reward_points=reward_points + 3 where u_id='".$source_uid."'";
                        $dataReader=$connection->createCommand($sql2);
                        $num2= $dataReader->execute();
                    }
                    /* END OF REWARDS CODE */
                   

                    if($num > 0)
                    {       
                   
                        //Record receipt Scan
                        //offer transaction id
                        $sql="INSERT INTO `scan_receipts_master`(`srm_id`, `o_id`, `u_id`, `date`, `receipt_image`, `product_image`, `ot_id`) 
                                VALUES (NULL,'".$o_id."','".$u_id."','".$today."','".$receiptImage."','".$productImage."','".$ot_id."')";
                        $dataReader=$connection->createCommand($sql);
                        $num2= $dataReader->execute();

                    }
                    if($num>0 && $num2 >0){                     
                        $response = array("status" => '1');
                        echo Yii::app()->JsonWebservice->response($response);
                    }
                    else
                    {
                        /*echo "<pre>";
                        print_r($_REQUEST);*/
                       $response = array("status" => '-1');
                       echo Yii::app()->JsonWebservice->response($response);
                    }
                    
                }
                else 
                {
                    $re[]="Offer Expired.";
                    $response = array("status" => '-4',"error_msg" => $re) ;
                    echo Yii::app()->JsonWebservice->response($response);
                }
               
            }
        }
    }
//FEtch Current Payment DETAILS
    public function actionpaymentInfo()
    {
        $u_id=empty($_REQUEST['u_id'])?0:$_REQUEST['u_id'];
        $post["table"] = 'user_master';
        $post["fields"] = 'paypal_email,active_payment_method_id';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = "u_id=".$u_id;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');        
        echo Yii::app()->JsonWebservice->response($response);
    }
//feedback service
    public function actionFeedback()
    {
        $u_id=$_REQUEST['u_id'];
        $msg=$_REQUEST['msg'];
        $date=gmdate('Y-m-d');
        $connection = Yii::app()->db;    
        $sql1="INSERT INTO `feedback_master`(`f_id`, `u_id`, `message`, `date`) VALUES (NULL,'".$u_id."','".$msg."','".$date."')";
        $dataReader=$connection->createCommand($sql1);
        $num= $dataReader->execute();
        if($num>0)
        {   
            $res = array("status" => '1') ;
                    echo Yii::app()->JsonWebservice->response($res); 
       }                    
       else
        {
            $res = array("status" => '-1') ;
                        echo Yii::app()->JsonWebservice->response($res);
        }
    }
//delete savedOffers
    public function actiondeleteSavedOffer()
    {
        $u_id=$_REQUEST['u_id'];       
        $o_id=$_REQUEST['o_id'];
        $connection = Yii::app()->db;    
        $sql1="DELETE FROM `user_wallet_list` WHERE `u_id`=".$u_id." and o_id=".$o_id ;
        $dataReader=$connection->createCommand($sql1);
        $num= $dataReader->execute();
        if($num>0)
        {   
            $res = array("status" => '1') ;
                    echo Yii::app()->JsonWebservice->response($res); 
       }                    
       else
        {
            $res = array("status" => '-1') ;
                        echo Yii::app()->JsonWebservice->response($res);
        }
    } 
//Off Premise offers list
    public function actionOffPremiseList()
    {
        
        $post = $_REQUEST ;
        $validate = array(
                           "unique_code" => array("require" => 1 ,"msg" => "unique_code is required."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            //and product_master.pm_id in (SELECT pm_id from product_master where upc_code='".$upc_code."')
            //and o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
            $code=$_REQUEST['unique_code'];
            $whereCon=" ABS(DATEDIFF(now(),`end_date`)) >= 0 
                        
                        and offer_master.is_active=0 
                        and offer_master.is_delete=0
                        and offer_type='2'                        
                        and offer_master.pm_id=product_master.pm_id";
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'DISTINCT o_id,offer_name,"Texas City" as location,offer_type, maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = $whereCon;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
           
            //Get Redemption count of the offers
            if($response['status']==1)       
            {
                    foreach ($response['data'] as $key => &$value) 
                    {
                        $post["table"] = 'offer_transaction_master';
                        $post["fields"] = 'count(*) as count';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = "o_id=".$value['o_id'];
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        //chk if count less then max limit or not
                        if($response2['status']==1)
                        {
                            if($response2['data'][0]['count'] < $value['maximum_redemption'])
                                $value['left_count']=$value['maximum_redemption'] - $response2['data'][0]['count'];
                            else{
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']);
                            }
                        }
                        else
                        {
                            $value['left_count']=$value['maximum_redemption'];
                        }
                    }
            }
            echo Yii::app()->JsonWebservice->response($response); 
        }
        
    }  
//generate User Specific QR CODES
    public function actionGenarteOneTimeQR()
    {
        $u_id=$_REQUEST['u_id'];
        $unique_code=$_REQUEST['code'];
        $o_id=$_REQUEST['o_id'];
        $source_uid=empty($_REQUEST['source_uid'])? NULL:$_REQUEST['source_uid'];

        /* GENERATE QR CODE */
        if(!empty($source_uid))
            $urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice_v3/offer_detail?o_id=".$o_id."&unique_code=".$unique_code;
        else
            $urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice_3/offer_detail?souce_uid=".$source_uid."&o_id=".$o_id."&unique_code=".$unique_code;

        $code=new QRCode($urlpath);      
        $codefilename=$o_id."_".$unique_code.'.png';
        // to save the code to file             
        $codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "OneTimeQR" . DIRECTORY_SEPARATOR.$codefilename;
        $code->create($codepath);
        /* END OF QR CODE */
        $qr_code['qr_code']='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/OneTimeQR/".$codefilename;
        $res = array("status" => '1','data'=>$qr_code) ;
        echo Yii::app()->JsonWebservice->response($res);
    }

    /* ******************************************PHASE 2****************************************** */


//Send NEAR BY Push Notifications based on nearby merchants
    public function actionNearByOffersNotification()
    {
        $post = $_REQUEST;
        $validate = array("longitude" => array("require" => 1 ,"msg" => "longitude is required."),
                         "latitude" => array("require" => 1 ,"msg" => "latitude is required."),
                         "u_id" => array("require" => 1 ,"msg" => "u_id  is required."), 
                        
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
              $res = array("status" => '-4',"error_msg" => $errorMsg) ;
             echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $user_lat=$_REQUEST['latitude'];
            $user_long=$_REQUEST['longitude'];
            $u_id=$_REQUEST['u_id'];
            
            /*
            * fetch all active merchants around 2miles of users lat long whcih has bevrage offers running
            */
            if(!empty($user_lat) && !empty($user_long))
            {
                $post["table"] = 'merchant_master';
                $post["fields"] = 'ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                    * cos( radians( m_lat ) ) 
                                    * cos( radians( m_long ) - 
                                   radians( "'.$user_long.'" ) ) + 
                                   sin( radians( "'.$user_lat.'" ) ) * 
                                   sin( radians( m_lat ) ) ) ),2) AS "distance"';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id in (select o_id from offer_master where is_active=0))  
                                       and merchant_master.is_active=0 and "distance" <= "2"
                                      ';
                $post["r_p_p"] = '';
                $post["start"] = '';
                $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                if($merchantlist['status']==1)
                {                
                    /*if merchants available send push to user*/

                    //Fetch device token
                    $post["table"] = 'user_device_master';
                    $post["fields"] = '*';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = 'u_id in( select u_id from user_master where u_id='.$_REQUEST['u_id'].' and is_login=0 )';
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $devicetokenRes = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    if($devicetokenRes['status']==1)
                    {
                        $deviceToken=$devicetokenRes['data'][0]['token'];
                        $message="Click to check out nearby Bevrage promotion !";

                        $deviceType=$devicetokenRes['data'][0]['dt_id'];
                        if($deviceType==1) //IOS PUSH NOTIFICTAIONS
                        {
                            $badge=$devicetokenRes['data'][0]['badge'];
                            $ud_id=$devicetokenRes['data'][0]['ud_id'];
                            $newBadge=$badge+1;

                            //Update Badge Count
                            $connection = Yii::app()->db;  
                            $sql="UPDATE user_device_master SET badge=".$newBadge." WHERE ud_id=".$ud_id;
                            $dataReader=$connection->createCommand($sql);
                            $num= $dataReader->execute();
                            $certificate="BevRageck.pem";
                            $apnsRes = Yii::app()->JsonWebservice
                                           ->sendPushNotificationToIos($deviceToken, $message, $certificate,$type=3,$newBadge); 
                        }
                        else //ANDROID  PUSH NOTIFICTAIONS
                        {
                            $registatoin_ids= array($deviceToken);
                            $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4"; 
                            $gcmRes = Yii::app()->JsonWebservice
                                           ->sendPushNotificationToAndroid($registatoin_ids, $message,$apiKey,$type=3);
                        }
                    }
                    else
                    {
                        echo Yii::app()->JsonWebservice->response($devicetokenRes);
                    }
                }  
            }
        }
    } 

//LIST OF OFFERS V2
    public function actionListOffers_V2()
    {
        //$location=empty($_REQUEST['location_id'])?NUll:$_REQUEST['location_id'];
        $code=empty($_REQUEST['code'])?NUll:$_REQUEST['code'];
        $upc_code=empty($_REQUEST['upc_code'])?NULL : $_REQUEST['upc_code'];
        $timestamp=empty($_REQUEST['timestamp'])?NUll: $_REQUEST['timestamp'];
        $action=empty($_REQUEST['action'])?2: $_REQUEST['action'];
        $user_lat=empty($_REQUEST['latitude'])?NUll:$_REQUEST['latitude'];
        $user_long=empty($_REQUEST['longitude'])? NULL:$_REQUEST['longitude'];
        $m_ids='';
        $today=gmdate('Y-m-d');
        
        /*
        * fetch all active merchants around 2miles of users lat long which has bevrage offers running
        */
        if( (!empty($user_lat) && !empty($user_long)) && empty($location))
        {
            $post["table"] = 'merchant_master';
            $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                * cos( radians( m_lat ) ) 
                                * cos( radians( m_long ) - 
                               radians( "'.$user_long.'" ) ) + 
                               sin( radians( "'.$user_lat.'" ) ) * 
                               sin( radians( m_lat ) ) ) ),2) AS "distance"
                               ';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc)  
                                   and merchant_master.is_active=0 
                                   HAVING distance < 2 ';
            $post["r_p_p"] = '';
            $post["start"] = '';
            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            if($merchantlist['status']==1)
            {
                foreach ($merchantlist['data'] as $key => $value) 
                {
                   $m_ids .= $value['m_id'].',';
                }
                $m_ids=trim($m_ids,',');
            }  
        }
      
        if($upc_code) //upc code scan- get offer list 
        {
            
           //and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id  in(SELECT u_id from user_master where unique_code='".$code."'))
	        $whereCon='';
	        $whereCon .="   offer_type=".$action."
	                        and ABS(DATEDIFF(now(),`end_date`)) >= 0                                     
	                        and offer_master.start_date <='".$today."'                                    
	                        
	                        and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
	                        and offer_master.is_active=0 
	                        and offer_master.is_delete=0
	                        and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id)
	                        and offer_master.pm_id in (SELECT pm_id from product_master where upc_code='".$upc_code."')
	                        and offer_master.pm_id=product_master.pm_id";
	        if($m_ids)
	        {
	        	$columns=" ,'' as 'miles' ,'' as 'merchant_count'";
	            $whereCon .=" and o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
	        }

            $whereCon .= " group by o_id order by modified_on DESC ";
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'DISTINCT o_id,offer_name,offer_type,redeem_type, maximum_redemption,SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",
                                product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$imagePath.'",offer_image) as offer_image,
                                modified_on as "timestamp"'.$columns;
            $post["beforeWhere"] = '';
            $post["afterWhere"] = $whereCon;
            $post["r_p_p"] = '5';
            $post["start"] = '0';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');

            
            if(empty($response['data']))
            {
                unset($response['data']);
                $response['status']='-3';
            }
            else
            {
                //Add Merchant details
                if(!empty($m_ids) && empty($location)) //if current location not avilable send list of merchants with miles
                {
                    /*foreach ($response['data'] as $key => &$value) 
                    {
                        $post["table"] = 'merchant_master';
                        $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                           radians( "'.$user_long.'" ) ) + 
                                           sin( radians( "'.$user_lat.'" ) ) * 
                                           sin( radians( m_lat ) ) ) ),2 ) AS "distance"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$value['o_id'].'")
                                                and merchant_master.is_active=0
                                                HAVING distance < 2
                                                order by distance';
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($merchantlist['status']==1)
                        {
                            //$value['merchantlist']=$merchantlist['data'];
                            $value['miles']=$merchantlist['data'][0]['distance'];
                            $value['merchant_count']=count($merchantlist['data']);
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']); //remove offer if no participating merchants found
                        }
                    } */
                   
                }
                else //if current location not avilable send list of merchants without miles or lcoation specified
                {

                    foreach ($response['data'] as $key => &$value) 
                    {
                       	$post["table"] = 'offer_merchant_assoc';
                        $post["fields"] = 'count(*) as "m_counts"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" ' ;
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($merchantlist['status']==1)
                        {
                            $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                            $value['miles']='';
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']);
                        }
                    }
                }
            }

            if(empty($response['data']))
            {
                unset($response['data']);
                $response['status']='-3';
            }
            else
            {
            	$response['pagination']=count($response['data']);
                
            }
            echo Yii::app()->JsonWebservice->response($response); 
        }       
        else //fetch all offers of app
        {   
            $whereCon='';
            if($timestamp)
                $whereCon=" and modified_on <'".$timestamp."' ";
            if($m_ids)
            {
            	$columns=" ,'' as 'miles' ,'' as 'merchant_count'";
                $whereCon .=" and o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids."))  ";
            }
            
            /*
            * return -3 and exit if location not emty and merchants not available.
            */

            if(empty($m_ids) && !empty($user_lat) &&!empty($user_long))
            {
                $response['status']='-3';
                echo Yii::app()->JsonWebservice->response($response); 
                die();
            }
            $whereCon .= " group by o_id order by modified_on DESC  ";
           
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'o_id,offer_name,redeem_type,product_name, offer_type, SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",
                                maximum_redemption,offer_desc,start_date,CONCAT("'.$imagePath.'",offer_image) as offer_image,
                                end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left, modified_on as "timestamp"'.$columns;
            $post["beforeWhere"] = '';
            $post["afterWhere"] = " offer_type=".$action."
                                    and offer_master.is_active=0 
                                    and offer_master.is_delete=0
                                    and ABS(DATEDIFF(now(),`end_date`)) >= 0   
                                    and offer_master.start_date <='".$today."' 
                                    and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id)
                                    and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id  in(SELECT u_id from user_master where unique_code='".$code."'))
                                    and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')                                    
                                    and offer_master.pm_id=product_master.pm_id 
                                    ".$whereCon."  ";                                    
            $post["r_p_p"] = '5';
            $post["start"] = '0';            
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
           
            if(empty($response['data']))
            {
                unset($response['data']);
                $response['status']='-3';
            }
            else //add merchant counts and miles
            {
                if(!empty($m_ids))
                {
                    /*foreach ($response['data'] as $key => &$value) 
                    {

                        $post["table"] = 'merchant_master';
                        $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                           radians( "'.$user_long.'" ) ) + 
                                           sin( radians( "'.$user_lat.'" ) ) * 
                                           sin( radians( m_lat ) ) ) ),2) AS "distance"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = ' m_id in ('.$m_ids.')
                                                and merchant_master.is_active=0 
                                                HAVING distance < 2
                                                order by distance';
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($merchantlist['status']==1)
                        {
                            //$value['merchantlist']=$merchantlist['data'];
                            $value['miles']="";
                            $value['merchant_count']="";
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']);
                        }
                    } */
                   
                }
                else
                {
                    foreach ($response['data'] as $key => &$value) 
                    {
                        
                        $post["table"] = 'offer_merchant_assoc';
                        $post["fields"] = 'count(*) as "m_counts"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" ' ;
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($merchantlist['status']==1)
                        {
                            //$value['merchantlist']=$merchantlist['data'];                            
                            $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                            $value['miles']='';
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']);
                        }
                    }
                   
                }
                
            }
           
            if(empty($response['data']))
            {
                unset($response['data']);
                $response['status']='-3';
            }
            else
            {
            	$response['pagination']=count($response['data']);
                
            }
            echo Yii::app()->JsonWebservice->response($response); 
        }

    } 
//REGISTER END USER V2
    public function actionregisterUser_V2()
    {
        $post = $_REQUEST;
        $validate = array("user_name" => array("require" => 1 ,"msg" => "Username is require."),
                         "user_pass" => array("require" => 1 ,"msg" => "Password is require."),
                         "ur_id" => array("require" => 1 ,"msg" => "user role id  is require."), 
                         //"login_type" => array("require" => 1 ,"msg" => "login type is require."),                        
                         //"zipcode" => array("require" => 1 ,"msg" => "zipcode  is require."), 
                         //"dob" => array("require" => 1 ,"msg" => "birth date  is require."), 
                         //"email" => array("require" => 1 ,"msg" => "email  is require."),
                         "dt_id" => array("require" => 1 ,"msg" => "device type  is require."),
                         "token" => array("require" => 1 ,"msg" => "device token  is require.")
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
          if(array_key_exists($key,$validate) )
          {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
              $res = array("status" => '-4',"error_msg" => $errorMsg) ;
             echo Yii::app()->JsonWebservice->response($res);
        } 
        else 
        { 
            $username=empty($_REQUEST['user_name'])?null:$_REQUEST['user_name'];
            $password=empty($_REQUEST['user_pass'])?null:$_REQUEST['user_pass'];
            
            $password=empty($_REQUEST['user_pass'])?null:$_REQUEST['user_pass'];
            $email=$_REQUEST['email'];
            $password = base64_encode($post["user_pass"]);
            $ur_id=$_REQUEST['ur_id'];
            $zipcode=$_REQUEST['zipcode'];
            $picture=empty($_REQUEST['picture'])?NULL:$_REQUEST['picture'];
            $dob=empty($_REQUEST['dob'])?NUll:$_REQUEST['dob'];
            $device_type=$_REQUEST['dt_id'];
            $token=$_REQUEST['token'];
            $login_type=empty($_REQUEST['login_type'])?"normal":$_REQUEST['login_type'];

            $user_lat=empty($_REQUEST['latitude'])?null:$_REQUEST['latitude'];
            $user_long=empty($_REQUEST['longitude'])?null:$_REQUEST['longitude'];

            $table = "user_master";
            $where = "user_name = '{$post["user_name"]}' or email = '{$post["user_name"]}' LIMIT 1";        
            $response = Yii::app()
                    ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
            if($response['status']!=1 )
            {   
                $connection = Yii::app()->db;    
                $codeflag=0;   
                
                while($codeflag==0)
                {
                    $rnd=rand(000000,999999);
                    if($ur_id==4)
                    {
                        $uniqueCode=$rnd;
                        
                    }
                    else{
                        $uniqueCode='m'.$rnd;
                    }
                    $chkSql='SELECT u_id from user_master where unique_code="'.$uniqueCode.'"';
                    $dataReader=$connection->createCommand($chkSql)->query();
                    $rows=$dataReader->readAll();
                    if(empty($rows))
                        $codeflag=1;
                }                            
                $sql="INSERT INTO `user_master`(`u_id`, `unique_code`, `login_type`,`ur_id`, `ut_id`,email, `user_name`, `user_pass`, `addressline_1`, `addressline_2`, `zipcode`, `phone_no`, `fullname`, `profile_picture`, `dob`,`reward_points`, `active_payment_method_id`, `created_on`,`current_lat`,`current_long`) VALUES (NULL,'".$uniqueCode."','".$login_type."','".$ur_id."','2','".$email."','".$username."', '".$password."',NULL,NULL,
                    '".$zipcode."',NULL,NULL,'".$picture."','".$dob."',0, NULL,'".date('Y-m-d')."','".$user_lat."','".$user_long."')";
                $dataReader=$connection->createCommand($sql);
                $num= $dataReader->execute();
                $id= Yii::app()->db->getLastInsertId();
                if($num >0)
                {
                    $sql1="INSERT INTO `user_device_master`(`ud_id`, `dt_id`, `token`, `u_id`) 
                                    VALUES (NULL,'".$device_type."','".$token."','".$id."')";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();
                    if($ur_id==4)
                        $res = array("status" => '1','id'=>$id,'code'=>$uniqueCode,'name'=>$username);
                    else
                        $res = array("status" => '1','id'=>$id,'name'=>$username);
                    echo Yii::app()->JsonWebservice->response($res);
                }
                else
                {
                    $errorMsg[]='Error.Registration failed.';
                    $res = array("status" => '-4',"error_msg" => $errorMsg) ;
                    echo Yii::app()->JsonWebservice->response($res);
                }
                  
            }
            else
            {
                $errorMsg[]="Username Exists.";
                $res = array("status" => '-2',"error_msg" => $errorMsg) ;
                echo Yii::app()->JsonWebservice->response($res);
            }
        }        
    }
// USER  LOGIN  V2
    public function actionUserValidate_V2()
    {
        $post = $_REQUEST ;
        $validate = array("user_name" => array("require" => 1 ,"msg" => "Username is require."),
                          "user_pass" => array("require" => 1 ,"msg" => "Password is require."),  
                          "token" => array("require" => 1 ,"msg" => "device token  is require."),
                          "dt_id" => array("require" => 1 ,"msg" => "device type  is require."),
                          "role_id" => array("require" => 1 ,"msg" => "role type  is require."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $role=$_REQUEST['role_id'];
            $connection = Yii::app()->db;  
            $password = base64_encode($post["user_pass"]);
            $table = "user_master";
            $where = "(user_name = '{$post["user_name"]}' OR email='{$post["user_name"]}') AND user_pass = '{$password}' AND ur_id='{$role}' LIMIT 1";        
            $response = Yii::app()
                    ->JsonWebservice->userValidate($table, $where ,$dataobj = 'db');
            if($response['status']!=-1)
            {   
                $user_lat=empty($_REQUEST['latitude'])?null:$_REQUEST['latitude'];
                $user_lng=empty($_REQUEST['longitude'])?null:$_REQUEST['longitude'];
                
                $post["table"] = 'user_master';
                $post["fields"] = 'u_id,unique_code,ur_id,user_name,phone_no,profile_picture,active_payment_method_id';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "(user_name = '{$post["user_name"]}' OR email='{$post["user_name"]}') AND user_pass = '{$password}' AND ur_id='{$role}' LIMIT 1";
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');  
                
                $post["table"] = 'user_device_master';
                $post["fields"] = 'dt_id';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "u_id='".$response['data'][0]['u_id']."'";
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db'); 
                
                //update users current lat long on evry time login
                if(!empty($user_lat) &&!empty($user_long))
                {
                    $sql1="UPDATE `user_master` SET current_lat='".$user_lat."' ,current_long='".$user_long."'  WHERE `u_id`='".$response['data'][0]['u_id']."'";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute(); 
                }
                // updaye login flag
                $sql1="UPDATE `user_master` SET is_login=0 WHERE `u_id`='".$response['data'][0]['u_id']."'";
                $dataReader=$connection->createCommand($sql1);
                $num1= $dataReader->execute(); 

                //update token on evry time login
                $dt_id=$_REQUEST['dt_id'];
                if($response2['status']==1 && $dt_id==$response2['data'][0]['dt_id'])
                {
                    $sql1="UPDATE `user_device_master` SET  dt_id='".$dt_id."', `token`='".$_REQUEST['token']."'  WHERE `u_id`='".$response['data'][0]['u_id']."'";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();    
                }
                else
                {
                    $sql1="INSERT INTO `user_device_master`(`ud_id`, `dt_id`, `token`, `u_id`) 
                                    VALUES (NULL,'".$dt_id."','".$_REQUEST['token']."','".$response['data'][0]['u_id']."')";
                    $dataReader=$connection->createCommand($sql1);
                    $num1= $dataReader->execute();
                }
                $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/profileImages/";
                $pic=$imagePath.$response['data'][0]['profile_picture'];
                if(empty($response['data'][0]['active_payment_method_id']) )
                        $active_payment_method_id='';
                    else
                        $active_payment_method_id=$response['data'][0]['active_payment_method_id'];
                if($response['data'][0]['ur_id']==4)
                {

                    //$data=array('id'=>$response['data'][0]['u_id'],'code'=>$response['data'][0]['unique_code'],'name'=>$response['data'][0]['user_name']);
                   
                    $response=array('status'=>'1','profile_picture'=>$pic,'payment_flag'=>$active_payment_method_id,'phone_no'=>$response['data'][0]['phone_no'],'id'=>$response['data'][0]['u_id'],'code'=>$response['data'][0]['unique_code'],'name'=>$response['data'][0]['user_name']);    
                }
                else
                {
                    //$data=array('id'=>$response['data'][0]['u_id'],'name'=>$response['data'][0]['user_name']);
                   $response=array('status'=>'1','profile_picture'=>$pic,'payment_flag'=>$active_payment_method_id,'phone_no'=>$response['data'][0]['phone_no'],'id'=>$response['data'][0]['u_id'],'name'=>$response['data'][0]['user_name']);     
                }            
                echo Yii::app()->JsonWebservice->response($response);
                
            }
            else
            {
                echo Yii::app()->JsonWebservice->response($response); 
            }
            
        }      
        
    }
//Search services V2
    public function actionSearch_V2()
    {
        $is_advance=$_REQUEST['is_advance'];
        $cust_code=$_REQUEST['code'];
        $searchterm=empty($_REQUEST['searchterm'])?NUll:$_REQUEST['searchterm'];
        $location=empty($_REQUEST['city_id'])? NUll:$_REQUEST['city_id'];
        $discount=empty($_REQUEST['discount'])? NULL:$_REQUEST['discount'];
        $category=empty($_REQUEST['category_id'])? NULL: $_REQUEST['category_id'];
        $user_lat=empty($_REQUEST['latitude'])? NULL: $_REQUEST['latitude'];
        $user_long=empty($_REQUEST['longitude'])? NULL: $_REQUEST['longitude'];
        $timestamp=empty($_REQUEST['timestamp'])? NULL: $_REQUEST['timestamp'];
        $action=empty($_REQUEST['action'])?2: $_REQUEST['action'];
        $today=gmdate('Y-m-d');
        $table="offer_master,product_master";
        $whereCondition="";

        if($is_advance==1) //id advance search 
        {
            
            //if location eg: new York,New Jersey,LA,etc
            if(!empty($location))
            {
               
               $whereCondition.=" offer_master.o_id IN (select o_id from offer_location_assoc where city_id=".$location.")";
            }
            //if on-premise,off-premise
            if(!empty($category))
            {
                $category_id="3,".$category;
                if(!empty($whereCondition))
                    $whereCondition .=" and offer_type IN(".$category_id.")";
                else
                    $whereCondition .=" offer_type IN (".$category_id.")";
            }
           
            
            //if serach ter, eg :vodka,gin,absolute
            if(!empty($searchterm))
            {
                $searchterm=addslashes($searchterm);
                if(!empty($whereCondition))
                    $whereCondition .=" and (offer_name like '%".$searchterm."%' 
                                    or offer_desc like '%".$searchterm."%' 
                                    or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%')
                                    or offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in 
                                                                (select m_id from merchant_master 
                                                                            where m_name like '%".$searchterm."%' or establishment_name like '%".$searchterm."%'  or m_street like '%".$searchterm."%' and merchant_master.is_active=0)))";
                else
                    $whereCondition .=" (offer_name like '%".$searchterm."%' 
                                    or offer_desc like '%".$searchterm."%' 
                                    or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%')
                                    or offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in 
                                                                (select m_id from merchant_master 
                                                                            where m_name like '%".$searchterm."%' or establishment_name like '%".$searchterm."%'  or m_street like '%".$searchterm."%' and merchant_master.is_active=0)))";
            }
        }
        else //if normal search term
        {
            $searchterm=addslashes($searchterm);
            $whereCondition .=" (offer_name like '%".$searchterm."%' 
                                or offer_desc like '%".$searchterm."%' 
                                or offer_master.pm_id in(select pm_id from product_master where product_name like '%".$searchterm."%')
                                or offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in 
                                                                (select m_id from merchant_master 
                                                                            where m_name like '%".$searchterm."%' or establishment_name like '%".$searchterm."%'  or m_street like '%".$searchterm."%' and merchant_master.is_active=0)))";
        }
        // if location is empty then fecth near by offers with merchant details
        /*
        * fetch all active merchants around 2 miles of users lat long whcih has bevrage offers running
        */
            if( (!empty($user_lat) && !empty($user_long)) && empty($location))
            { 
                $post["table"] = 'merchant_master';
                $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                    * cos( radians( m_lat ) ) 
                                    * cos( radians( m_long ) - 
                                   radians( "'.$user_long.'" ) ) + 
                                   sin( radians( "'.$user_lat.'" ) ) * 
                                   sin( radians( m_lat ) ) ) ),2 ) AS "distance"
                                   ';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc)  
                                       and merchant_master.is_active=0 
                                       HAVING distance < 2 ';
                $post["r_p_p"] = '';
                $post["start"] = '';
                $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');

                $m_ids='';
                if($merchantlist['status']==1)
                {
                    
                    foreach ($merchantlist['data'] as $key => $value) 
                    {
                       $m_ids .= $value['m_id'].',';
                    }
                    $m_ids=trim($m_ids,',');
                }  
            }
            
            if(!empty($m_ids))
            {
            	$columns=" ,'' as 'miles' ,'' as 'merchant_count'";
                $whereCondition .=" and offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
            }
            
            //fetch offer based on search criteria
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            
            if(!empty($timestamp))
            {
                if(!empty($whereCondition))
                    $whereCondition .="  and modified_on <'".$timestamp."' ";
                else
                    $whereCondition .=" modified_on <'".$timestamp."'";
            }
            //and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id  in(SELECT u_id from user_master where unique_code='".$cust_code."'))
            $post["table"] = $table;
            $post["fields"] = 'offer_master.o_id,offer_name,maximum_redemption,
            				   SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",
            				   product_name,offer_type,redeem_type,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,
            				   CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"'.$columns;
            $post["beforeWhere"] = '';
            $post["afterWhere"] =  $whereCondition ." and offer_type=".$action." and ABS(DATEDIFF(now(),`end_date`)) > 0 
                                    and offer_master.is_active=0 
                                    and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id)
                                    
                                    and offer_master.o_id  NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$cust_code."')
                                    and offer_master.is_delete=0
                                    and end_date >='".$today."'
                                    and offer_master.start_date <='".$today."'
                                    and product_master.is_active=0
                                    and product_master.is_delete=0
                                    and product_master.pm_id=offer_master.pm_id group by o_id order by offer_master.modified_on DESC  ";
                                    
            $post["r_p_p"] = '5';
            $post["start"] = '0';
           /* echo "<pre>";
            print_r( $post["table"]);
            print_r($post["afterWhere"]);die;*/
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');       
           
             //Add Merchant details
                if($response['status']==1)
                {

                    if( !empty($m_ids)  && empty($location)) //if current location not avilable send list of merchants with miles
                    {
                       /* foreach ($response['data'] as $key => &$value) 
                        {
                            $post["table"] = 'merchant_master';
                            $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,m_zipcode,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                                * cos( radians( m_lat ) ) 
                                                * cos( radians( m_long ) - 
                                               radians( "'.$user_long.'" ) ) + 
                                               sin( radians( "'.$user_lat.'" ) ) * 
                                               sin( radians( m_lat ) ) ) ),2) AS "distance"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$value['o_id'].'")
                                                    and merchant_master.is_active=0
                                                    HAVING distance < 2
                                                    order by distance';
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                            if($merchantlist['status']==1)
                            {
                               // $value['merchantlist']=$merchantlist['data'];
                                $value['miles']=$merchantlist['data'][0]['distance'];
                                $value['merchant_count']=count($merchantlist['data']);
                            }
                            else
                            {
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']); //remove offer if no participating merchants found
                            }
                        } 
                        */
                    }
                    else if($location)//if current location not avilable but city available send list of merchants with specified city
                    {
                        foreach ($response['data'] as $key => &$value) 
                        {
                            $cond=" and  m_id in (SELECT m_id from merchant_master where m_city=".$location." and merchant_master.is_active=0) ";
                            $post["table"] = 'offer_merchant_assoc';
                            $post["fields"] = 'count(offer_merchant_assoc.m_id) as "m_counts"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'o_id="'.$value['o_id'].'"'.$cond;
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                           
                            if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0)
                            {                                                	 
                                $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                                $value['miles']='';
                            }
                            else
                            {
                                unset($response['data'][$key]);
                                $response['data'] = array_values($response['data']);
                            }
                        }
                            
                    } 
                    else if(empty($location) && (empty($user_lat) && empty($user_long)))
                    {
                    	foreach ($response['data'] as $key => &$value) 
                        {
	                        $post["table"] = 'offer_merchant_assoc';
	                        $post["fields"] = 'count(*) as "m_counts"';
	                        $post["beforeWhere"] = '';
	                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" ' ;
	                        $post["r_p_p"] = '';
	                        $post["start"] = '';
	                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
	                       
	                        if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0 )
	                        {
	                            $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
	                            $value['miles']='';
	                        }
	                        else
	                        {
	                            unset($response['data'][$key]);
	                            $response['data'] = array_values($response['data']);
	                        }
	                    }
                    }
                    else
                    {
                        unset($response['data']);
                        $response['status']='-3';
                    }
                    
                }
                
                if(empty($response['data'])){
                    unset($response['data']);
                    $response['status']='-3';
                }
                else
                {
                	$response['pagination']=count($response['data']);
                }
                echo Yii::app()->JsonWebservice->response($response);
    }
//fetch saved offers/redeemed offers V2
    public function actionfetchOfferWallet_V2()
    {
        $post = $_REQUEST ;
        $validate = array("code" => array("require" => 1 ,"msg" => "code  is required."),                           
                            "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $u_id=$_REQUEST['u_id'];
            $code=$_REQUEST['code'];
            $user_lat=empty($_REQUEST['latitude'])?NULL:$_REQUEST['latitude'];
            $user_long=empty($_REQUEST['longitude'])?NULL:$_REQUEST['longitude'];
            $location=empty($_REQUEST['location'])?NULL:$_REQUEST['location'];
            $today=gmdate('Y-m-d');
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
           
            $whereCondition='';
            $pagination=5;
            $columns='';
            //fetch saved offers
            
            /*
            * fetch all active merchants around 2miles of users lat long whcih has bevrage offers running
            */
            if( (!empty($user_lat) && !empty($user_long)))
            {
                $post["table"] = 'merchant_master';
                $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                           radians( "'.$user_long.'" ) ) + 
                                           sin( radians( "'.$user_lat.'" ) ) * 
                                           sin( radians( m_lat ) ) ) ),2) AS "distance"
                                   ';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc)  
                                       and merchant_master.is_active=0 
                                       HAVING distance < 2 ';
                $post["r_p_p"] = '';
                $post["start"] = '';
                $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                $m_ids='';
                if($merchantlist['status']==1)
                {
                    
                    foreach ($merchantlist['data'] as $key => $value) 
                    {
                       $m_ids .= $value['m_id'].',';
                    }
                    $m_ids=trim($m_ids,',');
                }  
                if(!empty($m_ids))
                {
                	$columns=' ,"" as "miles","" as "merchant_count", "0" as "error_status"';
                    $whereCondition =" and offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
                }
            }
            


            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'offer_master.o_id,offer_name,offer_type, redeem_type,start_date,end_date,
                                maximum_redemption,product_name,offer_desc,start_date,end_date,
                                SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",
                                 DATEDIFF(`end_date`,now()) AS days_left,CONCAT("'.$imagePath.'",offer_image) as offer_image,
                                offer_master.is_active,offer_master.is_delete,offer_master.modified_on as "timestamp"'.$columns;
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id

									and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id) 
                                   	and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
                                    and offer_master.o_id IN (SELECT o_id from user_wallet_list where u_id=".$u_id.")" . $whereCondition ." group by o_id order by offer_master.modified_on desc  limit 5";
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response1 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            if($response1['status']==1)
            {
                foreach ($response1['data'] as $key => &$value) 
                {
                	
                	
                    //Scenario 1
                    if( ($value['end_date'] < $today) || ($value['is_active']==1) || ($value['is_delete']==1) || ($value['left_count'] > $value['maximum_redemption'] || $value['left_count']==0 ))
                    {
                    	
                        $value['offer_status']='-4';
                    }
                    else
                    {
                     	$value['offer_status']='1';//offer still available
                    } 

                    if($value['days_left'] <0)                  
                    {
                        $value['days_left']=0;
                    }                    
                           
                }
            }
            
            //Add Merchant details
                if($response1['status']==1)
                {

                   if(!empty($m_ids) && !empty($user_lat) && !empty($user_long)) //if current lat long avilable send list of merchants with miles
                    {

                        /*foreach ($response1['data'] as $key => &$value) 
                        {
                        	$value['miles']="";//$merchantlist['data'][0]['distance'];
                            $value['merchant_count']="";//ount($merchantlist['data']);
                            $value['error_status']='0';
                            $post["table"] = 'merchant_master';
                            $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,m_zipcode,
                                                ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                                * cos( radians( m_lat ) ) 
                                                * cos( radians( m_long ) - 
                                                radians( "'.$user_long.'" ) ) + 
                                                sin( radians( "'.$user_lat.'" ) ) * 
                                                sin( radians( m_lat ) ) ) ),2) AS "distance"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$value['o_id'].'")
                                                    and merchant_master.is_active=0
                                                    HAVING distance < 2
                                                    order by distance';
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                            if($merchantlist['status']==1)
                            {
                                //$value['merchantlist']=$merchantlist['data'];
                                $value['miles']=""$merchantlist['data'][0]['distance'];
                                $value['merchant_count']=count($merchantlist['data']);
                                $value['error_status']='0';
                            }
                            else
                            {
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                                
                            }
                        } */
                       
                    }
                    else if($location)//if current location not avilable send list of merchants without miles or lcoation specified
                    {
                        foreach ($response1['data'] as $key => &$value) 
                        {
                            $cond=" and  m_id in (SELECT m_id from merchant_master where m_city=".$location." and merchant_master.is_active=0) ";
                            $post["table"] = 'offer_merchant_assoc';
                            $post["fields"] = 'count(offer_merchant_assoc.m_id) as "m_counts"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'o_id="'.$value['o_id'].'"'.$cond;
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                           
                            if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0)
                            {
                                //$value['merchantlist']=$merchantlist['data'];                            
                                $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                                $value['miles']='';
                                $value['error_status']='0';
                            }
                            else
                            {
                                /*unset($response1['data'][$key]);
                                $response1['data'] = array_values($response1['data']);*/
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                            }
                        }
                    } 
                    else if(empty($location) && (empty($user_lat) && empty($user_long)))
                    {
                        foreach ($response1['data'] as $key => &$value) 
                        {
                            $post["table"] = 'offer_merchant_assoc';
	                        $post["fields"] = 'count(*) as "m_counts"';
	                        $post["beforeWhere"] = '';
	                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" ' ;
	                        $post["r_p_p"] = '';
	                        $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                            if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0)
                            {
                                
                                $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                                $value['miles']='';
                                $value['error_status']='0';
                            }
                            else
                            {
                                
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                            }
                        }
                    }
                    else
                    {
                        
                        foreach ($response1['data'] as $key => &$value) 
                        {
                            $value['error_status']='-4';
                            $value['merchant_count']='';
                            $value['miles']='';
                        }
                    }
                }
                else
                {
                    $response = array("status" => '-3') ;
                }

                if($response1['status']==1)
                    $finalArray['save']=$response1['data'];
                else
                    $finalArray['save']=array();


            //fetch redeemed offers
            $post["table"] = 'offer_master,product_master,offer_transaction_master';
            $post["fields"] = 'offer_transaction_master.ot_id,offer_master.o_id,offer_name,redeem_date ,offer_type,start_date,end_date,
                             maximum_redemption,product_name,offer_desc,start_date,end_date,CONCAT(`redeem_date`," ",`transaction_time`) as "timestamp",
                             ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                             CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_transaction_master.type as "offer_redeem_type",
                             IF(offer_master.redeem_type="2", `customer_payment_status` , `payment_status`) as payment_status,offer_master.is_active,offer_master.is_delete';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = "offer_master.pm_id=product_master.pm_id 
                                   and offer_master.o_id=offer_transaction_master.o_id 
                                    and offer_transaction_master.unique_code='".$code."' order by redeem_date DESC limit 5";                           
                                   
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response3 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
          
            if($response3['status']==1)
            {
                foreach ($response3['data'] as $key => &$value) 
                {
                    $value['offer_status']='-4';
                }
            }
            
            
            if($response3['status']==1){
            

                $finalArray['redeem']=$response3['data'];
            }
            else
                $finalArray['redeem']=array();
            if(empty($finalArray['save']) && empty($finalArray['redeem']))
                $res = array("status" => '-1') ;
            else{

                $res = array("status" => '1','pagination'=>count($finalArray),'data'=>$finalArray) ;
            }

            echo Yii::app()->JsonWebservice->response($res);
        }
    }

//fetch next set of offer wallet list
    public function actionfetchNextWalletList()
    {
        $post = $_REQUEST ;
        $validate = array("code" => array("require" => 1 ,"msg" => "code  is required."),                           
                          "u_id" => array("require" => 1 ,"msg" => "u_id is required."),                            
                          //"timestamp" => array("require" => 1 ,"msg" => "timestamp is required."),
                          "action" => array("require" => 1 ,"msg" => "action is required."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $u_id=$_REQUEST['u_id'];
            $code=$_REQUEST['code'];
            $user_lat=empty($_REQUEST['latitude'])?NULL:$_REQUEST['latitude'];
            $user_long=empty($_REQUEST['longitude'])?NUll:$_REQUEST['longitude'];
            $action=$_REQUEST['action'];
            $location=$_REQUEST['location'];
            $today=gmdate('Y-m-d');
            
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
           
            $whereCondition='';

            //fetch saved offers
            if($action=="ongoing")
            {

                /*
                * fetch all active merchants around 2miles of users lat long whcih has bevrage offers running
                */
                if( (!empty($user_lat) && !empty($user_long)))
                {
                    $post["table"] = 'merchant_master';
                    $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                                * cos( radians( m_lat ) ) 
                                                * cos( radians( m_long ) - 
                                               radians( "'.$user_long.'" ) ) + 
                                               sin( radians( "'.$user_lat.'" ) ) * 
                                               sin( radians( m_lat ) ) ) ),2) AS "distance"
                                       ';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc)  
                                           and merchant_master.is_active=0 
                                           HAVING distance < 2 ';
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    $m_ids='';
                    if($merchantlist['status']==1)
                    {
                        foreach ($merchantlist['data'] as $key => $value) 
                        {
                           $m_ids .= $value['m_id'].',';
                        }
                        $m_ids=trim($m_ids,',');
                    }                    
                    if(!empty($m_ids))
                        $whereCondition =" and offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
                }
                
                $timestamp=empty($_REQUEST['timestamp']) ?NULL :$_REQUEST['timestamp'];
                $timestamp_cond='';
                if($timestamp)
                {
                    $timestamp_cond="modified_on < '".$timestamp."' and ";
                }
                $post["table"] = 'offer_master,product_master';
                $post["fields"] = 'offer_master.o_id,offer_name,offer_type, redeem_type,start_date,end_date,
                                    maximum_redemption,SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",product_name,offer_desc,start_date,end_date,
                                    DATEDIFF(`end_date`,now()) AS days_left,CONCAT("'.$imagePath.'",offer_image) as offer_image,
                                    offer_master.is_active,offer_master.is_delete,offer_master.modified_on as "timestamp"';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = $timestamp_cond." offer_master.pm_id=product_master.pm_id 
                                    and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id)
                                    and offer_master.o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
                                    and offer_master.o_id IN (SELECT o_id from user_wallet_list where u_id=".$u_id.")" . $whereCondition ." group by o_id order by offer_master.modified_on desc";
                $post["r_p_p"] = '5';
                $post["start"] = '0';
                $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                if($response['status']==1)
                {
                    foreach ($response['data'] as $key => &$value) 
                    {
                        //Scenario 1
	                    if( ($value['end_date'] < $today) || ($value['is_active']==1) || ($value['is_delete']==1) || ($value['left_count'] > $value['maximum_redemption'] || $value['left_count']==0 ))
	                    {
	                    	
	                        $value['offer_status']='-4';
	                    }
	                    else
	                    {
	                     	$value['offer_status']='1';//offer still available
	                    }  
                        if($value['days_left'] <0)                  
                        {
                            $value['days_left']=0;
                        }         
                    }
                }

                //Add Merchant details
                if($response['status']==1)
                {

                    if(!empty($m_ids) && !empty($user_lat) && !empty($user_long)) //if current lat long avilable send list of merchants with miles
                    {
                        foreach ($response['data'] as $key => &$value) 
                        {
                            $post["table"] = 'merchant_master';
                            $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,m_zipcode,
                                                ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                                * cos( radians( m_lat ) ) 
                                                * cos( radians( m_long ) - 
                                                radians( "'.$user_long.'" ) ) + 
                                                sin( radians( "'.$user_lat.'" ) ) * 
                                                sin( radians( m_lat ) ) ) ),2) AS "distance"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$value['o_id'].'")
                                                    and merchant_master.is_active=0
                                                    HAVING distance < 2
                                                    order by distance';
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                            if($merchantlist['status']==1)
                            {
                                //$value['merchantlist']=$merchantlist['data'];
                                $value['miles']=$merchantlist['data'][0]['distance'];
                                $value['merchant_count']=count($merchantlist['data']);
                                $value['error_status']='0';
                            }
                            else
                            {
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                                /*unset($response1['data'][$key]);
                                $response1['data'] = array_values($response1['data']);*/ //remove offer if no participating merchants found
                            }
                        } 
                       
                    }
                    else if($location)//if current location not avilable send list of merchants without miles or lcoation specified
                    {
                        foreach ($response['data'] as $key => &$value) 
                        {
                            $cond=" and  m_id in (SELECT m_id from merchant_master where m_city=".$location." and merchant_master.is_active=0) ";
                            $post["table"] = 'offer_merchant_assoc';
                            $post["fields"] = 'count(offer_merchant_assoc.m_id) as "m_counts"';
                            $post["beforeWhere"] = '';
                            $post["afterWhere"] = 'o_id="'.$value['o_id'].'"'.$cond;
                            $post["r_p_p"] = '';
                            $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                           
                            if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0)
                            {
                                //$value['merchantlist']=$merchantlist['data'];                            
                                $value['merchant_count']=$merchantlist['data'][0]['m_counts'];   
                                $value['miles']='';
                                $value['error_status']='0';
                            }
                            else
                            {
                                /*unset($response1['data'][$key]);
                                $response1['data'] = array_values($response1['data']);*/
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                            }
                        }
                    } 
                    else if(empty($location) && (empty($user_lat) && empty($user_long)))
                    {
                        foreach ($response['data'] as $key => &$value) 
                        {
                            $post["table"] = 'offer_merchant_assoc';
	                        $post["fields"] = 'count(*) as "m_counts"';
	                        $post["beforeWhere"] = '';
	                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" ' ;
	                        $post["r_p_p"] = '';
	                        $post["start"] = '';
                            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                            if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0)
                            {
                                
                                $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                                $value['miles']='';
                                $value['error_status']='0';
                            }
                            else
                            {
                                
                                $value['error_status']='-4';
                                $value['merchant_count']='';
                                $value['miles']='';
                            }
                        }
                    }
                    else
                    {
                        foreach ($response['data'] as $key => &$value) 
                        {
                            $value['error_status']='-4';
                            $value['merchant_count']='';
                            $value['miles']='';
                        }
                    }
                }
                else
                {
                    $response = array("status" => '-3') ;
                }

            }
            else
            {
                //echo "here";die();
                /*
                * fetch redeemed offers
                */
                $timestamp=empty($_REQUEST['timestamp']) ?NULL :$_REQUEST['timestamp'];
                $timestamp_cond='';
                if($timestamp)
                {
                    $timestamp_cond=" 'timestamp' < '".$timestamp."' and ";
                }
                $post["table"] = 'offer_master,product_master,offer_transaction_master';
                $post["fields"] = 'offer_transaction_master.ot_id,offer_master.o_id,offer_name,redeem_date ,offer_type,start_date,end_date,
                                 maximum_redemption,product_name,offer_desc,start_date,end_date,"-4" as "offer_status",CONCAT(`redeem_date`," ",`transaction_time`) as "timestamp",
                                 ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,
                                 CONCAT("'.$imagePath.'",offer_image) as offer_image,offer_transaction_master.type as "offer_redeem_type",
                                 IF(offer_master.redeem_type="2", `customer_payment_status` , `payment_status`) as payment_status, offer_master.is_active,offer_master.is_delete';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = $timestamp_cond." offer_master.pm_id=product_master.pm_id 
                                        and offer_master.o_id=offer_transaction_master.o_id 
                                        and offer_transaction_master.unique_code='".$code."' order by redeem_date DESC ";                           
                                       
                $post["r_p_p"] = '5';
                $post["start"] = '0';
                $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
    
            }

            if($response['status']==1)
            {
            	$response['pagination']=count($response['data']);
            }
            else
            {
            	$response['status']='-3';
            }
            echo Yii::app()->JsonWebservice->response($response);
        }
    }
//Off Premise offers list v2
     public function actionOffPremiseList_V2()
    {
        
        $post = $_REQUEST ;
        $whereCon='';

        $today=gmdate('Y-m-d');
        $validate = array(
                           "unique_code" => array("require" => 1 ,"msg" => "unique_code is required."),
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            
            $code=$_REQUEST['unique_code'];
            $user_lat=empty($_REQUEST['latitude'])? NULL: $_REQUEST['latitude'];
            $user_long=empty($_REQUEST['longitude'])? NULL: $_REQUEST['longitude'];
            $location=empty($_REQUEST['location'])? NULL: $_REQUEST['location'];
            /*
            * fetch all active merchants around 2miles of users lat long which has bevrage offers running
            */
                if( (!empty($user_lat) && !empty($user_long)) && empty($location))
                {
                    $post["table"] = 'merchant_master';
                    $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                           radians( "'.$user_long.'" ) ) + 
                                           sin( radians( "'.$user_lat.'" ) ) * 
                                           sin( radians( m_lat ) ) ) ),2) AS "distance"
                                       ';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc)  
                                           and merchant_master.is_active=0 
                                           HAVING distance < 2 ';
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    $m_ids='';
                    if($merchantlist['status']==1)
                    {
                        
                        foreach ($merchantlist['data'] as $key => $value) 
                        {
                           $m_ids .= $value['m_id'].',';
                        }
                        $m_ids=trim($m_ids,',');
                    }  
                }
            $timestamp=empty($_REQUEST['timestamp'])?NULL:$_REQUEST['timestamp'];
            if($timestamp)
                	$whereCon.=" modified_on <'".$timestamp."'  and";
            $whereCon.=" ABS(DATEDIFF(now(),`end_date`)) >= 0 
                        and offer_master.start_date <='".$today."'
                        and o_id NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$code."')
                        and offer_master.o_id NOT IN (SELECT o_id from user_wallet_list where u_id in (SELECT u_id from user_master where unique_code='".$code."'))
                        and maximum_redemption > (select count(*) from offer_transaction_master where o_id=offer_master.o_id)
                        and offer_master.is_active=0 
                        and offer_master.is_delete=0
                        and product_master.is_active=0 
                        and product_master.is_delete=0
                        and offer_type='2'                        
                        and offer_master.pm_id=product_master.pm_id ";
            
            if(!empty($m_ids))
            {
            	$columns=" ,'' as 'miles' ,'' as 'merchant_count'";
                $whereCon .=" and offer_master.o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
            }
            $whereCon.=" group by o_id order by modified_on desc";
            $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
            
            $post["table"] = 'offer_master,product_master';
            $post["fields"] = 'DISTINCT o_id,offer_name,offer_type, maximum_redemption,product_name,offer_desc,
                               start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,
                               SUM(maximum_redemption-(select count(*) from offer_transaction_master where o_id=offer_master.o_id)) as "left_count",
                               CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp"'.$columns;
            $post["beforeWhere"] = '';
            $post["afterWhere"] = $whereCon;
            $post["r_p_p"] = '5';
            $post["start"] = '0';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            
            
            //Add Merchant details
            if($response['status']==1)
            {

               if(!empty($m_ids) && !empty($user_lat) && !empty($user_long)) //if current lat long avilable send list of merchants with miles
                {
                    /*foreach ($response['data'] as $key => &$value) 
                    {
                        $post["table"] = 'merchant_master';
                        $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,m_zipcode, ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                            radians( "'.$user_long.'" ) ) + 
                                            sin( radians( "'.$user_lat.'" ) ) * 
                                            sin( radians( m_lat ) ) ) ),2) AS "distance"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$value['o_id'].'")
                                                and merchant_master.is_active=0
                                                HAVING distance < 2
                                                order by distance';
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                        if($merchantlist['status']==1)
                        {
                            //$value['merchantlist']=$merchantlist['data'];
                            $value['miles']=$merchantlist['data'][0]['distance'];
                            $value['merchant_count']=count($merchantlist['data']);
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']); //remove offer if no participating merchants found
                        }
                    } */
                   
                }
                else //if current location not avilable send list of merchants without miles or location specified
                {
                    foreach ($response['data'] as $key => &$value) 
                    {
                    	$cond='';
                    	if($location)
                       	 	$cond=" and  m_id in (SELECT m_id from merchant_master where m_city=".$location." and merchant_master.is_active=0) ";

                        $post["table"] = 'offer_merchant_assoc';
                        $post["fields"] = 'count(*) as "m_counts"';
                        $post["beforeWhere"] = '';
                        $post["afterWhere"] = ' o_id="'.$value['o_id'].'" '.$cond ;
                        $post["r_p_p"] = '';
                        $post["start"] = '';
                        $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                       
                        if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0 )
                        {
                            $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                            $value['miles']='';
                        }
                        else
                        {
                            unset($response['data'][$key]);
                            $response['data'] = array_values($response['data']);
                        }
                  
                    }
                } 
            }
            if(count($response['data'])<=0){
                 unset($response['data']);
                 $response['status'] = '-3';
                 $response['error_msg'] = array('Data Not Found');
            }
            else
            {
            	$response['pagination']=count($response['data']);
                
            }
            echo Yii::app()->JsonWebservice->response($response); 
            
        }
        
    }
//Details of Offers after QR CODE scan V2
    public function actionoffer_detail_V2()
    {
        $o_id=$_REQUEST['o_id'];
        $code=empty($_REQUEST['cust_code'])?0:$_REQUEST['cust_code'];
        $location=empty($_REQUEST['location'])?null:$_REQUEST['location'];
        $user_lat=empty($_REQUEST['latitude'])?NUll:$_REQUEST['latitude'];
        $user_long=empty($_REQUEST['longitude'])? NULL:$_REQUEST['longitude'];
        $m_ids='';

        /*
        * fetch all active merchants around 2miles of users lat long whcih has bevrage offers running
        */
        if( (!empty($user_lat) && !empty($user_long)) && empty($location))
        {
            $post["table"] = 'merchant_master';
            $post["fields"] = 'm_id,ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                            * cos( radians( m_lat ) ) 
                                            * cos( radians( m_long ) - 
                                           radians( "'.$user_long.'" ) ) + 
                                           sin( radians( "'.$user_lat.'" ) ) * 
                                           sin( radians( m_lat ) ) ) ),2) AS "distance"
                               ';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id ='.$o_id.')  
                                   and merchant_master.is_active=0 
                                   HAVING distance < 2 ';
            $post["r_p_p"] = '';
            $post["start"] = '';
            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            if($merchantlist['status']==1)
            {
                
                foreach ($merchantlist['data'] as $key => $value) 
                {
                   $m_ids .= $value['m_id'].',';
                }
                $m_ids=trim($m_ids,',');
            }  
        }
        $whereCon='';
        if($m_ids)
            $whereCon =" and o_id IN (select o_id from offer_merchant_assoc where m_id in (".$m_ids.")) ";
        $codePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_codes/";//sd1.jpg
        $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/offer_images/";//sd1.jpg
        $post["table"] = 'offer_master,product_master';
        $post["fields"] = 'o_id,offer_name,redeem_type, maximum_redemption,product_name,offer_desc,start_date,end_date,ABS(DATEDIFF(now(),`end_date`)) AS days_left,CONCAT("'.$codePath.'",code_redeem_file) as qr_code,CONCAT("'.$imagePath.'",offer_image) as offer_image,modified_on as "timestamp",offer_type';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = " ABS(DATEDIFF(now(),`end_date`)) > 0 
                                and offer_master.is_active=0 
                                and offer_master.is_delete=0
                                and end_date >='".$today."'
                                and offer_master.pm_id=product_master.pm_id
                                and o_id  NOT IN (SELECT o_id from offer_transaction_master where unique_code='".$cust_code."')
                                and o_id=".$o_id." ".$whereCon;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        //**********Get Redemption count of the offers
            if($response['status']==1)       
            {               
                $post["table"] = 'offer_transaction_master';
                $post["fields"] = 'count(*) as count';
                $post["beforeWhere"] = '';
                $post["afterWhere"] = "o_id=".$o_id;
                $post["r_p_p"] = '';
                $post["start"] = '';
                $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                
                //*********chk if count less then max limit or not
                if($response2['status']==1)
                {
                    $cnt=$response2['data'][0]['count'];
                    $max=$response['data'][0]['maximum_redemption'];
                    if($cnt< $max)
                        $response['data'][0]['left_count']=$response['data'][0]['maximum_redemption'] - $response2['data'][0]['count'];
                    else{
                        $re[]="Maximum redeem limit reached.";
                        $response = array("status" => '-4',"error_msg" => $re) ;
                    }
                        
                }
                else
                {
                    $response['data'][0]['left_count']=$response['data'][0]['maximum_redemption'];
                }
            }

            if(!empty($m_ids) && !empty($response['data']))
            {
               
                    $post["table"] = 'merchant_master,bevrage_cities';
                    $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,bevrage_cities.city_name,m_zipcode,bevrage_cities.state_code
                                        ';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$o_id.'")
                                            and merchant_master.m_city=bevrage_cities.bc_id
                                            and merchant_master.is_active=0 
                                            HAVING distance < 2
                                            order by distance';
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    if($merchantlist['status']==1)
                    {
                        //$value['merchantlist']=$merchantlist['data'];
                        $value['miles']=$merchantlist['data'][0]['distance'];
                        $value['merchant_count']=count($merchantlist['data']);
                    }
                    else
                    {
                        unset($response['data'][$key]);
                        $response['data'] = array_values($response['data']);
                    }            
            }
            else
            {

                	if($location)
                    	$cond=" and m_id in (select m_id from merchant_master where merchant_master.m_city=".$location." and merchant_master.is_active=0)";
                  	
                  	$post["table"] = 'offer_merchant_assoc';
                    $post["fields"] = 'count(*) as "m_counts"';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = ' o_id="'.$o_id.'" ' ;
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                   
                    if($merchantlist['status']==1 && $merchantlist['data'][0]['m_counts']!=0 )
                    {
                        $value['merchant_count']=$merchantlist['data'][0]['m_counts'];
                        $value['miles']='';
                    }
                    else
                    {
                        unset($response['data'][$key]);
                        $response['data'] = array_values($response['data']);
                    }
            }

            if(empty($response['data']))
            {
                unset($response['data']);
                $response['status']='-3';
            }
            echo Yii::app()->JsonWebservice->response($response); 
    }

//Fecth MERCHANT LISTING BASED ON OFFERS
    public function actionfetchMerchants()
    {
        $location=empty($_REQUEST['city_id'])?'':$_REQUEST['city_id'];
        $user_lat=empty($_REQUEST['latitude'])?'':$_REQUEST['latitude'];
        $user_long=empty($_REQUEST['longitude'])?'':$_REQUEST['longitude'];
        $o_id=$_REQUEST['o_id'];
        $cond='';

         if($location)
           $cond=" and merchant_master.m_city=".$location;

        if(!empty($user_lat) && !empty($user_long))
        {
            $post["table"] = 'merchant_master,bevrage_cities';
            $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,bevrage_cities.city_name,m_zipcode,bevrage_cities.state_code,
                                ROUND (( 3959 * acos( cos( radians( "'.$user_lat.'") )
                                * cos( radians( m_lat ) ) 
                                * cos( radians( m_long ) - 
                               radians( "'.$user_long.'" ) ) + 
                               sin( radians( "'.$user_lat.'" ) ) * 
                               sin( radians( m_lat ) ) ) ),2) AS "distance"';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$o_id.'")
                                    and merchant_master.m_city=bevrage_cities.bc_id
                                    and merchant_master.is_active=0 
                                    HAVING distance < 2
                                    order by distance';
            $post["r_p_p"] = '';
            $post["start"] = '';
            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            $merchantlist['miles']=$merchantlist['data'][0]['distance'];
            $merchantlist['merchant_count']=count($merchantlist['data']);
        }
        else
        {

            $post["table"] = 'merchant_master,bevrage_cities';
            $post["fields"] = 'm_id,m_name,phone,m_lat,m_long,establishment_name,m_street,bevrage_cities.city_name,m_zipcode,bevrage_cities.state_code';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = 'm_id in(select m_id from offer_merchant_assoc where o_id="'.$o_id.'") '.$cond.' 
                                    and merchant_master.m_city=bevrage_cities.bc_id and merchant_master.is_active=0' ;
            $post["r_p_p"] = '';
            $post["start"] = '';
            $merchantlist = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            $merchantlist['miles']="";
            $merchantlist['merchant_count']=count($merchantlist['data']); 
        }
        
        echo Yii::app()->JsonWebservice->response($merchantlist); 
    }

//Logiut User from The App
    public function actionLogoutUser()
    {
        $u_id=$_REQUEST['u_id'];
        $connection = Yii::app()->db;  
        $sql="UPDATE user_master SET is_login=1 WHERE u_id=".$u_id;
        $dataReader=$connection->createCommand($sql);
        $num= $dataReader->execute();
    }

//Clear Badge User from The App
    public function actionclearBadge()
    {
        $u_id=$_REQUEST['u_id'];
        $connection = Yii::app()->db;  
        $sql="UPDATE user_device_master SET badge=0 WHERE u_id=".$u_id;
        $dataReader=$connection->createCommand($sql);
        $num= $dataReader->execute();
    }
//List of mrchandise based on rewards 
    public function actionmerchandiseList()
    {
        $u_id=$_REQUEST['u_id'];

        //fecth reward points of user
        $post["table"] = 'user_master';
        $post["fields"] = 'reward_points,addressline_1,addressline_2,zipcode';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = 'u_id='.$u_id;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $reward_response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        if($reward_response['status']==1)
        {
            if(!empty($reward_response['data'][0]['addressline_1']) && !empty($reward_response['data'][0]['zipcode']))
            {
               // echo "here2";
                $addFlag='1'; //set
            }   
            else{
                //echo "here";
                $addFlag='0';//not set
            }


            $imagePath='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/merchandise_images/";//sd1.jpg
            $points=$reward_response['data'][0]['reward_points'];
            //Fetch all merchandise
            $post["table"] = 'merchandise_master';
            $post["fields"] = 'm_id,name,point_required,CONCAT("'.$imagePath.'",`image`) as image';
            $post["beforeWhere"] = '';
            $post["afterWhere"] = 'is_active=0 order by point_required ASC ';
            $post["r_p_p"] = '';
            $post["start"] = '';
            $response = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
            if($response['status']==1)
            {   
                 $response['address_flag']=$addFlag;
                $response['reward_points']=$points;
                foreach ($response['data'] as $key => &$value) 
                {

                    
                    $post["table"] = 'user_merchandise_assoc';
                    $post["fields"] = '*';
                    $post["beforeWhere"] = '';
                    $post["afterWhere"] = 'm_id='.$value['m_id'].' and u_id='.$_REQUEST['u_id'];
                    $post["r_p_p"] = '';
                    $post["start"] = '';
                    $response2 = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
                    if($response2['status']==1){
                        $value['redeem_date']=$response2['data'][0]['redeem_date'];
                        $value['history_flag']=1;
                    }
                    else
                    {
                        $value['redeem_date']='';
                        $value['history_flag']=0;
                    }

                    if($value['point_required'] <= $points)
                    {
                        if($value['history_flag']==0)
                            $value['is_lock']=0;
                        else
                            $value['is_lock']='';
                    }
                    else
                    {
                        if($value['history_flag']==0)
                            $value['is_lock']=1;
                        else
                            $value['is_lock']='';
                    }
                }
                
            } 
            else
            {
                $response = array("status" => '-1') ;                
            }
        }
        else
        {
            $response = array("status" => '-1') ;
        }
        echo Yii::app()->JsonWebservice->response($response);
    } 
//buy merchnadise 
    public function actionbuyMerchandise()
    {
        $u_id=$_REQUEST['u_id'];
        $m_id=empty($_REQUEST['m_id'])?NULL:$_REQUEST['m_id'];
        $connection = Yii::app()->db; 
        $date=gmdate('Y-m-d') ;

        $post["table"] = 'merchandise_master';
        $post["fields"] = '*';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = 'is_active=0 and m_id='.$m_id;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $res = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
       
        if($res['status']==1)
        {
            $sql="INSERT INTO `user_merchandise_assoc`(`um_id`, `u_id`, `m_id`, `redeem_date`) 
                VALUES (NULL,'".$u_id."','".$m_id."','".$date."')";
            $dataReader=$connection->createCommand($sql);
            $num= $dataReader->execute();


            $sql2="UPDATE  user_master SET reward_points=reward_points-'".$res['data'][0]['point_required']."' where u_id=".$u_id;
            $dataReader=$connection->createCommand($sql2);
            $num1= $dataReader->execute();
            if($num>0)
            {
                $res = array("status" => '1') ;
                
            }
            else
            {
                $res = array("status" => '-1') ;
                
            } 
        }
        else
        {
            $res = array("status" => '-1') ;
        }
        echo Yii::app()->JsonWebservice->response($res);
    }   
//Demo Near by Push    
    public function actionGeneratePush()
    {
        
        $connection = Yii::app()->db;
        $cmd="SELECT distinct * from user_device_master where u_id in (SELECT u_id from user_master where ur_id=4) and dt_id=1";
        $dataReader=$connection->createCommand($cmd)->query();      
        $rows=$dataReader->readALl();
        if(!empty($rows))
        {
            foreach ($rows as $key => $value) 
            {
                $deviceToken=$value['token'];
                //echo "<br><br>".$deviceToken;
                if (!empty($deviceToken) &&  $deviceToken != '0' )
                {
                    $message="Click to check out nearby Bevrage promotion !";
                    $ctx = stream_context_create();            
                    $passphrase = '1234';
                    $certificate= 'BevRageck.pem';
                    stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
                    stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
                    $fp = stream_socket_client('ssl://gateway.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
                    $body['aps'] = array(
                        'badge' => 1,
                        'alert' => $message,
                        'sound' => 'default',
                       // 'url'=>$imageurl,
                        'flag'=>3 
                    );
                    $payload = json_encode($body);
                    $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
                    $result = fwrite($fp, $msg, strlen($msg));
                    $flag = 0;

                    if (!$result) {               
                        $flag = 1;
                    }
                    fclose($fp);
                    if ($flag == 0) {
                       
                        //return true;
                    } else {
                      
                        //return FALSE;
                    }
                } 
                else 
                {
                   //return FALSE;
                }   
            }
            
        }
        $this->redirect('http://bevrageapp.com/bevrage/index.php/site/index');
    }
//save cheque details
    public function actionsaveChequeDetails()
    {
        $post = $_REQUEST ;
        $validate = array("fullname" => array("require" => 1 ,"msg" => "full_name is require."),
                            "u_id" => array("require" => 1 ,"msg" => "u_id is require."),                                                   
            );
        $errorMsg = array();
        $i=0;
        foreach ($post as $key => $value) 
        {
            if(array_key_exists($key,$validate) )
            {
                $required = $validate[$key]["require"];
                $msg =  $validate[$key]["msg"];
                if($required == 1) 
                {
                    if($value == '') 
                    {
                        $errorMsg[$i] = $msg ;
                        $i++;
                    }
                }
            }
        }
        $errorCount = count($errorMsg);
        if($errorCount > 0) 
        {
            $res = array("status" => '-4',"error_msg" => $errorMsg) ;
            echo Yii::app()->JsonWebservice->response($res);
        } 
        else
        {
            $connection = Yii::app()->db; 

            if(!empty($post['address']))
                $sql1="UPDATE user_master set fullname='".$post['fullname']."' ,active_payment_method_id=3 ,addressline_1='".$post['address']."' where u_id=".$post['u_id'];
            else
                $sql1="UPDATE user_master set fullname='".$post['fullname']."', active_payment_method_id=3 where u_id=".$post['u_id'];

            $dataReader=$connection->createCommand($sql1);
            $num= $dataReader->execute();
            if($num>0)
            {
                $res = array("status" => '1') ;
                
            }
            else
            {
                $res = array("status" => '-1') ;
                
            } 
            echo Yii::app()->JsonWebservice->response($res);
        }

           
    }    

//GET CHEQUE DETAILS

    public function actiongetChequeDetails()
    {
        $u_id=$_REQUEST['u_id'];
        $post["table"] = 'user_master';
        $post["fields"] = 'fullname,addressline_1';
        $post["beforeWhere"] = '';
        $post["afterWhere"] = 'u_id='.$u_id;
        $post["r_p_p"] = '';
        $post["start"] = '';
        $res = Yii::app()->JsonWebservice->fetchData($post, $dataobj = 'db');
        if($res['status']==1)
        {
            if(empty($res['data'][0]['addressline_1']))
                $res['data'][0]['addressline_1']='';
        }
        echo Yii::app()->JsonWebservice->response($res);

    }
//QR CODES FOR SOCIAL MEDIA

    public function actionGenarteShareQR()
    {
        $type=$_REQUEST['type'];
        $u_id=$_REQUEST['u_id'];
        $o_id=$_REQUEST['o_id'];
        $unique_code=$_REQUEST['unique_code'];
        if($type=='offer')
        {
            $unique_code=$_REQUEST['code'];
            $urlpath="http://".$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/index.php/webservice_v3/offer_detail_V2?source=QR&source_uid=".$u_id."&o_id=".$o_id."&unique_code=".$unique_code;
            $urlpath=base64_encode($urlpath);
        }
        else
        {
           $urlpath="app url";
        }
        /* GENERATE QR CODE */
        
        $code=new QRCode($urlpath);      
        $codefilename=date('ymdhis').'.png';
        // to save the code to file             
        $codepath = getcwd() . DIRECTORY_SEPARATOR . "images" . DIRECTORY_SEPARATOR . "shareQR" . DIRECTORY_SEPARATOR.$codefilename;
        $code->create($codepath);
        /* END OF QR CODE */
        $qr_code['qr_code']='http://'.$_SERVER['HTTP_HOST'].Yii::app()->baseurl."/images/shareQR/".$codefilename;
        $res = array("status" => '1','data'=>$qr_code) ;
        echo Yii::app()->JsonWebservice->response($res);
    }

/**********************************************
 AUTOMATIC PAYMENT FOR ON PREMISE TRANSACTIONS
**********************************************
 */
    public function actioncheckPaymentMethod($o_id,$ot_id,$amount)
    {
        //print_r($_REQUEST);die();
       // $o_id=$_REQUEST['o_id'];
        //$ot_id=$_REQUEST['ot'];
        $error_flag=0;
        $error_msg='';
        //if($_REQUEST['type']=='on_pre')
            $certificate='BevRageMerchant_Developer.pem';
        /*else
            $certificate='BevRageck.pem';*/
        if(!empty($o_id))
        {

            /*
            * fecth order details and user details
            */
            $connection = Yii::app()->db;
            $payer_id=$_REQUEST['payer_id'];
            $cmd="SELECT `dt_id`,`token`,badge,user_master.* FROM `user_device_master`,user_master WHERE user_master.u_id in (select u_id from user_master where unique_code in(select unique_code from offer_transaction_master where ot_id=".$ot_id."))
                    and user_master.`u_id`=user_device_master.`u_id`  and user_master.is_active=0";
            $dataReader=$connection->createCommand($cmd)->query();
            $rows=$dataReader->readAll();
            if(!empty($rows))
            {

                $uid=$rows[0]['u_id'];
                $type="redemeed";           
                $deviceToken=$rows[0]['token'];
                //chk is deposit amount is suffecient for payment
                
                $cmd2="SELECT * FROM user_master WHERE user_master.u_id IN (SELECT u_id from offer_master where o_id=".$o_id.")";
                $dataReader=$connection->createCommand($cmd2)->query();
                $rows2=$dataReader->readAll();
                
                if(!empty($rows2))
                { 
                    
                    if($rows2[0]['deposit_amount']!='' && ($rows2[0]['deposit_amount']>=$_REQUEST['amount']))
                    {
                        
                        if($rows[0]['active_payment_method_id']==1 )
                        {

                            //PAYPAL Transaction
                            $receiver_email='pooja.shah@credencys.com';
                            $amount=$amount;
                            $response= $this->actionPayapi($receiver_email,$amount);    
                            
                            if(!empty($response))
                            {
                                if($response["responseEnvelope.ack"] == "Success")
                                {
                                    $error_flag=0;
                                    $pay_key_id=$response["payKey"];
                                }
                                else
                                {
                                    $error_flag=1;
                                    $error_msg=urldecode($kArray["error(0).message"]);
                                }
                            }
                        }
                        else if($rows[0]['active_payment_method_id']==3 )
                        {   
                           //success popup
                            exit;                       
                        }
                        else
                        {
                            //success popup
                            exit();
                        }
                    }
                    else
                    {
                        //amount not sufficient to pay customers
                        $error_flag=1;
                        $error_msg="Brand Partner Deposit Amount Limit Reached! ";
                    }

                    //SEND PUSH NOTIFICATIONs
                    if($error_flag==0 && $deviceToken!='')
                    {
                        $status=1;
                        $msg="Your Reimbursement is Successfull,Cheers!";
                        if($rows[0]['dt_id']==1)
                        {   
                            $badge=$rows[0]['badge'];
                            $this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
                            $cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
                            $dataReader=$connection->createCommand($cmd);
                            $rows=$dataReader->execute();
                            
                        }
                        else
                        {
                            $this->androidPush($uid,$type,$msg,$deviceToken);
                        }
                    }
                    else
                    {   
                        $status=3;      
                        $msg="Sorry, Error in Reimbursement of offer !";
                        if($rows[0]['dt_id']==1)
                        {           
                            $badge=$rows[0]['badge'];       
                            $this->Pushios($uid,$type,$msg,$deviceToken,$badge,$certificate);
                            $cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where `u_id`=".$uid.' and token="'.$deviceToken.'"';
                            $dataReader=$connection->createCommand($cmd);
                            $rows=$dataReader->execute();
                        }
                        else
                        {
                            $this->androidPush($uid,$type,$msg,$deviceToken);
                        }
                    }
                }
                else
                {
                    //$this->redirect(array('admin'));
                    //$this->actionAdmin();
                    exit;
                }
                
                    
            }
            else
            {
                $status=2;  
                //user id not found
                $error_flag=1;
                $error_msg="Merchant Not Active Any More! ";
            }


            //CHECK ERROR FLAG AND SEND PUSH MESSAGE 
            if($error_flag==1)
            {
                $status=2;
                //send to view page with error message.
                $error_msg="Merchant Not Active Any More! ";
            }
            else
            {
                if(!empty($pay_key_id))
                {
                    /*if($_REQUEST['type']=='off_pre')
                    $cmd="UPDATE `offer_transaction_master` SET customer_payment_status=".$status." ,transaction_id='".$pay_key_id."' where ot_id=".$_REQUEST['ot'];
                    else*/
                    $cmd="UPDATE `offer_transaction_master` SET payment_status=".$status.",transaction_id='".$pay_key_id."' where ot_id=".$ot_id;
                    
                    $dataReader=$connection->createCommand($cmd);
                    $rows=$dataReader->execute();
                    $error_msg="Successfull Payment !";
                    
                }               
            } 
        }
        else
        {
            
          // response fail
        }
    }

    public function actionPayapi($receiver_email,$amount)
    {
           //turn php errors on
           ini_set("track_errors", true);

           //set PayPal Endpoint to sandbox
           $url = trim("https://svcs.sandbox.paypal.com/AdaptivePayments/Pay");

           //set PayPal Endpoint to live
           //$url = trim("https://svcs.paypal.com/AdaptivePayments/Pay");

           
           

           //PayPal SANDBOX API Credentials
           $app_id = 'APP-80W284485P519543T';//  sandbox
           $api_username = "mitchb-facilitator_api1.bevrage.com";
           $api_password = "AYKGUWNDNNHMFXVP";
           $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AazG6HsiIpUp3NxmRNO-4NaxSnkT";
           


           //PayPal LIVE API Credentials
          /* $app_id = 'APP-2RM4658855985480F';//live;
           $api_username = "mitchb_api1.bevrage.com";
           $api_password = "55PSXR9XQTZTY93E";
           $api_signature = "AFcWxV21C7fd0v3bYYYRCpSSRl31AbJpKwnQGTIHON6MQofMMvBIaggN";*/
           

           $amount =1 ;//$amount; //TODO
           $API_RequestFormat = "NV";
           $API_ResponseFormat = "NV";

           $receiver_email = $receiver_email; //TODO
           //Create request payload with minimum required parameters
           $bodyparams = array ("requestEnvelope.errorLanguage" => "en_US",
                             "actionType" => "PAY",
                             "cancelUrl" => "http://localhost/beverage/index.php/site/index",
                             "returnUrl" => "http://localhost/commonadmin/index.php/CategoryMaster/admin",
                             "currencyCode" => "USD",
                             "receiverList.receiver(0).email" => 'mitchb-buyer@bevrage.com',//$receiver_email,
                             "receiverList.receiver(0).amount" => $amount,
                             "senderEmail"=> "mitchb-facilitator@bevrage.com",
               );

           // convert payload array into url encoded query string
           $body_data = http_build_query($bodyparams, "", chr(38));


            try
            {

                //create request and add headers
                $params = array("http" => array(
                   "method" => "POST",                                                 
                   "content" => $body_data,                                             
                   "header" =>  "X-PAYPAL-SECURITY-USERID: " . $api_username . "\r\n" .
                                "X-PAYPAL-SECURITY-SIGNATURE: " . $api_signature . "\r\n" .
                                "X-PAYPAL-SECURITY-PASSWORD: " . $api_password . "\r\n" .
                                "X-PAYPAL-APPLICATION-ID: " . $app_id . "\r\n" .
                                "X-PAYPAL-REQUEST-DATA-FORMAT: " . $API_RequestFormat . "\r\n" .
                                "X-PAYPAL-RESPONSE-DATA-FORMAT: " . $API_ResponseFormat . "\r\n"
               ));


                //create stream context
                $ctx = stream_context_create($params);


                //open the stream and send request
                $fp = @fopen($url, "r", false, $ctx);

                //get response
                $response = stream_get_contents($fp);

                //check to see if stream is open
                if ($response === false) 
                {
                   throw new Exception("php error message = " . "$php_errormsg");
                }

                //close the stream
                fclose($fp);

                //parse the ap key from the response

                $keyArray = explode("&", $response);

                foreach ($keyArray as $rVal)
                {
                    list($qKey, $qVal) = explode ("=", $rVal);
                    $kArray[$qKey] = $qVal;
                }

                //print the response to screen for testing purposes
                If( $kArray["responseEnvelope.ack"] == "Success") 
                {

                   /* foreach ($kArray as $key =>$value)
                    {
                        echo $key . ": " .$value . "<br/>";
                    }*/
                    
                    return $kArray;
                }
                else 
                {
                    return $kArray;
                    //echo 'ERROR Code: ' .  $kArray["error(0).errorId"] . " <br/>";
                    //echo 'ERROR Message: ' .  urldecode($kArray["error(0).message"]) . " <br/>";
                    //die();
                }

            }
            catch(Exception $e) 
            {
                echo "Message: ||" .$e->getMessage()."||";
            }

        
    }

 }

?>
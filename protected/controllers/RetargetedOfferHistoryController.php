<?php

class RetargetedOfferHistoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
                $session=new CHttpSession;
                $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','generate','changeStatus'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new RetargetedOfferHistory;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RetargetedOfferHistory']))
		{
			$model->date=gmdate('Y-m-d');
			$model->attributes=$_POST['RetargetedOfferHistory'];
			//$model->date=gmdate('Y-m-d');
			if($model->save())
			{
				//send PUSH NOTIFICATIONS
				 //fetch all users
				$connection = Yii::app()->db;
				$cmd="SELECT distinct `ud_id`,`dt_id`,`token`,`u_id`,`badge` FROM `user_device_master` 
					where u_id in (select u_id from user_master 
									where is_login=0 and unique_code in(select unique_code from offer_transaction_master 
														where o_id=".$_POST['RetargetedOfferHistory']['targeted_o_id']." 
					))order by ud_id desc";
				$dataReader=$connection->createCommand($cmd)->query();
				$rows=$dataReader->readAll();
				//print_r($rows);die();
				//send push notifications
				if(!empty($rows))
				{
					
					foreach ($rows as $key => $value) 
					{
						if(!empty($value['token']))
						{
							$msg= "Check out latest Bevrage offer!";
							$oid=$_POST['RetargetedOfferHistory']['o_id'];
							
							if($value['dt_id']==1) //IOS PN
							{
								
								$badge=$value['badge'];
								$deviceToken=$value['token'];
								$certificate='BevRageck.pem';
								
								$result=$this->Pushios($msg,$deviceToken,$badge,$certificate,$oid);
								
								//UPDATE Badge Count.
								$cmd="UPDATE `user_device_master` SET `badge`=`badge`+1  where  ud_id=".$value['ud_id'];
								$dataReader=$connection->createCommand($cmd);
								$num=$dataReader->execute();
							}
							else //ANDROID PN
							{
								
								$deviceToken=$value['token'];
								$this->Pushandroid($msg,$deviceToken,$oid);
							}
						}
					}	
				}
				
				$this->redirect(array('admin'));
				/*$this->render('create',array(
						'model'=>$model,
					));
				exit;*/
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['RetargetedOfferHistory']))
		{
			$model->attributes=$_POST['RetargetedOfferHistory'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->rt_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('RetargetedOfferHistory');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new RetargetedOfferHistory('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['RetargetedOfferHistory']))
			$model->attributes=$_GET['RetargetedOfferHistory'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return RetargetedOfferHistory the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=RetargetedOfferHistory::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param RetargetedOfferHistory $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='retargeted-offer-history-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}


	public function Pushios($msg,$deviceToken,$badge,$certificate,$oid)
	{
		
		if ($deviceToken != '0') 
		{
            $ctx = stream_context_create();            
            $passphrase = '1234';
            $certificate= $certificate;//'BevRageMerchant_Developer.pem';//'BevRageck.pem';            
            stream_context_set_option($ctx, 'ssl', 'local_cert', Yii::app()->basePath . DIRECTORY_SEPARATOR . $certificate);
            stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
            $fp = stream_socket_client('ssl://gateway.sandbox.push.apple.com:2195', $err, $errstr, 60, STREAM_CLIENT_CONNECT | STREAM_CLIENT_PERSISTENT, $ctx);
            $body['aps'] = array(
                'badge' => $badge,
                'alert' => $msg,
                'sound' => 'default',
                'flag'=>5,
                'oid'=>$oid,
                
            );
            $payload = json_encode($body);
            $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;
            $result = fwrite($fp, $msg, strlen($msg));
            $flag = 0;
            if (!$result) {
                $flag = 1;
            }
            fclose($fp);
            if ($flag == 0) {

                return true;
            } else {
                return FALSE;
            }
        } 
        else 
        {
            return FALSE;
        }
	}

	//android push on successfull cashback  
    public function Pushandroid($msg,$deviceToken,$oid)
    {
     // echo $deviceToken;die();
        // Replace with real BROWSER API key from Google APIs
        $apiKey = "AIzaSyCSKvb5yU-7eN_vvFE7LLP7bxATqijyC-4";       
        // Replace with real client registration IDs 
        $registrationIDs =array($deviceToken);       
        // Set POST variables
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array(
        'registration_ids'  => $registrationIDs,
        'data'              => array( "message" => $msg,'flag'=>5,'oid'=>$oid),
        );
        
        $headers = array( 
        'Authorization: key=' . $apiKey,
        'Content-Type: application/json',
        );
      
        $ch = curl_init();
        // Set the url, number of POST vars, POST data
        curl_setopt( $ch, CURLOPT_URL, $url );
        curl_setopt( $ch, CURLOPT_POST, true );
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields));
        curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, false);
        // Execute post
        $result = curl_exec($ch);
        if ($result === FALSE) 
        {
                die('Problem occurred: ' . curl_error($ch));
        }
        // Close connection
        curl_close($ch);
        //echo $result;die();
    }
}

<?php

class MerchantMasterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        $session=new CHttpSession;
        $session->open();
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','admin','delete','addmerchants'),
				'users'=>array($session["uname"]),
			),
			
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new MerchantMaster;
		$model->scenario = 'uplaodform';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MerchantMaster']))
		{
			
			$session=new CHttpSession;
        	$session->open();
        
			$model->attributes=$_POST['MerchantMaster'];
			$model->created_by=$session['uid'];

			$connection = Yii::app()->db;
			$cmd="SELECT city_name from  `bevrage_cities`WHERE bc_id=".$_POST['MerchantMaster']['m_city'];
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			
			$city=$rows[0]['city_name'];
			/* get lat long from address */
			$add=/*$_POST['MerchantMaster']['m_name']." ".*/$_POST['MerchantMaster']['m_street']." ".$city." ".$_POST['MerchantMaster']['m_state']." ".$_POST['MerchantMaster']['m_zipcode'];
            $prepAddr = str_replace(' ','+',$add);
			          
         
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
             
            $lat = $output->results[0]->geometry->location->lat;
            $long = $output->results[0]->geometry->location->lng;
			
			/*save lat long in db */
			$model->m_lat=$lat;
			$model->m_long=$long;

			if($model->save())
				$this->redirect('admin');
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		$model->scenario = 'uplaodform';
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['MerchantMaster']))
		{
			$model->attributes=$_POST['MerchantMaster'];
			
			$connection = Yii::app()->db;
			$cmd="SELECT city_name from  `bevrage_cities`WHERE bc_id=".$_POST['MerchantMaster']['m_city'];
			$dataReader=$connection->createCommand($cmd)->query();
			$rows=$dataReader->readAll();
			
			$city=$rows[0]['city_name'];
			/* get lat long from address */
			$add=/*$_POST['MerchantMaster']['m_name']." ".*/$_POST['MerchantMaster']['m_street']." ".$city." ".$_POST['MerchantMaster']['m_state']." ".$_POST['MerchantMaster']['m_zipcode'];
            $prepAddr = str_replace(' ','+',$add);
            //echo 'http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false';
         
            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
            $output= json_decode($geocode);
            //print_r($output);
            $lat = $output->results[0]->geometry->location->lat;            
            $long = $output->results[0]->geometry->location->lng;
			
			/*save lat long in db */

			$model->m_lat=$lat;
			$model->m_long=$long;
			if($model->save()){
				$this->redirect(array('admin'));
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//$this->loadModel($id)->delete();
		
		$connection = Yii::app()->db;
		$cmd="UPDATE  `merchant_master` SET is_active='1' WHERE m_id=".$id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();

		$connection = Yii::app()->db;
		$cmd="DELETE FROM `offer_merchant_assoc` WHERE `m_id`=".$id;
		$dataReader=$connection->createCommand($cmd);
		$num=$dataReader->execute();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{

		$dataProvider=new CActiveDataProvider('MerchantMaster');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		if (isset($_GET['pageSize'])) {
		    Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
		    unset($_GET['pageSize']);  // would interfere with pager and repetitive page size change
		}
		$model=new MerchantMaster('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['MerchantMaster']))
			$model->attributes=$_GET['MerchantMaster'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MerchantMaster the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=MerchantMaster::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MerchantMaster $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='merchant-master-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionaddmerchants()
    {
    	$model=new MerchantMaster();
    	$model->scenario = 'uplaodcsv';

    	if(isset($_POST['MerchantMaster']['m_name']))
    	{
    		 
    		if($model->validate())
    		{
    			$csv = array();
	    		$fail='';
	    		$count=0;
			    $total=0;
			    $flag=0;    		
				$merchantlist=CUploadedFile::getInstance($model,'m_name');
				if($merchantlist)
				{
					$fileName = "temp.csv";  			
					$temppath=getcwd() . DIRECTORY_SEPARATOR . "merchantcsv" . DIRECTORY_SEPARATOR .$fileName;
			        $merchantlist->saveAs($temppath);

			        $lines = file($temppath, FILE_IGNORE_NEW_LINES);
			        

			        foreach ($lines as $key => $value)
			        {                          
			            $csv[$key] = str_getcsv($value);
			        }
			        $count=0;
			        $total=count($csv) -1;
			        /*echo "<pre>";
			        print_r($csv);die();*/
			        foreach ($csv as $key => $value) 
			        {
			        	if($key!= 0)
			        	{
			        		//insert in merchant table
			        
			         		//find city id ans state code of merchant
			         		$connection = Yii::app()->db;
							$cmd="SELECT `bc_id`, `city_name`, `state_code`, `type` FROM `bevrage_cities` WHERE `city_name` like '".$value[4]."'";
							$dataReader=$connection->createCommand($cmd)->query();
							$rows=$dataReader->readALL();
							
							if(!empty($rows))
							{
								
								//insert merchants
								$city=$rows[0]['bc_id'];
								$city_name=$rows[0]['city_name'];
								$state=$rows[0]['state_code'];

								/* get lat long from address */
								$add=$value[3]." ".$city_name." ".$state." ".$value[5];
					            $prepAddr = str_replace(' ','+',$add);
					           
					            $geocode=file_get_contents('http://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
					            $output= json_decode($geocode);
					            

					            if(!empty($output->results))
					            {
					            	$lat = $output->results[0]->geometry->location->lat;
					            	$long = $output->results[0]->geometry->location->lng;
					            }
					           	
					            /* eoc */

					            /* session uid */		         
					            $session=new CHttpSession;
			        			$session->open();
			        			$uid=$session['uid'];
			        			/* eoc */
					            if(!empty($lat) && !empty($long))
					            {

					            	/* if merchnat already exist */
					            	$cmd='SELECT * from merchant_master where (m_name like "%'.$value[0].'%" or establishment_name like "%'.$value[1].'%") and merchant_type='.$value[2].' and m_city ='.$city;
									$dataReader=$connection->createCommand($cmd)->query();
									$resultRow=$dataReader->readAll();
									
									
									if(empty($resultRow))
									{
										$count++;
										$cmd='INSERT INTO `merchant_master`(`m_id`, `m_name`, `establishment_name`, `m_street`, `m_zipcode`, `m_city`, `m_state`, `phone`, `m_lat`, `m_long`, `is_active`, `merchant_type`, `created_by`)
										 	VALUES (NULL,"'.$value[0].'","'.$value[1].'","'.$value[3].'","'.$value[5].'","'.$city.'","'.$state.'",NULL,"'.$lat.'","'.$long.'",0,"'.$value[2].'","'.$uid.'") ';
										$dataReader=$connection->createCommand($cmd)->query();
									}
									else
									{
										$fail[$value[0]]="Merchant Already Exists.";									
									}							
								}
								else
								{
									
									$fail[$value[0]]="Address Not Found.";
								}
							}
							else
							{
								//failed array
								//$fail[]=$value[0];
								$fail[$value[0]]="City Unavailable.";
							}

			        	}
			        }
				}
			    else
			    {
			    	$fail['Missing File']="File not found.";
			    }
		        
		       	$this->render('addmerchants',array(
					'model'=>$model,'fail'=>$fail,'count'=>$count,'total' =>$total
				));	
    		}
    		
    		
	     
    	}
        
    	$this->render('addmerchants',array(
			'model'=>$model,
		));
    }
}
